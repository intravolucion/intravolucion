<!DOCTYPE html>
<html lang="en-US" class="backgroundImage">
    <head>
    <meta charset="utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="Alexis Valdez">
    <meta name="title" content="INTRAVOLUCION" />
    <meta name="Keywords" content="cursos, tutoria, acreditación, talleres, eventos, aprender, formación, académico, académica" />
    <meta name="Description" content="INTRAVOLUCION forma parte del grupo empresarial peruano Haro & Macciotta Asociados S.A.C que fue legalmente constituida a finales del mes de octubre de 2015 por dos socios que se conocieron mientras estudiaban una maestría en dirección de empresas en una de las más importantes casas de estudios de postgrados del país." />
    <meta name="robots" content="NOINDEX, NOFOLLOW" />
    <meta name="verify-iw" content="INTRAVOLUCION forma parte del grupo empresarial peruano Haro & Macciotta Asociados S.A.C que fue legalmente constituida a finales del mes de octubre de 2015 por dos socios que se conocieron mientras estudiaban una maestría en dirección de empresas en una de las más importantes casas de estudios de postgrados del país." />
    
    <meta property="og:url" content="http://intravolucion.com/"/>

     <!--Open Graph -->
    <meta property="og:image" content="http://idclogic-proyectos.com/xcursos/assets/frontend/img/nosotros/img1.jpg"/>
    <meta property="og:title" content="INTRAVOLUCION" />
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="http://intravolucion.com"/>
    <meta property="og:description" content="INTRAVOLUCION forma parte del grupo empresarial peruano Haro & Macciotta Asociados S.A.C que fue legalmente constituida a finales del mes de octubre de 2015 por dos socios que se conocieron mientras estudiaban una maestría en dirección de empresas en una de las más importantes casas de estudios de postgrados del país."/>
                    
    <!--Fin Open Graph -->
        
        <title>INTRAVOLUCION</title>
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700|Lemon' rel='stylesheet' type='text/css'>
        
        <link rel="shortcut icon" href="images/favicon.png">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
	   <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="css/animate.css" />
        <link rel="stylesheet" type="text/css" href="css/YTPlayer.css" />
        <link rel="stylesheet" type="text/css" href="css/supersized.css" />
        <link rel="stylesheet" type="text/css" href="css/styles.css" />

        
    </head>
    <body>

        <!-- CONTAINER -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="section clearfix">
                        <h1 class="logo animated fadeInDown"><img src="images/logo.png" alt="" style="width: 350px;"></h1>
                        
                        <div id="text_slider">
                            <div class="slide clearfix"><h2>Estamos trabajando en nuestra web.</h2></div>

                            <div class="slide clearfix"><h2>Pronto estará lista.</h2></div>
                        </div>
                        
                    </div>

                    

                    <div class="section clearfix animated fadeIn">
                        <p>¿Quieres más Información?</p>
                        <form id="form_newsletter" method="post" action="process_form.php">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="input-group input-group-lg">
                                        <input type="email" name="email" class="form-control" placeholder="Ingresa tu Email" >
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="fa fa-check"></i> Contáctame!</button>
                                        </span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div id="form_newsletter_result"></div>
                                </div>
                            </div>
                        </form>

                    </div>

                    <div class="section clearfix animated fadeIn" id="contact">
                        <a href="mailto:alexis.valdez@alexisvaldez.club"><i class="fa fa-envelope-o"></i> info@intravolucion.com</a>
                        <a href="mailto:info@alexisvaldez.club" style="padding-right: 1em;"><i class="fa fa-envelope-o"></i> ventas@intravolucion.com</a>
                        <a href="tel:+511970381122" onclick="return (navigator.userAgent.match(/Android|iPhone|iPad|iPod|Mobile/i))!=null;"><i class="fa fa-phone"></i> (+01)  970 381 122</a>
                    </div>


                    <div class="section clearfix">
                        <a href="https://www.facebook.com/intravolucion" target="_blank" class="btn btn-transparent btn-facebook"><i class="fa fa-facebook fa-fw"></i></a>
                        <a href="https://twitter.com/intravolucion" target="_blank" class="btn btn-transparent btn-twitter"><i class="fa fa-twitter fa-fw"></i></a>
                        <a href="https://plus.google.com/103289303330652925669" target="_blank" class="btn btn-transparent btn-googleplus"><i class="fa fa-google-plus fa-fw"></i></a>
                        <a href="https://www.youtube.com/channel/UCkjYRD_jlBRoPO5FHnNcL7A" target="_blank" class="btn btn-transparent btn-youtube"><i class="fa fa-youtube fa-fw"></i></a>
                       
                        <a href="https://www.instagram.com/intravolucion/" target="_blank" class="btn btn-transparent btn-instagram"><i class="fa fa-instagram fa-fw"></i></a>
                        
                    </div>
                    
                   

                </div>
            </div>
        </div>
        <!-- END CONTAINER -->

        <!-- FOOTER -->
        <div id="footer">

                <p>Diseñado por <a href="phsperu.com" title="Webnovant">phsperu</a></p>
        </div>
        <!-- END FOOTER -->


    
        
		<!-- JS -->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.plugin.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- <script src="js/jquery.countdown.min.js"></script> -->
        <script src="js/supersized.min.js"></script>
        <script src="js/jquery.cycle.min.js"></script>
        <script src="js/jquery.mb.YTPlayer.js"></script>
        <script src="js/scripts.js"></script>
        
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->

        <script>



</script>
       

    </body>
</html>