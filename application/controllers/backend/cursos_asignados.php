<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cursos_asignados extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'cursos_asignados';
		
		$config['table'] = 'cursos_asignados';
		$config['title'] = array('espanol' => 'Listado de Cursos Asignados');
		$config['type'] = 'table';
		$config['where'] = array('estado' => 1);

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Curso'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Curso'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Curso'));
		// Fin de los Botones

		// Elementos
		$items['id_curso'] = array('type' => 'select', 'text' => array('espanol' => 'Curso'), 'items' => $this->module_model->seleccionar('cursos', array('estado' => 1, 'activado' => 1)), 'required' => TRUE, 'table' => TRUE, 'value' => array('key' => 'id', 'item' => 'titulo', 'table' => 'cursos'));
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		$this->initialize($config);
	}
}