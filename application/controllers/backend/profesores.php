<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class profesores extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'correo_electronico';

		$config['controller'] = 'profesores';
		
		$config['table'] = 'administrador';
		$config['title'] = array('espanol' => 'Listado de Profesores');
		$config['type'] = 'table';
		$config['publish'] = TRUE;
		$config['actions'] = array('tutor' => 'Tutor');
		$config['where'] = array('estado' => 1, 'nivel' => 2); // Profesores
		$config['export'] = TRUE;

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Profesor'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Profesor'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Profesor'));
		// Fin de los Botones

		// Elementos
		$items['nivel'] = array('type' => 'hidden', 'text' => array('espanol' => 'Nivel'), 'value' => 2);
		$items['correo_electronico'] = array('type' => 'text', 'text' => array('espanol' => 'Correo Electrónico', 'english' => 'Email'), 'placeholder' => 'Ingrese su correo electrónico', 'required' => array('valid_email', 'is_unique'), 'table' => TRUE);
		$items['contrasenia'] = array('type' => 'password', 'text' => array('espanol' => 'Contraseña'), 'required' => TRUE);
		$items['nombres'] = array('type' => 'text', 'text' => array('espanol' => 'Nombres', 'english' => 'Name'), 'placeholder' => 'Ingrese sus nombres', 'required' => TRUE, 'table' => TRUE);
		$items['apellidos'] = array('type' => 'text', 'text' => array('espanol' => 'Apellidos', 'english' => 'Last Name'), 'placeholder' => 'Ingrese sus apellidos', 'required' => TRUE, 'table' => TRUE);
		$items['numero_documento'] = array('type' => 'text', 'text' => array('espanol' => 'DNI'), 'required' => TRUE, 'table' => TRUE);
		$items['telefono'] = array('type' => 'text', 'text' => array('espanol' => 'Teléfono'));
		$items['imagen'] = array('type' => 'photo', 'text' => array('espanol' => 'Imagen (250x270)', 'english' => 'Photo'), 'sizes' => array('33x33', '250x270'), 'table' => TRUE);
		$items['acerca_de'] = array('type' => 'textarea', 'text' => array('espanol' => 'Resumen Profesor'));
		$items['experiencia'] = array('type' => 'file', 'text' => array('espanol' => 'Experiencia'));
		$items['redes_sociales'] = array('type' => 'label', 'text' => array('espanol' => 'Redes Sociales'));
		$items['facebook'] = array('type' => 'text', 'text' => array('espanol' => 'Facebook'));
		$items['twitter'] = array('type' => 'text', 'text' => array('espanol' => 'Twitter'));
		$items['linkedin'] = array('type' => 'text', 'text' => array('espanol' => 'Linkedin'));
		$items['youtube'] = array('type' => 'text', 'text' => array('espanol' => 'YouTube'));
		// $items['contenido'] = array('type' => 'editor', 'text' => array('espanol' => 'Descripción'));
		
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		// Cursos
		$items = array(); $buttons = array();

		$cursos['campo_referencia'] = 'titulo';

		$cursos['controller'] = 'cursos';
		
		$cursos['table'] = 'cursos';
		$cursos['title'] = array('espanol' => 'Listado de Cursos');
		$cursos['type'] = 'table';

		if($this->mostrar_session('nivel') == 0 OR $this->mostrar_session('nivel') == 1)
		{
			$cursos['publish'] = TRUE;
		}
		
		$cursos['where'] = array('estado' => 1);

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Curso'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Curso'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Curso'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Curso'), 'placeholder' => 'Ingrese sus apellidos', 'required' => TRUE, 'table' => TRUE);
		$items['logo'] = array('type' => 'photo', 'text' => array('espanol' => 'Logo'), 'sizes' => array(), 'table' => TRUE, 'required' => TRUE, 'original' => TRUE);
		// Fin de los Elementos

		$cursos['buttons'] = $buttons;
		$cursos['items'] = $items;

		$config['elementos_adicionales'] = array($cursos);

		$this->initialize($config);
	}
}