<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class principios extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'principios';
		$config['where'] = array('estado' => 1);
		$config['table'] = 'principios';
		$config['title'] = array('espanol' => 'Listado de Principios');
		$config['type'] = 'table';

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Principio'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Principio'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Principio'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Título'), 'table' => TRUE, 'required' => TRUE);
		$items['imagen'] = array('type' => 'photo', 'text' => array('espanol' => 'Imagen'), 'table' => TRUE, 'required' => TRUE, 'sizes' => array('360x245'));
		$items['resumen'] = array('type' => 'textarea', 'text' => array('espanol' => 'Resumen'), 'required' => TRUE);
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		$this->initialize($config);
	}

}