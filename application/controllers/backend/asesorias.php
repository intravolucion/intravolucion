<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class asesorias extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'asesorias';
		
		$config['table'] = 'cursos';
		$config['title'] = array('espanol' => 'Listado de Asesorías');
		$config['type'] = 'table';
		$config['alias'] = 'titulo';
		$config['export'] = TRUE;

		if($this->mostrar_session('nivel') == 0 OR $this->mostrar_session('nivel') == 1)
		{
			$config['publish'] = TRUE;
		}
		
		$config['where'] = array('estado' => 1, 'categoria' => '9999');

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Curso'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Curso'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Curso'));
		// Fin de los Botones

		// Elementos
		$items['id_padre'] = array('type' => 'select', 'text' => array('espanol' => 'Profesor'), 'items' => $this->module_model->seleccionar('administrador', array('estado' => 1, 'nivel' => 2)), 'table' => TRUE, 'required' => TRUE, 'value' => array('key' => 'id', 'item' => 'nombres|apellidos', 'table' => 'administrador', 'separador' => ' '));
		$items['categoria'] = array('type' => 'hidden', 'text' => array('espanol' => 'Categoria'), 'value' => '9999');
		$items['tutor'] = array('type' => 'select', 'text' => array('espanol' => 'Tutor'), 'table' => TRUE, 'items' => $this->module_model->seleccionar('administrador', array('nivel' => 2, 'estado' => 1, 'tutor' => 1, 'activado' => 1)), 'value' => array('key' => 'id', 'item' => 'nombres|apellidos', 'table' => 'administrador', 'separador' => ' '));
		$items['categoria'] = array('type' => 'hidden', 'text' => array('espanol' => 'Categoría'), 'value' => '9999');
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Curso'), 'placeholder' => 'Ingrese sus apellidos', 'required' => TRUE, 'table' => TRUE);
		$items['logo'] = array('type' => 'photo', 'text' => array('espanol' => 'Logo (370x250)'), 'sizes' => array('370x250'), 'table' => TRUE, 'required' => TRUE, 'original' => TRUE);
		$items['banner'] = array('type' => 'photo', 'text' => array('espanol' => 'Banner (1150x480)'), 'sizes' => array(), 'required' => TRUE, 'original' => TRUE);
		$items['precio_soles'] = array('type' => 'text', 'text' => array('espanol' => 'Precio (S/)'), 'required' => TRUE, 'table' => TRUE, 'class' => 'col-xs-3');
		$items['precio_dolares'] = array('type' => 'text', 'text' => array('espanol' => 'Precio ($)'), 'required' => TRUE, 'table' => TRUE, 'class' => 'col-xs-3');
		$items['precio_certificacion_soles'] = array('type' => 'text', 'text' => array('espanol' => 'Precio Certificación (S/)'), 'required' => TRUE, 'table' => TRUE, 'class' => 'col-xs-3');
		$items['precio_certificacion_dolares'] = array('type' => 'text', 'text' => array('espanol' => 'Precio Certificación ($)'), 'required' => TRUE, 'table' => TRUE, 'class' => 'col-xs-3');
		$items['comision'] = array('type' => 'text', 'text' => array('espanol' => 'Comisión (%)'), 'required' => TRUE, 'table' => TRUE, 'class' => 'col-xs-6');
		$items['comision_tutor'] = array('type' => 'text', 'text' => array('espanol' => 'Comisión Tutor (%)'), 'required' => TRUE, 'table' => TRUE, 'class' => 'col-xs-6');
		$items['fecha_inicio'] = array('type' => 'date', 'text' => array('espanol' => 'Fecha Inicio'), 'required' => TRUE, 'table' => TRUE);
		$items['resumen'] = array('type' => 'textarea', 'text' => array('espanol' => 'Resumen'), 'required' => TRUE);
		$items['descripcion'] = array('type' => 'textarea', 'text' => array('espanol' => 'Descripción'), 'required' => TRUE);
		$items['aprenderas'] = array('type' => 'textarea', 'text' => array('espanol' => '¿Qué Aprenderás?'), 'required' => TRUE);
		$items['dirigido'] = array('type' => 'textarea', 'text' => array('espanol' => '¿A quién va dirigido?'), 'required' => TRUE);
		$items['youtube'] = array('type' => 'youtube', 'text' => array('espanol' => 'YouTube'));
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		// Semanas
		$items = array(); $buttons = array();

		$semanas['campo_referencia'] = 'titulo';

		$semanas['controller'] = 'semanas';
		
		$semanas['table'] = 'semanas';
		$semanas['title'] = array('espanol' => 'Listado de Semanas');
		$semanas['type'] = 'table';
		$semanas['order'] = TRUE;
		
		$semanas['where'] = array('estado' => 1);

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar una Semana'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar una Semana'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Semana'));
		// Fin de los Botones

		// Elementos
		$items['id_padre'] = array('type' => 'select', 'text' => array('espanol' => 'Curso'), 'items' => $this->module_model->seleccionar('cursos', array('estado' => 1)), 'table' => TRUE, 'required' => TRUE, 'value' => array('key' => 'id', 'item' => 'titulo', 'table' => 'cursos'));
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Semana'), 'placeholder' => 'Ingrese sus apellidos', 'required' => TRUE, 'table' => TRUE);
		$items['duracion'] = array('type' => 'text', 'text' => array('espanol' => 'Duración'), 'required' => TRUE, 'table' => TRUE);
		// Fin de los Elementos

		$semanas['buttons'] = $buttons;
		$semanas['items'] = $items;

		$config['elementos_adicionales'] = array($semanas);
		
		$this->initialize($config);
	}
}