<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class alumnos extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'correo_electronico';

		$config['controller'] = 'alumnos';
		
		$config['table'] = 'administrador';
		$config['title'] = array('espanol' => 'Listado de Alumnos');
		$config['type'] = 'table';
		$config['publish'] = TRUE;
		$config['where'] = array('estado' => 1, 'nivel' => 3); // Alumnos
		$config['export'] = TRUE;

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Alumno'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Alumno'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Alumno'));
		// Fin de los Botones

		// Elementos
		$items['nivel'] = array('type' => 'hidden', 'text' => array('espanol' => 'Nivel'), 'value' => 3);
		$items['correo_electronico'] = array('type' => 'text', 'text' => array('espanol' => 'Correo Electrónico', 'english' => 'Email'), 'placeholder' => 'Ingrese su correo electrónico', 'required' => array('valid_email', 'is_unique'), 'table' => TRUE);
		$items['contrasenia'] = array('type' => 'password', 'text' => array('espanol' => 'Contraseña'), 'required' => TRUE);
		$items['nombres'] = array('type' => 'text', 'text' => array('espanol' => 'Nombres', 'english' => 'Name'), 'placeholder' => 'Ingrese sus nombres', 'required' => TRUE, 'table' => TRUE);
		$items['apellidos'] = array('type' => 'text', 'text' => array('espanol' => 'Apellidos', 'english' => 'Last Name'), 'placeholder' => 'Ingrese sus apellidos', 'required' => TRUE, 'table' => TRUE);
		$items['imagen'] = array('type' => 'photo', 'text' => array('espanol' => 'Imagen', 'english' => 'Photo'), 'sizes' => array('33x33'), 'table' => TRUE);
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		// Cursos
		$items = array(); $buttons = array();

		$cursos['campo_referencia'] = 'titulo';

		$cursos['controller'] = 'cursos_asignados';
		
		$cursos['table'] = 'cursos_asignados';
		$cursos['title'] = array('espanol' => 'Listado de Cursos Asignados');
		$cursos['type'] = 'table';
		$cursos['where'] = array('estado' => 1);

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Curso'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Curso'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Curso'));
		// Fin de los Botones

		// Elementos
		$items['id_curso'] = array('type' => 'select', 'text' => array('espanol' => 'Curso'), 'items' => $this->module_model->seleccionar('cursos', array('estado' => 1, 'activado' => 1)), 'required' => TRUE, 'table' => TRUE, 'value' => array('key' => 'id', 'item' => 'titulo', 'table' => 'cursos'));
		$items['porcentaje'] = array('type' => 'hidden', 'text' => array('espanol' => 'Porcentaje'), 'table' => TRUE, 'readonly' => TRUE, 'value' => '');
		// Fin de los Elementos

		$cursos['buttons'] = $buttons;
		$cursos['items'] = $items;

		$config['elementos_adicionales'] = array($cursos);

		$this->initialize($config);
	}
}