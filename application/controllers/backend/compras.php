<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class compras extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'id_pedido';

		$config['controller'] = 'compras';
		
		$config['table'] = 'cursos_asignados';
		$config['title'] = array('espanol' => 'Listado de Cursos Comprados');
		$config['type'] = 'table';
		$config['where'] = array('estado' => 1);
		$config['publish'] = TRUE;
		$config['status'] = array('campo' => 'activado', 'items' => array('Pendiente', 'Pagado'), 'colors' => array('danger', 'primary'));

		$config['help'] = 'Los cursos que se compraron con la modalidad de Depósito en Banco y/o Transferencia se podrán validar directamente desde este panel. Los precios y moneda ya están definidos en la compra.';

		$config['export'] = TRUE;

		// Botones
		// $buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Curso'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Compra'));
		// Fin de los Botones

		// Elementos
		$items['id_padre'] = array('type' => 'select', 'text' => array('espanol' => 'Estudiante'), 'items' => $this->module_model->seleccionar('administrador', array('estado' => 1, 'activado' => 1, 'nivel' => 3)), 'required' => TRUE, 'table' => TRUE, 'value' => array('key' => 'id', 'item' => 'nombres|apellidos', 'table' => 'administrador', 'separador' => ' '), 'readonly' => TRUE);
		$items['id_pedido'] = array('type' => 'text', 'text' => array('espanol' => 'Pedido'), 'readonly' => TRUE, 'table' => TRUE);
		$items['id_curso'] = array('type' => 'select', 'text' => array('espanol' => 'Curso'), 'items' => $this->module_model->seleccionar('cursos', array('estado' => 1, 'activado' => 1)), 'table' => TRUE, 'value' => array('key' => 'id', 'item' => 'titulo', 'table' => 'cursos'), 'readonly' => TRUE, 'adicional' => array('key' => 'precio', 'add' => 'moneda'));
		$items['moneda'] = array('type' => 'select', 'text' => array('espanol' => 'Moneda'), 'items' => array('' => 'Sin Definir', '_soles' => 'Soles', '_dolares' => 'Dólares'), 'readonly' => TRUE, 'table' => TRUE);
		$items['tipo_pago'] = array('type' => 'select', 'text' => array('espanol' => 'Tipo de Pago'), 'items' => array(1 => 'Depósito en Banco', 2 => 'Tarjeta'), 'readonly' => TRUE, 'table' => TRUE);
		$items['certificacion'] = array('type' => 'select', 'items' => array('No', 'Si'), 'table' => TRUE, 'readonly' => TRUE, 'text' => array('espanol' => 'Certificación'));
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		$this->initialize($config);
	}

	function export()
	{
		$objPHPExcel = new PHPExcel();
		// Set the active Excel worksheet to sheet 0 
		$objPHPExcel->setActiveSheetIndex(0);  
		// Initialise the Excel row number 
		$row = 1;  
		//start of printing column names as names of MySQL fields  
		$column = 0;

		foreach($this->items as $key => $value)
		{
			if($value['type'] != 'hidden' AND $value['type'] != 'password' AND $value['type'] != 'photo' AND $value['type'] != 'file')
			{
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $value['text']['espanol']);
				$column++;

				if($key == 'id_padre')
				{
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Correo Electrónico');
					$column++;
				}

				if($key == 'id_curso')
				{
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Precio');
					$column++;
				}
			}
		}

		$results = $this->module_model->seleccionar($this->table, $this->where);

		foreach($results as $key => $result)
		{
			$row++; $column = 0;

			foreach($this->items as $key_item => $item)
			{
				if($item['type'] != 'hidden' AND $item['type'] != 'password' AND $item['type'] != 'photo' AND $item['type'] != 'file')
				{
					// SELECT
					$price = NULL; $correo_electronico = NULL;

					if($item['type'] == 'select')
					{
						if(is_array(@$item['items']) && count(@$item['items']) > 0)
						{
							$is_array = FALSE; $value = NULL;

							foreach($item['items'] as $k => $v)
							{
								$find = FALSE; 

								if(is_array($v) && count($v) > 0)
								{
									$is_array = TRUE;
	                                $campo = explode('|', $item['value']['item']); $muestra = NULL;
	                                foreach($campo as $kc => $vc)
	                                {
	                                	$muestra .= $v[$vc];
	                                    $muestra .= ($kc < (count($campo) - 1)) ? ((isset($item['value']['separador'])) ? $item['value']['separador'] : ' | ') : NULL;
	                                }
	                                $correo_electronico = (@$result[$key_item] == $v[$item['value']['key']]) ? @$v['correo_electronico'] : $correo_electronico;
	                                $value = (@$result[$key_item] == $v[$item['value']['key']]) ? $muestra : $value;
	                                $find = (@$result[$key_item] == $v[$item['value']['key']]) ? TRUE : FALSE;
								}
								else
								{
									$value = (@$result[$key_item] == $k) ? $v : $value; $find = (@$result[$key_item] == $k) ? TRUE : FALSE;
								}

								if(isset($item['adicional']))
								{
									$campo = $item['adicional']['key'] . $result[$item['adicional']['add']];

									if($find == TRUE)
									{
										$price = @$v[$campo];
									}
								}
							}
						}
						else
						{
							$value = NULL;
						}
					}
					elseif($item['type'] == 'youtube')
					{
						if($result[$key_item] != '')
						{
							$value = 'https://www.youtube.com/watch?v=' . $result[$key_item];
						}
						else
						{
							$value = NULL;
						}
					}
					elseif($item['type'] == 'checkbox')
					{
						$value = ($result[$key_item] == 1) ? 'SI' : 'NO';
					}
					else
					{
						$value = $result[$key_item];
					}

					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $value);
					$column++;

					if($key_item == 'id_padre')
					{
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $correo_electronico);
						$column++;
					}

					if($key_item == 'id_curso')
					{
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $price);
						$column++;
					}
				}
			}
		}

		// Redirect output to a client’s web browser (Excel5) 
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="'. $this->controller .'_'. date('YmdHis') .'.xls"'); 
		header('Cache-Control: max-age=0'); 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
		$objWriter->save('php://output');
	}
}