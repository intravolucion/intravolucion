<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class preguntas extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['publish'] = TRUE;
		$config['order'] = TRUE;
		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'preguntas';
		$config['where'] = array('estado' => 1);
		$config['table'] = 'preguntas';
		$config['title'] = array('espanol' => 'Listado de Preguntas');
		$config['type'] = 'table';

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar una Pregunta'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar una Pregunta'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Pregunta'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'table' => TRUE, 'text' => array('espanol' => 'Título'), 'required' => TRUE);
		$items['opciones'] = array('type' => 'text', 'text' => array('espanol' => 'Opciones (Separar por «|», no incluir la respuesta correcta.)'), 'class' => 'col-md-12');
		$items['respuesta'] = array('type' => 'text', 'text' => array('espanol' => 'Respuesta (Aquí va la respuesta correcta)'), 'class' => 'col-md-12');
		
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		$this->initialize($config);
	}
}