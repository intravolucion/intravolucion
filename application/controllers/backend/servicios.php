<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class servicios extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'servicios';
		$config['where'] = array('estado' => 1);
		$config['table'] = 'servicios';
		$config['title'] = array('espanol' => 'Listado de Servicios');
		$config['type'] = 'table';

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Servicio'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Servicio'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Servicio'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Título'), 'table' => TRUE);
		$items['logo'] = array('type' => 'photo', 'text' => array('espanol' => 'Logo'), 'sizes' => array('200x200'));
		$items['imagen'] = array('type' => 'photo', 'text' => array('espanol' => 'Imagen'), 'table' => TRUE, 'required' => TRUE, 'sizes' => array('800x741', '849x391', '110x70'));
		$items['resumen'] = array('type' => 'textarea', 'text' => array('espanol' => 'Resumen'), 'required' => TRUE);
		$items['contenido'] = array('type' => 'editor', 'text' => array('espanol' => 'Contenido'), 'required' => TRUE);
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		$this->initialize($config);
	}

}