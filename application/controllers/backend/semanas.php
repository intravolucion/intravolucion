<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class semanas extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'semanas';
		
		$config['table'] = 'semanas';
		$config['title'] = array('espanol' => 'Listado de Semanas');
		$config['type'] = 'table';
		$config['order'] = TRUE;
		$config['alias'] = 'titulo';
		
		$config['where'] = array('estado' => 1);

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar una Semana'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar una Semana'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Semana'));
		// Fin de los Botones

		// Elementos
		if(!isset($_SESSION['semanas']['id_padre']))
		{
			$items['id_padre'] = array('type' => 'select', 'text' => array('espanol' => 'Curso'), 'items' => $this->module_model->seleccionar('cursos', array('estado' => 1)), 'table' => TRUE, 'required' => TRUE, 'value' => array('key' => 'id', 'item' => 'titulo', 'table' => 'cursos'));
		}
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Semana'), 'placeholder' => 'Ingrese sus apellidos', 'required' => TRUE, 'table' => TRUE);
		$items['resumen'] = array('type' => 'textarea', 'text' => array('espanol' => 'Resumen'), 'required' => TRUE);
		$items['duracion'] = array('type' => 'text', 'text' => array('espanol' => 'Duración'), 'required' => TRUE, 'table' => TRUE);
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		// Lecciones
		$items = array(); $buttons = array();

		$lecciones['campo_referencia'] = 'titulo';

		$lecciones['controller'] = 'lecciones';
		
		$lecciones['table'] = 'lecciones';
		$lecciones['title'] = array('espanol' => 'Listado de Lecciones');
		$lecciones['type'] = 'table';
		$lecciones['order'] = TRUE;
		
		$lecciones['where'] = array('estado' => 1);

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar una Lección'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar una Lección'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Lección'));
		// Fin de los Botones

		// Elementos
		$items['id_padre'] = array('type' => 'select', 'text' => array('espanol' => 'Semana'), 'items' => $this->module_model->seleccionar('semanas', array('estado' => 1)), 'table' => TRUE, 'required' => TRUE, 'value' => array('key' => 'id', 'item' => 'titulo', 'table' => 'semanas'));
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Lección'), 'placeholder' => 'Ingrese sus apellidos', 'required' => TRUE, 'table' => TRUE);
		$items['tipo'] = array('type' => 'select', 'text' => array('espanol' => 'Tipo de Contenido'), 'items' => array('contenido' => 'Texto', 'imagen' => 'Imagen', 'video' => 'Video', 'archivo' => 'Archivo'), 'required' => TRUE, 'table' => TRUE);
		$items['contenido'] = array('type' => 'textarea', 'text' => array('espanol' => 'Contenido'));
		$items['duracion'] = array('type' => 'text', 'text' => array('espanol' => 'Duración'), 'required' => TRUE, 'table' => TRUE);
		// Fin de los Elementos

		$lecciones['buttons'] = $buttons;
		$lecciones['items'] = $items;

		// Tests
		$items = array(); $buttons = array();

		$tests['campo_referencia'] = 'titulo';

		$tests['controller'] = 'tests';
		
		$tests['table'] = 'tests';
		$tests['title'] = array('espanol' => 'Listado de Tests');
		$tests['type'] = 'table';
		$tests['order'] = TRUE;
		
		$tests['where'] = array('estado' => 1);

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Test'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Test'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Test'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Test'), 'required' => TRUE, 'table' => TRUE);
		$items['tipo'] = array('type' => 'select', 'text' => array('espanol' => 'Tipo de Encuesta'), 'items' => array('Objetiva', 'Subjetiva'), 'required' => TRUE, 'table' => TRUE);
		// Fin de los Elementos

		$tests['buttons'] = $buttons;
		$tests['items'] = $items;

		$config['elementos_adicionales'] = array($lecciones, $tests);
		
		$this->initialize($config);
	}
}