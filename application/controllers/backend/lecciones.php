<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lecciones extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'lecciones';
		
		$config['table'] = 'lecciones';
		$config['title'] = array('espanol' => 'Listado de Lecciones');
		$config['type'] = 'table';
		$config['order'] = TRUE;
		$config['alias'] = 'titulo';
		
		$config['where'] = array('estado' => 1);

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar una Lección'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar una Lección'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Lección'));
		// Fin de los Botones

		// Elementos
		if(!isset($_SESSION['lecciones']['id_padre']))
		{
			$items['id_padre'] = array('type' => 'select', 'text' => array('espanol' => 'Semana'), 'items' => $this->module_model->seleccionar('semanas', array('estado' => 1)), 'table' => TRUE, 'required' => TRUE, 'value' => array('key' => 'id', 'item' => 'titulo', 'table' => 'semanas'));
		}
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Lección'), 'placeholder' => 'Ingrese sus apellidos', 'required' => TRUE, 'table' => TRUE);
		$items['tipo'] = array('type' => 'select', 'text' => array('espanol' => 'Tipo de Contenido'), 'items' => array('contenido' => 'Texto', 'imagen' => 'Imagen', 'video' => 'Video', 'archivo' => 'Archivo'), 'required' => TRUE, 'table' => TRUE);
		$items['contenido'] = array('type' => 'textarea', 'text' => array('espanol' => 'Contenido'));
		$items['imagen'] = array('type' => 'photo', 'text' => array('espanol' => 'Imagen'));
		$items['video'] = array('type' => 'youtube', 'text' => array('espanol' => 'Video (YouTube)'));
		$items['archivo'] = array('type' => 'file', 'text' => array('espanol' => 'Archivo'));
		$items['duracion'] = array('type' => 'text', 'text' => array('espanol' => 'Duración'), 'required' => TRUE, 'table' => TRUE);
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;
		
		$this->initialize($config);
	}
}