<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class encuestas extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['publish'] = TRUE;
		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'encuestas';
		$config['where'] = array('estado' => 1);
		$config['table'] = 'encuestas';
		$config['title'] = array('espanol' => 'Listado de Encuestas');
		$config['type'] = 'table';

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar una Encuesta'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar una Encuesta'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Encuesta'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'table' => TRUE, 'text' => array('espanol' => 'Título'), 'required' => TRUE);
		$items['descripcion'] = array('type' => 'textarea', 'text' => array('espanol' => 'Descripción de la Encuesta'));
		$items['fecha_inicio'] = array('type' => 'date', 'text' => array('espanol' => 'Fecha de Inicio'), 'required' => array('custom[date]'), 'class' => 'col-md-6');
		$items['fecha_fin'] = array('type' => 'date', 'text' => array('espanol' => 'Fecha de Fin'), 'required' => array('custom[date], future[#fecha_inicio]'), 'class' => 'col-md-6');
		
		// Fin de los Elementos
		$config['buttons'] = $buttons;
		$config['items'] = $items;

		$items = array(); $buttons = array();

		$preguntas['publish'] = TRUE;
		$preguntas['order'] = TRUE;
		$preguntas['alias'] = 'titulo';
		$preguntas['campo_referencia'] = 'titulo';

		$preguntas['controller'] = 'preguntas';
		$preguntas['where'] = array('estado' => 1);
		$preguntas['table'] = 'preguntas';
		$preguntas['title'] = array('espanol' => 'Listado de Preguntas');
		$preguntas['type'] = 'table';

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar una Pregunta'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar una Pregunta'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Pregunta'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'table' => TRUE, 'text' => array('espanol' => 'Título'), 'required' => TRUE);
		$items['ayuda'] = array('type' => 'textarea', 'text' => array('espanol' => 'Descripción de la Encuesta'));
		$items['tipo'] = array('type' => 'select', 'text' => array('espanol' => 'Tipo'), 'items' => array(1 => 'Entrada Múltiple', 2 => 'Entrada Única'));
		$items['opciones'] = array('type' => 'text', 'text' => array('espanol' => 'Opciones (Separar por comas para múltiples opciones)'));
		
		// Fin de los Elementos
		$preguntas['buttons'] = $buttons;
		$preguntas['items'] = $items;

		$config['elementos_adicionales'] = array($preguntas);

		$this->initialize($config);
	}
}