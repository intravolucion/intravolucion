<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class galeria extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'galeria';
		$config['where'] = array('estado' => 1);
		$config['table'] = 'galeria';
		$config['title'] = array('espanol' => 'Galería de Fotos');
		$config['type'] = 'dropzone';

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar Fotos'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar una Foto'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Foto'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Título'), 'table' => TRUE);
		$items['imagen'] = array('type' => 'photo', 'text' => array('espanol' => 'Imagen'), 'table' => TRUE, 'dropzone' => TRUE, 'sizes' => array('849x391'));
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		$this->initialize($config);
	}

}