<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class banners extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'banners';
		$config['where'] = array('estado' => 1);
		$config['table'] = 'banners';
		$config['title'] = array('espanol' => 'Listado de Banners');
		$config['type'] = 'table';

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Banner'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Banner'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Banner'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Título'), 'table' => TRUE);
		$items['subtitulo'] = array('type' => 'text', 'text' => array('espanol' => 'Subtítulo'), 'table' => TRUE);
		$items['imagen'] = array('type' => 'photo', 'text' => array('espanol' => 'Imagen (1600x1100)'), 'table' => TRUE, 'required' => TRUE, 'sizes' => array('1600x1100'));
		$items['enlace'] = array('type' => 'text', 'text' => array('espanol' => 'Enlace'), 'table' => TRUE);
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		$this->initialize($config);
	}

}