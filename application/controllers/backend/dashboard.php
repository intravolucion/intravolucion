<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}

	function idioma($titulo = FALSE)
	{
		$this->validar_usuario();

		if($titulo <> FALSE)
		{
			$this->session->set_userdata('language', $titulo);
		}
	}

	function login_redes()
	{
		$array = []; $contrasenia = $this->contrasenia_aleatoria(); $configuracion = $this->module_model->seleccionar('configuracion', array(), 1, 1);

		foreach($_POST as $key => $value)
		{
			$array[$key] = $value; $$key = $value;
		}

		$busqueda = $this->module_model->seleccionar('administrador', array('correo_electronico' => $correo_electronico), 1, 1);

		if(count($busqueda) == 0)
		{
			$array['nivel'] = 3; $array['estado'] = 1; $array['activado'] = 0; $array['contrasenia'] = $this->encrypt->sha1($contrasenia);
			$array['usuario_creacion'] = 1; $array['usuario_modificacion'] = 1;
			$array['fecha_creacion'] = $this->fecha(); $array['fecha_modificacion'] = $this->fecha();

			$contenido = '<h1>Registrado Correctamente</h1>';
            $contenido .= 'Bienvenido '. $array['nombres'] .', sus datos de acceso son los siguientes:<br />';
            $contenido .= 'Correo Electrónico: ' . $array['correo_electronico'] . '<br />';
            $contenido .= 'Contraseña: ' . $contrasenia . '<br /><br />';
            $contenido .= 'Para poder ingresar correctamente, debe de activar su cuenta en el siguiente <a href="' . base_url() . 'activar_cuenta?correo_electronico=' . $array['correo_electronico'] . '" target="_blank">enlace</a>.';

            $contacto = $configuracion['email_contacto']; $destinatario = $array['correo_electronico']; $asunto = "Registro - " . $configuracion['titulo'];

            $data['content'] = 'Ocurrió un percance al registrar el usuario. Por favor, intente nuevamente.'; $data['type'] = 'danger';

            if($this->enviar_email($contacto, $destinatario, $asunto, $contenido))
            {
                $data['content'] = $this->module_model->guardar('administrador', $array);
                $data['type'] = 'success'; $data['content'] = ' Revise su correo para validar su cuenta.';
            }
		}
		else
		{
			$data = [];

			if($busqueda['activado'] == 1)
			{
				foreach($busqueda as $key => $value)
				{
					if($key != 'contrasenia')
					{
						$this->cargar_session($key, $value);
					}
				}

				$data['url'] = '1';
			}
			else
			{
				$data['content'] = 'Debe activar su cuenta para continuar. Por favor, revise su bandeja de entrada.'; $data['type'] = 'warning';
			}
		}

		$this->load->view("backend/templates/json_view", array('resultado' => $data));
	}

	function index()
	{
		if(!isset($_SESSION[$this->session_name]['correo_electronico']))
		{
			if($this->input->post() === FALSE)
			{
				$this->load->view("backend/index_view");
			}
			else
			{
				$this->identificarse($this->input->post("correo_electronico"));
			}
		}
		else
		{
			if($this->input->post("logout") == "true")
			{
				$this->cerrar_session();
			}
			else
			{
				if($this->mostrar_session('nivel') == 2 OR $this->mostrar_session('nivel') == 3)
				{
					redirect("/sys_dashboard", "refresh");
				}
				else
				{
					$data = array();

					if($this->mostrar_session('nivel') == 0 || $this->mostrar_session('nivel') == 1)
					{
						/*
						$config['item_order'] = array('key' => 'id', 'value' => 'desc');
						$this->initialize($config);
						$data['logs'] = $this->module_model->seleccionar('log_administrador', array(), 2, 7);
						*/

						$data['cursos'] = $this->module_model->seleccionar('cursos', array('estado' => 1, 'activado' => 1));

						$usuarios = $this->module_model->seleccionar('administrador', array('estado' => 1, 'activado' => 1));

						foreach($usuarios as $key => $value)
						{
							$data['usuarios'][$value['id']] = $value;
						}
					}

					$this->load->view("backend/home_view", $data);
				}
			}
		}
	}

	function token()
	{
		$data['token'] = $this->mostrar_session('token');
		$this->load->view("backend/templates/json_view", array('resultado' => $data));
	}

	function perfil()
	{
		$this->validar_usuario();

		// Items para el Perfil..
		$items['correo_electronico'] = array('type' => 'text', 'text' => array('espanol' => 'Correo Electrónico', 'english' => 'Email'), 'placeholder' => 'Ingrese su correo electrónico', 'required' => array('valid_email'), 'session' => TRUE);
		$items['nombres'] = array('type' => 'text', 'text' => array('espanol' => 'Nombres', 'english' => 'Name'), 'placeholder' => 'Ingrese sus nombres', 'required' => TRUE, 'session' => TRUE);
		$items['apellidos'] = array('type' => 'text', 'text' => array('espanol' => 'Apellidos', 'english' => 'Last Name'), 'placeholder' => 'Ingrese sus apellidos', 'session' => TRUE);
		$items['imagen'] = array('type' => 'photo', 'text' => array('espanol' => 'Imagen (250x270)', 'english' => 'Photo'), 'sizes' => array('33x33', '250x270'), 'session' => TRUE);
		// $items['acerca_de'] = array('type' => 'textarea', 'text' => array('espanol' => 'Información Adicional', 'english' => 'Information Aditional'), 'placeholder' => 'Ingrese una información adicional', 'session' => TRUE);
		// Fin de los Items para el Perfil..

		$config['title'] = array('espanol' => 'Perfil', 'english' => 'Profile');
		$config['items'] = $items;
		$config['table'] = 'administrador';
		$config['controller'] = 'perfil';
		$config['breadcrumb'] = FALSE;

		$buttons = array();
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar el Perfil', 'english' => 'Update Profile'));

		$config['buttons'] = $buttons;

		$this->initialize($config); // Inicializando valores..
		$this->actualizar($this->mostrar_session('id'));
	}

	function contrasenia()
	{
		$this->validar_usuario();

		// Items para el Perfil..
		$items['contrasenia_anterior'] = array('type' => 'password', 'text' => array('espanol' => 'Contraseña Anterior', 'english' => 'Old Password'), 'placeholder' => 'Ingrese su contraseña anterior', 'required' => TRUE);
		$items['nueva_contrasenia'] = array('type' => 'password', 'text' => array('espanol' => 'Nueva Contraseña', 'english' => 'New Password'), 'placeholder' => 'Ingrese su nueva contraseña' , 'required' => TRUE);
		$items['repetir_contrasenia'] = array('type' => 'password', 'text' => array('espanol' => 'Repetir Contraseña', 'english' => 'Repeat Password'), 'placeholder' => 'Repita su nueva contraseña', 'required' => TRUE);
		// Fin de los Items para el Perfil..

		$config['table'] = 'administrador';
		$config['controller'] = 'contrasenia';
		$config['title'] = array('espanol' => 'Contraseña', 'english' => 'Password');
		$config['items'] = $items;
		$config['breadcrumb'] = FALSE;

		$buttons = array();
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar la Contraseña', 'english' => 'Update Password'));

		$config['buttons'] = $buttons;

		$this->initialize($config);

		if($this->input->post() === FALSE)
		{
			$this->actualizar($this->mostrar_session('id'));
		}
		else
		{
			$busqueda = $this->module_model->buscar($this->table, $this->mostrar_session('id'));
			
			if($this->encrypt->sha1($this->input->post('contrasenia_anterior')) == $busqueda['contrasenia'])
			{
				if($this->input->post('nueva_contrasenia') === $this->input->post('repetir_contrasenia'))
				{
					$array['contrasenia'] = $this->encrypt->sha1($this->input->post('nueva_contrasenia'));
					$data['mensaje'] = $this->module_model->actualizar($this->table, $array, $this->mostrar_session('id')); // Guardar datos
				}
				else
				{
					$data['mensaje'] = $this->lang->line('contrasenias_no_coinciden');;
				}
			}
			else
			{
				$data['mensaje'] = $this->lang->line('contrasenia_incorrecta');
			}

			$data['url'] = NULL;

			if($this->input->post('retorno') == 1)
			{
				$data['url'] = $this->controller; // Verificando que se quiere cerrar el formulario.
			}

			$this->load->view("backend/templates/print_json_view", array('data' => $data));
		}
	}

	function configuracion()
	{
		$this->validar_usuario(); // Verificando la sesión del usuario..

		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Título', 'english' => 'Title'), 'placeholder' => 'Ingrese el título de la empresa', 'required' => TRUE);
		$items['logo'] = array('type' => 'photo', 'text' => array('espanol' => 'Logo', 'english' => 'Logo'), 'help' => 'Logotipo de la Empresa.', 'sizes' => array(), 'original' => TRUE);
		$items['logo_alternativo'] = array('type' => 'photo', 'text' => array('espanol' => 'Logo (Alternativo)', 'english' => 'Logo'), 'help' => 'Logotipo de la Empresa.', 'sizes' => array(), 'original' => TRUE);
		$items['email_contacto'] = array('type' => 'text', 'text' => array('espanol' => 'Correo Electrónico', 'english' => 'Email'), 'placeholder' => 'Ingrese el correo corporativo de su empresa');
		$items['etiqueta'] = array('type' => 'label', 'text' => array('espanol' => 'Datos Adicionales'));
		$items['video_introduccion'] = array('type' => 'youtube', 'text' => array('espanol' => 'Video de Introducción'));
		$items['nosotros'] = array('type' => 'textarea', 'text' => array('espanol' => 'Nosotros Footer'));
		$items['direccion'] = array('type' => 'text', 'text' => array('espanol' => 'Dirección'));
		$items['telefonos'] = array('type' => 'text', 'text' => array('espanol' => 'Teléfonos'));
		$items['email_nosotros'] = array('type' => 'text', 'text' => array('espanol' => 'Correo Electrónico para muestra'));
		$items['cuentas'] = array('type' => 'editor', 'text' => array('espanol' => 'Detalle de Cuentas'));
		$items['redes_sociales'] = array('type' => 'label', 'text' => array('espanol' => 'Redes Sociales'));
		$items['facebook'] = array('type' => 'text', 'text' => array('espanol' => 'Facebook'));
		$items['twitter'] = array('type' => 'text', 'text' => array('espanol' => 'Twitter'));
		$items['google'] = array('type' => 'text', 'text' => array('espanol' => 'Google+'));
		$items['instagram'] = array('type' => 'text', 'text' => array('espanol' => 'Instagram'));
		$items['linkedin'] = array('type' => 'text', 'text' => array('espanol' => 'Linkedin'));
		$items['youtube'] = array('type' => 'text', 'text' => array('espanol' => 'YouTube'));

		$config['title'] = array('espanol' => 'Configuración', 'english' => 'Configuration');
		$config['items'] = $items;
		$config['table'] = 'configuracion';
		$config['controller'] = 'configuracion';
		$config['breadcrumb'] = FALSE;

		$buttons = array();
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar Configuración', 'english' => 'Update Configuration'));

		$config['buttons'] = $buttons;

		$this->initialize($config); // Inicializando valores..
		$this->actualizar(1);
	}

	function encuesta($id)
	{
		if(isset($_POST) && count($_POST) > 0)
		{
			$array = array(); $busqueda = $this->module_model->total('votaciones', array('id_padre' => $this->mostrar_session('id'), 'id_encuesta' => $id, 'estado' => 1));

			if($busqueda == 0)
			{
				foreach($_POST as $key => $value)
				{
					$this->module_model->guardar('respuestas', array('id' => '', 'id_padre' => $this->mostrar_session('id'), 'pregunta' => $key, 'respuesta' => $value, 'estado' => 1, 'usuario_creacion' => $this->mostrar_session('id'), 'usuario_modificacion' => $this->mostrar_session('id'), 'fecha_creacion' => $this->fecha(), 'fecha_modificacion' => $this->fecha()));
				}

				$this->module_model->guardar('votaciones', array('id_padre' => $this->mostrar_session('id'), 'id_encuesta' => $id, 'estado' => 1, 'usuario_creacion' => $this->mostrar_session('id'), 'usuario_modificacion' => $this->mostrar_session('id'), 'fecha_creacion' => $this->fecha(), 'fecha_modificacion' => $this->fecha()));

				$data['mensaje'] = 'Se envió la encuesta correctamente, gracias por participar.';
			}
			else
			{
				$data['mensaje'] = 'Ya se ha enviado la encuesta anteriormente; no se puede participar dos veces. Gracias.';
			}

			$data['url'] = null;
			
			$this->load->view("backend/templates/print_json_view", array('data' => $data));
		}
		else
		{
			redirect("/backend", "refresh");
		}
	}

	function reportes()
	{
		$this->validar_usuario();

		if(isset($_POST) && $this->input->post('pregunta') != '')
		{
			$pregunta = $this->module_model->seleccionar('preguntas', array('alias' => $this->input->post('pregunta')), 1, 1);

			if(count($pregunta) > 0)
			{
				if($pregunta['tipo'] == 1)
				{
					$opciones = explode(",", $pregunta['opciones']);
				}
				else
				{
					$opciones = array(1 => 'SI', 2 => 'NO');
				}

				foreach($opciones as $key => $value)
				{
					$respuestas = $this->module_model->total('respuestas', array('pregunta' => $this->input->post('pregunta'), 'respuesta' => $value));
					$cantidad[] = array('x' => $value, 'y' => $respuestas);
				}
			}

			$data['cantidad'] = $cantidad; $data['opciones'] = $opciones;

			$html['resultado'] = $data;
			$this->load->view("backend/templates/json_view", $html);
		}
		else
		{
			$data = array(); $data['preguntas'] = array();
			$data['encuestas'] = $this->module_model->seleccionar('encuestas', array('estado' => 1, 'activado' => 1, 'fecha_fin <=' => date("Y-m-d")));

			$data['usuarios'] = $this->module_model->seleccionar('administrador', array('estado' => 1, 'activado' => 1, 'nivel >' => 1));

			if(isset($_POST) && $this->input->post('encuesta') > 0)
			{
				$data['encuesta'] = $this->module_model->seleccionar('encuestas', array('id' => $this->input->post('encuesta')), 1, 1);
			}
			else
			{
				$config['item_order'] = array('key' => 'id', 'value' => 'desc');
				$this->initialize($config);
				$data['encuesta'] = $this->module_model->seleccionar('encuestas', array('estado' => 1, 'activado' => 1, 'fecha_fin <=' => date("Y-m-d")), 1, 1);
			}

			if(count($data['encuesta']) > 0)
			{
				$data['preguntas'] = $this->module_model->seleccionar('preguntas', array('id_padre' => $data['encuesta']['id']));
			}
			
			$html['resultado'] = $this->load->view("backend/reportes_view", $data, TRUE);
			$this->load->view("backend/templates/json_view", $html);
		}
			
	}
}

?>