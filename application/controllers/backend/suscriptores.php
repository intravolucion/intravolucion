<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class suscriptores extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'correo_electronico';

		$config['controller'] = 'suscriptores';
		
		$config['table'] = 'suscriptores';
		$config['title'] = array('espanol' => 'Listado de Suscriptores');
		$config['type'] = 'table';
		$config['where'] = array('estado' => 1); // Alumnos
		$config['export'] = TRUE;

		// Botones
		// $buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Suscriptor'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Suscriptor'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Suscriptor'));
		// Fin de los Botones

		// Elementos
		$items['correo_electronico'] = array('type' => 'text', 'text' => array('espanol' => 'Correo Electrónico', 'english' => 'Email'), 'placeholder' => 'Ingrese su correo electrónico', 'required' => array('valid_email', 'is_unique'), 'table' => TRUE);
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		$this->initialize($config);
	}
}