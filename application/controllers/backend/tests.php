<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tests extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$tests['campo_referencia'] = 'titulo';

		$tests['controller'] = 'tests';
		
		$tests['table'] = 'tests';
		$tests['title'] = array('espanol' => 'Listado de Tests');
		$tests['type'] = 'table';
		$tests['order'] = TRUE;
		
		$tests['where'] = array('estado' => 1);

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Test'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Test'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Test'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Test'), 'required' => TRUE, 'table' => TRUE);
		$items['tipo'] = array('type' => 'select', 'text' => array('espanol' => 'Tipo de Encuesta'), 'items' => array('Objetiva', 'Subjetiva'), 'required' => TRUE, 'table' => TRUE);
		$items['duracion'] = array('type' => 'text', 'text' => array('espanol' => 'Duración'), 'table' => TRUE, 'required' => TRUE);
		// Fin de los Elementos

		$tests['buttons'] = $buttons;
		$tests['items'] = $items;

		// Preguntas
		$items = array(); $buttons = array();

		$config['publish'] = TRUE;
		$config['order'] = TRUE;
		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'preguntas';
		$config['where'] = array('estado' => 1);
		$config['table'] = 'preguntas';
		$config['title'] = array('espanol' => 'Listado de Preguntas');
		$config['type'] = 'table';

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar una Pregunta'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar una Pregunta'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Pregunta'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'table' => TRUE, 'text' => array('espanol' => 'Título'), 'required' => TRUE);
		$items['opciones'] = array('type' => 'text', 'text' => array('espanol' => 'Opciones'), 'help' => 'Separar por comas para múltiples opciones', 'class' => 'col-md-4');
		$items['respuesta'] = array('type' => 'text', 'text' => array('espanol' => 'Respuesta'), 'class' => 'col-md-4');
		
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		$tests['elementos_adicionales'] = array($config);
		
		$this->initialize($tests);
	}
}