<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class categorias extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'categorias';
		
		$config['table'] = 'categorias';
		$config['title'] = array('espanol' => 'Listado de Categorías');
		$config['type'] = 'table';
		$config['alias'] = 'titulo';
		$config['where'] = array('estado' => 1);

		$config['export'] = TRUE;

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar una Categoría'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar una Categoría'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Categoría'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Categoría'), 'placeholder' => 'Ingrese sus apellidos', 'required' => TRUE, 'table' => TRUE);
		$items['resumen'] = array('type' => 'textarea', 'text' => array('espanol' => 'Resumen'), 'required' => TRUE);
		$items['imagen'] = array('type' => 'photo', 'text' => array('espanol' => 'Imagen'), 'required' => TRUE, 'table' => TRUE);
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;

		$this->initialize($config);
	}
}