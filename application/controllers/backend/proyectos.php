<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class proyectos extends MY_Controller {

	function __construct() {
		parent::__construct();

		$items = array(); $buttons = array();

		$config['campo_referencia'] = 'titulo';

		$config['controller'] = 'proyectos';
		$config['where'] = array('estado' => 1);
		$config['table'] = 'proyectos';
		$config['title'] = array('espanol' => 'Listado de Proyectos');
		$config['type'] = 'table';

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar un Proyecto'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar un Proyecto'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar un Proyecto'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Título'), 'table' => TRUE);
		$items['imagen'] = array('type' => 'photo', 'text' => array('espanol' => 'Imagen'), 'table' => TRUE, 'required' => TRUE, 'sizes' => array('360x245', '849x391', '110x70'));
		$items['resumen'] = array('type' => 'textarea', 'text' => array('espanol' => 'Resumen'), 'required' => TRUE);
		$items['contenido'] = array('type' => 'editor', 'text' => array('espanol' => 'Contenido'), 'required' => TRUE);
		// Fin de los Elementos

		$config['buttons'] = $buttons;
		$config['items'] = $items;


		// GALERIA
		$items = array(); $buttons = array();

		$galeria['campo_referencia'] = 'titulo';

		$galeria['controller'] = 'galeria';
		$galeria['where'] = array('estado' => 1);
		$galeria['table'] = 'galeria';
		$galeria['title'] = array('espanol' => 'Galería de Fotos');
		$galeria['type'] = 'dropzone';

		// Botones
		$buttons['agregar'] = array('type' => 'add', 'text' => array('espanol' => 'Agregar Fotos'));
		$buttons['actualizar'] = array('type' => 'update', 'text' => array('espanol' => 'Actualizar una Foto'));
		$buttons['eliminar'] = array('type' => 'delete', 'text' => array('espanol' => 'Eliminar una Foto'));
		// Fin de los Botones

		// Elementos
		$items['titulo'] = array('type' => 'text', 'text' => array('espanol' => 'Título'), 'table' => TRUE);
		$items['imagen'] = array('type' => 'photo', 'text' => array('espanol' => 'Imagen'), 'table' => TRUE, 'dropzone' => TRUE, 'sizes' => array('849x391'));
		// Fin de los Elementos

		$galeria['buttons'] = $buttons;
		$galeria['items'] = $items;

		$config['elementos_adicionales'] = array($galeria);

		$this->initialize($config);
	}

}