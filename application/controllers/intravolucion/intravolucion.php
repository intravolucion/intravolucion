<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Cargamos Requests y Culqi PHP
require 'assets/backend/requests/library/Requests.php';
Requests::register_autoloader();
require 'assets/backend/culqi/lib/culqi.php';

class intravolucion extends MY_Controller {

    public $categorias = array(); public $mensajes = array();
    public $comercio = "pk_live_WoqG2XxSGZxKB3QL";
    public $key_secret = "sk_live_YNF7rpkKJHRfu6p3";

    function __construct()
    {
        parent::__construct();

        $sql = "SELECT * FROM mensajes WHERE id_receptor = ? AND estado = 1 AND activado = 0 GROUP BY id_emisor";
        $query = $this->db->query($sql, array($this->mostrar_session('id')));
        $this->mensajes = $query->result_array();

        $this->categorias = $this->module_model->seleccionar('categorias', array('estado' => 1));

        if(isset($_GET['profesor']) OR isset($_GET['alumno']) OR isset($_GET['administrador']))
        {
            $busqueda = $this->module_model->buscar('administrador', $this->mostrar_session('id'));

            if(isset($_GET['profesor']) && $_GET['profesor'] == 'true')
            {
                if($busqueda['nivel'] < 2)
                {
                    $this->cargar_session('ver_como', 'profesor'); $this->cargar_session('nivel', 2);
                }
                else
                {
                    $this->descargar_session('ver_como'); $this->cargar_session('nivel', $busqueda['nivel']);
                }
            }

            if(isset($_GET['alumno']) && $_GET['alumno'] == 'true' && $busqueda['nivel'] <= 2)
            {
                $this->cargar_session('ver_como', 'alumno'); $this->cargar_session('nivel', 3);
            }

            if(isset($_GET['administrador']) && $_GET['administrador'] == 'true' && $busqueda['nivel'] <= 1)
            {
                $this->cargar_session('ver_como', 'administrador'); $this->cargar_session('nivel', 1);
            }
        }
    }

    function index()
    {
        $data = array(); $data['urlvista'] = 'inicio';

        $sql = "SELECT c.*, COUNT( ca.id ) AS cantidad FROM cursos AS c, cursos_asignados AS ca WHERE c.estado = 1 AND ca.activado = 1 ORDER BY cantidad DESC LIMIT 0 , 4";
        $query = $this->db->query($sql);

        $cursos = $query->result_array(); $data['cursos'] = [];

        foreach($cursos as $key => $value)
        {
            if($value['cantidad'] > 0)
            {
                $data['cursos'][] = $value;
            }
        }

        $data['profesores'] = $this->module_model->seleccionar('administrador', array('nivel' => 2, 'estado' => 1));
        $data['banners'] = $this->module_model->seleccionar('banners', array('estado' => 1));

        $this->load->view("frontend/index_view", $data);
    }

    function suscribirse()
    {
        $correo_electronico = $this->input->post('correo_electronico'); $array = array();

        $busqueda = $this->module_model->seleccionar('suscriptores', array('correo_electronico' => $correo_electronico), 1, 1);

        if(count($busqueda) == 0)
        {
            $array['correo_electronico'] = $correo_electronico;
            $array['estado'] = 1; $array['usuario_creacion'] = 1; $array['usuario_modificacion'] = 1;
            $array['fecha_creacion'] = $this->fecha(); $array['fecha_modificacion'] = $this->fecha();

            $this->module_model->guardar('suscriptores', $array);

            $mensaje = array('type' => 'success', 'title' => 'Suscripción Correcta', 'content' => 'El correo electrónico se registró correctamente.');   
        }
        else
        {
            $mensaje = array('type' => 'danger', 'title' => 'Suscripción no realizada', 'content' => 'El correo electrónico ya está registrado.');
        }

        $this->session->set_flashdata('mensaje', $mensaje); redirect("/intravolucion", "refresh");
    }

    function registro()
    {
        $data['urlvista'] = 'registro'; $data['urlvista'] = 'inicio';

        if(isset($_POST) && count($_POST) > 0)
        {
            $array = array();

            $array['nivel'] = 3; $ignore = array('repetir_contrasenia', 'token');

            foreach($_POST as $key => $value)
            {
                if(!in_array($key, $ignore))
                {
                    if($key == 'contrasenia')
                    {
                        $value = $this->encrypt->sha1($value);
                    }

                    $array[$key] = $value;
                }
            }

            $array['estado'] = 1;
            $array['usuario_creacion'] = 1;
            $array['usuario_modificacion'] = 1;
            $array['fecha_creacion'] = $this->fecha();
            $array['fecha_modificacion'] = $this->fecha();

            $contenido = '<h1>Registrado Correctamente</h1>';
            $contenido .= 'Bienvenido '. $array['nombres'] . ' ' . $array['apellidos'] .', sus datos de acceso son los siguientes:<br />';
            $contenido .= 'Correo Electrónico: ' . $array['correo_electronico'] . '<br />';
            $contenido .= 'Contraseña: ' . $_POST['contrasenia'] . '<br /><br />';
            $contenido .= 'Para poder ingresar correctamente, debe de activar su cuenta en el siguiente <a href="' . base_url() . 'activar_cuenta?correo_electronico=' . $array['correo_electronico'] . '" target="_blank">enlace</a>.';

            $contacto = $this->configuracion['email_contacto']; $destinatario = $array['correo_electronico']; $asunto = "Registro - " . $this->configuracion['titulo'];

            if($this->enviar_email($contacto, $destinatario, $asunto, $contenido))
            {
                $data['mensaje'] = array('type' => 'success', 'text' => $this->module_model->guardar('administrador', $array));
            }
        }

        $this->load->view("frontend/registro_view", $data);
    }

    function activar_cuenta()
    {
        $correo_electronico = $_GET['correo_electronico']; $usuario = $this->module_model->seleccionar('administrador', array('correo_electronico' => $correo_electronico, 'estado' => 1, 'activado' => 0), 1, 1);

        if(count($usuario) > 0)
        {
            $this->module_model->actualizar('administrador', array('activado' => 1), $usuario['id']);

            $usuario['activado'] = 1;

            foreach($usuario as $key => $value)
            {
                if($key <> 'contrasenia')
                {
                    $this->cargar_session($key, $value);
                }
            }

            $mensaje = array('type' => 'success', 'title' => 'Registro Completo', 'content' => 'El usuario ha activado correctamente su cuenta.');
            $this->session->set_flashdata('mensaje', $mensaje);
        }

        redirect("/", "refresh");
    }

    function recuperar_contrasenia()
    {
        $busqueda = $this->module_model->seleccionar('administrador', array('correo_electronico' => $this->input->post('correo_electronico'), 'estado' => 1), 1, 1); $message = [];

        if(count($busqueda) == 0)
        {
            $message = array('type' => 'danger', 'title' => 'Error de Identificación', 'content' => 'El usuario no se encuenta registrado. Por favor, intente nuevamente.');
        }
        else
        {
            if($busqueda['activado'] == 1)
            {
                // Correcto..
                $contrasenia = $this->contrasenia_aleatoria();

                $contenido = '<h1>Olvidé mi Contraseña</h1>';
                $contenido .= 'Estimado(a): '. $busqueda['nombres'] . ' ' . $busqueda['apellidos'] .', su nueva contraseña es:<br />';
                $contenido .= 'Contraseña: ' . $contrasenia . '<br /><br />';
                
                $contacto = $this->configuracion['email_contacto']; $destinatario = $busqueda['correo_electronico']; $asunto = "Olvidé mi contraseña - " . $this->configuracion['titulo'];

                if($this->enviar_email($contacto, $destinatario, $asunto, $contenido))
                {
                    $data['mensaje'] = $this->module_model->actualizar('administrador', array('contrasenia' => $this->encrypt->sha1($contrasenia)), $busqueda['id']);
                }

                $message = array('type' => 'success', 'title' => 'Contraseña enviada correctamente', 'content' => 'La contraseña se envió al correo electrónico ingresado.');
            }
            else
            {
                $message = array('type' => 'warning', 'title' => 'Error de Activación', 'content' => 'El usuario no se encuenta activado. Por favor, intente nuevamente.');
            }
        }

        $this->session->set_flashdata('mensaje', $message); redirect("/", "refresh");

    }

    function nosotros()
    {
        $data = array(); $data['urlvista'] = 'nosotros';

        $this->load->view("frontend/nosotros_view", $data);
    }

    function categorias()
    {
        $data = array(); $data['categorias'] = $this->module_model->seleccionar('categorias', array('estado' => 1));
        $data['urlvista'] = 'cursos';

        $this->load->view("frontend/categorias_view", $data);
    }

    function categoria($id = FALSE)
    {
        $data = array(); $pagina = (isset($_GET['pagina']) ? $_GET['pagina'] : 0); $cantidad_por_pagina = 24;
        $data['urlvista'] = 'cursos'; $id = (int) $id; $order_by = array('id', 'desc');

        if(isset($_GET['order_by']) && $_GET['order_by'] != '')
        {
            $order_by = array($_GET['order_by'], $_GET['value']);
        }

        $data['cursos'] = $this->module_model->paginacion('cursos', $cantidad_por_pagina, $pagina, $order_by, array('estado' => 1, 'activado' => 1, 'categoria' => $id));
        $totales = $this->module_model->total('cursos', array('estado' => 1, 'activado' => 1, 'categoria' => $id));
        $data['links'] = ceil($totales / $cantidad_por_pagina); $data['pagina_actual'] = ($pagina + 1); $data['por_pagina'] = $cantidad_por_pagina; $data['pagina'] = $pagina;
        
        $this->load->view("frontend/cursos_view", $data);
    }

    function busqueda()
    {
        $data = array(); $pagina = (isset($_GET['pagina']) ? $_GET['pagina'] : 0); $cantidad_por_pagina = 24;
        $data['urlvista'] = 'cursos'; $word = @$_GET['word'];

        $sql = "SELECT * FROM cursos WHERE estado=1 AND (titulo LIKE '%" . $word . "%' OR resumen LIKE '%" . $word . "%' OR descripcion LIKE '%" . $word . "%') ORDER BY titulo ASC LIMIT " . ($pagina * $cantidad_por_pagina) . " , " . $cantidad_por_pagina;
        $query = $this->db->query($sql);

        $data['cursos'] = $query->result_array();

        $sql = "SELECT * FROM cursos WHERE estado=1 AND (titulo LIKE '%" . $word . "%' OR resumen LIKE '%" . $word . "%' OR descripcion LIKE '%" . $word . "%')";
        $query = $this->db->query($sql);
        $totales = $query->num_rows();

        $data['links'] = ceil($totales / $cantidad_por_pagina); $data['pagina_actual'] = ($pagina + 1); $data['por_pagina'] = $cantidad_por_pagina; $data['pagina'] = $pagina;

        $this->load->view("frontend/cursos_view", $data);
    }

    function cursos()
    {
        $data = array(); $pagina = (isset($_GET['pagina']) ? $_GET['pagina'] : 0); $cantidad_por_pagina = 24; $where = array('estado' => 1, 'activado' => 1, 'categoria != 9999 AND estado' => 1); $order_by = array('id', 'desc');
        $data['urlvista'] = 'cursos';

        if(isset($_GET['categoria']) && $_GET['categoria'] != '')
        {
            $where['categoria'] = (int) $_GET['categoria'];
        }

        if(isset($_GET['order_by']) && $_GET['order_by'] != '')
        {
            $order_by = array($_GET['order_by'], $_GET['value']);
        }

        $data['cursos'] = $this->module_model->paginacion('cursos', $cantidad_por_pagina, $pagina, $order_by, $where);
        $totales = $this->module_model->total('cursos', array('estado' => 1, 'activado' => 1));
        $data['links'] = ceil($totales / $cantidad_por_pagina); $data['pagina_actual'] = ($pagina + 1); $data['por_pagina'] = $cantidad_por_pagina; $data['pagina'] = $pagina;

        // $data['cursos'] = $this->module_model->seleccionar('cursos', array('estado' => 1, 'activado' => 1));

        $this->load->view("frontend/cursos_view", $data);
    }

    function curso($id = FALSE)
    {
        $data = array(); $id = (int) $id; $data['curso'] = $this->module_model->buscar('cursos', $id);
        $data['urlvista'] = 'cursos'; $data['existe'] = $this->module_model->seleccionar('cursos_asignados', array('id_curso' => $id, 'id_padre' => $this->mostrar_session('id'), 'estado' => 1, 'activado' => 1), 1, 1);

        if(count($data['curso']) > 0)
        {
            $data['profesor'] = $this->module_model->buscar('administrador', $data['curso']['id_padre']);

            if(count($data['profesor']) > 0)
            {
                $array['ip'] = $this->input->ip_address(); $array['fecha'] = date("Y-m-d"); $busqueda = $this->module_model->seleccionar('visitas', array('fecha' => date("Y-m-d"), 'ip' => $this->input->ip_address()), 1, 1);

                if(count($busqueda) == 0)
                {
                    $this->module_model->guardar('visitas', $array); $this->module_model->actualizar('cursos', array('vistas' => ($data['curso']['vistas'] + 1)), $id);
                }


                $data['categorias'] = $this->module_model->seleccionar('categorias', array('estado' => 1));
                $data['semanas'] = $this->module_model->seleccionar('semanas', array('estado' => 1, 'id_padre' => $id));
                $data['cursos_relacionados'] = $this->module_model->seleccionar('cursos', array('id !=' => $id, 'categoria' => $data['curso']['categoria'], 'activado' => 1, 'estado' => 1), 2, 3);

                $this->load->view("frontend/curso_view", $data);
            }
            else
            {
                redirect("/", "refresh");
            }
        }
        else
        {
            redirect("/", "refresh");
        }
    }

    function actualizar_carrito($id = FALSE)
    {
        $cantidad = $_GET['cantidad']; $carrito = (array) $this->mostrar_session('carrito'); $existe = FALSE; $nuevo_carrito = [];

        foreach($carrito as $key => $value)
        {
            if($key == $id)
            {
                if($cantidad > 0)
                {
                    $nuevo_carrito[$key] = $cantidad;
                }

                $existe = TRUE;
            }
            else
            {
                $nuevo_carrito[$key] = $value;
            }
        }

        if($existe === FALSE)
        {
            if($cantidad > 0)
            {
                $nuevo_carrito[$id] = $cantidad;
            }
        }

        $this->cargar_session('carrito', $nuevo_carrito);

        redirect("/carrito", "refresh");
    }

    function pagar()
    {
        $data = array(); $pedidoId = date("Ymd") . date("His");

        if(isset($_POST) && count($_POST) > 0)
        {
            $array = array(); $cursos = (array) $this->mostrar_session('carrito'); $monto_total = 0;

            $array['token'] = $_POST['token'];
            $array['tipo_pago'] = $_POST['tipo_pago'];

            foreach($cursos as $key => $value)
            {
                $curso = $this->module_model->buscar('cursos', $key);

                $array['id'] = '';
                $array['moneda'] = $this->moneda;
                $array['id_curso'] = $key;
                $array['id_pedido'] = $pedidoId;
                $array['id_padre'] = $this->mostrar_session('id');
                $array['estado'] = 1;

                if($array['tipo_pago'] == 0)
                {
                    $array['activado'] = 1;
                }

                $array['activado'] = 0;
                
                $array['usuario_creacion'] = $this->mostrar_session('id');
                $array['usuario_modificacion'] = $this->mostrar_session('id');
                $array['fecha_creacion'] = date("Y-m-d H:i:s");
                $array['fecha_modificacion'] = date("Y-m-d H:i:s");

                $monto_total += $curso['precio' . $this->moneda];

                $this->module_model->guardar('cursos_asignados', $array);
            }

            /*
            // Notificación por correo electrónico de una compra..
            $destinatario = $this->configuracion['email_contacto']; $contacto = $this->mostrar_session('correo_electronico'); $contenido = ''; $asunto = 'Compra Realizada'; $tipo_pago = array(0 => 'Depósito en Banco', 1 => 'Pasarela Culqui');

            $contenido .= '<h1>Venta generada</h1>';
            $contenido .= '<p>' . $this->mostrar_session('nombres') . ' ' . $this->mostrar_session('apellidos') . ' ha realizado una compra de ' . $this->input->post('total') . ' a través del medio de pago ' . $tipo_pago[$this->input->post('tipo')] . ', ingresa al administrador para visualizar el detalle de la compra</p>';
            
            $this->enviar_email($contacto, $destinatario, $asunto, $contenido);
            */
            // Notificación de una compra..

            if($array['tipo_pago'] == 1 OR $array['tipo_pago'] == 0)
            {
                if($monto_total > 0)
                {
                    $message = array('type' => 'warning', 'content' => 'El curso se activará después de haber realizado el pago y haber confirmado con el administrador de la plataforma.');

                    $this->session->set_flashdata('message', $message);
                }

                $this->descargar_session('carrito');

                redirect("/sys_dashboard", "refresh");
            }
            
            if($array['tipo_pago'] == 2)
            {
                $SECRET_KEY = $this->key_secret;
                $culqi = new Culqi\Culqi(array('api_key' => $SECRET_KEY));

                $monto = (int) ($monto_total * 100);

                try
                {
                    $charge = $culqi->Charges->create(
                        array(
                            "amount" => $monto,
                            "capture" => true,
                            "currency_code" => (($this->moneda == '_soles') ? "PEN" : "USD"),
                            "description" => "Compra de Productos",
                            "email" => $this->mostrar_session('correo_electronico'),
                            "installments" => 0,
                            "source_id" => $_POST['token']
                        )
                    );

                    //Respuesta de la creación de la venta. Cadena cifrada.
                    
                    $activado = 0;

                    if($charge->outcome->code == 'AUT0000')
                    {
                        $message = array('type' => 'success', 'content' => 'La compra se generó correctamente. Gracias por confiar en nosotros.'); $activado = 1;
                    }
                    else
                    {
                        $message = array('type' => 'alert', 'content' => 'Ocurrió un error al realizar la compra. Por favor, podría intentar nuevamente.');
                    }

                    $this->session->set_flashdata('message', $message);

                    // $data['cursos_asignados'] = $this->module_model->seleccionar('cursos_asignados', array('id_pedido' => $pedidoId));

                    $this->module_model->actualizar('cursos_asignados', array('activado' => $activado, 'detalle' => $charge->outcome->user_message), array('id_pedido' => $pedidoId));

                    // $this->load->view("frontend/pagar_view", $data);

                    $this->descargar_session('carrito');

                    redirect("/sys_dashboard", "refresh");

                }
                catch (Exception $e)
                {
                    $message = array('type' => 'alert', 'content' => $e->getMessage()); $this->session->set_flashdata('message', $message);

                    redirect("/sys_dashboard", "refresh");
                }
            }
        }
        else
        {
            redirect("/", "refresh");
        }
    }

    function pagar_certificacion()
    {
        $data = array(); $pedidoId = date("Ymd") . date("His");

        if(isset($_POST) && count($_POST) > 0)
        {
            $curso = $this->module_model->buscar('cursos', $this->input->post('curso'));

            Culqi::$codigoComercio = $this->comercio;
            Culqi::$llaveSecreta = $this->key_secret;
            Culqi::$servidorBase = $this->url;

            $monto = (int) ($curso['precio_certificacion'] * 100);

            try
            {
                $data = Pago::crearDatospago(array(

                //Numero de pedido de la venta, y debe ser único (de no ser así, recibirá como respuesta un error)
                Pago::PARAM_NUM_PEDIDO => $pedidoId,

                //Moneda de la venta ("PEN" O "USD")
                Pago::PARAM_MONEDA => "PEN",

                //Monto de la venta (ejem: 10.25, no se incluye el punto decimal)
                Pago::PARAM_MONTO => $monto,

                //Descripción de la venta
                Pago::PARAM_DESCRIPCION => "Compra de Productos",

                //Código del país del cliente Ej. PE, US
                Pago::PARAM_COD_PAIS => "PE",

                //Ciudad del cliente
                Pago::PARAM_CIUDAD => 'LIMA',

                //Dirección del cliente
                Pago::PARAM_DIRECCION => 'LIMA',

                // Numero de Teléfono
                Pago::PARAM_NUM_TEL => "992765900",

                //Correo electrónico del cliente
                "correo_electronico" => $this->mostrar_session('correo_electronico'),

                //Identificador de usuario del cliente
                "id_usuario_comercio" => $this->mostrar_session('id'),

                //Nombres
                "nombres" => $this->mostrar_session('nombres'),

                //Apellidos
                "apellidos" => $this->mostrar_session('apellidos'),

                ));

                //Respuesta de la creación de la venta. Cadena cifrada.

                $certificacion = 0; // print_r($data); die;

                if($data['codigo_respuesta'] == 'venta_exitosa' OR $data['codigo_respuesta'] == 'venta_registrada')
                {
                    $message = array('type' => 'success', 'text' => 'La compra se generó correctamente. Gracias por confiar en nosotros.'); $certificacion = 1;
                }
                else
                {
                    $message = array('type' => 'alert', 'text' => 'Ocurrió un error al realizar la compra. Por favor, podría intentar nuevamente.');
                }

                // $data['cursos_asignados'] = $this->module_model->seleccionar('cursos_asignados', array('id_pedido' => $pedidoId));

                $this->module_model->actualizar('cursos_asignados', array('certificacion' => $certificacion), array('id_padre' => $this->mostrar_session('id'), 'id_curso' => $curso['id']));

                // $this->load->view("frontend/pagar_view", $data);

                // print_r($this->db->last_query()); die;

                // redirect("/sys_dashboard", "refresh");

            }
            catch (Exception $e)
            {
                echo $e->getMessage()."\n";
            }
        }
        else
        {
            redirect("/sys_dashboard", "refresh");
        }
    }

    function asociados()
    {
        $data = array(); $data['profesores'] = $this->module_model->seleccionar('administrador', array('nivel' => 2, 'estado' => 1));
        $data['urlvista'] = 'profesores';

        $this->load->view("frontend/profesores_view", $data);
    }

    function profesor($profesor)
    {
        $data = array(); $profesor = (int) $profesor; $data['profesor'] = $this->module_model->buscar('administrador', $profesor);
        $data['urlvista'] = 'profesores';


        if(count($data['profesor']) > 0)
        {
            $this->load->view("frontend/interna_profesores_view", $data);
        }
        else
        {
            redirect("/profesores", "refresh");
        }
    }

    function asesorias()
    {
        $data = array(); $id = '9999'; $pagina = (isset($_GET['pagina']) ? $_GET['pagina'] : 0); $cantidad_por_pagina = 24; $where = array('estado' => 1, 'activado' => 1, 'categoria' => $id); $order_by = array('id', 'desc');
        $data['urlvista'] = 'cursos'; $data['titulo'] = 'Todas las Asesorías';

        if(isset($_GET['order_by']) && $_GET['order_by'] != '')
        {
            $order_by = array($_GET['order_by'], $_GET['value']);
        }

        $data['cursos'] = $this->module_model->paginacion('cursos', $cantidad_por_pagina, $pagina, $order_by, $where);
        $totales = $this->module_model->total('cursos', array('estado' => 1, 'activado' => 1, 'categoria' => $id));
        $data['links'] = ceil($totales / $cantidad_por_pagina); $data['pagina_actual'] = ($pagina + 1); $data['por_pagina'] = $cantidad_por_pagina; $data['pagina'] = $pagina;
        
        $this->load->view("frontend/cursos_view", $data);
    }

    function contacto()
    {
        $data = array(); $data['urlvista'] = 'contacto';

        if(isset($_POST) && count($_POST) > 0)
        {
            $this->form_validation->set_rules('nombre', 'Nombres', 'required');
            $this->form_validation->set_rules('correo_electronico', 'Correo Electrónico', 'required|valid_email');
            $this->form_validation->set_rules('mensaje', 'Mensaje', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $data['mensaje'] = array('type' => 'danger', 'text' => validation_errors());
            }
            else
            {
                $mensaje = array('type' => 'warning', 'text' => "El correo electrónico no ha podido ser enviado. Por favor, intente nuevamente.");

                $html = '<h1>Información Recibida</h1>';

                $html .= 'Nombres y Apellidos: <strong>' . $_POST['nombre'] . '</strong><br />';
                $html .= 'Correo Electrónico: <strong>' . $_POST['correo_electronico'] . '</strong><br />';
                $html .= 'Mensaje:<br /><strong>' . nl2br($_POST['mensaje']) . '</strong><br />';

                $destinatario = $this->configuracion['email_contacto']; $contacto = $_POST['correo_electronico']; $asunto = 'Formulario de Contacto - ' . $this->configuracion['titulo'];

                if($this->enviar_email($contacto, $destinatario, $asunto, $html))
                {
                    $mensaje = array('type' => 'success', 'text' => "El correo electrónico fue enviado correctamente. Dentro de unos instantes nos comunicaremos con usted.");
                }

                $data['mensaje'] = $mensaje;
            }
        }

        $this->load->view("frontend/contacto_view", $data);
    }

    function carrito()
    {
        $data['urlvista'] = 'carrito'; $data['carrito'] = (array) $this->mostrar_session('carrito');

        $this->load->view("frontend/carrito_view", $data);
    }

    function salir()
    {
        $this->cerrar_session();
    }

    // Funciones del Sistema de E-Learning..
    function sys_eliminar($id, $table)
    {
        $this->module_model->actualizar($table, array('estado' => 0), $id);
    }

    function sys_busqueda_reporte()
    {
        $anio = $this->input->post('anio'); $mes = $this->input->post('mes'); $mes = ($mes < 10) ? ('0' . $mes) : $mes; $data['cursos'] = [];

        if($this->mostrar_session('nivel') == 2)
        {
            if($this->mostrar_session('ver_como') == 'profesor' OR $this->mostrar_session('nivel') <= 1)
            {
                $cursos = $this->module_model->seleccionar('cursos', array('estado' => 1));
            }
            else
            {
                $cursos = $this->module_model->seleccionar('cursos', array('id_padre' => $this->mostrar_session('id'), 'estado' => 1));

                $cursos = array_merge($this->module_model->seleccionar('cursos', array('tutor' => $this->mostrar_session('id'), 'estado' => 1)), $cursos);

                @array_unique($cursos);
            }

            foreach($cursos as $key => $value)
            {
                if(count($value) > 0 AND $value['activado'] == 1) // existe y está activo ?
                {
                    $data['cursos'][] = $value;
                }
            }

            // CURSOS
            $alumnos = [];

            foreach($data['cursos'] as $key => $value)
            {
                $alumnos[$value['id']] = $this->module_model->seleccionar('cursos_asignados', array('moneda' => $this->input->post('moneda'), 'id_curso' => $value['id'], 'DATE_FORMAT(fecha_creacion, "%Y-%m") =' => $anio . '-' . $mes, 'estado' => 1, 'activado' => 1));
            }

            $data['alumnos'] = $alumnos;
        }
        else
        {
            if($this->mostrar_session('ver_como') == 'profesor' OR $this->mostrar_session('nivel') <= 1)
            {
                $cursos = $this->module_model->seleccionar('cursos', array('estado' => 1));
            }
            else
            {
                $cursos = $this->module_model->seleccionar('cursos', array('id_padre' => $this->mostrar_session('id'), 'estado' => 1));

                $cursos = array_merge($this->module_model->seleccionar('cursos', array('tutor' => $this->mostrar_session('id'), 'estado' => 1)), $cursos);

                @array_unique($cursos);
            }
            
            // $cursos = $this->module_model->seleccionar('cursos', array('estado' => 1));

            foreach($cursos as $key => $value)
            {
                if(count($value) > 0 AND $value['activado'] == 1) // existe y está activo ?
                {
                    $data['cursos'][] = $value;
                }
            }

            // CURSOS
            $alumnos = []; $profesor = [];

            foreach($data['cursos'] as $key => $value)
            {
                $alumnos[$value['id']] = $this->module_model->seleccionar('cursos_asignados', array('moneda' => $this->input->post('moneda'), 'id_curso' => $value['id'], 'DATE_FORMAT(fecha_creacion, "%Y-%m") =' => $anio . '-' . $mes, 'estado' => 1, 'activado' => 1));
                $profesor[$value['id']] = $this->module_model->buscar('administrador', $value['id_padre']);
            }

            $data['alumnos'] = $alumnos; $data['profesor'] = $profesor;
        }

        $this->load->view("backend/templates/json_view", array('resultado' => $data));
    }

    function sys_dashboard()
    {
        $this->validar_usuario();

        $data = array(); $tipo = $this->mostrar_session('nivel'); $data['urlvista'] = 'inicio';
        $data['cursos'] = []; $data['cursos_asignados'] = [];

        if($this->mostrar_session('correo_electronico') != '' AND ($tipo == 3 OR $tipo == 2))
        {
            if($tipo == 3) // Estudiante..
            {
                $cursos_asignados = $this->module_model->seleccionar('cursos_asignados', array('id_padre' => $this->mostrar_session('id'), 'estado' => 1, 'activado' => 1));

                foreach($cursos_asignados as $key => $value)
                {
                    $curso = $this->module_model->buscar('cursos', $value['id_curso']);

                    if(count($curso) > 0 AND $curso['activado'] == 1) // existe y está activo ?
                    {
                        $data['cursos'][$key] = $curso; $data['cursos_asignados'][$key] = $value;
                    }
                }

                $config['item_order'] = array('key' => 'id', 'value' => 'desc'); $this->initialize($config);
                $data['calificaciones'] = $this->module_model->seleccionar('calificaciones', array('id_usuario' => $this->mostrar_session('id')), 2, 5);
                $this->clear_data();

            }
            if($tipo == 2) // Profesor..
            {
                if($this->mostrar_session('ver_como') == 'profesor')
                {
                    $cursos = $this->module_model->seleccionar('cursos', array('estado' => 1));
                }
                else
                {
                    $cursos = $this->module_model->seleccionar('cursos', array('id_padre' => $this->mostrar_session('id'), 'estado' => 1));

                    $cursos = array_merge($this->module_model->seleccionar('cursos', array('tutor' => $this->mostrar_session('id'), 'estado' => 1)), $cursos);

                    @array_unique($cursos);
                }

                foreach($cursos as $key => $value)
                {
                    if(count($value) > 0 AND $value['activado'] == 1) // existe y está activo ?
                    {
                        $data['cursos'][] = $value;
                    }
                }
            }

            $this->load->view("frontend/sistema/dashboard_view", $data);
        }
        else
        {
            redirect("/backend", "refresh");
        }
    }

    function sys_curso_agregar()
    {
        $this->validar_usuario();

        $data['urlvista'] = 'cursos'; $data['categorias'] = $this->module_model->seleccionar('categorias', array('estado' => 1));

        if(isset($_POST) && count($_POST) > 0)
        {
            $array = array(); $array['id_padre'] = $this->mostrar_session('id');

            foreach($_POST as $key => $value)
            {
                $array[$key] = $value;
                    
                if($key == 'youtube')
                {
                    if($value != '')
                    {
                        $item = explode('&', $value); $item = explode('v=', $item[0]);

                        if(count($item) > 1)
                        {
                            $array[$key] = $item[1];
                        }
                        else
                        {
                            $array[$key] = '';
                        }
                    }
                }
                
                if($key == 'titulo')
                {
                    $array['alias'] = $this->limpiar_texto($value);
                }
            }

            foreach($_FILES as $key => $value)
            {
                if($key == 'logo')
                {
                    $value = $this->cargar_imagen('', $key, array('370x250'));
                }
                else
                {
                    $value = $this->cargar_imagen('', $key, array());
                }

                if($value != '')
                {
                    $array[$key] = $value;
                }
            }

            $array['estado'] = 1;
            $array['usuario_creacion'] = $this->mostrar_session('id');
            $array['usuario_modificacion'] = $this->mostrar_session('id');
            $array['fecha_creacion'] = $this->fecha();
            $array['fecha_modificacion'] = $this->fecha();

            $mensaje = $this->module_model->guardar('cursos', $array); $ultimo_id = $this->module_model->last_insert_id();

            $this->session->set_flashdata('mensaje', $mensaje);

            redirect("/sys_curso_editar/" . $ultimo_id . "-" . $array['alias'], "refresh");
        }
        else
        {
            $this->load->view("frontend/sistema/curso_editar_view", $data);
        }
    }

    function sys_semana_agregar($curso = FALSE)
    {
        $this->validar_usuario(); $data['urlvista'] = 'cursos'; $curso = (int) $curso; $data['curso'] = $this->module_model->buscar('cursos', $curso);

        if($curso != FALSE)
        {
            if(isset($_POST) && count($_POST) > 0)
            {
                $array = array(); $array['id_padre'] = $curso;

                foreach($_POST as $key => $value)
                {
                    if($key == 'titulo')
                    {
                        $array['alias'] = $this->limpiar_texto($value);
                    }

                    $array[$key] = $value;
                }

                foreach($_FILES as $key => $value)
                {
                    $value = $this->cargar_imagen('', $key, array());

                    if($value != '')
                    {
                        $array[$key] = $value;
                    }
                }

                $array['estado'] = 1;
                $array['usuario_creacion'] = $this->mostrar_session('id');
                $array['usuario_modificacion'] = $this->mostrar_session('id');
                $array['fecha_creacion'] = $this->fecha();
                $array['fecha_modificacion'] = $this->fecha();

                $mensaje = $this->module_model->guardar('semanas', $array); $ultimo_id = $this->module_model->last_insert_id();

                $this->session->set_flashdata('mensaje', $mensaje);

                redirect("/sys_semana_editar/" . $ultimo_id . "-" . $array['alias'], "refresh");
            }
            else
            {
                $this->load->view("frontend/sistema/semana_agregar_view", $data);
            }
        }
        else
        {
            redirect("/sys_cursos", "refresh");
        }
    }

    function sys_semana_editar($semana = FALSE)
    {
        $this->validar_usuario(); $semana = (int) $semana; $data['semana'] = $this->module_model->buscar('semanas', $semana);
        $data['curso'] = $this->module_model->buscar('cursos', $data['semana']['id_padre']);

        if(isset($_POST) && count($_POST) > 0)
        {
            $array = array();

            foreach($_POST as $key => $value)
            {
                if($key == 'titulo')
                {
                    $array['alias'] = $this->limpiar_texto($value);
                }

                $array[$key] = $value;
            }

            foreach($_FILES as $key => $value)
            {
                $value = $this->cargar_imagen($data['semana'][$key], $key, array());

                if($value != '')
                {
                    $array[$key] = $value;
                }
            }

            $array['usuario_modificacion'] = $this->mostrar_session('id');
            $array['fecha_modificacion'] = $this->fecha();

            $data['mensaje'] = $this->module_model->actualizar('semanas', $array, $semana);
        }

        $data['urlvista'] = 'cursos';
        $data['semana'] = $this->module_model->buscar('semanas', $semana);

        $this->load->view("frontend/sistema/semana_agregar_view", $data);
    }

    function sys_examenes($semana = FALSE)
    {
        $this->validar_usuario(); $semana = (int) $semana; $data['semana'] = $this->module_model->buscar('semanas', $semana);

        if($semana != FALSE)
        {
            $data['urlvista'] = 'cursos'; $data['examenes'] = $this->module_model->seleccionar('tests', array('estado' => 1, 'id_padre' => $semana));

            $this->load->view("frontend/sistema/examenes_view.php", $data);
        }
        else
        {
            redirect("/sys_cursos", "refresh");
        }
    }

    function sys_examen_agregar($semana = FALSE)
    {
        $this->validar_usuario();

        if($semana != FALSE)
        {
            $data['urlvista'] = 'cursos'; $semana = (int) $semana; $data['semana'] = $this->module_model->buscar('semanas', $semana);

            if(isset($_POST) && count($_POST) > 0)
            {
                $array = array(); $array['id_padre'] = $semana;

                foreach($_POST as $key => $value)
                {
                    $array[$key] = $value;
                }

                $array['estado'] = 1;
                $array['usuario_creacion'] = $this->mostrar_session('id');
                $array['usuario_modificacion'] = $this->mostrar_session('id');
                $array['fecha_creacion'] = $this->fecha();
                $array['fecha_modificacion'] = $this->fecha();

                $mensaje = $this->module_model->guardar('tests', $array); $ultimo_id = $this->module_model->last_insert_id();

                $this->session->set_flashdata('mensaje', $mensaje);

                redirect("/sys_examen_editar/" . $ultimo_id, "refresh");
            }
            else
            {
                $this->load->view("frontend/sistema/examen_agregar_view.php", $data);
            }
        }
        else
        {
            redirect("/sys_cursos", "refresh");
        }
    }

    function sys_examen_editar($test = FALSE)
    {
        $data['urlvista'] = 'cursos';

        if($test != FALSE)
        {
            $test = (int) $test; $data['examen'] = $this->module_model->buscar('tests', $test);
            // Semanas..
            $data['semana'] = $this->module_model->buscar('semanas', $data['examen']['id_padre']);

            if(isset($_POST) && count($_POST) > 0)
            {
                $array = array();

                foreach($_POST as $key => $value)
                {
                    $array[$key] = $value;
                }

                $array['usuario_modificacion'] = $this->mostrar_session('id');
                $array['fecha_modificacion'] = $this->fecha();

                $data['mensaje'] = $this->module_model->actualizar('tests', $array, $test);
            }

            $data['examen'] = $this->module_model->buscar('tests', $test);
            $data['preguntas'] = $this->module_model->seleccionar('preguntas', array('id_padre' => $test, 'estado' => 1));

            $this->load->view("frontend/sistema/examen_agregar_view.php", $data);
        }
    }

    function sys_pregunta_agregar($test = FALSE)
    {
        $data['urlvista'] = 'cursos';

        if($test != FALSE)
        {
            $test = (int) $test; $data['examen'] = $this->module_model->buscar('tests', $test);

            if(isset($_POST) && count($_POST) > 0)
            {
                $array = array(); $array['id_padre'] = $test; $array['opciones'] = '';

                foreach($_POST as $key => $value)
                {
                    if(is_array($value))
                    {
                        $array[$key] = implode("|", $value);
                    }
                    else
                    {
                        $array[$key] = $value;
                    }
                }

                $array['estado'] = 1; $array['activado'] = 1;
                $array['usuario_creacion'] = $this->mostrar_session('id');
                $array['usuario_modificacion'] = $this->mostrar_session('id');
                $array['fecha_creacion'] = $this->fecha();
                $array['fecha_modificacion'] = $this->fecha();

                $mensaje = $this->module_model->guardar('preguntas', $array); $ultimo_id = $this->module_model->last_insert_id();

                $this->session->set_flashdata('mensaje', $mensaje);

                redirect("/sys_pregunta_editar/" . $ultimo_id, "refresh");
            }
            else
            {
                $this->load->view("frontend/sistema/pregunta_agregar_view.php", $data);
            }
        }
        else
        {
            redirect("/sys_cursos", "refresh");
        }
    }

    function sys_pregunta_editar($pregunta = FALSE)
    {
        $data['urlvista'] = 'cursos';

        if($pregunta != FALSE)
        {
            $pregunta = (int) $pregunta; $data['pregunta'] = $this->module_model->buscar('preguntas', $pregunta);

            if(isset($_POST) && count($_POST) > 0)
            {
                $array = array(); $array['opciones'] = '';

                foreach($_POST as $key => $value)
                {
                    if(is_array($value))
                    {
                        $array[$key] = implode("|", $value);
                    }
                    else
                    {
                        $array[$key] = $value;
                    }
                }

                $array['usuario_modificacion'] = $this->mostrar_session('id');
                $array['fecha_modificacion'] = $this->fecha();

                $data['mensaje'] = $this->module_model->actualizar('preguntas', $array, $pregunta);
            }

            $data['examen'] = $this->module_model->buscar('tests', $data['pregunta']['id_padre']);
            $data['pregunta'] = $this->module_model->buscar('preguntas', $pregunta);

            $this->load->view("frontend/sistema/pregunta_agregar_view.php", $data);
        }
        else
        {
            redirect("/sys_cursos", "refresh");
        }
    }

    function sys_lecciones($semana = FALSE)
    {
        $this->validar_usuario(); $semana = (int) $semana; $data['semana'] = $this->module_model->buscar('semanas', $semana);

        if($semana != FALSE)
        {
            $data['urlvista'] = 'cursos'; $data['lecciones'] = $this->module_model->seleccionar('lecciones', array('estado' => 1, 'id_padre' => $semana));

            $this->load->view("frontend/sistema/sesiones_view.php", $data);
        }
        else
        {
            redirect("/sys_cursos", "refresh");
        }
    }

    function sys_leccion_agregar($semana = FALSE)
    {
        $this->validar_usuario();

        if($semana != FALSE)
        {
            $data['urlvista'] = 'cursos'; $semana = (int) $semana; $data['semana'] = $this->module_model->buscar('semanas', $semana);

            if(isset($_POST) && count($_POST) > 0)
            {
                $array = array(); $array['id_padre'] = $semana;

                foreach($_POST as $key => $value)
                {
                    if($key == 'video')
                    {
                        if($value != '')
                        {
                            $item = explode('&', $this->input->post($key)); $item = explode('v=', $item[0]);
                            $array[$key] = $item[1];
                        }
                    }
                    else
                    {
                        if($key == 'titulo')
                        {
                            $array['alias'] = $this->limpiar_texto($value);
                        }

                        $array[$key] = $value;
                    }
                }

                foreach($_FILES as $key => $value)
                {
                    $value = $this->cargar_archivo('', $key);

                    if($value != '')
                    {
                        $array[$key] = $value;
                    }
                }

                $array['estado'] = 1;
                $array['usuario_creacion'] = $this->mostrar_session('id');
                $array['usuario_modificacion'] = $this->mostrar_session('id');
                $array['fecha_creacion'] = $this->fecha();
                $array['fecha_modificacion'] = $this->fecha();

                $mensaje = $this->module_model->guardar('lecciones', $array); $ultimo_id = $this->module_model->last_insert_id();

                $this->session->set_flashdata('mensaje', $mensaje);

                redirect("/sys_leccion_editar/" . $ultimo_id . "-" . $array['alias'], "refresh");
            }
            else
            {
                $this->load->view("frontend/sistema/sesion_agregar_view.php", $data);
            }
        }
        else
        {
            redirect("/sys_cursos", "refresh");
        }
    }

    function sys_leccion_editar($leccion = FALSE)
    {
        $data['urlvista'] = 'cursos';

        if($leccion != FALSE)
        {
            $leccion = (int) $leccion; $data['leccion'] = $this->module_model->buscar('lecciones', $leccion);
            // Semanas..
            $data['semana'] = $this->module_model->buscar('semanas', $data['leccion']['id_padre']);

            if(isset($_POST) && count($_POST) > 0)
            {
                $array = array();

                foreach($_POST as $key => $value)
                {
                    if($key == 'video')
                    {
                        if($value != '')
                        {
                            $item = explode('&', $this->input->post($key)); $item = explode('v=', $item[0]);
                            $array[$key] = $item[1];
                        }
                    }
                    else
                    {
                        if($key == 'titulo')
                        {
                            $array['alias'] = $this->limpiar_texto($value);
                        }

                        $array[$key] = $value;
                    }
                }

                foreach($_FILES as $key => $value)
                {
                    $value = $this->cargar_archivo('', $key);

                    if($value != '')
                    {
                        $array[$key] = $value;
                    }
                }

                $array['usuario_modificacion'] = $this->mostrar_session('id');
                $array['fecha_modificacion'] = $this->fecha();

                $data['mensaje'] = $this->module_model->actualizar('lecciones', $array, $leccion);
            }

            $data['leccion'] = $this->module_model->buscar('lecciones', $leccion);

            $this->load->view("frontend/sistema/sesion_agregar_view.php", $data);
        }
    }

    function sys_curso_editar($curso = FALSE)
    {
        $this->validar_usuario();

        if($curso != FALSE)
        {
            $data = array(); $data['urlvista'] = 'cursos'; $curso = (int) $curso; $data['curso'] = $this->module_model->buscar('cursos', $curso);

            if(isset($_POST) && count($_POST) > 0)
            {
                $array = array();

                foreach($_POST as $key => $value)
                {
                    $array[$key] = $value;
                    
                    if($key == 'youtube')
                    {
                        if($value != '')
                        {
                            $item = explode('&', $value); $item = explode('v=', $item[0]);

                            if(count($item) > 1)
                            {
                                $array[$key] = $item[1];
                            }
                            else
                            {
                                $array[$key] = '';
                            }
                        }
                    }
                    
                    if($key == 'titulo')
                    {
                        $array['alias'] = $this->limpiar_texto($value);
                    }
                }

                foreach($_FILES as $key => $value)
                {
                    if($key == 'logo')
                    {
                        $value = $this->cargar_imagen('', $key, array('370x250'));
                    }
                    else
                    {
                        $value = $this->cargar_imagen('', $key, array());
                    }

                    if($value != '')
                    {
                        $array[$key] = $value;
                    }
                }

                $array['usuario_modificacion'] = $this->mostrar_session('id');
                $array['fecha_modificacion'] = $this->fecha();

                $data['mensaje'] = $this->module_model->actualizar('cursos', $array, $curso);
            }

            $data['curso'] = $this->module_model->buscar('cursos', $curso);
            $data['semanas'] = $this->module_model->seleccionar('semanas', array('estado' => 1, 'id_padre' => $curso));
            $data['categorias'] = $this->module_model->seleccionar('categorias', array('estado' => 1));

            $this->load->view("frontend/sistema/curso_editar_view", $data);
        }
        else
        {
            redirect("/sys_cursos", "refresh");
        }
    }

    function sys_perfil()
    {
        $this->validar_usuario();

        $data = array(); $tipo = $this->mostrar_session('nivel'); $data['urlvista'] = 'perfil';

        if($this->mostrar_session('correo_electronico') != '')
        {
            $this->load->view("frontend/sistema/perfil_view", $data);
        }
        else
        {
            redirect("/backend", "refresh");
        }
    }

    function sys_configuracion()
    {
        $this->validar_usuario();

        $data = array(); $data['urlvista'] = 'configuracion';

        if(isset($_POST) && count($_POST) > 0)
        {
            $array = array();

            foreach($_POST as $key => $value)
            {
                $array[$key] = $value;
                $this->cargar_session($key, $value);
            }

            foreach($_FILES as $key => $value)
            {
                if($key == 'imagen')
                {
                    $value = $this->cargar_imagen($this->mostrar_session($key), $key, array('33x33', '250x270'));
                }
                
                if($key == 'experiencia')
                {
                    $value = $this->cargar_archivo($this->mostrar_session($key), $key);
                }

                if($value != '')
                {
                    $array[$key] = $value;

                    $this->cargar_session($key, $value);
                }
            }

            $data['mensaje'] = $this->module_model->actualizar('administrador', $array, $this->mostrar_session('id'));
        }

        if($this->mostrar_session('correo_electronico') != '')
        {
            $this->load->view("frontend/sistema/configuracion_view", $data);
        }
        else
        {
            redirect("/backend", "refresh");
        }
        
    }

    function sys_contrasenia()
    {
        $this->validar_usuario();

        $data = array(); $data['urlvista'] = 'contrasenia';

        if(isset($_POST) && count($_POST) > 0)
        {
            $busqueda = $this->module_model->buscar('administrador', $this->mostrar_session('id')); $array = array();
            
            if($this->encrypt->sha1($this->input->post('contrasenia_anterior')) == $busqueda['contrasenia'])
            {
                if($this->input->post('nueva_contrasenia') === $this->input->post('repetir_contrasenia'))
                {
                    $array['contrasenia'] = $this->encrypt->sha1($this->input->post('nueva_contrasenia'));
                    $data['mensaje'] = $this->module_model->actualizar('administrador', $array, $this->mostrar_session('id')); // Guardar datos
                }
                else
                {
                    $data['mensaje'] = $this->lang->line('contrasenias_no_coinciden');
                }
            }
            else
            {
                $data['mensaje'] = $this->lang->line('contrasenia_incorrecta');
            }
        }

        if($this->mostrar_session('correo_electronico') != '')
        {
            $this->load->view("frontend/sistema/contrasenia_view", $data);
        }
        else
        {
            redirect("/backend", "refresh");
        }
        
    }

    function sys_eliminar_mensajes($id = FALSE)
    {
        $sql = "UPDATE mensajes SET eliminado_receptor=1 WHERE (id_receptor=? AND id_emisor=?) AND estado=1";
        $query = $this->db->query($sql, array($this->mostrar_session('id'), $id));

        $sql = "UPDATE mensajes SET eliminado_emisor=1 WHERE (id_emisor=? AND id_receptor=?) AND estado=1";
        $query = $this->db->query($sql, array($this->mostrar_session('id'), $id));
    }

    function sys_mensajes()
    {
        $sql = "SELECT * FROM mensajes WHERE (id_receptor=?) AND estado=1 AND eliminado_receptor=0 GROUP BY id_emisor";
        $query = $this->db->query($sql, array($this->mostrar_session('id')));
        $data['mensajes'] = $query->result_array();

        $this->load->view("frontend/sistema/mensajes_view", $data);
    }

    function sys_mensaje($emisor = FALSE)
    {
        $emisor = (int) $emisor;

        if(isset($_POST) && count($_POST) > 0)
        {
            $array = array();
            $array['id_emisor'] = $this->mostrar_session('id');
            $array['id_receptor'] = $emisor;
            $array['contenido'] = $this->input->post('contenido');
            $array['estado'] = 1;
            $array['usuario_creacion'] = $this->mostrar_session('id');
            $array['usuario_modificacion'] = $this->mostrar_session('id');
            $array['fecha_creacion'] = $this->fecha();
            $array['fecha_modificacion'] = $this->fecha();

            $this->module_model->guardar('mensajes', $array);
        }
        else
        {
            $sql = "UPDATE mensajes SET activado=1 WHERE (id_receptor=? AND id_emisor=?) AND activado=0";
            $query = $this->db->query($sql, array($this->mostrar_session('id'), $emisor));
        }

        $sql = "SELECT * FROM mensajes WHERE (id_receptor=? AND id_emisor=? AND eliminado_receptor=0) OR (id_emisor=? AND id_receptor=? AND eliminado_emisor=0)  AND estado=1";
        $query = $this->db->query($sql, array($this->mostrar_session('id'), $emisor, $this->mostrar_session('id'), $emisor));
        $data['mensajes'] = $query->result_array(); $data['emisor'] = $this->module_model->buscar('administrador', $emisor);

        $this->load->view("frontend/sistema/mensaje_view", $data);
    }

    function sys_alumnos()
    {
        $this->validar_usuario();

        $data['cursos'] = array_merge($this->module_model->seleccionar('cursos', array('tutor' => $this->mostrar_session('id'))), $this->module_model->seleccionar('cursos', array('id_padre' => $this->mostrar_session('id'))));

        @array_unique($data['cursos']);

        $this->load->view("frontend/sistema/alumnos_view", $data);
    }

    function sys_alumnos_curso($curso = FALSE)
    {
        $this->validar_usuario();

        $curso = (int) $curso; $data['curso'] = $this->module_model->buscar('cursos', $curso); $semanas = $this->module_model->seleccionar('semanas', array('estado' => 1, 'id_padre' => $curso)); $tests = [];

        foreach($semanas as $key => $value)
        {
            $tests = array_merge($tests, $this->module_model->seleccionar('tests', array('estado' => 1, 'id_padre' => $value['id'])));
        }

        $data['tests'] = $tests; $data['alumnos'] = $this->module_model->seleccionar('cursos_asignados', array('id_curso' => $curso, 'estado' => 1, 'activado' => 1));

        $this->load->view("frontend/sistema/alumnos_curso_view", $data);
    }

    function sys_reporte_notas($curso = FALSE, $alumno = FALSE)
    {
        $this->validar_usuario(); $alumno = (int) $alumno; $curso = (int) $curso;
        $data['curso'] = $this->module_model->buscar('cursos', $curso); $data['alumno'] = $this->module_model->buscar('administrador', $alumno);

        $this->load->view("frontend/sistema/reporte_notas_view", $data);
    }

    function sys_profesores()
    {
        $this->validar_usuario();
        
        $data['cursos'] = $this->module_model->seleccionar('cursos', array('tutor' => $this->mostrar_session('id')));

        $this->load->view("frontend/sistema/profesores_view", $data);
    }

    function sys_cursos()
    {
        $this->validar_usuario();

        $data = array(); $data['urlvista'] = 'cursos';

        if($this->mostrar_session('correo_electronico') != '')
        {
            if($this->mostrar_session('nivel') == 2)
            {
                if($this->mostrar_session('ver_como') == 'profesor')
                {
                    $data['cursos'] = $this->module_model->seleccionar('cursos', array('estado' => 1));
                }
                else
                {
                    $data['cursos'] = $this->module_model->seleccionar('cursos', array('estado' => 1, 'id_padre' => $this->mostrar_session('id')));
                }
            }

            if($this->mostrar_session('nivel') == 3)
            {
                $cursos_asignados = $this->module_model->seleccionar('cursos_asignados', array('id_padre' => $this->mostrar_session('id'), 'estado' => 1, 'activado' => 1));
                $data['cursos_asignados'] = []; $data['cursos'] = []; // Habilitando el array de cursos..

                foreach($cursos_asignados as $key => $value)
                {
                    $curso = $this->module_model->buscar('cursos', $value['id_curso']);

                    if(count($curso) > 0 AND $curso['estado'] == 1) // existe y está activo ?
                    {
                        $data['cursos'][] = $curso; $data['cursos_asignados'][] = $value;
                    }
                }
            }

            $this->load->view("frontend/sistema/cursos_view", $data);
        }
        else
        {
            redirect("/backend", "refresh");
        }
    }

    function sys_test($id = FALSE, $semana = FALSE, $test = FALSE)
    {
        $this->validar_usuario();

        $data = array(); $semana = (int) $semana; $id = (int) $id; $test = (int) $test;

        $data['test'] = $this->module_model->buscar('tests', $test);

        $busqueda = $this->module_model->seleccionar('calificaciones', array('id_padre' => $test, 'id_usuario' => $this->mostrar_session('id')), 1, 1);

        $data['curso'] = $this->module_model->buscar('cursos', $id);
        $data['semana'] = $this->module_model->buscar('semanas', $semana);

        if(count($busqueda) == 0 OR ($busqueda['intentos'] < $data['test']['intentos']))
        {
            $data['lecciones'] = $this->module_model->seleccionar('lecciones', array('estado' => 1, 'id_padre' => $semana));
            $data['preguntas'] = $this->module_model->seleccionar('preguntas', array('id_padre' => $test, 'activado' => 1, 'estado' => 1));

            $this->load->view("frontend/sistema/test_view", $data);
        }
        else
        {
            redirect("/sys_curso/" . $data['curso']['id'] . "-" . $data['curso']['alias'] . "/" . $data['semana']['id'] . "-" . $data['semana']['alias'], "refresh");
        }
    }

    function sys_resultados($test = FALSE)
    {
        $this->validar_usuario();

        if(isset($_POST) && count($_POST) > 0)
        {
            $data = array(); $test = (int) $test; $data['test'] = $this->module_model->buscar('tests', $test); $correctas = 0; $incorrectas = 0;

            $data['preguntas'] = array(); $data['respuesta'] = array(); $respuestas = array();

            $data['lecciones'] = $this->module_model->seleccionar('lecciones', array('estado' => 1, 'id_padre' => $data['test']['id_padre']));
            $data['semana'] = $this->module_model->seleccionar('semanas', array('estado' => 1, 'id' => $data['test']['id_padre']), 1, 1);
            $data['curso'] = $this->module_model->seleccionar('cursos', array('estado' => 1, 'id' => $data['semana']['id_padre']), 1, 1);

            $busqueda = $this->module_model->seleccionar('calificaciones', array('id_padre' => $test, 'id_usuario' => $this->mostrar_session('id')), 1, 1);

            if(count($busqueda) == 0 || $busqueda['intentos'] <= $data['test']['intentos'])
            {
                foreach($_POST['pregunta'] as $key => $value)
                {
                    $pregunta = $this->module_model->buscar('preguntas', $value);

                    if($data['test']['tipo'] == 0)
                    {
                        $data['preguntas'][] = $pregunta; $data['respuesta'][] = @$_POST['respuesta'][$key];

                        if($pregunta['respuesta'] == @$_POST['respuesta'][$key])
                        {
                            $correctas += 1;
                        }
                        else
                        {
                            $incorrectas += 1;
                        }
                    }
                    else
                    {
                        $respuestas[] = (isset($_POST['respuesta'][$key])) ? $_POST['respuesta'][$key] : '-';
                    }
                }

                $data['correctas'] = $correctas; $data['incorrectas'] = $incorrectas; $respuestas = implode("|", $respuestas);

                $calificacion = array('id_padre' => $test, 'id_usuario' => $this->mostrar_session('id'), 'correctas' => $correctas, 'incorrectas' => $incorrectas, 'respuestas' => $respuestas);

                if(count($busqueda) == 0)
                {
                    $calificacion['intentos'] = 1;

                    $this->module_model->guardar('calificaciones', $calificacion);
                }
                else
                {
                    $calificacion['intentos'] = $busqueda['intentos'] + 1;
                    
                    $this->module_model->actualizar('calificaciones', $calificacion, $busqueda['id']);
                }

                // print_r($data); die;

                $this->load->view("frontend/sistema/resultado_test_view", $data);
            }
            else
            {
                redirect("/backend", "refresh");
            }
        }
        else
        {
            redirect("/backend", "refresh");
        }
    }

    function sys_examen_revisar($id = FALSE, $semana = FALSE, $test = FALSE)
    {
        $this->validar_usuario();

        $data = array(); $semana = (int) $semana; $id = (int) $id; $test = (int) $test;

        $busqueda = $this->module_model->buscar('calificaciones', $id);

        $data['semana'] = $this->module_model->buscar('semanas', $semana);
        $data['curso'] = $this->module_model->buscar('cursos', $data['semana']['id_padre']);

        if(count($busqueda) > 0)
        {
            if(isset($_POST) && count($_POST) > 0)
            {
                $data['mensaje'] = $this->module_model->actualizar('calificaciones', array('nota' => $this->input->post('nota')), $id);

                redirect("/sys_dashboard", "refresh");
            }

            $data['lecciones'] = $this->module_model->seleccionar('lecciones', array('estado' => 1, 'id_padre' => $semana));

            $data['test'] = $this->module_model->buscar('tests', $busqueda['id_padre']);
            $data['preguntas'] = $this->module_model->seleccionar('preguntas', array('id_padre' => $busqueda['id_padre'], 'activado' => 1, 'estado' => 1));

            $data['calificacion'] = $busqueda;

            $this->load->view("frontend/sistema/test_revisar_view", $data);
        }
        else
        {
            redirect("/sys_curso/" . $data['curso']['id'] . "-" . $data['curso']['alias'] . "/" . $data['semana']['id'] . "-" . $data['semana']['alias'], "refresh");
        }
    }

    function sys_calificaciones($id = FALSE)
    {
        $data['curso'] = $this->module_model->buscar('cursos', $id);

        if(count($data['curso']) > 0)
        {
            $data['semanas'] = $this->module_model->seleccionar('semanas', array('estado' => 1, 'id_padre' => $id)); $calificaciones = array();
            $data['profesor'] = $this->module_model->buscar('administrador', $data['curso']['id_padre']);

            foreach($data['semanas'] as $key => $value)
            {
                $tests = $this->module_model->seleccionar('tests', array('id_padre' => $value['id']));

                foreach($tests as $k => $v)
                {
                    $calificaciones = array_merge($calificaciones, $this->module_model->seleccionar('calificaciones', array('id_usuario' => $this->mostrar_session('id'), 'id_padre' => $v['id'])));
                }
            }

            $data['calificaciones'] = $calificaciones;

            $this->load->view("frontend/sistema/calificaciones_view", $data);
        }
        else
        {
            redirect("/sys_dashboard", "refresh");
        }
    }

    function sys_calificacion($id = FALSE)
    {
        $id = (int) $id; $busqueda = $this->module_model->buscar('cursos', $id);

        if(count($busqueda) > 0)
        {
            if(isset($_POST) && count($_POST) > 0)
            {
                $array = []; $array['id_padre'] = $id; $array['id_usuario'] = $this->mostrar_session('id');

                foreach($_POST as $key => $value)
                {
                    $array[$key] = $value;
                }

                $array['estado'] = 1;
                $array['usuario_creacion'] = $this->mostrar_session('id');
                $array['usuario_modificacion'] = $this->mostrar_session('id');
                $array['fecha_creacion'] = $this->fecha();
                $array['fecha_modificacion'] = $this->fecha();

                $this->module_model->guardar('feedback', $array);
            }

            redirect("sys_curso/" . $busqueda['id'] . "-" . $busqueda['alias'] , "refresh");
        }
        else
        {
            redirect("/sys_dashboard", "refresh");
        }
    }

    function sys_curso($id = FALSE, $semana = FALSE, $leccion = FALSE)
    {
        $this->validar_usuario();

        $data = array(); $tipo = $this->mostrar_session('nivel'); $id = (int) $id;

        $data['feedback'] = $this->module_model->seleccionar('feedback', array('estado' => 1, 'id_padre' => $id, 'id_usuario' => $this->mostrar_session('id')), 1, 1);

        if($this->mostrar_session('correo_electronico') != '')
        {
            if($tipo == 2)
            {
                $data['curso'] = $this->module_model->buscar('cursos', $id); $data['semanas'] = $this->module_model->seleccionar('semanas', array('id_padre' => $id, 'estado' => 1));

                $config['item_order'] = array('key' => 'orden', 'value' => 'ASC'); $this->initialize($config);
                foreach($data['semanas'] as $key => $value)
                {
                    $data['lecciones'][$key] = $this->module_model->seleccionar('lecciones', array('id_padre' => $value['id'], 'estado' => 1));
                }
                $this->clear_data();

                if($semana != FALSE AND $leccion != FALSE)
                {
                    $leccion = (int) $leccion; $semana = (int) $semana;
                    
                    if(isset($_POST) && count($_POST) > 0)
                    {
                        $array['id_padre'] = $leccion; $array['id_emisor'] = $this->mostrar_session('id'); $array['contenido'] = $this->input->post('contenido');
                        $array['estado'] = 1; $array['usuario_creacion'] = $this->mostrar_session('id'); $array['usuario_modificacion'] = $this->mostrar_session('id');
                        $array['time'] = strtotime (date("Y-m-d H:i:s"));
                        $array['fecha_creacion'] = $this->fecha(); $array['fecha_modificacion'] = $this->fecha();

                        $this->module_model->guardar('discusion', $array);

                        $response['contenido'] = $array['contenido']; $response['time'] = $this->input->post('time') - 1;

                        $this->load->view("backend/templates/print_json_view", array('data' => $response));
                    }
                    else
                    {
                        $data['leccion'] = $this->module_model->buscar('lecciones', $leccion);
                        $data['semana'] = $this->module_model->buscar('semanas', $semana);
                        $data['tests'] = $this->module_model->seleccionar('tests', array('id_padre' => $semana, 'tipo' => 1));

                        $data['time'] = strtotime(date("Y-m-d H:i:s"));
                        $data['discusion'] = $this->module_model->seleccionar('discusion', array('id_padre' => $leccion, 'estado' => 1));
                        
                        $this->load->view("frontend/sistema/leccion_profesor_view.php", $data);
                    }
                }
                else
                {
                    $this->load->view("frontend/sistema/curso_profesor_view.php", $data);
                }
            }

            if($tipo == 3) // Estudiante
            {
                $curso = $this->module_model->seleccionar('cursos_asignados', array('id_curso' => $id, 'id_padre' => $this->mostrar_session('id'), 'estado' => 1, 'activado' => 1), 1, 1);

                $data['curso_asignado'] = $curso;

                if(count($curso) > 0)
                {
                    $data['curso'] = $this->module_model->buscar('cursos', $id);

                    if($data['curso']['estado'] == 1)
                    {
                        $data['profesor'] = $this->module_model->buscar('administrador', $data['curso']['id_padre']);
                        $data['semanas'] = $this->module_model->seleccionar('semanas', array('id_padre' => $id, 'estado' => 1));

                        $config['item_order'] = array('key' => 'orden', 'value' => 'ASC'); $this->initialize($config);
                        foreach($data['semanas'] as $key => $value)
                        {
                            $data['lecciones'][$key] = $this->module_model->seleccionar('lecciones', array('id_padre' => $value['id'], 'estado' => 1));
                        }
                        $this->clear_data();
                    }
                    else
                    {
                        redirect("/sys_cursos", "refresh");
                    }
                }

                if(count($data['profesor']) > 0)
                {
                    if($leccion != FALSE)
                    {
                        if(isset($_POST) && count($_POST) > 0)
                        {
                            $array['id_padre'] = $leccion; $array['id_emisor'] = $this->mostrar_session('id'); $array['contenido'] = $this->input->post('contenido');
                            $array['estado'] = 1; $array['usuario_creacion'] = $this->mostrar_session('id'); $array['usuario_modificacion'] = $this->mostrar_session('id');
                            $array['time'] = strtotime (date("Y-m-d H:i:s"));
                            $array['fecha_creacion'] = $this->fecha(); $array['fecha_modificacion'] = $this->fecha();

                            $this->module_model->guardar('discusion', $array);

                            $response['contenido'] = $array['contenido']; $response['time'] = $this->input->post('time') - 1;

                            $this->load->view("backend/templates/print_json_view", array('data' => $response));
                        }
                        else
                        {
                            $leccion = (int) $leccion; $semana = (int) $semana;

                            // Actualización de Lección
                            $busqueda_leccion = $this->module_model->seleccionar('cursos_asignados', array('id_padre' => $this->mostrar_session('id'), 'id_curso' => $id, 'estado' => 1, 'activado' => 1), 1, 1);

                            if($busqueda_leccion['id_leccion'] <= $leccion OR $busqueda_leccion['id_leccion'] == 0)
                            {
                                $this->module_model->actualizar('cursos_asignados', array('id_leccion' => $leccion), $curso['id']);

                                $curso = $this->module_model->buscar('cursos_asignados', $curso['id']); $data['curso_asignado'] = $curso;
                            }

                            $config['item_order'] = array('key' => 'id', 'value' => 'asc'); $this->initialize($config);
                            $data['siguiente'] = $this->module_model->seleccionar('lecciones', array('estado' => 1, 'id_padre' => $semana, 'id >' => $leccion, 'id !=' => $leccion), 1, 1);
                            $this->clear_data();

                            $config['item_order'] = array('key' => 'id', 'value' => 'desc'); $this->initialize($config);
                            $data['anterior'] = $this->module_model->seleccionar('lecciones', array('estado' => 1, 'id_padre' => $semana, 'id <' => $leccion, 'id !=' => $leccion), 1, 1);
                            $this->clear_data();

                            $data['time'] = strtotime(date("Y-m-d H:i:s"));
                            $data['discusion'] = $this->module_model->seleccionar('discusion', array('id_padre' => $leccion, 'estado' => 1));

                            $data['semana'] = $this->module_model->buscar('semanas', $semana);
                            $data['lecciones'] = $this->module_model->seleccionar('lecciones', array('estado' => 1, 'id_padre' => $semana));
                            $data['leccion'] = $this->module_model->buscar('lecciones', $leccion);

                            $data['tests'] = $this->module_model->seleccionar('tests', array('estado' => 1, 'id_padre' => $semana));

                            $this->load->view("frontend/sistema/leccion_view", $data);
                        }
                    }
                    elseif($semana != FALSE)
                    {
                        $semana = (int) $semana; $data['semana'] = $this->module_model->buscar('semanas', $semana);
                        $data['lecciones'] = $this->module_model->seleccionar('lecciones', array('estado' => 1, 'id_padre' => $semana));
                        $data['tests'] = $this->module_model->seleccionar('tests', array('estado' => 1, 'id_padre' => $semana));

                        $this->load->view("frontend/sistema/semana_view", $data);
                    }
                    else
                    {
                        $this->load->view("frontend/sistema/curso_view", $data);
                    }
                }
                else
                {
                    redirect("/backend", "refresh");
                }
            }
        }
        else
        {
            redirect("/backend", "refresh");
        }
    }

    function sys_update($leccion = FALSE, $time = FALSE)
    {
        $data['time'] = strtotime("-4 second", time()); $data['fecha'] = date("Y-m-d H:i:s", $time);
        $data['discusion'] = $this->module_model->seleccionar('discusion', array('id_padre' => $leccion, 'estado' => 1, 'time >' => $data['time']));
        $data['emisor'] = [];

        foreach($data['discusion'] as $key => $value)
        {
            $data['emisor'][$key] = $this->module_model->buscar('administrador', $value['id_emisor']);
        }

        $this->load->view("backend/templates/json_view", array('resultado' => $data));
    }

    function sys_exportar_comisiones()
    {
        $fecha = date("Ymd");

        //Inicio de la instancia para la exportación en Excel
        header('Content-type: application/vnd.ms-excel;charset=UTF-8');
        header("Content-Disposition: attachment; filename=Listado_$fecha.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        $anio = $this->input->post('anio'); $mes = $this->input->post('mes'); $mes = ((int) $mes < 10) ? ('0' . (int) $mes) : $mes; $moneda = $this->input->post('moneda');

        if($this->mostrar_session('nivel') <= 1)
        {
            $cursos = $this->module_model->seleccionar('cursos', array('estado' => 1));
        }
        else
        {
            $cursos = array_merge($this->module_model->seleccionar('cursos', array('id_padre' => $this->mostrar_session('id'), 'estado' => 1)), $this->module_model->seleccionar('cursos', array('tutor' => $this->mostrar_session('id'), 'estado' => 1)));
        }

        @array_unique($cursos);

        $objPHPExcel = new PHPExcel();
        // Set the active Excel worksheet to sheet 0 
        $objPHPExcel->setActiveSheetIndex(0);  
        // Initialise the Excel row number 
        $row = 1;  
        //start of printing column names as names of MySQL fields  
        $column = 0;

        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Curso');
        $column++;
        if($this->mostrar_session('nivel') <= 1)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Profesor');
            $column++;
        }
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Cantidad de Alumnos');
        $column++;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Comisión por Curso');
        $column++;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Precio del Curso');
        $column++;
        $row++;

        if(count($cursos) > 0)
        {
            $comisiones = [];

            foreach($cursos as $key => $value)
            {
                $alumnos = $this->module_model->seleccionar('cursos_asignados', array('moneda' => $moneda, 'id_curso' => $value['id'], 'DATE_FORMAT(fecha_creacion, "%Y-%m") =' => $anio . '-' . $mes, 'estado' => 1, 'activado' => 1));

                // print_r($this->db->last_query()); die;

                if($this->mostrar_session('nivel') <= 1)
                {
                    $cadena = 'comision';
                }
                else
                {
                    $cadena = (($value['id_padre'] == MY_Controller::mostrar_session('id')) ? 'comision' : 'comision_tutor');
                }

                $column = 0;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $value['titulo']);
                $column++;

                if($this->mostrar_session('nivel') <= 1)
                {
                    $profesor = $this->module_model->buscar('administrador', $value['id_padre']);

                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, (@$profesor['nombres'] . " " . @$profesor['apellidos']));
                    $column++;
                }

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, count($alumnos));
                $column++;

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, ((($moneda == '_soles') ? 'S/ ' : 'US$ ') . number_format($value['precio' . $moneda], 2)));
                $column++;
                
                $comision_total = (count($alumnos) * ($value['precio' . $moneda] * $value[$cadena] / 100)); $comisiones[] = array($value['titulo'], $comision_total);

                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, ((($moneda == '_soles') ? 'S/ ' : 'US$ ') . number_format($comision_total, 2)));
                $column++;
                $row++;
            }
        }

        // Redirect output to a client’s web browser (Excel5) 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="reporte_notas_curso_'. date('YmdHis') .'.xls"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        $objWriter->save('php://output');
    }

    function sys_exportar_notas_curso($curso = FALSE)
    {
        $this->validar_usuario();

        $fecha = date("Ymd"); $curso = (int) $curso;

        //Inicio de la instancia para la exportación en Excel
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=Listado_$fecha.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        $curso = $this->module_model->buscar('cursos', $curso);

        $alumnos = $this->module_model->seleccionar('cursos_asignados', array('id_curso' => $curso['id'], 'estado' => 1, 'activado' => 1));

        $objPHPExcel = new PHPExcel();
        // Set the active Excel worksheet to sheet 0 
        $objPHPExcel->setActiveSheetIndex(0);  
        // Initialise the Excel row number 
        $row = 1;  
        //start of printing column names as names of MySQL fields  
        $column = 0;

        foreach($alumnos as $a => $b)
        {
            $alumno = $this->module_model->buscar('administrador', $alumno);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Alumno');
            $column++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, ($alumno['nombres'] . ' ' . $alumno['apellidos']));
            $row++; $column = 0;

            $semanas = $this->module_model->seleccionar('semanas', array('estado' => 1, 'id_padre' => $curso['id'])); $curso_asignado = $this->module_model->seleccionar('cursos_asignados', array('id_padre' => $alumno['id'], 'id_curso' => $curso['id']), 1, 1);

            foreach($semanas as $key => $value)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Semana');
                $column++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $value['titulo']);
                $row++; $column = 0;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Test');
                $column++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Tipo');
                $column++;
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Nota');

                $tests = $this->module_model->seleccionar('tests', array('estado' => 1, 'id_padre' => $value['id'])); $tipo = array('Objetivo', 'Subjetivo');

                $row++;

                foreach($tests as $k => $v)
                {
                    $calificacion = $this->module_model->seleccionar('calificaciones', array('id_padre' => $v['id'], 'id_usuario' => $alumno['id']), 1, 1);
                    if(count($calificacion) > 0)
                    {
                        $column = 0;
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $v['titulo']);
                        $column++;
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $tipo[$v['tipo']]);
                        $column++;
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, (($v['tipo'] == 0) ? ($calificacion['correctas'] / ($calificacion['correctas'] + $calificacion['incorrectas']) * 20) : ($calificacion['nota'])));

                        $row++;
                    }
                }
            }
        }

        // Redirect output to a client’s web browser (Excel5) 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="reporte_notas_curso_'. date('YmdHis') .'.xls"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        $objWriter->save('php://output');
    }

    function sys_exportar_notas($curso = FALSE, $alumno = FALSE)
    {
        $this->validar_usuario();

        $fecha = date("Ymd"); $alumno = (int) $alumno; $curso = (int) $curso;

        //Inicio de la instancia para la exportación en Excel
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=Listado_$fecha.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        $curso = $this->module_model->buscar('cursos', $curso); $alumno = $this->module_model->buscar('administrador', $alumno);

        $semanas = $this->module_model->seleccionar('semanas', array('estado' => 1, 'id_padre' => $curso['id'])); $curso_asignado = $this->module_model->seleccionar('cursos_asignados', array('id_padre' => $alumno['id'], 'id_curso' => $curso['id']), 1, 1);

        $objPHPExcel = new PHPExcel();
        // Set the active Excel worksheet to sheet 0 
        $objPHPExcel->setActiveSheetIndex(0);  
        // Initialise the Excel row number 
        $row = 1;  
        //start of printing column names as names of MySQL fields  
        $column = 0;

        foreach($semanas as $key => $value)
        {
            $tests = $this->module_model->seleccionar('tests', array('estado' => 1, 'id_padre' => $value['id'])); $tipo = array('Objetivo', 'Subjetivo');

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Semana');
            $column++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $value['titulo']);
            $row++; $column = 0;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Test');
            $column++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Tipo');
            $column++;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, 'Nota');

            foreach($tests as $k => $v)
            {
                $calificacion = $this->module_model->seleccionar('calificaciones', array('id_padre' => $v['id'], 'id_usuario' => $alumno['id']), 1, 1);
                if(count($calificacion) > 0)
                {
                    $column = 0;
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $v['titulo']);
                    $column++;
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, $tipo[$v['tipo']]);
                    $column++;
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, $row, (($v['tipo'] == 0) ? ($calificacion['correctas'] / ($calificacion['correctas'] + $calificacion['incorrectas']) * 20) : ($calificacion['nota'])));

                    $row++;
                }
            }
        }

        // Redirect output to a client’s web browser (Excel5) 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="reporte_notas_'. date('YmdHis') .'.xls"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
        $objWriter->save('php://output');
    }

    // Fin de las funciones de E-Learning

    function sys_descargar_archivo()
    {
        $archivo = @$_GET['archivo'];

        if($archivo != '')
        {
            $this->descargar($archivo);
        }
        else
        {
            redirect("/backend", "refresh");
        }
    }
}