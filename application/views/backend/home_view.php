<?php $this->load->view("backend/templates/header_view"); ?>

<?php $tipo_usuario = $this->tipo_usuarios; ?>

<?php if(MY_Controller::mostrar_session('nivel') == 0 || MY_Controller::mostrar_session('nivel') == 1 || MY_Controller::mostrar_session('nivel') == 2): ?>
	<div class="col-md-12" style="margin-bottom:10px;">
		<div class="navbar navbar-inverse" role="navigation">
			<div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>
	            <a class="navbar-brand" href="<?php echo backend_url(); ?>">INICIO</a>
	        </div>
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<?php if(MY_Controller::mostrar_session('nivel') == 0): ?>
				    	<li><a href="javascript:;" onclick="javascript:abrir('backend_menu', this);" data-title="<?php echo $this->lang->line('backend_menu'); ?>" data-icon="fa-th-list"><i class="fa fa-th-list"></i> Menú Principal</a></li>
					<?php endif; ?>
					<?php if(MY_Controller::mostrar_session('nivel') == 0 || MY_Controller::mostrar_session('nivel') == 1): ?>
						<li><a href="javascript:;" onclick="javascript:abrir_pestania('configuracion', this);" data-title="<?php echo $this->lang->line('configuracion'); ?>" data-icon="fa-cogs"><i class="fa fa-cogs"></i> Configuración</a></li>
						<li><a href="javascript:;" onclick="javascript:abrir('usuarios', this);" data-title="<?php echo $this->lang->line('usuarios'); ?>" data-icon="fa-group"><i class="fa fa-group"></i> <?php echo $this->lang->line('usuarios'); ?></a></li>
					<?php endif; ?>
					<?php $menu = MY_Controller::mostrar_menu(); ?>
				    <?php if(count($menu) > 0): ?>
						<?php foreach($menu as $k => $v): ?>
							<?php if(count($v) == 1): ?>
								<?php foreach($v as $key => $value): ?>
									<?php if(!isset($this->items_unlocked[$value['url']])): ?>
										<?php if(isset($this->permisos[$value['url']]) || MY_Controller::mostrar_session('nivel') == 0 || MY_Controller::mostrar_session('nivel') == 1): ?>
											<li><a href="javascript:;" onclick="javascript:abrir('<?php echo $value['url']; ?>', this);" data-title="<?php echo $this->lang->line($value['url']); ?>" data-icon="<?php echo $value['icono']; ?>"><i class="fa <?php echo $value['icono']; ?>"></i> <?php echo $this->lang->line($value['url']); ?></a></li>
										<?php endif; ?>
									<?php else: ?>
										<?php foreach($this->items_unlocked[$value['url']] as $a => $b): ?>
											<?php $item = $this->module_model->buscar($value['url'], $b); ?>
											<?php if(count($item) > 0): ?>
												<li><a href="javascript:;" onclick="javascript:actualizar_item('<?php echo $value['url']; ?>', <?php echo $b; ?>);" data-title="<?php echo $item['titulo']; ?>" data-icon="<?php echo $value['icono']; ?>"><i class="fa <?php echo $value['icono']; ?>"></i> <?php echo $item['titulo']; ?></a></li>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
								<?php endforeach; ?>
							<?php else: ?>
								<li class="dropdown">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> <?php echo $k; ?> <b class="caret"></b></a>
									<ul class="dropdown-menu">
									<?php foreach($v as $key => $value): ?>
										<?php if(!isset($items_unlocked[$value['url']])): ?>
											<?php if(isset($this->permisos[$value['url']]) || MY_Controller::mostrar_session('nivel') == 0 || MY_Controller::mostrar_session('nivel') == 1): ?>
												<li><a href="javascript:;" onclick="javascript:abrir('<?php echo $value['url']; ?>', this);" data-title="<?php echo $this->lang->line($value['url']); ?>" data-icon="<?php echo $value['icono']; ?>"><i class="fa <?php echo $value['icono']; ?>"></i> <?php echo $this->lang->line($value['url']); ?></a></li>
											<?php endif; ?>
										<?php else: ?>
											<?php foreach($this->items_unlocked[$value['url']] as $a => $b): ?>
												<?php $item = $this->module_model->buscar($value['url'], $b); ?>
												<li><a href="javascript:;" onclick="javascript:actualizar_item('<?php echo $value['url']; ?>', <?php echo $b; ?>);" data-title="<?php echo $item['titulo']; ?>" data-icon="<?php echo $value['icono']; ?>"><i class="fa <?php echo $value['icono']; ?>"></i> <?php echo $item['titulo']; ?></a></li>
											<?php endforeach; ?>
										<?php endif; ?>
									<?php endforeach; ?>
									</ul>
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
<?php endif; ?>

<!-- div class="col-lg-3 hidden-xs hidden-sm hidden-md">
	<?php /* ?>
	<div class="profile-nav alt">
        <section class="panel">
            <div class="user-heading alt clock-row bg-inverse badge">
            	<?php $fecha = explode(' ', str_replace(array('del', 'de', ','), array(NULL, NULL, NULL), MY_Controller::fecha_muestra(date('Y-m-d')))); ?>
                <h1><?php echo $fecha[3]; ?> <?php echo $fecha[1]; ?></h1>
                <p class="text-left"><?php echo $fecha[5]; ?>, <?php echo $fecha[0]; ?></p>
                <p class="text-left time"><?php echo date('H:i:s'); ?></p>
            </div>
            <ul id="clock">
                <li id="sec"></li>
                <li id="hour"></li>
                <li id="min"></li>
            </ul>
            <ul class="clock-category">
            </ul>
        </section>
    </div>
    <?php */ ?>

    <aside class="profile-nav alt">
        <section class="panel">
            <div class="user-heading alt gray-bg">
                <a href="javascript:;" onclick="javascript:abrir_pestania('perfil', this);" data-title="<?php echo $this->lang->line('usuario'); ?>" data-icon="fa-user">
                    <img alt="<?php echo MY_Controller::mostrar_session('imagen'); ?>" src="<?php echo base_url(); ?>uploads/100x100/<?php echo MY_Controller::mostrar_session('imagen'); ?>">
                </a>
                <h4><?php echo MY_Controller::mostrar_session('nombres'); ?> <?php echo MY_Controller::mostrar_session('apellidos'); ?></h4>
                <p style="font-style:italic;font-size:12px;"><?php echo $tipo_usuario[MY_Controller::mostrar_session('nivel')]; ?></p>
            </div>
            <ul class="nav nav-pills nav-stacked">
                <li><a href="javascript:;" onclick="javascript:abrir_pestania('perfil', this);" data-title="<?php echo $this->lang->line('usuario'); ?>" data-icon="fa-user"> <i class="fa fa-user"></i> <?php echo $this->lang->line('usuario'); ?> <span class="badge label-primary pull-right r-activity"><i class="fa fa-edit"></i></span></a></li>
                <li><a href="javascript:;" onclick="javascript:abrir_pestania('contrasenia', this);" data-title="<?php echo $this->lang->line('contrasenia'); ?>" data-icon="fa-lock"> <i class="fa fa-lock"></i> <?php echo $this->lang->line('contrasenia'); ?> <span class="badge label-primary pull-right r-activity"><i class="fa fa-edit"></i></span></a></li>
                <li><a href="javascript:;"> <i class="fa fa-bell-o"></i> <?php echo $this->lang->line('notificaciones'); ?> <span class="badge label-success pull-right r-activity">0</span></a></li>
            </ul>
        </section>
    </aside>
</div -->

<section style="background:transparent;" class="col-lg-12 col-md-12">
	<section id="content-main">
		<div class="row">
			<div class="col-md-12">
				<?php if(MY_Controller::mostrar_session('nivel') == 0 || MY_Controller::mostrar_session('nivel') == 1): ?>
					<section class="panel">
				        <header class="panel-heading">
				            <i class="fa fa-clock-o"></i> Encuestas por Cursos
				            <span class="tools pull-right">
				                <a href="javascript:;" class="fa fa-chevron-down"></a>
				            </span>
				        </header>
				        <div class="panel-body">
				        	<!-- div class="alert alert-warning">	
				                <span class="alert-icon"><i class="fa fa-bell-o"></i></span>
				                <div class="notification-info">
				                    <ul class="clearfix notification-meta" style="padding-left:0px !important;">
				                        <li class="pull-left notification-sender"><?php echo $this->lang->line('informacion_adicional'); ?></li>
				                    </ul>
				                    <p>
				                        La información aquí mostrada es de caracter técnico para futuras auditorías.<br /><small>Máximo 7 registros.</small>
				                    </p>
				                </div>
				            </div -->

				            <table class="table table-bordered table-striped">
					            <thead>
					            <tr>
					                <th>Curso</th>
					                <th>Cantidad de Opiniones</th>
					                <th>Enseñanza</th>
					                <th>Metodología</th>
					                <th>Silabus</th>
					                <th>Plataforma</th>
					                <th>Promedio</th>
					            </tr>
					            </thead>
					            <tbody id="resultado_comision">
					            	<?php if(count($cursos) > 0): ?>
										<?php foreach($cursos as $key => $value): ?>
										<?php $feedback = $this->module_model->seleccionar('feedback', array('id_padre' => $value['id'])); ?>

										<?php

										$ensenianza = 0; $metodologia = 0; $silabus = 0; $plataforma = 0; $promedio = 0;

										$reporte = array('Muy mala', 'Mala', 'Normal', 'Buena', 'Muy buena');

										if(count($feedback) > 0):
											
											foreach($feedback as $k => $v)
											{
												$ensenianza += $v['ensenianza'];
												$metodologia += $v['metodologia'];
												$silabus += $v['silabus'];
												$plataforma += $v['plataforma'];
											}

											$ensenianza = ($ensenianza / count($feedback));
											$metodologia = ($metodologia / count($feedback));
											$silabus = ($silabus / count($feedback));
											$plataforma = ($plataforma / count($feedback));

											$promedio = ($ensenianza + $metodologia + $silabus + $plataforma) / 4;

										?>
										<tr>
											<td><?php echo $value['titulo']; ?></td>
											<td><?php echo count($feedback); ?></td>
											<td><?php echo $reporte[$ensenianza]; ?></td>
											<td><?php echo $reporte[$metodologia]; ?></td>
											<td><?php echo $reporte[$silabus]; ?></td>
											<td><?php echo $reporte[$plataforma]; ?></td>
											<td><?php echo $reporte[$promedio]; ?></td>
										</tr>
										<?php endif; ?>
										<?php endforeach; ?>
									<?php else: ?>
										<tr>
											<td colspan="5" scope="row">No hay información para mostrar.</td>
										</tr>
									<?php endif; ?>
					            </tbody>
					        </table>
					    </div>
					</section>
					
					<section class="panel">
				        <header class="panel-heading">
				            <i class="fa fa-clock-o"></i> Comisiones por Cursos - Dólares
				            <span class="tools pull-right">
				                <a href="javascript:;" class="fa fa-chevron-down"></a>
				            </span>
				        </header>
				        <div class="panel-body">
				        	<!-- div class="alert alert-warning">	
				                <span class="alert-icon"><i class="fa fa-bell-o"></i></span>
				                <div class="notification-info">
				                    <ul class="clearfix notification-meta" style="padding-left:0px !important;">
				                        <li class="pull-left notification-sender"><?php echo $this->lang->line('informacion_adicional'); ?></li>
				                    </ul>
				                    <p>
				                        La información aquí mostrada es de caracter técnico para futuras auditorías.<br /><small>Máximo 7 registros.</small>
				                    </p>
				                </div>
				            </div -->

				            <?php $meses = array(1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Setiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Octubre'); ?>

				            <div class="container-fluid" style="margin-bottom:20px;">
					   			Filtros: 
					   			<form method="POST" action="<?php echo base_url(); ?>sys_exportar_comisiones">
					   				<input type="hidden" value="_soles" name="moneda" />
						   			<select name="mes" id="mes" onchange="javascript:filtrar_comision_dolares('mes', this);">
						   				<option value="">Seleccione un Mes</option>
						   				<?php foreach($meses as $key => $value): ?>
						   				<option value="<?php echo ($key < 10) ? ('0' . $key) : $key; ?>" <?php echo ($key == (int) date("m")) ? 'selected="selected"' : ''; ?>><?php echo $value; ?></option>
						   				<?php endforeach; ?>
						   			</select>

						   			<select name="anio" id="anio" onchange="javascript:filtrar_comision_dolares('anio', this);">
						   				<option value="">Seleccione un Año</option>
						   				<?php for($i=(date("Y") - 10);$i<=(date("Y") + 10);$i++): ?>
						   				<option value="<?php echo $i; ?>" <?php echo (date("Y") == $i) ? 'selected="selected"' : ''; ?>><?php echo $i; ?></option>
						   				<?php endfor; ?>
						   			</select>

						   			<button type="submit">Exportar comisiones</button>
					   			</form>
					   		</div>
				        
					        <table class="table table-bordered table-striped">
					            <thead>
					            <tr>
					                <th>Curso</th>
					                <th>Profesor</th>
					                <th>Cantidad de Alumnos</th>
					                <th>Comisión del Curso</th>
					                <th>Precio del Curso (con IGV)</th>
					                <th>Precio del Curso (sin IGV</th>
					                <th>Comisión Total</th>
					            </tr>
					            </thead>
					            <tbody id="resultado_comision_dolares">
					            	<?php if(count($cursos) > 0): ?>
										<?php foreach($cursos as $key => $value): ?>
										<?php $profesor = $this->module_model->buscar('administrador', $value['id_padre']); ?>
										<?php $alumnos = $this->module_model->seleccionar('cursos_asignados', array('moneda' => '_dolares', 'id_curso' => $value['id'])); $precio = $value['precio_dolares']; ?>
										<?php if(count($profesor) > 0): ?>
										<tr>
											<td><?php echo $value['titulo']; ?></td>
											<td><?php echo $profesor['nombres']; ?> <?php echo $profesor['apellidos']; ?></td>
											<td><?php echo count($alumnos); ?></td>
											<td><?php echo $value['comision']; ?>%</td>
											<td>US$ <?php echo number_format($precio, 2); ?></td>
											<td>US$ <?php echo number_format(($precio / 1.18), 2); ?></td>
											<td>US$ <?php echo number_format((count($alumnos) * ($value['comision'] * ($precio / 1.18) / 100)), 2); ?></td>
										</tr>
										<?php endif; ?>
										<?php endforeach; ?>
									<?php else: ?>
										<tr>
											<td colspan="5" scope="row">No hay información para mostrar.</td>
										</tr>
									<?php endif; ?>
					            </tbody>
					        </table>
					    </div>
					</section>

					<section class="panel">
				        <header class="panel-heading">
				            <i class="fa fa-clock-o"></i> Comisiones por Cursos - Soles
				            <span class="tools pull-right">
				                <a href="javascript:;" class="fa fa-chevron-down"></a>
				            </span>
				        </header>
				        <div class="panel-body">
				            <?php $meses = array(1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Setiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Octubre'); ?>

				            <div class="container-fluid" style="margin-bottom:20px;">
					   			Filtros: 
					   			<form method="POST" action="<?php echo base_url(); ?>sys_exportar_comisiones">
					   				<input type="hidden" value="_dolares" name="moneda" />
						   			<select name="mes" id="mes" onchange="javascript:filtrar_comision_soles('mes', this);">
						   				<option value="">Seleccione un Mes</option>
						   				<?php foreach($meses as $key => $value): ?>
						   				<option value="<?php echo ($key < 10) ? ('0' . $key) : $key; ?>" <?php echo ($key == (int) date("m")) ? 'selected="selected"' : ''; ?>><?php echo $value; ?></option>
						   				<?php endforeach; ?>
						   			</select>

						   			<select name="anio" id="anio" onchange="javascript:filtrar_comision_soles('anio', this);">
						   				<option value="">Seleccione un Año</option>
						   				<?php for($i=(date("Y") - 10);$i<=(date("Y") + 10);$i++): ?>
						   				<option value="<?php echo $i; ?>" <?php echo (date("Y") == $i) ? 'selected="selected"' : ''; ?>><?php echo $i; ?></option>
						   				<?php endfor; ?>
						   			</select>

						   			<button type="submit">Exportar comisiones</button>
						   		</form>
					   		</div>
				        
					        <table class="table table-bordered table-striped">
					            <thead>
					            <tr>
					                <th>Curso</th>
					                <th>Profesor</th>
					                <th>Cantidad de Alumnos</th>
					                <th>Comisión del Curso</th>
					                <th>Precio del Curso (con IGV)</th>
					                <th>Precio del Curso (sin IGV)</th>
					                <th>Comisión Total</th>
					            </tr>
					            </thead>
					            <tbody id="resultado_comision_soles">
					            	<?php if(count($cursos) > 0): ?>
										<?php foreach($cursos as $key => $value): ?>
										<?php $profesor = $this->module_model->buscar('administrador', $value['id_padre']); ?>
										<?php $alumnos = $this->module_model->seleccionar('cursos_asignados', array('moneda' => '_soles', 'id_curso' => $value['id'])); $precio = $value['precio_soles']; ?>
										<?php if(count($profesor) > 0): ?>
										<tr>
											<td><?php echo $value['titulo']; ?></td>
											<td><?php echo $profesor['nombres']; ?> <?php echo $profesor['apellidos']; ?></td>
											<td><?php echo count($alumnos); ?></td>
											<td><?php echo $value['comision']; ?>%</td>
											<td>S/ <?php echo number_format($precio, 2); ?></td>
											<td>S/ <?php echo number_format($precio / 1.18, 2); ?></td>
											<td>S/ <?php echo number_format((count($alumnos) * ($value['comision'] * ($precio / 1.18) / 100)), 2); ?></td>
										</tr>
										<?php endif; ?>
										<?php endforeach; ?>
									<?php else: ?>
										<tr>
											<td colspan="5" scope="row">No hay información para mostrar.</td>
										</tr>
									<?php endif; ?>
					            </tbody>
					        </table>
					    </div>
					</section>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<!-- div id="content-main" class="tab-pane"></div -->
</section>

<script type="text/javascript">

	var xnmes_soles = '<?php echo date("m"); ?>'; var xnanio_soles = '<?php echo date("Y"); ?>';
	var xnmes_dolares = '<?php echo date("m"); ?>'; var xnanio_dolares = '<?php echo date("Y"); ?>';

	function filtrar_comision_dolares(type, element)
	{
		eval('xn' + type + '_dolares = ' + $(element).val());

		$.ajax({
			url: "<?php echo base_url(); ?>sys_busqueda_reporte",
			type: 'POST',
			data: { mes: xnmes_dolares, anio: xnanio_dolares, moneda: '_dolares' },
			dataType: 'json',
			success: function(data){
				var html = '';

				$.each(data['cursos'], function(i, item) {

					var precio = item['precio_dolares'];

					var comision = parseFloat(data['alumnos'][item['id']].length * ((precio / 1.18) * item['comision'] / 100)).toFixed(2);

					html += '<tr>';
					html += '<td>' + item['titulo'] + '</td>';
					html += '<td>' + data['profesor'][item['id']]['nombres'] + ' ' + data['profesor'][item['id']]['apellidos'] + '</td>';
					html += '<td>' + data['alumnos'][item['id']].length + '</td>';
					html += '<td>' + item['comision'] + '%</td>';
					html += '<td>US$ ' + parseFloat(precio).toFixed(2) + '</td>';
					html += '<td>US$ ' + parseFloat(precio * 1.18).toFixed(2) + '</td>';
					html += '<td>US$ ' + comision + '</td>';
					html += '</tr>';
				});

				$('#resultado_comision_dolares').html(html);
			},
			error: function(data){
				console.log(data);
			}
		});
	}

	function filtrar_comision_soles(type, element)
	{
		eval('xn' + type + '_soles = ' + $(element).val());

		$.ajax({
			url: "<?php echo base_url(); ?>sys_busqueda_reporte",
			type: 'POST',
			data: { mes: xnmes_soles, anio: xnanio_soles, moneda: '_soles' },
			dataType: 'json',
			success: function(data){
				var html = '';

				$.each(data['cursos'], function(i, item) {

					var precio = item['precio_soles'];
					var comision = parseFloat(data['alumnos'][item['id']].length * ((precio / 1.18) * item['comision'] / 100)).toFixed(2);

					html += '<tr>';
					html += '<td>' + item['titulo'] + '</td>';
					html += '<td>' + data['profesor'][item['id']]['nombres'] + ' ' + data['profesor'][item['id']]['apellidos'] + '</td>';
					html += '<td>' + data['alumnos'][item['id']].length + '</td>';
					html += '<td>' + item['comision'] + '%</td>';
					html += '<td>S/ ' + parseFloat(precio).toFixed(2) + '</td>';
					html += '<td>S/ ' + parseFloat(precio * 1.18).toFixed(2) + '</td>';
					html += '<td>S/ ' + comision + '</td>';
					html += '</tr>';
				});

				$('#resultado_comision_soles').html(html);
			},
			error: function(data){
				console.log(data);
			}
		});
	}

	setInterval(function() {
		// Generación del Tiempo..

		var f=new Date();
		var hora = f.getHours(); var minuto = f.getMinutes(); var segundo = f.getSeconds();
		
		if(hora < 10)
		{
			hora = '0'+hora.toString();
		}

		if(minuto < 10)
		{
			minuto = '0'+minuto.toString();
		}

		if(segundo < 10)
		{
			segundo = '0'+segundo.toString();
		}

		$('.time').html(hora+":"+minuto+":"+segundo);

	}, 1000);
</script>
<?php $this->load->view("backend/templates/footer_view"); ?>