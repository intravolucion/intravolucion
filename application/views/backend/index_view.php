<!DOCTYPE html>
<html lang="es"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="google-signin-client_id" content="1076154863504-m18onbldp95h651tvvq9eplb5sg3o662.apps.googleusercontent.com">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="CMS, administrador, contenidos">
    <meta name="author" content="PHSmedia">
    <link rel="shortcut icon" href="<?php echo base_view(); ?>favicon.png">
    <title>Administrador PHSmedia <?php echo date("Y"); ?></title>
    <link href="<?php echo backend_view(); ?>bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo backend_view(); ?>assets/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
    <link href="<?php echo backend_view(); ?>css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo backend_view(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo backend_view(); ?>assets/jvector-map/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="<?php echo backend_view(); ?>css/clndr.css" rel="stylesheet">
    <!--clock css-->
    <link href="<?php echo backend_view(); ?>assets/css3clock/css/style.css" rel="stylesheet">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="assets/morris-chart/morris.css">
    <!-- Custom styles for this template -->
    <link href="<?php echo backend_view(); ?>css/style.css" rel="stylesheet">
    <link href="<?php echo backend_view(); ?>css/style-responsive.css" rel="stylesheet"/>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="<?php echo backend_view(); ?>js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo backend_view(); ?>https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="<?php echo backend_view(); ?>https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
  <body class="login-body">
    <div class="container">
      <form class="form-signin" method="POST" action="<?php echo current_url(); ?>" autocomplete="off">
        <!-- h2 class="form-signin-heading"><?php echo ($this->configuracion['titulo'] != '') ? $this->configuracion['titulo'] : 'Ingresar al Sistema'; ?></h2 -->
        <div class="login-wrap" style="padding-bottom:10px;">
            <?php if($this->configuracion['logo'] != ''): ?>
                <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>uploads/<?php echo $this->configuracion['logo']; ?>" style="max-width:100%;max-height:100px;margin:0 auto;display:block;" /></a>
            <?php endif; ?>
        </div>
        <?php if(isset($message)): ?>
        <div class="alert alert-danger fade in" style="margin-bottom:0px;">
          <button data-dismiss="alert" class="close close-sm" type="button">
              <i class="fa fa-times"></i>
          </button>
          <strong>Ocurrió un Error!</strong> <?php echo $message; ?>
        </div>
        <?php endif; ?>
        <div class="login-wrap" style="padding-top:10px;">
            <div class="alert" id="_facebook_message" style="display:none"></div>
            <div class="container-fluid" style="overflow:hidden;">
                <button href="javascript:;" type="button" onclick="javascript:checkLoginState();" class="btn btn-info col-xs-6 btn btn-sm" style="margin: 7px 0px; box-shadow: 0 2px 4px 0 rgba(0,0,0,.25); height: 36px; background: #3B5998; border-color: #3B5998;">
                    <i class="fa fa-facebook"></i> Iniciar sesión
                </button>
                <div class="g-signin2 col-xs-6 btn btn-sm" data-onsuccess="onSignIn"></div>
            </div>
            <p><br /></p>
            <p> Ingrese los detalles de su cuenta.</p>
            <input type="hidden" name="token" value="<?php echo MY_Controller::mostrar_session('token'); ?>" />
            <input class="form-control" placeholder="Correo Electrónico" required <?php if(isset($correo_electronico) && $correo_electronico === ""){ ?> autofocus=""<?php } ?> name="correo_electronico" type="text"<?php if(isset($correo_electronico)){ ?> value="<?php echo $correo_electronico ?>"<?php } ?>>
            <input class="form-control" placeholder="Contrase&ntilde;a" required <?php if(isset($correo_electronico) && $correo_electronico !== NULL){ ?> autofocus=""<?php } ?> type="password" name="contrasenia">
            <div class="olvidaste-con text-right">
              <a href="javascript:;" data-toggle="modal" data-target="#olv_contra">¿Olvidaste tu contraseña?</a><br />
            </div>
            <button class="btn btn-lg btn-danger btn-block" type="submit">INGRESAR</button>
        </div>
      </form>

      <!-- Modal -->
      <div class="modal fade" id="olv_contra" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Olvidaste Contraseña</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="alert alert-warning alert-dismissable" style="margin: 1em;">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <span>Se enviará un correo para recuperar contraseña.</span>
                </div>
              </div>
              <form action="<?php echo base_url(); ?>recuperar_contrasenia" class="subscribe__form" method="POST">
                <input type="hidden" name="_token" name="<?php echo MY_Controller::mostrar_session('token'); ?>">
                <input type="text" class="subscribe__input form-control" name="correo_electronico" placeholder="Tu correo ..." required="true" style="width: 85%; display: inline-block;">
                <button class="subscribe__btn btn btn-success btn-effect">Enviar</button>
              </form>
              
            </div>
            
          </div>
        </div>
      </div>
      <!-- fin del modal -->

    </div>
    <!-- Placed js at the end of the document so the pages load faster -->
    <!--Core js-->
    <script src="<?php echo backend_view(); ?>js/lib/jquery.js"></script>
    <script src="<?php echo backend_view(); ?>assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
    <script src="<?php echo backend_view(); ?>bs3/js/bootstrap.min.js"></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <script type="text/javascript">
      // GOOGLE
      function signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
          console.log('User signed out.');
        });
      }

      function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();

        $.ajax({
            url: "<?php echo backend_url(); ?>dashboard/login_redes",
            type: 'POST',
            dataType: 'json',
            data: { correo_electronico: profile.getEmail(), nombres: profile.getName(), 'type': 'google', id_social: profile.id },
            success: function(data) {
                imprimir(data);
            },
            error: function(data) {
                console.log(data);
            }
        });
      }

      // This is called with the results from from FB.getLoginStatus().
      function statusChangeCallback(response) {
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
          // Logged into your app and Facebook.
          testAPI();
        } else if (response.status === 'not_authorized') {
          // The person is logged into Facebook, but not your app.
        } else {
          // The person is not logged into Facebook, so we're not sure if
          // they are logged into this app or not.
        }
      }

      // This function is called when someone finishes with the Login
      // Button.  See the onlogin handler attached to it in the sample
      // code below.
      function checkLoginState() {
        FB.login(function(response) {
            statusChangeCallback(response);
            // handle the response
        }, {scope: 'email', 'return_scopes': true});
        
        /*
        FB.getLoginStatus(function(response) {
          statusChangeCallback(response);
        });
        */
      }

      window.fbAsyncInit = function() {
        FB.init({
          appId      : '414631235555271',
          xfbml      : true,
          version    : 'v2.5'
        });

          // Now that we've initialized the JavaScript SDK, we call 
          // FB.getLoginStatus().  This function gets the state of the
          // person visiting this page and can return one of three states to
          // the callback you provide.  They can be:
          //
          // 1. Logged into your app ('connected')
          // 2. Logged into Facebook, but not your app ('not_authorized')
          // 3. Not logged into Facebook and can't tell if they are logged into
          //    your app or not.
          //
          // These three cases are handled in the callback function.

          FB.getLoginStatus(function(response) {
            //statusChangeCallback(response);
          });
      };

      // Load the SDK asynchronously
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/es_LA/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));

      // Here we run a very simple test of the Graph API after login is
      // successful.  See statusChangeCallback() for when this call is made.
      function testAPI() {
        $('#_facebook_message').css('display', 'none');

        FB.api('/me', { locale: 'es_LA', fields: 'name, email' }, function(response) {
            $.ajax({
                url: "<?php echo backend_url(); ?>dashboard/login_redes",
                type: 'POST',
                dataType: 'json',
                data: { correo_electronico: response.email, nombres: response.name, type: 'facebook', id_social: response.id },
                success: function(data) {
                  imprimir(data);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        });
      }

      function imprimir(data)
      {
        $('#_facebook_message').css('display', 'none');
        
        if(data['url'] == undefined)
        {
          $('#_facebook_message').addClass('alert-' + data.type);
          $('#_facebook_message').html(data.content);

          $('#_facebook_message').slideToggle();
        }
        else
        {
          signOut();

          setTimeout(function(){ window.location.reload(); }, 150);
        }
      }

      signOut();
    </script>
  </body>
</html>