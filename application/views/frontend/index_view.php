<?php $this->load->view('frontend/templates/head_view.php'); ?>
<style>
  .cb{
    color: #fff !important;
  }
</style>
<body>

<!-- Loader -->
<!-- <div id="page-preloader"><span class="spinner"></span></div> -->
<!-- Loader end -->
<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">
  <div id="wrapper">

    <!-- HEADER -->
    <?php $this->load->view('frontend/templates/header_view.php'); ?>
    <!-- end header -->
    <div class="main-content">
      <?php if(isset($banners) AND count($banners) > 0): ?>
        <div id="sliderpro1" class="slider-pro main-slider">
          <div class="sp-slides">
            <?php foreach($banners as $key => $value): ?>
            <div class="sp-slide">
              <img class="sp-image" src="<?php echo base_url(); ?>uploads/1600x1100/<?php echo $value['imagen']; ?>" data-src="<?php echo base_url(); ?>uploads/<?php echo $value['imagen']; ?>" data-retina="<?php echo base_view(); ?>media/main-slider/1.jpg" alt="img" />
              <div class="item-wrap sp-layer  sp-padding" data-horizontal="700" data-vertical="1" data-show-transition="left" data-hide-transition="up" data-show-delay="400" data-hide-delay="200">
                <div class="main-slider__inner text-center">
                  <div class="main-slider__title" ><?php echo $value['titulo']; ?></div>
                  <div class="main-slider__subtitle "><?php echo $value['subtitulo']; ?></div>
                  <a class="main-slider__btn btn btn-warning btn-effect" href="http://<?php echo str_replace(array('http://', 'https://'), '', $value['enlace']); ?>">Inicia un curso</a> </div>
              </div>
            </div>
            <?php endforeach; ?>
          </div>
        </div>
      <?php endif; ?>
      <!-- end main-slider -->
      <?php $this->load->view('frontend/templates/cuenta/login_view.php'); ?>
      
      <div class="section_mod-a">
        <div class="container">
          <div class="section_mod-a__inner">
            <div class="row">
              <div class="col-sm-8">
                <section class="section-advantages">
                  <h2 class="ui-title-block ui-title-block_mod-a">Nuestros cursos y asesorías virtuales te mostraran una nueva forma de alcanzar tu potencial.</h2>
                  <div class="ui-subtitle-block ui-subtitle-block_mod-a">Nuestros usuarios nos respaldan.</div>
                  <ul class="advantages advantages_mod-a list-unstyled">
                    <li class="advantages__item"> <span class="advantages__icon"><i class="icon stroke icon-Cup"></i></span>
                      <div class="advantages__inner">
                        <h3 class="ui-title-inner decor decor_mod-a">Certificados</h3>
                        <div class="advantages__info">
                          <p>Obtendrás certificados de reconocimiento por tu progreso.</p>
                        </div>
                      </div>
                    </li>
                    <li class="advantages__item"> <span class="advantages__icon"><i class="icon stroke icon-DesktopMonitor"></i></span>
                      <div class="advantages__inner">
                        <h3 class="ui-title-inner decor decor_mod-a">CLASES Y ASESORÍAS VIRTUALES</h3>
                        <div class="advantages__info">
                          <p>Asesorías y clases virtuales con la mejor metodología.</p>
                        </div>
                      </div>
                    </li>
                    <li class="advantages__item"> <span class="advantages__icon"><i class="icon stroke icon-WorldGlobe"></i></span>
                      <div class="advantages__inner">
                        <h3 class="ui-title-inner decor decor_mod-a">Conocimiento Mundial</h3>
                        <div class="advantages__info">
                          <p>Con nuestros cursos y asesorías podrás alcanzar tus objetivos donde sea que estés.</p>
                        </div>
                      </div>
                    </li>
                    <li class="advantages__item"> <span class="advantages__icon"><i class="icon stroke icon-Users"></i></span>
                      <div class="advantages__inner">
                        <h3 class="ui-title-inner decor decor_mod-a">Sistema de Tutores</h3>
                        <div class="advantages__info">
                          <p>Tenemos a nuestra disposición un staff de tutores que te acompañarán a lo largo del proceso de aprendizaje.</p>
                        </div>
                      </div>
                    </li>
                  </ul>
                </section>
                <!-- end section-advantages --> 
              </div>
              <!-- end col -->
              <div class="col-sm-4">
                <section class="find-course find-course_mod-a">
                  <h2 class="find-course__title"><i class="icon stroke icon-Search"></i>ENCUENTRA TU CURSO</h2>
                  <form class="find-course__form" method="GET" action="<?php echo base_url(); ?>busqueda">
                    <div class="form-group">
                      <input class="form-control" name="word" type="text" placeholder="Palabras clave ..." required />
                      <div class="jelect" >
                        <input value="" type="text" class="jelect-input">
                        <div tabindex="0" role="button" class="jelect-current">Categoría</div>
                        <ul class="jelect-options">
                          <li class="jelect-option jelect-option_state_active">Seleccione</li>
                          <?php foreach($this->categorias as $key => $value): ?>
                          <li class="jelect-option" data-value="<?php echo $value['id']; ?>"><?php echo $value['titulo']; ?></li>
                          <?php endforeach; ?>
                        </ul>
                      </div>
                      <!-- end jelect -->
                    </div>
                    <!-- end form-group -->
                    <div class="find-course__wrap-btn">
                      <button class="btn btn-info btn-effect">BUSCAR CURSO</button>
                    </div>
                  </form>
                </section>
                <!-- end find-course --> 
              </div>
              <!-- end col --> 
            </div>
            <!-- end row --> 
          </div>
          <!-- end section_mod-a__inner --> 
        </div>
        <!-- end container --> 
      </div>
      <!-- end section_mod-a -->
      
      <section class="section-default">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="wrap-title">
                <h2 class="ui-title-block">Los cursos <strong>más Populares</strong></h2>
                <div class="ui-subtitle-block ui-subtitle-block_mod-b">Tenemos muchos estudiantes que los eligieron.</div>
              </div>

              <div class="posts-wrap">
                <?php foreach($cursos as $key => $value): ?>
                <?php $profesor = $this->module_model->buscar('administrador', $value['id_padre']); ?>
                <?php if(count($profesor) > 0): ?>

                <article class="post post_mod-c clearfix">
                  
                  <div class="entry-media">
                    <div class="entry-thumbnail"> <a href="<?php echo base_url(); ?>curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" ><img class="img-responsive" src="<?php echo base_url(); ?>uploads/370x250/<?php echo $value['logo']; ?>" width="370" height="250" alt="<?php echo $value['titulo']; ?>" /></a> </div>
                    <div class="entry-hover">
                        <!-- div class="box-comments"> <a href="javascript:void(0);"><i class="icon stroke icon-Message"></i>25</a> <a href="javascript:void(0);"><i class="icon stroke icon-User"></i>65</a> </div -->
                        <a href="<?php echo base_url(); ?>curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" class=""><button class="post-btn btn btn-primary btn-effect">VER MÁS</button></a> </div>
                  </div>

                  <div class="entry-main" style="height: 300px;">
                    <div class="entry-meta"> <span class="entry-autor"> <span>By </span> <a class="post-link" href="javascript:void(0);"><?php echo $profesor['nombres']; ?> <?php echo $profesor['apellidos']; ?></a> </span> <span class="entry-date"><a href="javascript:void(0);"><?php echo MY_Controller::fecha_muestra($value['fecha_inicio']); ?></a></span> </div>
                    <h3 class="entry-title ui-title-inner decor decor_mod-b"><a href="<?php echo base_url(); ?>curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><?php echo $value['titulo']; ?></a></h3>
                    <div class="entry-content" style="height: 75px; overflow: hidden;">
                      <p><?php echo nl2br($value['resumen']); ?></p>
                    </div>
                  </div>
                </article>
                
                <?php endif; ?>
                <?php endforeach; ?>
                <div class="boton-home-curso text-center">
                  <a href="<?php echo base_url(); ?>cursos" class="post-btn btn btn-primary btn-effect">Ver todos los cursos</a>
                </div>
                <!-- end post --> 
              </div>
              <!-- end posts-wrap --> 
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end container --> 
      </section>
      <!-- end section-default -->
      
   
      
      <section class="section-video" >
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="video-block"> <a class="video-block__link" href="https://www.youtube.com/watch?v=<?php echo $this->configuracion['video_introduccion']; ?>" rel="prettyPhoto"><i class="icon stroke icon-Play"></i></a>
                <h2 class="video-block__title">Explicación de la Plataforma INTRAVOLUCION</h2>
                <div class="video-block__subtitle">Tenemos muchos cursos de donde elegir.</div>
              </div>
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end container --> 
      </section>
      <!-- end section-video -->
      
      <section class="section-subscribe">
        <div class="subscribe">
          <div class="container">
            <div class="row">
              <div class="col-sm-6">
                <div class="subscribe__icon-wrap"> <i class="icon_bg stroke icon-Imbox"></i><i class="icon stroke icon-Imbox"></i> </div>
                <div class="subscribe__inner">
                  <h2 class="subscribe__title">Déjanos tu correo para enviarte información.</h2>
                  <div class="subscribe__description">o suscribete para informacion mensual.</div>
                </div>
              </div>
              <!-- end col -->
              <div class="col-sm-6">
                <form class="subscribe__form" method="POST" action="<?php echo base_url(); ?>suscribirse">
                  <input class="subscribe__input form-control" type="text" name="correo_electronico" placeholder="Tu Correo ..." required />
                  <button type="submit" class="subscribe__btn btn btn-success btn-effect">Suscribirse</button>
                </form>
              </div>
              <!-- end col --> 
            </div>
            <!-- end row --> 
          </div>
          <!-- end container --> 
        </div>
      </section>
      <!-- end section-subscribe -->
      
      <section class="section-clients">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <h2 class="ui-title-block ui-title-block_mod-b">Nuestro <strong>Equipo INTRAVOLUCION</strong></h2>
              <ul class="list-clients list-unstyled clearfix slider-nav">

                <?php foreach ($profesores as $key => $value): ?>
                <li class="list-clients__item">
                  <a href="<?php echo base_url(); ?>profesor/<?php echo $value['id']; ?>-<?php echo MY_Controller::limpiar_texto($value['nombres']); ?>">
                    <img class="img-responsive" src="<?php echo base_url(); ?>uploads/250x270/<?php echo $value['imagen']; ?>" alt="<?php echo $value['nombres']; ?> <?php echo $value['apellidos']; ?>">
                    <p style="color: #fff; height: 40px;"><?php echo $value['nombres']; ?> <?php echo $value['apellidos']; ?></p>
                    <ul class="social-links list-unstyled app-asociados" style="background: transparent;">
                      <?php if($value['facebook'] != ''): ?><li><a target="_blank" class="icon fa fa-facebook cb" href="https://<?php echo str_replace(array('https://', 'http://'), '', $value['facebook']); ?>"></a></li><?php endif; ?>
                        <?php if($value['twitter'] != ''): ?><li><a target="_blank" class="icon fa fa-twitter cb" href="https://<?php echo str_replace(array('https://', 'http://'), '', $value['twitter']); ?>"></a></li><?php endif; ?>
                        <!-- li><a class="icon fa fa-google-plus" href="javascript:void(0);"></a></li -->
                        <?php if($value['linkedin'] != ''): ?><li><a target="_blank" class="icon fa fa-linkedin cb" href="https://<?php echo str_replace(array('https://', 'http://'), '', $value['linkedin']); ?>"></a></li><?php endif; ?>
                        <?php if($value['youtube'] != ''): ?><li><a target="_blank" class="icon fa fa-youtube cb" href="https://<?php echo str_replace(array('https://', 'http://'), '', $value['youtube']); ?>"></a></li><?php endif; ?>
                    </ul>
                  </a>
                </li>
                <?php endforeach; ?>

              </ul>
              <div class="list-clients__description">Tenemos el honor de poder ayudarte en tu crecimiento profesional y emocional.</div>
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end container --> 
      </section>
      <!-- end section-clients -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <section class="section_mod-b text-center">
              <h2 class="ui-title-block">Nuestro sistema de Tutoria te ayudará a crecer no solo profesionalmente sino <strong>también emocionalmente.</strong></h2>
              <div class="ui-subtitle-block ui-subtitle-block_mod-c">En INTRAVOLUCION, nos preocupamos por que nuestros estudiantes crezcan aún después de terminar cualquier curso.</div>
              <a class="btn btn-primary btn-effect" href="<?php echo base_url(); ?>contacto">CONTÁCTANOS</a> </section>
            <!-- end section-default --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
      
    </div>
    <!-- end main-content -->
    
 <?php $this->load->view('frontend/templates/footer_view.php'); ?>


</body>
</html>
