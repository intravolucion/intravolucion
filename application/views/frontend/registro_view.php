<?php $this->load->view('frontend/templates/head_view.php'); ?>
<body>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">
  <div id="wrapper">

    <!-- HEADER -->
    <?php $this->load->view('frontend/templates/header_view.php'); ?>
    <!-- end header -->
      <section class="registro text-center" style="padding: 1em 0">
          <div class="container">
            <h2 style="font-size: 30px;">Registro de Usuario</h2>
            <hr>
            <?php if(isset($mensaje) && count($mensaje) > 0): ?>
            <div class="alert alert-<?php echo $mensaje['type']; ?>">
                <?php echo $mensaje['text']; ?>
            </div>
            <?php endif; ?>
            <form action="<?php echo current_url(); ?>" method="post">
              <div class="form-group">
                <input type="text" class="form-control" name="nombres" placeholder="Nombre" required />
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="apellidos" placeholder="Apellido" required />
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="correo_electronico" placeholder="Correo" required />
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="telefono" placeholder="Teléfono" required />
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="numero_documento" placeholder="DNI" required />
              </div>
              <div class="form-group">
                <input type="password" class="form-control" name="contrasenia" placeholder="password" required />
              </div>
              <div class="form-group">
                <input type="password" class="form-control" name="repetir_contrasenia" placeholder="repite password" required />
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-info btn-effect">Registrar</button>
              </div>
            </form>
          </div>
      </section>
      <!-- end main-slider -->
      <?php $this->load->view('frontend/templates/cuenta/login_view.php'); ?>
      <!-- end container --> 
    </div>
    <!-- end main-content -->
    
 <?php $this->load->view('frontend/templates/footer_view.php'); ?>


</body>
</html>
