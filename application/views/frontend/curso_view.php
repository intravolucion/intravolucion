<?php $this->load->view('frontend/templates/head_view.php'); ?>

<style>
  .video-link:hover{
    text-decoration: none;
    font-family: Montserrat , arial;

  }
</style>

<body>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  

  <div id="wrapper"> 
    
    <!-- HEADER -->
    <?php $this->load->view('frontend/templates/header_view.php'); ?>
    <!-- end header -->
    
      <!-- <img src="<?php echo base_url(); ?>uploads/<?php echo $curso['banner']; ?>" alt="<?php echo $curso['titulo']; ?>" style="width:100%;" /> -->
    
    <?php if($curso['banner'] != ''): ?>
    <div class="wrap-title-page" style="padding-top: 100px;background-size: cover;background-position: center center; background-repeat: no-repeat;padding-bottom: 100px;background-image: url('<?php echo base_url(); ?>uploads/<?php echo $curso['banner']; ?>')">
    <?php endif; ?>
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="ui-title-page" style="text-shadow: 2px 2px 8px rgba(0,0,0,.5);"><?php echo $curso['titulo']; ?></h1>
          </div>
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="icon stroke icon-House"></i></a></li>
                <li><a href="javascript:void(0);">Categorias</a></li>
                <li><a href="javascript:void(0);">cursos</a></li>
                <li class="active"><?php echo $curso['titulo']; ?></li>
              </ol>
            </div>
          </div>
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </div>
    <!-- end section-breadcrumb -->
    
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <main class="main-content rtd">
            <article class="course-details">
              <h2 class="course-details__title"><?php echo $curso['titulo']; ?></h2>
              <img class="img-responsive" src="<?php echo base_url(); ?>uploads/<?php echo $curso['logo']; ?>" height="480" width="750" alt="foto">
              <h3 class="course-details__title-inner decor">Objetivo</h3>
              <p><?php echo nl2br($curso['resumen']); ?></p>
              <h3 class="course-details__title-inner decor">¿QUÉ APRENDERÁS?</h3>
              <p><?php echo $curso['aprenderas']; ?></p>
              <h3 class="course-details__title-inner decor">¿A QUIÉN VA DIRIGIDO?</h3>
              <p><?php echo $curso['dirigido']; ?></p>
              <h3 class="course-details__title-inner decor">Syllabus</h3>
              <?php foreach($semanas as $key => $value): ?>
              <?php $lecciones = $this->module_model->seleccionar('lecciones', array('estado' => 1, 'id_padre' => $value['id'])); ?>
              <h4 class="course-details__title-accordion">Unidad <?php echo ($key + 1); ?>: <?php echo $value['titulo']; ?></h4>
              <p><?php echo nl2br($value['resumen']); ?></p>
              <ul class="list-collapse list-unstyled">
                <?php foreach($lecciones as $k => $v): ?>
                <li class="list-collapse__item">
                  <div class="list-collapse__inner"> <i class="icon stroke icon-Files"></i> <span class="list-collapse__title"><?php echo $v['titulo']; ?></span>
                    <button class="list-collapse__btn" data-toggle="collapse" data-target="#content-<?php echo $value['id']; ?>-<?php echo $v['id']; ?>"><i class="fa fa-angle-down"></i></button>
                  </div>
                  <div class="list-collapse__content collapse" id="content-<?php echo $value['id']; ?>-<?php echo $v['id']; ?>"><?php echo nl2br($v['resumen']); ?></div>
                </li>
                <?php endforeach; ?>
              </ul>
              <?php endforeach; ?>
            </article>
            <!-- end course-details --> 
          </main>
          <!-- end main-content --> 
        </div>
        <!-- end col -->
        
        <div class="col-md-4">
          <aside class="sidebar">
            <div class="widget widget_course-description">
              <div class="block_content">
                <?php if(count($existe) == 0): ?>
                  <a href="<?php echo base_url(); ?>actualizar_carrito/<?php echo $curso['id']; ?>?cantidad=1" class="btn btn-primary btn-effect">AGREGAR AL CARRITO</a>
                <?php endif; ?>
                <ul class="list-information list-unstyled">
                  <li class="list-information__item"> <span class="list-information__title"><i class="icon stroke icon-Agenda"></i>Comienza</span> <span class="list-information__description"><?php echo MY_Controller::fecha_muestra($curso['fecha_inicio']); ?></span> </li>
                  <li class="list-information__item"> <span class="list-information__title"><i class="icon stroke icon-Glasses"></i>Duración</span> <span class="list-information__description"><?php echo count($semanas); ?> semana(s)</span> </li>
                  <!-- li class="list-information__item"> <span class="list-information__title"><i class="icon stroke icon-Edit"></i>Institución </span> <span class="list-information__description">ABC University</span> </li -->                 
                  <?php $cantidad_alumnos = $this->module_model->total('cursos_asignados', array('id_curso' => $curso['id'], 'estado' => 1)); ?>
                  <!-- <li class="list-information__item"> <span class="list-information__title"><i class="icon stroke icon-Users"></i>Alumnos</span> <span class="list-information__description"><?php echo $cantidad_alumnos; ?></span> </li> -->
                  
                  <!-- ASESORIA CONTINUA -->
                  <li class="list-information__item" style="padding: 10px 0;"> <span class="list-information__title" style="font-size: 26px;"><i class="icon stroke icon-Glasses" style="font-size: 26px;"></i>Asesoria Continua</span>  </li>
                  
                  <!-- START SWITCH CURRENCY -->
                  <div class="currency-swith">
                    <div class="currency-item currency-pen">
                      <a href="<?php echo current_url(); ?>?moneda=pen"><img src="http://intravolucion.com/uploads/flag_peru.png" alt=""></a>
                    </div>
                    <div class="currency-item currency-dol">
                      <a href="<?php echo current_url(); ?>?moneda=usd"><img src="http://intravolucion.com/uploads/flag_usa.png" alt=""></a>
                    </div>
                  </div>
                  <!-- ENDS SWITCH CURRENCY -->

                  <!-- PRECIO  -->
                  <li class="list-information__item"> 
                    <span class="list-information__title"><i class="icon stroke icon-Bag"></i>PRECIO</span> <span class="list-information__description"><span class="list-information__number"><?php if($curso['precio' . $this->moneda] > 0): ?><?php echo ($this->moneda == '_soles') ? 'S/' : 'US$'; ?> <?php echo $curso['precio' . $this->moneda]; ?><?php else: ?>GRATIS<?php endif; ?></span></span> 
                  </li>

                  
                
                </ul>
              </div>
              <!-- end block_content --> 
            </div>
            <!-- end widget_course-description -->
            
            <?php if($curso['youtube'] != ''): ?>
            <div class="widget widget_video" style="position: relative;">
              <div class="ver_video" style="position: absolute;width: 100%; height: 100%; background: rgba(0,0,0,.5);">
                <a class="video-link" href="https://www.youtube.com/watch?v=<?php echo $curso['youtube']; ?>&rel=0" rel="prettyPhoto" title="YouTube" style="width: 100%; height:100%; "><h4 style="text-transform: uppercase;margin-top: 5em;font-size: 25px; color: #fff;">Ver Video</h4></a>
              </div>
              <div class="block_content"> <a class="video-link" href="https://www.youtube.com/watch?v=<?php echo $curso['youtube']; ?>&rel=0" rel="prettyPhoto" title="YouTube"> <img class="img-responsive" src="https://img.youtube.com/vi/<?php echo $curso['youtube']; ?>/0.jpg" height="250" width="350" alt="video">
                <!-- div class="video-link__inner"> <i class="icon stroke icon-Next"></i> <span class="video-link__title">VIDEO INTRO</span> </div -->
                </a>
              </div>
              <!-- end block_content --> 
            </div>
            <?php endif; ?>
            <!-- end widget_video -->
            
            <section class="widget widget-default widget_instructor">
              <h3 class="widget-title ui-title-inner decor decor_mod-a">Instructor</h3>
              <div class="block_content">
                <div class="instructor__img"><img src="<?php echo base_url(); ?>uploads/<?php echo $profesor['imagen']; ?>" height="60" width="60" alt=""></div>
                <div class="instructor__inner">
                  <div class="instructor__name"><?php echo $profesor['nombres']; ?> <?php echo $profesor['apellidos']; ?></div>
                  <!-- div class="instructor__categories"></div -->
                </div>
              </div>
              <!-- end block_content --> 
            </section>
            <!-- end widget_instructor -->
            
            <section class="widget widget-default widget_social">
              <h3 class="widget-title ui-title-inner decor decor_mod-a">COMPARTIR ESTE CURSO</h3>
              <div class="block_content">
                <ul class="list-social list-inline">
                  <li>
                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url(); ?>"><i class="icon fa fa-facebook"></i></a>
                  </li>
                  <li>
                    <a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo current_url(); ?>"><i class="icon fa fa-twitter"></i></a>
                  </li>
                  <li>
                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo current_url(); ?>"><i class="icon fa fa-google-plus"></i></a>
                  </li>
                  <!-- li>
                    <a href="https://www.instagram.com/intravolucion/"><i class="icon fa fa-instagram"></i></a>
                  </li>
                  <li>
                    <a href="https://www.youtube.com/channel/UCkjYRD_jlBRoPO5FHnNcL7A"><i class="icon fa fa-youtube-play"></i></a>
                  </li -->
                </ul>
              </div>
              <!-- end block_content --> 
            </section>
            <!-- end widget_social -->
            
            <?php if(isset($cursos_relacionados) && count($cursos_relacionados) > 0): ?>
            <section class="widget widget-default widget_courses">
              <h3 class="widget-title ui-title-inner decor decor_mod-a">CURSOS RELACIONADOS</h3>
              <div class="block_content">
                <ul class="list-courses list-unstyled">
                  <?php foreach($cursos_relacionados as $key => $value): ?>
                  <?php $profesor = $this->module_model->buscar('administrador', $value['id_padre']); ?>
                  <?php if(count($profesor) > 0): ?>
	                  <li class="list-courses__item">
	                    <section>
	                      <div class="list-courses__img">
                        <a href="<?php echo base_url(); ?>curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>">
                          <img class="img-responsive" src="<?php echo base_url(); ?>uploads/100x100/<?php echo $value['logo']; ?>" height="90" width="90" alt="foto">
                        </a>
                        </div>
	                      <div class="list-courses__inner">
	                        <a href="<?php echo base_url(); ?>curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>">
                          <h4 class="list-courses__title">
                            <?php echo $value['titulo']; ?></h4>
                          </a>
	                        <div class="list-courses__meta"><?php echo @$profesor['nombres']; ?> <?php echo @$profesor['apellidos']; ?></div>
	                        <div class="list-courses__price">Precio: <?php echo ($this->moneda == '_soles') ? 'S/' : 'US$'; ?> <span class="list-courses__number"><?php echo $value['precio' . $this->moneda]; ?></span></div>
	                      </div>
	                    </section>
	                  </li>
	              <?php endif; ?>
                  <?php endforeach; ?>
                </ul>
              </div>
              <!-- end block_content --> 
            </section>
            <?php endif; ?>
            <!-- end widget_courses -->
            
            <section class="widget widget-default widget_categories">
              <h3 class="widget-title ui-title-inner decor decor_mod-a">CATEGORÍAS</h3>
              <div class="block_content">
                <ul class="list-categories list-unstyled">
                  <?php foreach($categorias as $key => $value): ?>
                    <?php $total = $this->module_model->total('cursos', array('categoria' => $value['id'], 'estado' => 1, 'activado' => 1)); ?>
                    <li class="list-categories__item"> <a class="list-categories__link" href="<?php echo base_url(); ?>categoria/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"> <span class="list-categories__name"><?php echo $value['titulo']; ?></span> <span class="list-categories__number"><?php echo $total; ?></span> </a> </li>
                  <?php endforeach; ?>
                </ul>
              </div>
              <!-- end block_content --> 
            </section>
            <!-- end widget_categories --> 
            
          </aside>
          <!-- end sidebar --> 
        </div>
        <!-- end col --> 
      </div>
      <!-- end row --> 
    </div>
    <!-- end container -->
    
 <?php $this->load->view('frontend/templates/footer_view.php'); ?>
</body>
</html>
