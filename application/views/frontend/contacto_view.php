<?php $this->load->view('frontend/templates/head_view.php'); ?>
<body>
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">
			<div id="wrapper">
				<?php $this->load->view('frontend/templates/header_view.php'); ?>
				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">contacto</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->
				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="<?php echo base_url(); ?>"><i class="icon stroke icon-House"></i></a></li>
										<li class="active">CONTACTO</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->
				<main class="main-content">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<section class="section_contacts">
									<h2 class="ui-title-inner decor decor_mod-a">Contáctanos</h2>
									<p>Comunícate con nosotros por teléfono, por correo electrónico o por cualquiera de nuestras redes sociales y con gusto te brindaremos la información que necesites.</p>
									<ul class="list-social list-inline">
										<li>
											<a href="https://www.facebook.com/intravolucion"><i class="icon fa fa-facebook"></i></a>
										</li>
										<li>
											<a href="https://twitter.com/intravolucion"><i class="icon fa fa-twitter"></i></a>
										</li>
										<li>
											<a href="https://plus.google.com/103289303330652925669"><i class="icon fa fa-google-plus"></i></a>
										</li>
										<li>
											<a href="https://www.instagram.com/intravolucion/"><i class="icon fa fa-instagram"></i></a>
										</li>
										<li>
											<a href="https://www.youtube.com/channel/UCkjYRD_jlBRoPO5FHnNcL7A"><i class="icon fa fa-youtube-play"></i></a>
										</li>
									</ul>
									<ul class="list-contacts list-unstyled">
										<li class="list-contacts__item">
											<i class="icon stroke icon-Phone2"></i>
											<div class="list-contacts__inner">
												<div class="list-contacts__title">TELÉFONO</div>
												<div class="list-contacts__info"><?php echo $this->configuracion['telefonos']; ?></div>
											</div>
										</li>
										<li class="list-contacts__item">
											<i class="icon stroke icon-Mail"></i>
											<div class="list-contacts__inner">
												<div class="list-contacts__title">EMAIL</div>
												<div class="list-contacts__info"><?php echo $this->configuracion['email_contacto']; ?></div>
											</div>
										</li>
										<li class="list-contacts__item">
											<i class="icon stroke icon-WorldWide"></i>
											<div class="list-contacts__inner">
												<div class="list-contacts__title">WEB</div>
												<div class="list-contacts__info"><?php echo base_url(); ?></div>
											</div>
										</li>
										<li class="list-contacts__item">
											<i class="icon stroke icon-House"></i>
											<div class="list-contacts__inner">
												<div class="list-contacts__title">UBICACIÓN</div>
												<div class="list-contacts__info"> <?php echo $this->configuracion['direccion'] ?></div>
											</div>
										</li>
									</ul>
								</section><!-- end section_contacts -->
							</div><!-- end col -->
							<div class="col-sm-8 col-md-6">
									<h2 class="ui-title-inner decor decor_mod-a">Envianos un mensaje</h2>
									<p>Escríbenos tus consultas, dudas o sugerencias directamente por aquí y nosotros con gusto te responderemos a tu correo.</p>
									<?php if(isset($mensaje) && count($mensaje) > 0): ?>
			                        <div class="alert alert-<?php echo $mensaje['type']; ?>">
			                            <?php echo $mensaje['text']; ?>
			                        </div>
			                        <?php endif; ?>
									<form class="form-contact ui-form" action="<?php echo current_url(); ?>" method="post">
										<div class="row">
											<div class="col-md-6">
												<input class="form-control" name="nombre" id="nombre" type="text" placeholder="Nombre" required />
											</div>
											<div class="col-md-6">
												<input class="form-control" type="email" name="correo_electronico" id="correo_electronico" placeholder="correo" required />
											</div>
											<div class="col-xs-12">
												<textarea class="form-control" name="mensaje" id="mensaje" required rows="11">Mensaje</textarea>
												<button type="submit" class="btn btn-primary btn-effect">ENVIAR</button>
											</div>
										</div>
									</form>
								</div>

							<!-- <div class="col-md-7">
								<div class="section_map">
									<h2 class="ui-title-inner decor decor_mod-a">NUESTRA OFICINA</h2>
									<img class="img-responsive" src="<?php echo base_view(); ?>img/map.jpg" height="505" width="670" alt="map">
								</div>
							</div> --><!-- end col -->
						</div><!-- end row -->
					</div><!-- end container -->


					<!--<section class="section_contacts-form">
						<div class="container">
							<div class="row">
								<div class="col-sm-8">
									<h2 class="ui-title-block">Envíanos <strong>un mensaje</strong></h2>
									<div class="wrap-subtitle">
										<div class="ui-subtitle-block ui-subtitle-block_w-line">Si necesitas más información o tienes alguna duda.</div>
									</div>
									<?php if(isset($mensaje) && count($mensaje) > 0): ?>
			                        <div class="alert alert-<?php echo $mensaje['type']; ?>">
			                            <?php echo $mensaje['text']; ?>
			                        </div>
			                        <?php endif; ?>
									<form class="form-contact ui-form" action="<?php echo current_url(); ?>" method="post">
										<div class="row">
											<div class="col-md-6">
												<input class="form-control" name="nombre" id="nombre" type="text" placeholder="Nombre" required />
											</div>
											<div class="col-md-6">
												<input class="form-control" type="email" name="correo_electronico" id="correo_electronico" placeholder="correo" required />
											</div>
											<div class="col-xs-12">
												<textarea class="form-control" name="mensaje" id="mensaje" required rows="11">Mensaje</textarea>
												<button type="submit" class="btn btn-primary btn-effect">ENVIAR</button>
											</div>
										</div>
									</form>
								</div>


								<div class="col-sm-4">
									<a href="javascript:void(0);" class="support">
										<img class="img-responsive" src="<?php echo base_view(); ?>media/support/1.jpg" height="248" width="330" alt="Foto">
										<div class="support__title"><i class="icon stroke icon-Headset"></i>pronto chat...</div>
									</a>
								</div>

							</div>
						</div>
					</section>-->

				</main><!-- end main-content -->
<?php $this->load->view('frontend/templates/footer_view.php'); ?>
</body>
</html>
