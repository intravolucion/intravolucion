<?php $this->load->view('frontend/templates/head_view.php'); ?>
<body>

<style>
  .lista{
    list-style: none;
  }

  .lista li{
    padding-left: 0;
  }

  .app-none{
    display: none;
  }

  .btn-enviar-form {
    display: none;
  }

  .visto{
    display: block;
  }
</style>
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">
  <div id="wrapper">

    <!-- HEADER -->
    <?php $this->load->view('frontend/templates/header_view.php'); ?>
    <!-- end header -->
    <section style="background: #E5E1E1; padding: 3em 0 5em 0;">
      <div class="container">
        <h2 style="font-size: 30px; color: #BB403D;">Carrito de Compras</h2>
        <p>Usted va a comprar los siguientes cursos:</p>            
        <div class="row">
               
          <div class="col-md-8">
            <!-- START SWITCH CURRENCY -->
       <h2>Selecciona la moneda de tu preferencia</h2>
        <div class="currency-swith-cart">
          <div class="currency-item currency-pen">
            <a href="<?php echo current_url(); ?>?moneda=pen"><img src="http://intravolucion.com/uploads/flag_peru.png" alt=""></a>
          </div>
          <div class="currency-item currency-dol">
            <a href="<?php echo current_url(); ?>?moneda=usd"><img src="http://intravolucion.com/uploads/flag_usa.png" alt=""></a>
          </div>
        </div>
        <!-- ENDS SWITCH CURRENCY -->  
            <?php $total = 0; ?>
            <table class="table table-striped" style="font-size: 18px;margin: 2em 0; color: #000;">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Curso</th>
                  <th>Cantidad</th>
                  <th>Precio</th>
                  <th>Subtotal</th>
                  <th>Acción</th>
                </tr>
              </thead>
              <tbody>
                <?php if(count($carrito) > 0): ?>
                  <?php $item = 1; ?>
                  <?php foreach($carrito as $key => $value): ?>
                  <?php $curso = $this->module_model->buscar('cursos', $key); ?>
                  <tr>
                    <td><?php echo $item; ?></td>
                    <td><?php echo $curso['titulo']; ?></td>
                    <td><?php echo $value; ?></td>
                    <td><?php echo number_format($curso['precio' . $this->moneda], 2); ?></td>
                    <td><?php echo number_format(($curso['precio' . $this->moneda] * $value), 2); ?></td>
                    <?php $total += ($curso['precio' . $this->moneda] * $value); ?>
                    <td><a href="<?php echo base_url(); ?>actualizar_carrito/<?php echo $key; ?>?cantidad=0"><i class="fa fa-times" aria-hidden="true" style="color: #BB403D;"></i></a></td>
                  </tr>
                  <?php $item++; ?>
                  <?php endforeach; ?>
                <?php else: ?>
                  <tr>
                    <td scope="row" colspan="6">No hay productos en el carrito de compras</td>
                  </tr>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-4" style="padding-top: 4em;">
            <?php if(MY_Controller::mostrar_session('nivel') != ''): ?>
              <?php if(count($carrito) > 0): ?>
              <form action="<?php echo base_url(); ?>pagar" id="form-pay" method="POST">
                <input type="hidden" name="token" id="token" />
                <?php if(MY_Controller::mostrar_session('ver_como') != 'alumno'): ?>
                  <?php if($total > 0): ?>
                    <h3 style="font-size: 25px;">Total de Pedido</h3>
                    <label style="font-size: 33px; color: #000; font-weight: bold;"><?php echo ($this->moneda == '_soles') ? 'S/' : 'US$'; ?> <?php echo number_format($total, 2); ?></label>
                    <h5>Tipo de Pago</h5>
                    <label><input type="radio" name="tipo_pago" class="tipo_pago" value="1" required /> Depósito en Banco</label><br />
                    <label><input type="radio" name="tipo_pago" class="tipo_pago" value="2" required /> Tarjeta</label><br /><br />
                    <div class="app-btn-comprar">
                      <input type="button" id="comprar" value="Comprar" class="btn btn-info" style="font-size: 18px;" />
                      <button style="display:none;" class="btn btn-info" type="button" id="mostrar_modal" data-toggle="modal" data-target="#myModal">POPUP</button>
                    </div>
                  <?php else: ?>
                    <input type="hidden" name="tipo_pago" value="0" />
                    <div class="app-btn-comprar">
                      <input type="submit" value="Suscribirse" class="btn btn-info" style="font-size: 18px;" />
                    </div>
                  <?php endif; ?>
                <?php else: ?>
                  <input type="hidden" name="tipo_pago" value="0" />
                  <div class="app-btn-comprar">
                    <input type="submit" value="Suscribirse" class="btn btn-info" style="font-size: 18px;" />
                  </div>
                <?php endif; ?>
              </form>
              <?php endif; ?>
            <?php else: ?>
              <div class="alert alert-warning">Para continuar con la compra. Por favor, <a href="<?php echo backend_url(); ?>">inicia sesión</a> o <a href="<?php echo base_url(); ?>registro">registrate</a>.</div>
            <?php endif; ?>
          </div>
        </div>
      </div>


      <!-- inicio de popup -->
      
      <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="color: #BB403D;">Indicaciones</h4>
      </div>
      <div class="modal-body" style="overflow: hidden;">

      <!-- inicio del slider -->
        <div class="app-indicaciones text-center paso1 animated">
          <h4>Paso I</h4>
          <div class="app-imagen-paso text-center">
            <img src="<?php echo base_view(); ?>img/icono-dos.png" alt="paso1" style="width: 115px;">
            <p>Depositar o transferir a la cuenta de Interbank el monto total del curso. A continuación te mostramos los datos para que puedas realizar el pago en efectivo desde cualquier parte</p>
          </div>
          <div class="app-contenido-paso">
            <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    1.  Desde el Perú:</a>
                  </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Haro & Macciotta Asociados S.A.C <br>RUC 20601586283</p>
                    <div class="row">
                      <div class="col-md-6">
                        <p>Entidad: Interbank <br>Moneda: Dólares <br>Cuenta N°: 045-3094700702 <br>CCI: 003-045-013094700702-22</p>
                      </div>
                      <div class="col-md-6">
                        <p>Entidad: Interbank <br>Moneda: Soles <br>Cuenta N°: 045-300128092-3 <br>CCI: 003-045-003001280923-29</p>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    2.  Desde el extranjero:</a>
                  </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Entidad: Banco Internacional del Perú – Interbank <br>Código Swift: BINPPEPL <br> Moneda: Dólares <br>Cuenta N°: 045-3094700702 <br> Nombre de la empresa: Haro & Macciotta Asociados S.A.C <br> Domicilio: San Borja, Lima, Perú. </p>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          <div class="app-boton">
            <button type="button" id="btn-sgt-p1" class="btn btn-info" >Siguiente</button>
            
          </div>
        </div>

        <div class="app-none app-indicaciones text-center paso2 animated">
          <h4>Paso II</h4>
          <div class="app-imagen-paso text-center">
            <img src="<?php echo base_view(); ?>img/enviar.ico" alt="paso1" style="width: 115px;">
            <p>Enviar la constancia del depósito o transferencia junto con tu usuario en INTRAVOLUCION a cualquiera de los siguientes medios</p>
          </div>
          <div class="app-contenido-paso">
            <ul class="lista">
              <li><i class="fa fa-check"></i> Correo electrónico: ventas@intravolucion.com</li>
              <li><i class="fa fa-check"></i> Whatsapp: +51 970 381 122</li>
              <li><i class="fa fa-check"></i> Llamar: +51 970 381 122</li>
            </ul>
          </div>
          <div class="app-botones">
            <button type="button" id="btn-sgt-p2" class="btn btn-info">Siguiente</button>
            <button type="button" id="btn-reg-p2" class="btn btn-warning">Regresar</button>
          </div>
        </div>

        <div class="app-none app-indicaciones text-center paso3 animated">
          <h4>Paso III</h4>
          <div class="app-imagen-paso text-center">
            <img src="<?php echo base_view(); ?>img/recibir.png" alt="paso1" style="width: 115px;">
            <p>Una vez confirmado el pago, recibirás un correo de confirmación y habilitación del curso en tu perfil de usuario en INTRAVOLUCION</p>
          </div>
          <div class="app-botones">
            <button type="button" id="btn-sgt-p3" class="btn btn-info" >Siguiente</button>
            <button type="button" id="btn-reg-p3" class="btn btn-warning" >Regresar</button>
          </div>
        </div>

        <div class="app-none app-indicaciones text-center paso4 animated">
          <h4>Paso IV</h4>
          <div class="app-imagen-paso text-center">
            <img src="<?php echo base_view(); ?>img/aprender.png" alt="paso1" style="width: 115px;">
            <p>Disfruta de tu curso y empieza a INTRAVOLUCIONARTE!.</p>
          </div>
          <div class="app-botones">
           
            <button type="button" id="btn-reg-p4" class="btn btn-warning">Regresar</button>
          </div>
        </div>

          

        <!-- fin del slider -->

      </div>
      <div class="modal-footer">
        <button type="button" id="enviar_formulario" class="btn btn-primary btn-enviar-form animated" data-dismiss="modal" style="text-align: right;">Continuar</button>
      </div>
    </div>
  </div>
</div>

      <!-- fin de popup -->


</body>
</html>



    </section>
    
 <?php $this->load->view('frontend/templates/footer_view.php'); ?>

<?php if(MY_Controller::mostrar_session('correo_electronico') != ''): ?>
<!-- PASARELLA DE PAGOS CULQI -->
<!-- PASARELLA DE PAGOS CULQI -->
<script src="https://checkout.culqi.com/v2"></script>

<script>
    Culqi.publicKey = '<?php echo $this->comercio; ?>';
    Culqi.settings({
        title: '<?php echo $this->configuracion['titulo']; ?>',
        currency: '<?php echo (($this->moneda == '_soles') ? 'PEN' : 'USD'); ?>',
        description: 'Compra de Productos',
        amount: <?php echo $total * 100; ?>
    });
</script>

<script>  
  // Ejemplo: Tratando respuesta con AJAX (jQuery)
  function culqi()
  {
      if(Culqi.error)
      {
         // Mostramos JSON de objeto error en consola
         console.log(Culqi.error);
         alert(Culqi.error.mensaje);
      }
      else
      {
        var token = Culqi.token.id;

        $('#token').val(token); $('#token').attr('value', token);

        setTimeout(function() {
          $('#form-pay').submit();
        }, 150);
    }
  };
</script> 
<?php endif; ?>

<!-- ALERTS -->
<link href="<?php echo backend_view(); ?>css/alerts.css" rel="stylesheet">
<script src="<?php echo backend_view(); ?>js/alerts.js"></script>

<script>
    var nivel = parseInt('<?php echo MY_Controller::mostrar_session('nivel'); ?>');

    $('#comprar').on('click', function (e) {
        var tipo_pago = $('.tipo_pago:checked').attr('value');
        // Abre el formulario con las opciones de Culqi.configurar
        if(nivel > 0)
        {
          if(nivel == 3)
          {
            if(tipo_pago != undefined)
            {
              if(tipo_pago == 2)
              {
                Culqi.open(); e.preventDefault(); return false;
              }
              else
              {
                $('#mostrar_modal').trigger('click');
                e.preventDefault(); return false;
              }
            }
            else
            {
              jAlert("Seleccione un tipo de pago para continuar.", "Error de Tipo de Pago");
            }
          }
          else
          {
            jAlert("Como profesor no puedes comprar un curso. Por favor, inicia tu cuenta de alumno.", "Error de Autentificación");
          }
        }
        else
        {
          jAlert("Por favor, identifícate para poder continuar con la compra.", "Error de Identificación");
        }
    });

    $('#enviar_formulario').click(function(e) {
      $('#form-pay').submit();
    });
</script>
<script>
//siguiente - paso 2
$('#btn-sgt-p1').on('click', function(){
  $('.paso1').addClass('app-none');
  $('.paso2').removeClass('app-none');
  $('.paso2').addClass('slideInRight');
});

$('#btn-reg-p2').on('click', function(){
  $('.paso1').removeClass('app-none');
  $('.paso2').addClass('app-none');
  $('.paso1').addClass('slideInLeft');
});

//siguiente - paso 3
$('#btn-sgt-p2').on('click', function(){
  $('.paso2').addClass('app-none');
  $('.paso3').removeClass('app-none');
  $('.paso3').addClass('slideInRight');
});

$('#btn-reg-p3').on('click', function(){
  $('.paso2').removeClass('app-none');
  $('.paso3').addClass('app-none');
  $('.paso2').addClass('slideInLeft');
});

//siguiente - paso 3
$('#btn-sgt-p3').on('click', function(){
  $('.paso3').addClass('app-none');
  $('.paso4').removeClass('app-none');
  $('.paso4').addClass('slideInRight');
  $('.btn-enviar-form').css('display', 'block');
});

$('#btn-reg-p4').on('click', function(){
  $('.paso3').removeClass('app-none');
  $('.paso4').addClass('app-none');
  $('.paso3').addClass('slideInLeft');
});

  
</script>

</body>
</html>
