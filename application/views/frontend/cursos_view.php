<?php $this->load->view('frontend/templates/head_view.php'); ?>

<body>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  

  
  <div id="wrapper"> 
    
    <!-- HEADER -->

      
   
    <?php $this->load->view('frontend/templates/header_view.php'); ?>
    <!-- end header -->
    
    <div class="wrap-title-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="ui-title-page"><?php echo (isset($titulo)) ? $titulo : 'Todos los Cursos'; ?></h1>
          </div>
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="icon stroke icon-House"></i></a></li>
                <li><a href="<?php echo base_url(); ?>categorias">Categorias</a></li>
                <li class="active"><?php echo (isset($titulo)) ? 'Todas las Asesorías' : 'Todos los Cursos'; ?></li>
              </ol>
              <div class="sorting">
              	<?php if(!isset($titulo)): ?>
	                <div class="select jelect">
	                  <input id="category" name="category" value="0" data-text="imagemin" type="text" class="jelect-input">
	                  <div tabindex="0" role="button" class="jelect-current">Categoría</div>
	                  <ul class="jelect-options">
	                    <?php foreach($this->categorias as $key => $value): ?>
	                    <li class="jelect-option<?php echo ($value['id'] == (int) @$_GET['categoria']) ? ' jelect-option_state_active' : '' ?>" onclick="javascript:redireccionar('categoria=<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>', this);"><?php echo $value['titulo']; ?></li>
	                    <?php endforeach; ?>
	                  </ul>
	                </div>
	            <?php endif; ?>
                <div  class="select jelect">
                  <input id="sort" name="sorting" value="0" data-text="imagemin" type="text" class="jelect-input">
                  <div tabindex="0" role="button" class="jelect-current">Ordenar</div>
                  <ul class="jelect-options">
                    <li class="jelect-option<?php echo (@$_GET['value'] == 'asc' && @$_GET['order_by'] == 'titulo') ? ' jelect-option_state_active' : '' ?>" onclick="javascript:redireccionar('order_by=titulo&value=asc', this);">A-Z</li>
                    <li class="jelect-option<?php echo (@$_GET['value'] == 'desc' && @$_GET['order_by'] == 'titulo') ? ' jelect-option_state_active' : '' ?>" onclick="javascript:redireccionar('order_by=titulo&value=desc', this);">Z-A</li>
                  </ul>
                </div>
              </div>
              <!-- end sorting --> 
            </div>
          </div>
        </div>
        <!-- end row--> 
      </div>
      <!-- end container--> 
    </div>
    <!-- end section-breadcrumb-->
    
    <main class="main-content">
      <div class="container">
        <div class="row">
          <?php if(isset($cursos) && count($cursos) > 0): ?>
          <div class="col-sm-12">
            <div class="wrap-title wrap-title_mod-b">
              <div class="title-list">ver <span class="title-list__number"><?php echo ($pagina > 0) ? $pagina_actual * $pagina : 1; ?> - <?php echo ($links > ($por_pagina + ($pagina_actual * $pagina))) ? ($por_pagina + ($pagina_actual * $pagina)) : $links; ?></span> de un total de <span class="title-list__number"><?php echo ($links); ?></span> Curso(s)</div>
            </div>
            <!-- end wrap-title -->
            
            <div class="posts-wrap">
              <?php foreach($cursos as $key => $value): ?>
              <?php $profesor = $this->module_model->buscar('administrador', $value['id_padre']); ?>
              <?php if(count($profesor) > 0): ?>
              <article class="post post_mod-c clearfix">
                <div class="entry-media">
                  <div class="entry-thumbnail"> <a href="<?php echo base_url(); ?>curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" ><img class="img-responsive" src="<?php echo base_url(); ?>uploads/370x250/<?php echo $value['logo']; ?>" width="370" height="250" alt="Foto"/></a> </div>
                  <div class="entry-hover"><a href="<?php echo base_url(); ?>curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" class="post-btn btn btn-primary btn-effect">Ver Curso</a></div>
                </div>
                <div class="entry-main entry-main_mod-a" style="height: 300px;">
                  <h3 class="entry-title ui-title-inner"><a href="<?php echo base_url(); ?>curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><?php echo $value['titulo']; ?></a></h3>
                  <div class="entry-meta decor decor_mod-b"> <span class="entry-autor"> <span>Por </span> <a class="post-link" href="javascript:void(0);"><?php echo $profesor['nombres']; ?> <?php echo $profesor['apellidos']; ?></a> </span> <span class="entry-date"><a href="javascript:void(0);"><?php echo MY_Controller::fecha_muestra($value['fecha_inicio']); ?></a></span> </div>
                  <div class="entry-content" style="height: auto; overflow: hidden;">
                    <p><?php echo nl2br($value['resumen']); ?> <a href="<?php echo base_url(); ?>curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><span> ... </span></a></p>
                  </div>
                  <!-- div class="entry-footer">
                    <div class="box-comments"> <a href="<?php echo base_url(); ?>curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><i class="icon stroke icon-Message"></i>25</a> <a href="javascript:void(0);"><i class="icon stroke icon-User"></i>65</a> </div>
                    <ul class="rating">
                      <li><i class="icon fa fa-star"></i></li>
                      <li><i class="icon fa fa-star"></i></li>
                      <li><i class="icon fa fa-star"></i></li>
                      <li><i class="icon fa fa-star"></i></li>
                      <li><i class="icon fa fa-star-o"></i></li>
                    </ul>
                  </div -->
                </div>
              </article>
              <?php endif; ?>
              <?php endforeach; ?>
              <!-- end post --> 
            </div>
            <!-- end posts-wrap --> 
          </div>
          <?php else: ?>
            <div class="col-sm-12">
              <div class="alert alert-warning">No se han encontrado <?php echo (isset($titulo)) ? 'asesorías' : 'cursos'; ?> para mostrar. Por favor, intente con otro filtro de búsqueda y/o categoría.</div>
            </div>
          <?php endif; ?>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
      
    </main>
    <!-- end main-content -->
    
    <?php $this->load->view('frontend/templates/footer_view.php'); ?>

</body>
</html>
