<?php $this->load->view('frontend/templates/head_view.php'); ?>
<body>
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">
			<div id="wrapper">
				<?php $this->load->view('frontend/templates/header_view.php'); ?>
				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">Categorias</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->
				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="<?php echo base_url(); ?>"><i class="icon stroke icon-House"></i></a></li>
										<li class="active">Categorias</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->
				<main class="main-content">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="wrap-title wrap-title_mod-b">
									<div class="ui-subtitle-block">Tenemos muchos cursos de donde elegir, separados por categorias.</div>
								</div><!-- end wrap-title -->
								<div class="posts-wrap">

									<?php foreach($categorias as $key => $value): ?>
									<article class="post post_mod-h clearfix">
										<div class="entry-media">
											<div class="entry-thumbnail">
												<a href="<?php echo base_url(); ?>categoria/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" ><img class="img-responsive" src="<?php echo base_url(); ?>uploads/<?php echo $value['imagen']; ?>" width="370" height="250" alt="Foto"/></a>
											</div>
										</div><!-- end entry-media -->

										<div class="entry-main">
											<h3 class="entry-title ui-title-inner"><a href="<?php echo base_url(); ?>categoria/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><?php echo $value['titulo']; ?></a></h3>

											<div class="entry-content decor decor_mod-b">
												<p><?php echo nl2br($value['resumen']); ?></p>
											</div><!-- end entry-content -->

											<?php $total = $this->module_model->total('cursos', array('categoria' => $value['id'])); ?>

											<div class="entry-links">
												<i class="icon stroke icon-Message"></i>total de cursos <a class="post-link" href="javascript:void(0);"><?php echo $total; ?></a>
											</div>
											<div class="entry-footer">
												<a href="<?php echo base_url(); ?>categoria/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" class="post-btn btn btn-primary btn-effect">VER CURSOS</a>
											</div><!-- end entry-footer -->
										</div><!-- end entry-main -->
									</article><!-- end post -->
									<?php endforeach; ?>

								</div><!-- end posts-wrap -->
							</div><!-- end col -->
						</div><!-- end row -->
					</div><!-- end container -->
				</main><!-- end main-content -->
<?php $this->load->view('frontend/templates/footer_view.php'); ?>
</body>
</html>

