<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="<?php echo base_view(); ?>css/curso.css" rel="stylesheet" />
<body>
<!-- Fixed navbar -->
<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
<div class="open-sidebar app-resp-open" data-toggle="tooltip" data-placement="right" title="Menú">
	<div class="app-btn-open-sidebar">
		<i class="glyphicon glyphicon-align-left icono-fixed-open icono"></i>
	</div>
</div>
<div id="wrapper">
  	<?php $this->load->view("frontend/sistema/templates/sidebar_curso_view"); ?>
        <div id="main-wrapper" class="col-md-10 pull-right col-xs-12">
            <div id="main">
              <div class="page-header text-center">
                <h3 style="font-size: 33px; margin-bottom: .5em;"><?php echo $curso['titulo']; ?></h3>
                <p>por <?php echo $profesor['nombres']; ?> <?php echo $profesor['apellidos']; ?></p>
              </div>

              <div class="contenido" style="padding: 2em;">
              	<div class="app-profe-detalle sombra row" style="padding: 1em;">
              		<div class="col-md-2 text-center">
              			<img src="<?php echo base_url(); ?>uploads/100x100/<?php echo $profesor['imagen']; ?>" alt="" />
              			<p class="mb0">Prof. <?php echo $profesor['apellidos']; ?></p>
              		</div>
              		<div class="col-md-10">
              			<p><?php echo nl2br($profesor['acerca_de']); ?></p>
              			<div class="redes-sociales" style="margin-top: 1em">
					  		<ul class="app-redes-inst pull-right">
						  		<?php if($profesor['facebook'] != ''): ?><li><a target="_blank" href="<?php echo $profesor['facebook']; ?>" class="btn btn-white"><i class="fa fa-facebook"></i></a></li><?php endif; ?>
						  		<?php if($profesor['twitter'] != ''): ?><li><a target="_blank" href="<?php echo $profesor['twitter']; ?>" class="btn btn-white"><i class="fa fa-twitter"></i></a></li><?php endif; ?>
						  		<?php if($profesor['linkedin'] != ''): ?><li><a target="_blank" href="<?php echo $profesor['linkedin']; ?>" class="btn btn-white"><i class="fa fa-linkedin"></i></a></li><?php endif; ?>
						  	</ul>
					  	</div>
              		</div>
              	</div>
              </div>

              <?php if(count($calificaciones) > 0): ?>
				  <div class="col-md-12 text-right">
				  	<a href="<?php echo base_url(); ?>sys_exportar_notas/<?php echo $curso['id']; ?>/<?php echo MY_Controller::mostrar_session('id'); ?>" class="btn btn-primary mb1em">Exportar Notas</a>
				  </div>
			  <?php endif; ?>

				<div class="col-md-12">
		   			<div class="panel bg-morado p-sombra">
					  <!-- Default panel contents -->

					  <div class="panel-heading cb">Exámenes</div>
					  <!-- List group -->
					  <ul class="list-group">
					  	<?php foreach($calificaciones as $key => $value): ?>
						  	<?php $test = $this->module_model->buscar('tests', $value['id_padre']); $semana = $this->module_model->buscar('semanas', $test['id_padre']); $curso = $this->module_model->buscar('cursos', $semana['id_padre']); ?>
						  	<?php $nota = ($test['tipo'] == 0) ? ($value['correctas'] / ($value['correctas'] + $value['incorrectas']) * 20) : $value['nota']; ?>
							<?php if((($nota > 0 AND $test['tipo'] == 1) OR $test['tipo'] == 0) AND count($curso) > 0): ?>
						    	<li class="list-group-item"><?php echo $test['titulo']; ?> - <?php echo $curso['titulo']; ?> <span class="badge <?php echo ($nota > 13) ? 'success' : 'danger'; ?>"><?php echo $nota; ?></span></li>
						    <?php endif; ?>
						<?php endforeach; ?>
					  </ul>
					</div>
		   		</div>
            </div>
        </div>
</div>

<!-- SCRIPTS --> 
<script src="<?php echo base_view(); ?>js/jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_view(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_view(); ?>js/modernizr.custom.js"></script>

<!-- time line -->
<script src="<?php echo base_view(); ?>plugins/timeline/js/jquery.mobile.custom.min.js"></script>
<script src="<?php echo base_view(); ?>plugins/timeline/js/main.js"></script> <!-- Resource jQuery -->

<!-- *********** -->

<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

//mostrar sidebar

$('.app-resp-open').click(function(){
	$('#sidebar-wrapper').css('display','block');
	$('#sidebar-wrapper').removeClass('slideOutLeft');
	$('#sidebar-wrapper').addClass('slideInLeft');
});
//ocultar sidebar
$('#app-resp-close').click(function(){
	$('#sidebar-wrapper').removeClass('slideInRight');
	$('#sidebar-wrapper').addClass('slideOutLeft');
});

</script>
</body>
</html>
