<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
		<div class="container contenedor-respo">
    <div class="row profile">
		<div class="col-md-3 col-sm-4 col-xs-12">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
            <div class="profile-content">
			   	<div class="row">
			   		<div class="col-md-12 text-center" style="background: url('<?php echo base_url(); ?>uploads/<?php echo $curso['banner']; ?>'); padding: 2em 1em;">
			   			<h2 class="cb"><img src="<?php echo base_url(); ?>uploads/<?php echo $curso['logo']; ?>" alt="" style="width: 160px;"></h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Home</strong></a></li>
						  <li><a href="<?php echo base_url(); ?>sys_cursos">Cursos</a></li>
						  <li class="active"><?php echo $curso['titulo']; ?></li>
						</ol>
			   		</div>
					<div class="col-md-12">
						<div class="app-content-video" style="margin-top: 1.5em;">
							<div class="panel panel-default">
							  <div class="panel-heading">
							  	<div class="row">
							  		<div class="col-md-4" style="text-align: left;"></div>
							  		<div class="col-md-4" style="text-align: center;"><h3 class="panel-title"><?php echo $leccion['titulo']; ?></h3></div>
							  		<div class="col-md-4" style="text-align: right;"><a href="<?php echo base_url(); ?>sys_curso_editar/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>">Curso <i class="glyphicon glyphicon-pencil"></i></a></div>
							  	</div>
							  </div>
							  <div class="panel-body">
							  	<?php if($leccion['tipo'] == 'contenido'): ?>
								<p style="text-align: justify;"><?php echo nl2br($leccion['contenido']); ?></p>
								<?php endif; ?>
							  	<?php if($leccion['tipo'] == 'video'): ?>
							    <iframe width="100%" height="400" src="https://www.youtube.com/embed/<?php echo $leccion['video']; ?>" frameborder="0" allowfullscreen></iframe>
								<?php endif; ?>
								<?php if($leccion['tipo'] == 'imagen'): ?>
									<img src="<?php echo base_url(); ?>uploads/<?php echo $leccion['imagen']; ?>" class="img-responsive" alt="<?php echo $leccion['titulo']; ?>" />
								<?php endif; ?>
								<?php if($leccion['tipo'] == 'archivo'): ?>
									<iframe src="https://docs.google.com/viewer?url=<?php echo base_url(); ?>uploads/<?php echo $leccion['archivo']; ?>&embedded=true" style="height:550px;width:100%;border:0px;frameborder:0px;"></iframe>
								<?php endif; ?>
							  </div>
							</div>
						</div>
					</div>
					
			   		<div class="col-md-7">
						<div class="panel panel-info">
						  <!-- Default panel contents -->
						  <div class="panel-heading">Temas</div>
						  
						  <!-- List group -->
						  <ul class="list-group">
						    <?php foreach($semanas as $key => $value): ?>
								<?php foreach($lecciones[$key] as $k => $v): ?>
									<li class="list-group-item"><a href="<?php echo base_url(); ?>sys_curso/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>/<?php echo $v['id']; ?>-<?php echo $v['alias']; ?>" class="text-right"><?php echo $v['titulo']; ?></a> <span class="badge">Duración: <?php echo $v['duracion']; ?></span></li>
								<?php endforeach; ?>
							<?php endforeach; ?>
						  </ul>
						</div>
			   		</div>

			   		<div class="col-md-5">
	          			<div class="col-md-12">
		          			<h3>Discución</h3>
		          		</div>
		          		<div class="col-md-12 pl0 pr0">
		          			<div class="panel panel-default">
								<div class="panel-body" id="discusion" style="padding:7px; height:350px; overflow:scroll;">
									<?php foreach($discusion as $key => $value): ?>
									<?php $emisor = $this->module_model->buscar('administrador', $value['id_emisor']); ?>
									<div class="panel panel-default <?php echo ($emisor['id'] == MY_Controller::mostrar_session('id')) ? 'pull-right' : 'pull-left'; ?>">
									  <div class="panel-body" style="padding: 7px;">
									    <h4 class="mt0 mb0"><?php echo $emisor['nombres']; ?> <?php echo $emisor['apellidos']; ?></h4>
									    <p class="mt0 mb0"><?php echo $value['contenido']; ?></p>
									  </div>
									</div>
									<br class="clearfix" style="clear:both;width:100%;" />
									<?php endforeach; ?>
								</div>
								<iframe name="_oculto" style="display:none;"></iframe>
								<div class="app-cont-chat">
						    		<div class="col-md-12 pl0 pr0">
						    			<form method="POST" action="<?php echo current_url(); ?>" target="_oculto" autocomplete="off">
							    			<div class="input-group">
										      <span class="input-group-btn">
										        <button class="btn btn-default" type="submit">Enviar</button>
										      </span>
										      <input type="hidden" id="_time" name="time" value="<?php echo $time; ?>" />
										      <input type="text" id="contenido" class="form-control" placeholder="Escribe un mensaje.." name="contenido" required autocomplete="off" />
										    </div>
										</form>
						    		</div>
						    	</div>
							</div>
						</div>
					</div>

			   		<div class="col-md-12">
						<div class="panel panel-info">
						  <!-- Default panel contents -->
						  <div class="panel-heading">Calificaciones Pendientes</div>
						  <!-- List group -->
						  <ul class="list-group">
						  	<?php foreach($tests as $key => $value): ?>
						  		<?php $calificaciones = $this->module_model->seleccionar('calificaciones', array('id_padre' => $value['id'], 'nota' => 0)); ?>
						  		<?php foreach($calificaciones as $k => $v): ?>
						  			<?php $alumno = $this->module_model->buscar('administrador', $v['id_usuario']); ?>
									<li class="list-group-item"><a href="<?php echo base_url(); ?>sys_examen_revisar/<?php echo $v['id']; ?>/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>/<?php echo $value['id']; ?>" class="text-right"><?php echo $value['titulo']; ?> - <?php echo $alumno['nombres']; ?> <?php echo $alumno['apellidos']; ?></a> <span class="badge">Duración: <?php echo $value['duracion']; ?></span></li>
								<?php endforeach; ?>
							<?php endforeach; ?>
						  </ul>
						</div>
			   		</div>
			   		
			   	</div>
            </div>
		</div>
	</div>
</div>
</div>
</div>
<!-- SCRIPTS --> 
<script src="<?php echo base_view(); ?>js/jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_view(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_view(); ?>js/modernizr.custom.js"></script>

<script>
var time = '<?php echo $time; ?>'; var emisor = '<?php echo MY_Controller::mostrar_session('id'); ?>'; var usuario = '<?php echo MY_Controller::mostrar_session('nombres'); ?> <?php echo MY_Controller::mostrar_session('apellidos'); ?>';

$(function () {
  $("#discusion").scrollTop($("#discusion")[0].scrollHeight);
});

function imprimir(data)
{
	time = data['time']; $('#_time').val(time);

	if(data != undefined && (data['discusion'] != undefined || data['contenido'] != undefined))
	{
		$('#contenido').val(''); $('#contenido').attr('value', '');

		if(data['discusion'] != undefined)
		{
			$.ajax({
				url: '<?php echo base_url(); ?>sys_update/' + <?php echo $leccion['id']; ?> + '/' + time,
				type: 'POST',
				dataType: 'json',
				success: function(data)
				{
					$.each(data['discusion'], function(i, item){

						var html = ''; var xnEmisor = data['emisor'][i];

						html += '<div class="panel panel-default ' + ((xnEmisor['id'] == emisor) ? 'pull-right' : 'pull-left') + '">';
						html += '<div class="panel-body" style="padding: 7px;">';
						html += '<h4 class="mt0 mb0">' + xnEmisor['nombres'] + ' ' + ((xnEmisor['apellidos'] != '' && xnEmisor['apellidos'] != null) ? xnEmisor['apellidos'] : '') + '</h4>';
						html += '<p class="mt0 mb0">' + item['contenido'] + '</p>';
						html += '</div>';
						html += '</div>';
						html += '<br class="clearfix" style="clear:both;width:100%;" />';

						$('#discusion').append(html); $("#discusion").scrollTop($("#discusion")[0].scrollHeight);

						time = data['time'];

						setTimeout(function(){
							imprimir({'time': time});
						}, 3000);
					});
				},
				error: function(data)
				{
					console.log(data);
				}
			});
		}
		else
		{
			/*
			var html = '';

			html += '<div class="panel panel-default pull-right">';
			html += '<div class="panel-body" style="padding: 7px;">';
			html += '<h4 class="mt0 mb0">' + usuario + '</h4>';
			html += '<p class="mt0 mb0">' + data['contenido'] + '</p>';
			html += '</div>';
			html += '</div>';
			html += '<br class="clearfix" style="clear:both;width:100%;" />';

			$('#discusion').append(html); $("#discusion").scrollTop($("#discusion")[0].scrollHeight);
			*/

			// time = data['time'];
		}
	}
	else
	{
		$.ajax({
			url: '<?php echo base_url(); ?>sys_update/' + <?php echo $leccion['id']; ?> + '/' + time,
			type: 'POST',
			dataType: 'json',
			success: function(data)
			{
				$.each(data['discusion'], function(i, item){

					var html = ''; var xnEmisor = data['emisor'][i];

					html += '<div class="panel panel-default ' + ((xnEmisor['id'] == emisor) ? 'pull-right' : 'pull-left') + '">';
					html += '<div class="panel-body" style="padding: 7px;">';
					html += '<h4 class="mt0 mb0">' + xnEmisor['nombres'] + ' ' + ((xnEmisor['apellidos'] != '' && xnEmisor['apellidos'] != null) ? xnEmisor['apellidos'] : '') + '</h4>';
					html += '<p class="mt0 mb0">' + item['contenido'] + '</p>';
					html += '</div>';
					html += '</div>';
					html += '<br class="clearfix" style="clear:both;width:100%;" />';

					$('#discusion').append(html); $("#discusion").scrollTop($("#discusion")[0].scrollHeight);
				});

				time = data['time'];

				setTimeout(function(){
					imprimir({'time': time});
				}, 3000);
			},
			error: function(data)
			{
				console.log(data);
			}
		});
	}
}

setTimeout(function(){
	imprimir({'time': time});
}, 1000);
</script>

</body>
</html>