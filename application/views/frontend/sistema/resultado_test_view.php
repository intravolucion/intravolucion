<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<link href="<?php echo base_view(); ?>css/curso.css" rel="stylesheet" />
<body style="padding-top: 0!important;">
<!-- Fixed navbar -->
    <?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
<div id="wrapper" class="row">
  	<div id="sidebar-wrapper" class="col-md-2 col-xs-8 animated" >
  		<div style="position: absolute;top: 5em;right: 8px;">
        	<span id="app-resp-close" class="glyphicon glyphicon-remove-sign" style="display: block;"></span>
        </div>
        <div id="sidebar">
            <ul class="nav list-group text-center">
            	<h4 style="margin-top: .5em; margin-bottom: 2em;">Silabo</h4>
                <li  data-toggle="collapse" data-target="#modulo1" class="app-active" area-expanded="true">
                  <a href="#" class="cn active" data-toggle="tooltip" data-placement="top" title="Desplegar"><i class="fa fa-gift fa-lg"></i> <strong><?php echo $semana['titulo']; ?></strong> <i class="glyphicon glyphicon-chevron-down"></i></a>
                </li>
                <ul class="sub-menu collapse in" id="modulo1" area-expanded="true">
                    <?php foreach($lecciones as $key => $value): ?>
                    <li>
                        <a class="list-group-item" href="<?php echo base_url(); ?>sys_curso/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><i class="icon-home icon-1x"></i><?php echo $value['titulo']; ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </ul>
        </div>
    </div>
    <div id="main-wrapper" class="col-md-10 pull-right col-xs-12" style="padding-top: 85px;">
        <div class="contenedor-video-tabla">
        	<div id="main" >
	        	<div class="row">
	        		<div class="col-md-12 text-center">
	        			<div class="app-resultado">
                            <h4>Tu resultado es:</h4>
                            <?php if($test['tipo'] == 0): ?>
    	        				<!-- div class="contenedor-resultado-bien">
                                    <span class="glyphicon glyphicon-ok"></span>               
                                </div -->
    	        				<h2><?php echo (($correctas / ($correctas + $incorrectas) * 20) > 13) ? 'Aprobado' : 'Desaprobado'; ?></h2>
    	        				<p class="linea-resp"></p>
    	        				<p>Contestaste <strong style="font-size: 20px;"><?php echo $correctas; ?>/<?php echo ($correctas + $incorrectas); ?></strong> preguntas correctamente.</p>
    	        				<div class="app-preguntas">
    	        					<h4>- Tus respuestas -</h4>
    	        					<span class="glyphicon glyphicon-chevron-down"></span>
                                    <?php foreach($preguntas as $key => $value): ?>
    	        					<div class="app-preguntas-respuestas">
    	        						<p data-toggle="collapse" data-target="#p<?php echo ($key + 1); ?>"><strong>Pregunta <?php echo ($key + 1); ?>: <?php echo $value['titulo']; ?></strong> <i class="glyphicon glyphicon-chevron-down"></i></p>
    	        						<p id="p<?php echo ($key + 1); ?>" class="collapse open in">
                                            <?php if($value['respuesta'] == $respuesta[$key]): ?>
                                            <span class="bien"><strong>Respuesta:</strong> <?php echo $respuesta[$key]; ?></span>
                                            <?php else: ?>
                                            <span class="mal"><strong>Respuesta:</strong> <?php echo $respuesta[$key]; ?></span><br>
                                            <span class="bien"><strong>Respuesta Correcta:</strong> <?php echo $value['respuesta']; ?></span></p>
                                            <?php endif; ?>
                                        </p>
    	        					</div>
                                    <?php endforeach; ?>
    	        				</div>
                            <?php else: ?>
                                <p>Tus resultados aparecerán en la pantalla de inicio de la plataforma, el profesor responderá tu examen y te pondrá una nota subjetiva.</p>
                            <?php endif; ?>
	        			</div>
	        		</div>
	        		<!-- div class="col-md-12 mb1em" style="text-align: center;">
	        			<a href="#" class="btn btn-success">Siguiente Tema</a>
	        			<a href="#" class="btn btn-warning">Volver a Intentarlo</a>
	        		</div -->
	        	</div>
        	</div>
        </div>
    </div>
</div>
<!-- SCRIPTS --> 
<script src="<?php echo base_view(); ?>js/jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_view(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_view(); ?>js/modernizr.custom.js"></script>
<!-- time line -->
<script src="<?php echo base_view(); ?>plugins/timeline/js/jquery.mobile.custom.min.js"></script>
<script src="<?php echo base_view(); ?>plugins/timeline/js/main.js"></script> <!-- Resource jQuery -->
</body>
</html>