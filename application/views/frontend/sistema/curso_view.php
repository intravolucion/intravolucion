<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
<link href="<?php echo base_view(); ?>css/curso.css" rel="stylesheet" />
<body>
<!-- Fixed navbar -->
<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
<div class="open-sidebar app-resp-open" data-toggle="tooltip" data-placement="right" title="Menú">
	<div class="app-btn-open-sidebar">
		<i class="glyphicon glyphicon-align-left icono-fixed-open icono"></i>
	</div>
</div>
<div id="wrapper">
  	<?php $this->load->view("frontend/sistema/templates/sidebar_curso_view"); ?>
        <div id="main-wrapper" class="col-md-10 pull-right col-xs-12">
            <div id="main">
              <div class="page-header text-center">
                <h3 style="font-size: 33px; margin-bottom: .5em;"><?php echo $curso['titulo']; ?></h3>
                <p>por <?php echo $profesor['nombres']; ?> <?php echo $profesor['apellidos']; ?></p>
              </div>
              <div class="contenido" style="padding: 2em;">
              	<div class="app-profe-detalle sombra row" style="padding: 1em;">
              		<div class="col-md-2 text-center">
              			<img src="<?php echo base_url(); ?>uploads/100x100/<?php echo $profesor['imagen']; ?>" alt="" />
              			<p class="mb0">Prof. <?php echo $profesor['apellidos']; ?></p>
              		</div>
              		<div class="col-md-10">
              			<p><?php echo nl2br($profesor['acerca_de']); ?></p>
              			<div class="redes-sociales" style="margin-top: 1em">
					  		<ul class="app-redes-inst pull-right">
						  		<?php if($profesor['facebook'] != ''): ?><li><a target="_blank" href="https://<?php echo str_replace(array('https://', 'http://'), '', $profesor['facebook']); ?>" class="btn btn-white"><i class="fa fa-facebook"></i></a></li><?php endif; ?>
						  		<?php if($profesor['twitter'] != ''): ?><li><a target="_blank" href="https://<?php echo str_replace(array('https://', 'http://'), '', $profesor['twitter']); ?>" class="btn btn-white"><i class="fa fa-twitter"></i></a></li><?php endif; ?>
						  		<?php if($profesor['linkedin'] != ''): ?><li><a target="_blank" href="https://<?php echo str_replace(array('https://', 'http://'), '', $profesor['linkedin']); ?>" class="btn btn-white"><i class="fa fa-linkedin"></i></a></li><?php endif; ?>
						  	</ul>
					  	</div>
              		</div>
              	</div>
              </div>
				
				<!--     linea horizontal     -->
				<!-- <div class="contenedor-linea">
					<div class="linea-horizontal">
						<div class="cd-horizontal-timeline">
							<div class="timeline">
								<div class="events-wrapper">
									<div class="events">
										<?php $fecha_inicio = $curso['fecha_inicio']; ?>
										<ol>
											<li><a href="#0" data-date="<?php echo date("d/m/Y", strtotime($fecha_inicio)); ?>" class="selected">Inicio</a></li>
											<?php foreach($semanas as $key => $value): ?>
											<?php $fecha_inicio = date("Y-m-d", strtotime("+2 month", strtotime($fecha_inicio))); ?>
											<li><a href="#0" data-date="<?php echo date("d/m/Y", strtotime($fecha_inicio)); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $value['titulo']; ?>">Semana <?php echo ($key + 1); ?></a></li>
											<?php endforeach; ?>
										</ol>

										<span class="filling-line" aria-hidden="true"></span>
									</div> 
								</div> 
									
								<ul class="cd-timeline-navigation">
									<li><a href="#0" class="prev inactive">Prev</a></li>
									<li><a href="#0" class="next">Next</a></li>
								</ul> 
							</div> 
						</div>
					</div>
				</div> -->

				<div class="contenedor-linea" style="margin-top: 3em;">
					<div class="container contenedor-liena-responsive">
						<div class="app-slider center-slik">
							<?php foreach ($semanas as $key => $value): ?>
							<div>
						      <div class="app-linea active-linea">
						      	<p><strong><?php echo $value['titulo'] ?></strong></p>
						      	<div class="bolita active-bolita"></div>
						      </div>
						    </div>
							<?php endforeach; ?>
						    <!-- <div>
						      <div class="app-linea">
						      	<p><strong>semana</strong></p>
						      	<div class="bolita"></div>
						      </div>
						    </div>
						    <div>
						      <div class="app-linea">
						      	<p><strong>semana</strong></p>
						      	<div class="bolita"></div>
						      </div>
						    </div>
						    <div>
						      <div class="app-linea">
						      	<p><strong>semana</strong></p>
						      	<div class="bolita"></div>
						      </div>
						    </div>
						    <div>
						      <div class="app-linea">
						      	<p><strong>semana</strong></p>
						      	<div class="bolita"></div>
						      </div>
						    </div>
						    <div>
						      <div class="app-linea">
						      	<p><strong>semana</strong></p>
						      	<div class="bolita"></div>
						      </div>
						    </div> -->
						</div>
					</div>
				</div>

				<!--   fin de  linea horizontal     -->			

				<!-- acordeon -->
				<?php $ultimo = FALSE; ?>
				<?php foreach($semanas as $key => $value): ?>
				<div class="panel-group" id="semana<?php echo ($key + 1); ?>" role="tablist" aria-multiselectable="true">
				  <div class="panel panel-default">
				    <div class="panel-heading bgn br0" role="tab" id="headingSemana<?php echo ($key + 1); ?>">
					    <a role="button" data-toggle="collapse" data-parent="#semana<?php echo ($key + 1); ?>" href="#collapseSemana<?php echo ($key + 1); ?>" aria-expanded="true" aria-controls="collapseSemana<?php echo ($key + 1); ?>">
					      <div class="panel-title">
					          <p class="app-inline cb mb0">SEMANA <?php echo ($key + 1); ?></p>
					          <p class="app-inline text-right cb mb0">Duración Aproximada: <?php echo $value['duracion']; ?><i class="glyphicon glyphicon-chevron-down"></i></p>
					      </div>

					    </a>
				    </div>
				    <div id="collapseSemana<?php echo ($key + 1); ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingSemana<?php echo ($key + 1); ?>">
				      <div class="panel-body">
				        <div class="row">
				        	<div class="col-md-12 text-center" style="margin-bottom: 1.5em;">
				        		<h3>[ <?php echo $value['titulo']; ?> ]</h3>
				        	</div>
				        	<div class="col-md-12">
				        		<div class="form-horizontal">
									<fieldset>
									<!-- Text input-->
									<?php foreach($lecciones[$key] as $k => $v): ?>
									<div class="form-group col-md-6">
										<div class="col-xs-12">
										  <label for="<?php echo $v['alias']; ?>"><i class="<?php echo ($v['id'] <= $curso_asignado['id_leccion'] AND $curso_asignado['id_leccion'] > 0) ? 'icon-unlocked app-iconblok' : 'icon-lock app-iconblok'; ?>"></i> <?php echo $v['titulo']; ?></label>  
										  <?php $porcentaje = ($v['id'] <= $curso_asignado['id_leccion'] AND $curso_asignado['id_leccion'] > 0) ? '100%' : '0%'; ?>
										  <?php $ultimo = ($v['id'] == $curso_asignado['id_leccion']) ? TRUE : FALSE; ?>
										  <?php if((int) $porcentaje > 0): ?>
										  	<div class="progress pull-right" style="height: auto; overflow: auto; background-color: transparent; -webkit-box-shadow: none; box-shadow: none; margin: 1px; padding: 5px;">
											  <div class="progress-bar progress-bar-info pull-right" role="progressbar" style="width: <?php echo $porcentaje; ?>;">
											    <?php echo $porcentaje; ?>
											  </div>
											</div>
										  <?php endif; ?>
										</div>
									</div>
									<?php endforeach; ?>
									</fieldset>
								</div>
								<?php $fecha_semana = date("Y-m-d", strtotime("+". ($key) ." week", strtotime($curso['fecha_inicio']))); ?>
								<div class="app-boton-cer-session pull-right">
									<?php if(date("Y-m-d") >= date("Y-m-d", strtotime($fecha_semana))): ?>
				        				<a href="<?php echo current_url(); ?>/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" class="btn btn-info">Ver Sesión</a>
					        		<?php else: ?>
										<a href="javascript:;" class="btn btn-info">Próximamente</a>
					        		<?php endif; ?>
				        		</div>
				        	</div>
				        	<!-- div class="col-md-6">
				        		<ul style="list-style: none;" class="app-list-blok">
				        			<li><p><i class="icon-unlocked app-iconblok"></i> Ver todos los videos</p></li>
				        			<li><p><i class="icon-lock app-iconblok" data-toggle="tooltip" data-placement="top" title="Termina las actividades anteriores"></i> Resolver todos los cuestionarios</p></li>
				        			<li><p><i class="icon-lock app-iconblok" data-toggle="tooltip" data-placement="top" title="Termina las actividades anteriores"></i> Realizar todas las actividades</p></li>
				        		</ul>
				        		
				        	</div-->
				        </div>
				      </div>
				    </div>
				  </div>
				</div>
				<?php endforeach; ?>
				<!-- fin del acordeon  -->

				<?php if($ultimo == TRUE): ?>
					
					<?php if($curso_asignado['certificacion'] == 0): ?>
						<form action="<?php echo base_url(); ?>pagar_certificacion" id="form-pay" method="POST">
			              <input type="hidden" name="token" id="token" />
			              <input type="hidden" name="curso" id="curso" value="<?php echo $curso['id']; ?>" />
			            </form>
						<div class="row">
							<div class="col-md-12 text-center">
								<button type="button" id="pagar_certificacion" class="btn btn-primary btn-lg">
								  Pagar la Certificación [<?php echo (($this->moneda == '_soles') ? 'S/ ' : 'US$ '); ?> <?php echo $curso['precio_certificacion' . $this->moneda]; ?>]
								</button>
								<br /><br />
							</div>
						</div>
					<?php endif; ?>

					<?php if(count($feedback) == 0): ?>
					<!-- modal de calificacion de preofesor y curso -->
					<div class="row">
						<div class="col-md-12 text-center">
							<button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
							  Califica este curso
							</button>
						</div>
					</div>
					<!-- Modal -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header" style="background: #79A54D; color: #fff;">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title" id="myModalLabel">Calificación</h4>
					      </div>
					      <form action="<?php echo base_url(); ?>sys_calificacion/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>" method="POST">
						      <div class="modal-body">
						        <div class="app-formulario-calificacion">
					        		<div class="app-content-pregunta" style="border-bottom: 1px dashed; padding: 1em 0;">
					        			<div class="row">
					        				<div class="col-md-12">
					        					<label for="" style="color: #BB403D;">1. ¿cómo calificaría usted la metodología de enseñanza del profesor?</label>				        			
					        				</div>
					        				<div class="col-md-3 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="ensenianza" value="0"> muy mala</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="ensenianza" value="1"> mala</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="ensenianza" value="2" checked> normal</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="ensenianza" value="3"> buena</label>
					        				</div>
					        				<div class="col-md-3 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="ensenianza" value="4"> muy buena</label>
					        				</div>
					        			</div> 
					        		</div>
					        		<div class="app-content-pregunta" style="border-bottom: 1px dashed; padding: 1em 0;">
					        			<div class="row">
					        				<div class="col-md-12">
					        					<label for="" style="color: #BB403D;">2. ¿cómo calificaría usted la metodología del curso?</label>				        			
					        				</div>
					        				<div class="col-md-3 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="metodologia" value="0"> muy mala</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="metodologia" value="1"> mala</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="metodologia" value="2" checked> normal</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="metodologia" value="3"> buena</label>
					        				</div>
					        				<div class="col-md-3 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="metodologia" value="4"> muy buena</label>
					        				</div>
					        			</div> 
					        		</div>
					        		<div class="app-content-pregunta" style="border-bottom: 1px dashed; padding: 1em 0;">
					        			<div class="row">
					        				<div class="col-md-12">
					        					<label for="" style="color: #BB403D;">3. ¿cómo calificaría usted el sílabus del curso?</label>				        			
					        				</div>
					        				<div class="col-md-3 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="silabus" value="0"> muy mala</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="silabus" value="1"> mala</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="silabus" value="2" checked> normal</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="silabus" value="3"> buena</label>
					        				</div>
					        				<div class="col-md-3 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="silabus" value="4"> muy buena</label>
					        				</div>
					        			</div> 
					        		</div>
					        		<div class="app-content-pregunta" style="border-bottom: 1px dashed; padding: 1em 0;">
					        			<div class="row">
					        				<div class="col-md-12">
					        					<label for="" style="color: #BB403D;">3. ¿cómo calificaría usted la plataforma INTRAVOLUCION?</label>				        			
					        				</div>
					        				<div class="col-md-3 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="plataforma" value="0"> muy mala</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="plataforma" value="1"> mala</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="plataforma" value="2" checked> normal</label>
					        				</div>
					        				<div class="col-md-2 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="plataforma" value="3"> buena</label>
					        				</div>
					        				<div class="col-md-3 col-sm-6 col-xs-12">
					        					<label><input type="radio" name="plataforma" value="4"> muy buena</label>
					        				</div>
					        			</div> 
					        		</div>
						        </div>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">En otro Momento</button>
						        <button type="submit" class="btn" style="background: #663E6F;color: #fff;">Enviar Comentarios</button>
						      </div>
					      </form>
					    </div>
					  </div>
					</div>
					<!-- fin del modal de calificacion de profe y curso -->
					<?php else: ?>
					<div class="alert alert-warning">Ya se calificó este curso. Gracias por sus comentarios.</div>
					<?php endif; ?>
				<?php endif; ?>
            </div>
        </div>
</div>






<!-- SCRIPTS --> 
<script src="<?php echo base_view(); ?>js/jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_view(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_view(); ?>js/modernizr.custom.js"></script>

<!-- time line -->
<script src="<?php echo base_view(); ?>plugins/timeline/js/jquery.mobile.custom.min.js"></script>
<script src="<?php echo base_view(); ?>plugins/timeline/js/main.js"></script> <!-- Resource jQuery -->

<!-- *********** -->

<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

//mostrar sidebar

$('.app-resp-open').click(function(){
	$('#sidebar-wrapper').css('display','block');
	$('#sidebar-wrapper').removeClass('slideOutLeft');
	$('#sidebar-wrapper').addClass('slideInLeft');
});
//ocultar sidebar
$('#app-resp-close').click(function(){
	$('#sidebar-wrapper').removeClass('slideInRight');
	$('#sidebar-wrapper').addClass('slideOutLeft');
});


$('#pagar_certificacion').on('click', function (e) {
	Culqi.abrir();
	e.preventDefault();
});

</script>



<!-- PASARELLA DE PAGOS CULQI -->
<script src="<?php echo $this->url; ?>js/v1"></script>  

<script>  
  Culqi.codigoComercio = '<?php echo $this->comercio; ?>'; var nivel = <?php echo MY_Controller::mostrar_session('nivel'); ?>

  Culqi.configurar({
    nombre: '<?php echo $this->configuracion['titulo']; ?>', 
    orden: '<?php echo date("Ymd") . date("His"); ?>', 
    moneda: '<?php echo (($this->moneda == '_soles') ? 'PEN' : 'USD'); ?>',
    descripcion: 'Pagar Certificación - <?php echo $curso['titulo']; ?>',
    monto: <?php echo $curso['precio_certificacion' . $this->moneda] * 100; ?>,
    guardar: false
  });
</script>

<script>  
  // Ejemplo: Tratando respuesta con AJAX (jQuery)
  function culqi()
  {
      if(Culqi.error)
      {
         // Mostramos JSON de objeto error en consola
         console.log(Culqi.error);
         alert(Culqi.error.mensaje);
      }
      else
      {
        var token = Culqi.token.id;

        $('#token').val(token); $('#token').attr('value', token);

        setTimeout(function() {
          $('#form-pay').submit();
        }, 150);
    }
  };
</script> 

</body>
</html>
