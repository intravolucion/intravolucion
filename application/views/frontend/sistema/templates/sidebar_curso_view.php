<div id="sidebar-wrapper" class="col-md-2 col-xs-8 animated">
	<div style="position: absolute;top: 5em;right: 8px;">
    	<span id="app-resp-close" class="glyphicon glyphicon-remove-sign" style="display: block"></span>
    </div>
    <div id="sidebar" style="overflow-y: auto !important;">
        <ul class="nav list-group text-center">
        	<img src="<?php echo base_url(); ?>uploads/100x100/<?php echo MY_Controller::mostrar_session('imagen'); ?>" alt="" width="100">
        	<h4 style="margin-top: .5em; margin-bottom: 2em;"><?php echo MY_Controller::mostrar_session('nombres'); ?> <?php echo MY_Controller::mostrar_session('apellidos'); ?></h4>
            <li>
                <a class="list-group-item" href="<?php echo base_url(); ?>sys_cursos"><i class="glyphicon glyphicon glyphicon-arrow-left cr"></i><strong class="cn"> Regresar</strong></a>
            </li>
            <li  data-toggle="collapse" data-target="#semanas" class="app-active" area-expanded="true">
              <a href="#" class="cn active"> <strong>Lecciones</strong> <i class="glyphicon glyphicon-chevron-down"></i></a>
            </li>
            <ul class="sub-menu collapse in" id="semanas" area-expanded="true">
            	<?php foreach($semanas as $key => $value): ?>
            		<?php $fecha_semana = date("Y-m-d", strtotime("+". ($key) ." week", strtotime($curso['fecha_inicio']))); ?>
                    <?php if(date("Y-m-d") >= date("Y-m-d", strtotime($fecha_semana))): ?>
                    <li>
                        <a class="list-group-item" href="<?php echo base_url(); ?>sys_curso/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><i class="icon-home icon-1x"></i>Semana <?php echo ($key + 1); ?></a>
                    </li>
                    <?php else: ?>
                    <li>
                        <a class="list-group-item" href="javascript:;"><i class="icon-home icon-1x"></i>Semana <?php echo ($key + 1); ?> - Próximamente</a>
                    </li>
                    <?php endif; ?>
            	<?php endforeach; ?>
            </ul>
            
            <li>
                <a class="list-group-item" href="<?php if(date("Y-m-d") >= date("Y-m-d", strtotime($curso['fecha_inicio']))): ?><?php echo base_url(); ?>sys_calificaciones/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?><?php else: ?>javascript:;<?php endif; ?>"><i class="icon-home icon-1x"></i><strong class="cn">Calificaciones</strong></a>
            </li>
            <li>
                <a class="list-group-item" href="<?php echo base_url(); ?>curso/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>"><i class="icon-home icon-1x"></i><strong class="cn">Información</strong></a>
            </li>
            <li>
                <a class="list-group-item" href="<?php echo base_url(); ?>salir"><i class="glyphicon glyphicon-off cr"></i><strong class="cn"> Salir</strong></a>
            </li>
        </ul>
    </div>
</div>