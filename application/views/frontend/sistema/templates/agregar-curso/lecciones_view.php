<div class="panel panel-default p-sombra">
	<div class="panel-heading">
		Lecciones del Curso
	</div>
  	<div class="panel-body">
	    <div class="row">
		    <div class="col-md-12 mb1em">
		    	<a href="" class="btn success cb "><i class="glyphicon glyphicon-plus-sign"></i> Agregar</a>
		    </div>
	    	<div class="col-md-12">
				<a href="curso.php" style="text-decoration: none; color: #4F4F4F;">
					<div class="panel panel-default p-sombra">
					  <div class="panel-heading">
						<div class="row">
					  		<div class="col-md-6">Laravel 5.2</div>
							<div class="col-md-6">
								<a href="#" class="pull-right"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
							</div>
					  	</div>
					  </div>
					  <div class="panel-body">
					  	<div class="row">
					  		<div class="col-md-3 text-center">
					  			<img src="img/imgcurso2.jpg" alt="" style="width: 100px;">
					  			<h4>Laravel 5.2</h4>
					  		</div>
					  		<div class="col-md-9">
						    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit consectetur adipisicing elit consectetur adipisicing elit. </p>
						    	<p class="pull-left">
						    		<strong>Actualizado: 15/10/2016</strong>
						    	</p>
						    </div>
					  	</div>
					  </div>						  
					</div>
				</a>
	   		</div>
	   		<div class="col-md-12">
				<a href="curso.php" style="text-decoration: none; color: #4F4F4F;">
					<div class="panel panel-default p-sombra">
					  <div class="panel-heading">
						<div class="row">
					  		<div class="col-md-6">Ruby on Rails</div>
							<div class="col-md-6">
								<a href="#" class="pull-right"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
							</div>
					  	</div>
					  </div>
					  <div class="panel-body">
					  	<div class="row">
					  		<div class="col-md-3 text-center">
					  			<img src="img/logocurso3.png" alt="" style="width: 70px;">
					  			<h4>Ruby on Rails</h4>
					  		</div>
					  		<div class="col-md-9">
						    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit consectetur adipisicing elit consectetur adipisicing elit. </p>
						    	<p class="pull-left">
						    		<strong>Actualizado: 15/10/2016</strong>
						    	</p>
						    </div>
					  	</div>
					  </div>						  
					</div>
				</a>
	   		</div>
	   		<div class="col-md-12">
				<a href="curso.php" style="text-decoration: none; color: #4F4F4F;">
					<div class="panel panel-default p-sombra">
					  <div class="panel-heading">
						<div class="row">
					  		<div class="col-md-6">Angular 2</div>
							<div class="col-md-6">
								<a href="#" class="pull-right"><i class="glyphicon glyphicon-pencil"></i> Editar</a>
							</div>
					  	</div>
					  </div>
					  <div class="panel-body">
					  	<div class="row">
					  		<div class="col-md-3 text-center">
					  			<img src="img/imgcurso.jpg" alt="" style="width: 100px;">
					  			<h4>Angular 2</h4>
					  		</div>
					  		<div class="col-md-9">
						    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit consectetur adipisicing elit consectetur adipisicing elit. </p>
						    	<p class="pull-left">
						    		<strong>Actualizado: 15/10/2016</strong>
						    	</p>
						    </div>
					  	</div>
					  </div>						  
					</div>
				</a>
	   		</div>
	  	</div>						  
	</div>
	
</div>
