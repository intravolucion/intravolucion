<div class="panel panel-default p-sombra">
	<div class="panel-heading">Datos del Curso</div>
  	<div class="panel-body">
	    <div class="row">
	    	<form class="form-horizontal" method="POST" enctype="multipar/form-data" action="<?php echo current_url(); ?>">
				<fieldset>
					<div class="form-group">
					  <label class="col-md-3 control-label" for="titulo">Curso</label>  
					  <div class="col-md-8">
					  <input id="titulo" name="titulo" type="text" class="form-control input-md" required="" value="<?php echo $curso['titulo']; ?>">
					  </div>
					</div>
					<div class="form-group">
					  <label class="col-md-3 control-label" for="logo">Logo</label>  
					  <div class="col-md-8">
						  <div class="input-group image-preview">
			                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
				                <span class="input-group-btn">
				                    <!-- image-preview-clear button -->
				                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
				                        <span class="glyphicon glyphicon-remove"></span> Quitar
				                    </button>
				                    <!-- image-preview-input -->
				                    <div class="btn btn-default image-preview-input">
				                        <span class="glyphicon glyphicon-folder-open"></span>
				                        <span class="image-preview-input-title">Seleccionar</span>
				                        <input type="file" accept="image/png, image/jpeg, image/gif" name="logo" id="logo" /> <!-- rename it -->
				                    </div>
				                </span>
				            </div>
				            <br />
				            
				            <img class="img-responsive" src="<?php echo base_url(); ?>uploads/<?php echo $this->configuracion['logo']; ?>" style="max-width:100px;max-height:100px;" />
				        	
					  </div>
					</div>
					<div class="form-group">
					    <label class="col-md-3 control-label" for="banner">Banner</label>  
					    <div class="col-md-8">
						  <div class="input-group image-preview">
			                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
				                <span class="input-group-btn">
				                    <!-- image-preview-clear button -->
				                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
				                        <span class="glyphicon glyphicon-remove"></span> Quitar
				                    </button>
				                    <!-- image-preview-input -->
				                    <div class="btn btn-default image-preview-input">
				                        <span class="glyphicon glyphicon-folder-open"></span>
				                        <span class="image-preview-input-title">Seleccionar</span>
				                        <input type="file" accept="image/png, image/jpeg, image/gif" name="banner" id="banner" /> <!-- rename it -->
				                    </div>
				                </span>
				            </div>
				            <br />
				            
				            <img class="img-responsive" src="<?php echo base_url(); ?>uploads/<?php echo $this->configuracion['logo']; ?>" style="max-width:100px;max-height:100px;" />
				        				    
					  	</div>
					</div>
					<div class="form-group">
					  <label class="col-md-3 control-label" for="resumen">Resumen</label>  
					  <div class="col-md-8">
					  <textarea id="resumen" name="resumen" class="editor form-control"></textarea>
					  </div>
					</div>
					<div class="form-group">
					  <label class="col-md-3 control-label" for="descripcion">Descripción</label>  
					  	<div class="col-md-8">
						  <textarea id="descripcion" name="descripcion" class="editor form-control"></textarea>
						</div>
		    		</div>
		    		<div class="form-group">
					  <label class="col-md-3 control-label" for="aprenderas">¿Qué Aprenderás?</label>  
					  <div class="col-md-8">
					  <textarea id="aprenderas" name="aprenderas" class="editor form-control"></textarea>
					  </div>
					</div>
					<div class="form-group">
					  <label class="col-md-3 control-label" for="dirigido">¿A quién está dirigido?</label>  
					  	<div class="col-md-8">
						  <textarea id="dirigido" name="dirigido" class="editor form-control"></textarea>
						</div>
		    		</div>
					<div class="form-group">
					    <div class="col-md-4 pull-right">
					    	<button type="submit" class="btn success cb"><strong>Guardar</strong></button>
					    </div>
					</div>
				</fieldset>
			</form>
	  	</div>						  
	</div>
</div>