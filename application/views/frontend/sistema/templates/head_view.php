<!DOCTYPE html>
<html lang="es">
<head>
<title><?php echo $this->configuracion['titulo']; ?></title>
<meta charset="utf-8">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--timeline -->
<link rel="stylesheet" href="<?php echo base_view(); ?>plugins/timeline/css/reset.css"> <!-- CSS reset -->
<link rel="stylesheet" href="<?php echo base_view(); ?>plugins/timeline/css/style.css"> <!-- Resource style -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_view(); ?>css/estudiante.css">
<link rel="stylesheet" href="<?php echo base_view(); ?>css/animate.css">
<link href="<?php echo base_view(); ?>css/curso.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_view(); ?>css/responsive.css">
<script src="<?php echo base_view(); ?>plugins/jquery/jquery-1.11.3.min.js"></script>

<link href="<?php echo base_view(); ?>plugins/pace/themes/pace-theme-barber-shop.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo base_view(); ?>css/style_iconmon.css">

<!-- slik -->
<link rel="stylesheet" type="text/css" href="<?php echo base_view(); ?>plugins/slick-nuevo/slick/slick.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo base_view(); ?>plugins/slick-nuevo/slick/slick-theme.css"/>

<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_view(); ?>plugins/slick-nuevo/slick/slick.min.js"></script>

<!-- fin de slick-->

<link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">

<style>
	.ir_web{
		margin-top: 12px;

	}

	.ir_web:hover{
		color: #3B99D7;
		border-color: #3B99D7;
	}

	.app-linea > p {
		height:53px !important;
	}
</style>

<script>
	$(window).ready(function(){
		$('.app-slider').slick({
		  dots: false,
		  infinite: false,
		  speed: 300,
		  slidesToShow: 2,
		  slidesToScroll: 1,
		  arrows: true,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true,
		        arrows: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 1,
		        arrows: true,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        arrows: true,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});
	});
	
</script>
</head>