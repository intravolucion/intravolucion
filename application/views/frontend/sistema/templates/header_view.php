<nav class="navbar navbar-white sombra">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header mt1em">
	      <button type="button" class="navbar-toggle collapsed btn-respo" data-toggle="collapse" data-target="#avatar-estudiante" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand app-logo" href="<?php echo base_url(); ?>sys_dashboard"><h1><img src="<?php echo base_url(); ?>uploads/<?php echo $this->configuracion['logo']; ?>" alt=""></h1></a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="avatar-estudiante">
	      <!-- <ul class="nav navbar-nav">
	        <li class=""><a href="#"><i class="glyphicon glyphicon-th-list"></i></a></li>
	      </ul> -->
	      <!-- form class="navbar-form navbar-left app-std-nav-busq mt1em">
			  <input type="text" name="search" class="app-busqueda" placeholder="Buscar..">
	      </form -->
	      <ul class="nav navbar-nav navbar-right respo-nav-avatar">
	      	<li> <a href="<?php echo base_url(); ?>" class="btn btn-info ir_web">Ver Website</a></li>
	        <li>
	        	<a href="<?php echo base_url(); ?>sys_mensajes"><i class="glyphicon glyphicon-envelope" style="font-size:46px;"></i> <span class="badge success"><?php echo count($this->mensajes); ?></span></a>
	        </li>
	        <!-- li>
	        	<a href="javascript:;"><i class="glyphicon glyphicon-bell" style="font-size:46px;"></i> <span class="badge success">2</span></a>
	        </li -->
	        <li class="dropdown">
	          <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url(); ?>uploads/100x100/<?php echo MY_Controller::mostrar_session('imagen'); ?>" alt="" class="app-img-user"> <span class="caret"></span></a>
	          <ul class="dropdown-menu respo-down-avatar">
	            <li><a href="<?php echo base_url(); ?>sys_configuracion">Editar Perfil</a></li>
	            <li><a href="<?php echo base_url(); ?>sys_cursos">Cursos</a></li>
	            <!-- li><a href="<?php echo base_url(); ?>sys_ayuda">Ayuda</a></li -->
	            <?php $busqueda = $this->module_model->buscar('administrador', MY_Controller::mostrar_session('id')); ?>
	            <?php if(MY_Controller::mostrar_session('ver_como') != '' AND $busqueda['nivel'] <= 1): ?>
		            <li class="divider"></li>
		            <li><a href="<?php echo base_url(); ?>sys_dashboard?administrador=true">Ver como Administrador</a></li>
		            <li><a href="<?php echo base_url(); ?>sys_dashboard?profesor=true">Ver como Profesor</a></li>
		            <li><a href="<?php echo base_url(); ?>sys_dashboard?alumno=true">Ver como Alumno</a></li>
				<?php elseif(MY_Controller::mostrar_session('nivel') == 2): ?>
					<li><a href="<?php echo base_url(); ?>sys_dashboard?alumno=true">Ver como Alumno</a></li>
				<?php elseif(MY_Controller::mostrar_session('nivel') == 3 AND MY_Controller::mostrar_session('ver_como') == 'alumno'): ?>
					<li><a href="<?php echo base_url(); ?>sys_dashboard?profesor=true">Ver como Profesor</a></li>
		        <?php endif; ?>
	            <li class="divider"></li>
	            <li><a href="<?php echo base_url(); ?>salir">Salir</a></li>
	          </ul>
	        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>


	

	