<?php $niveles = ['', 'Administrador', 'Profesor', 'Estudiante']; ?>

<div class="profile-sidebar sombra">
	<!-- SIDEBAR USERPIC -->
	<div class="profile-userpic">
		<img src="<?php echo base_url(); ?>uploads/100x100/<?php echo MY_Controller::mostrar_session('imagen'); ?>" class="img-responsive" alt="">
	</div>
	<!-- END SIDEBAR USERPIC -->
	<!-- SIDEBAR USER TITLE -->
	<div class="profile-usertitle">
		<div class="profile-usertitle-name">
			<a href="<?php echo base_url(); ?>sys_perfil"><?php echo MY_Controller::mostrar_session('nombres'); ?> <?php echo MY_Controller::mostrar_session('apellidos'); ?></a>
		</div>
		<div class="profile-usertitle-job">
			<?php if(MY_Controller::mostrar_session('tutor') == 1): ?>
			Tutor
			<?php else: ?>
			<?php echo $niveles[MY_Controller::mostrar_session('nivel')]; ?>
			<?php endif; ?>
		</div>
	</div>
	<!-- END SIDEBAR USER TITLE -->
	<!-- SIDEBAR BUTTONS -->
	<div class="profile-userbuttons">
		<?php if(MY_Controller::mostrar_session('tutor') == 1): ?>
			<a href="<?php echo base_url(); ?>sys_alumnos" class="btn btn-success btn-sm mt1em">Alumnos</a>
			<a href="<?php echo base_url(); ?>sys_profesores" class="btn btn-warning btn-sm mt1em">Profesores</a>
		<?php else: ?>
			<!-- button type="button" class="btn btn-success btn-sm">Sígueme</button -->
		<?php endif; ?>
		<a href="<?php echo base_url(); ?>sys_mensajes" class="btn btn-danger btn-sm mt1em">Mensajes</a>
	</div>
	<!-- END SIDEBAR BUTTONS -->
	<!-- SIDEBAR MENU -->
	<div class="profile-usermenu">
		<ul class="nav">
			<li<?php echo (@$urlvista == 'inicio') ? ' class="active"' : ''; ?>>
				<a href="<?php echo base_url(); ?>sys_dashboard">
				<i class="glyphicon glyphicon-home"></i>
				Inicio </a>
			</li>
			<li<?php echo (@$urlvista == 'configuracion') ? ' class="active"' : ''; ?>>
				<a href="<?php echo base_url(); ?>sys_configuracion">
				<i class="glyphicon glyphicon-user"></i>
				Editar Perfil </a>
			</li>

			<li<?php echo (@$urlvista == 'contrasenia') ? ' class="active"' : ''; ?>>
				<a href="<?php echo base_url(); ?>sys_contrasenia">
				<i class="glyphicon glyphicon-lock"></i>
				Editar Contraseña </a>
			</li>

			<li<?php echo (@$urlvista == 'cursos') ? ' class="active"' : ''; ?>>
				<a href="<?php echo base_url(); ?>sys_cursos">
				<i class="glyphicon glyphicon-ok"></i>
				Cursos </a>
			</li>
			<!-- li<?php echo (@$urlvista == 'ayuda') ? ' class="active"' : ''; ?>>
				<a href="<?php echo base_url(); ?>sys_help">
				<i class="glyphicon glyphicon-flag"></i>
				Ayuda </a>
			</li -->
		</ul>
	</div>
	<!-- END MENU -->
</div>