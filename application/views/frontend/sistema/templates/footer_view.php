<!-- SCRIPTS --> 
<script src="<?php echo base_view(); ?>js/jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_view(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_view(); ?>js/modernizr.custom.js"></script>
<script src="<?php echo base_view(); ?>js/estudiante.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<script src="<?php echo base_view(); ?>plugins/datetimepicker/datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

<script>
  var table = $('#tabla_alumnos_curso').DataTable(
  		{
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });

  var table = $('#tabla_alumnos_curso-respo').DataTable(
  		{
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
    });
</script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/locales/bootstrap-datepicker.es.min.js"></script>

<!-- ALERTS -->
<link href="<?php echo backend_view(); ?>css/alerts.css" rel="stylesheet">
<script src="<?php echo backend_view(); ?>js/alerts.js"></script>

<script>

  var base_url = "<?php echo base_url(); ?>"; var xnToken = "<?php echo MY_Controller::mostrar_session('token'); ?>";

  function eliminar(id, table, element) {

    jConfirm("¿Desea eliminar el registro?", "Eliminar el Registro", function(a)
    {
        if (a == true)
        {
            $.ajax({
                url: base_url+"sys_eliminar/" + id + "/" + table,
                type: 'GET',
                dataType: 'json',
                data: { token : xnToken },
                success: function(data) {
                    if(data != '' && data != null)
                    {
                        window.location.reload();
                    }
                    else
                    {
                        window.location.reload();
                    }
                },
                error: function(data) {
                    window.location.href = backend_url;
                }
            });
            return false;
        }
        else
        {
            return false;
        };
    });
  }

	$('.default-date-picker').datepicker({
        format: 'yyyy-mm-dd',
        language: "es"
    });
</script>

<script>
	$('.center-slik').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});
	
</script>
</body>
</html>