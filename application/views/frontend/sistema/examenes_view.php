<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
	<div class="container">
    	<div class="row profile">
		<div class="col-md-3">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Curso</h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li><a href="<?php echo base_url(); ?>sys_cursos" class="">Curso</a></li>
						  <li>Exámenes</li>
						</ol>
			   		</div>
					
			   		<div class="col-md-12">
			   			<?php if(isset($mensaje)): ?>
	                    <div class="alert alert-success">
	                        <?php echo $mensaje; ?>
	                    </div>
	                    <?php endif; ?>

						<ul class="nav nav-tabs">
						  <li class="active"><a data-toggle="tab" href="#datos_curso">Exámenes</a></li>
						  <li><a href="<?php echo base_url(); ?>sys_semana_editar/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>">Regresar</a></li>
						</ul>
						<div class="tab-content">
							<div id="datos_curso" class="tab-pane fade in active">
							<div class="row">
							    <div class="col-md-12 mb1em">
							    	<a href="<?php echo base_url(); ?>sys_examen_agregar/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>" class="btn success cb " style="margin-top: 1em;"><i class="glyphicon glyphicon-plus-sign"></i> Agregar</a>
							    </div>

							    <?php $tipos = array('Objetivo', 'Subjetivo'); ?>
							    <?php foreach($examenes as $key => $value): ?>
						    	<div class="col-md-12">
									<a href="<?php echo base_url(); ?>sys_examen_editar/<?php echo $value['id']; ?>" style="text-decoration: none; color: #4F4F4F;">
										<div class="panel panel-default p-sombra">
										  <div class="panel-heading">
											<div class="row">
										  		<div class="col-md-6"><?php echo $value['titulo']; ?></div>
												<div class="col-md-6 text-right">
													<a href="<?php echo base_url(); ?>sys_examen_editar/<?php echo $value['id']; ?>"><i class="glyphicon glyphicon-pencil"></i> Editar</a> <a href="javascript:;" onclick="javascript:eliminar('<?php echo $value['id']; ?>', 'tests', this);"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
												</div>
										  	</div>
										  </div>
										  <div class="panel-body">
										  	<div class="row">
										  		<div class="col-md-12">
										  			<p>Tipo de Examen: <?php echo $tipos[$value['tipo']]; ?></p>
											    	<p class="pull-left">
											    		<strong>Fecha Creación:</strong> <?php echo MY_Controller::fecha_muestra($value['fecha_creacion']); ?><br />
											    		<strong>Fecha Modificación:</strong> <?php echo MY_Controller::fecha_muestra($value['fecha_modificacion']); ?>
											    	</p>
											    </div>
										  	</div>
										  </div>						  
										</div>
									</a>
						   		</div>
						   		<?php endforeach; ?>
						  	</div>	



							</div>
						</div>
						
			   		</div>
            	</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>
<script type="text/javascript">
CKEDITOR.replace("editor");
</script>