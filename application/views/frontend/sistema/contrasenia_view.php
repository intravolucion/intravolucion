<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>

<body>

<div class="content">
	
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>

	<div class="app-sidebar">
		<div class="container contenedor-respo">
    <div class="row profile">
		<div class="col-md-3 col-sm-4 col-xs-12">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Perfil</h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li><a href="<?php echo base_url(); ?>sys_perfil" class="">Perfil</a></li>
						  <li>Editar Contraseña</li>
						</ol>
			   		</div>
					
			   		<div class="col-md-12">
			   			<?php if(isset($mensaje)): ?>
	                    <div class="alert alert-success">
	                        <?php echo $mensaje; ?>
	                    </div>
	                    <?php endif; ?>

						<form class="form-horizontal respo-form-edit-perf" enctype="multipart/form-data" method="post" action="<?php echo current_url(); ?>">
						<div class="panel panel-default p-sombra">
						  <div class="panel-heading">Actualización de la contraseña</div>
						  <div class="panel-body">
						    <div class="row">
								<fieldset>
									<!-- Text input-->
									<div class="form-group">
									  <label class="col-md-4 control-label" for="contrasenia_anterior">Contraseña Anterior</label>  
									  <div class="col-md-5">
									  <input id="contrasenia_anterior" name="contrasenia_anterior" type="text" class="form-control input-md" required="" /> 
									  </div>
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label class="col-md-4 control-label" for="nueva_contrasenia">Nueva Contraseña</label>  
									  <div class="col-md-5">
									  <input id="nueva_contrasenia" name="nueva_contrasenia" type="text" class="form-control input-md" required="" />
									  </div>
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label class="col-md-4 control-label" for="repetir_contrasenia">Repetir Contraseña</label>  
									  <div class="col-md-5">
									  <input id="repetir_contrasenia" name="repetir_contrasenia" type="text" class="form-control input-md" required="" />
									  </div>
									</div>
								</fieldset>
						    </div>
						  </div>						  
						</div>
						<div class="app-botones pull-right">
					    	<div class="app-btn-editar">
					    		<button type="submit" class="btn success cb"><strong>Guardar</strong></button>
					    	</div>
						</div>
						</form>
					</div>
			   	</div>
            </div>
		</div>
	</div>
</div>
	</div>



</div>


<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>