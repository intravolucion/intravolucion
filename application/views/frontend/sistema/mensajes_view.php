<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
		<div class="container contenedor-respo">
			    <div class="row profile">
					<div class="col-md-3 col-sm-4 col-xs-12">
						<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
			            <div class="profile-content sombra">
						   	<div class="row">
						   		<div class="col-md-12 text-center">
						   			<h3>Mensajes</h3>
						   			<p><i class="glyphicon glyphicon-chevron-down"></i></p>
						   		</div>
						   		<?php if(count($mensajes) > 0): ?>
							   		<?php foreach($mensajes as $key => $value): ?>
							   		<?php $profesor = $this->module_model->buscar('administrador', $value['id_emisor']); $cantidad = $this->module_model->total('mensajes', array('id_emisor' => $value['id_emisor'], 'id_receptor' => $value['id_receptor'], 'activado' => 0)); ?>
							   		<div class="col-md-6">
										<a href="<?php if($value['id_emisor'] != MY_Controller::mostrar_session('id')): ?><?php echo base_url(); ?>sys_mensaje/<?php echo $value['id_emisor']; ?><?php else: ?>javascript:;<?php endif; ?>">
											<div class="panel panel-default p-sombra"<?php echo ($cantidad > 0) ? ' style="box-shadow: 2px 2px 8px red;"' : ''; ?>>
											  <div class="panel-body">
											  	<div class="row">
											  		<div class="col-md-4 text-center">
											  			<img src="<?php echo base_url(); ?>uploads/100x100/<?php echo $profesor['imagen']; ?>" alt="" style="width: 100px;border-radius: 50%">
											  		</div>
											  		<div class="col-md-8">
											  			<p style="color: #999"><i class="glyphicon glyphicon-user"></i> <?php echo $profesor['nombres']; ?> <?php echo $profesor['apellidos']; ?></p>
												    	<p><i class="glyphicon glyphicon-comment"></i> <?php echo $cantidad; ?> mensaje(s) nuevo(s).</p>
												    	<p><a href="javascript:;" class="btn btn-danger" onclick="javascript:borrar_mensaje('<?php echo $value['id_emisor']; ?>', this);"><i class="glyphicon glyphicon-minus"></i> Borrar Mensaje</a></p>
												    	
												    	<!-- p class="pull-right">
												    		<button style="background: transparent;border: none;;"><i class="glyphicon glyphicon-bell"></i> 10</button>
												    	</p -->
												    </div>
											  	</div>
											  </div>						  
											</div>	
										</a>								
							   		</div>
							   		<?php endforeach; ?>
							   	<?php else: ?>
						   			<div class="col-md-12">
						   				<div class="alert alert-warning">No tienes ningún mensaje en tu bandeja.</div>
						   			</div>
							   	<?php endif; ?>
						   	</div>
			            </div>
					</div>
				</div>
			</div>
	</div>
</div>
<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>

<script>

  var base_url = "<?php echo base_url(); ?>"; var xnToken = "<?php echo MY_Controller::mostrar_session('token'); ?>";

  function borrar_mensaje(id, element) {

    jConfirm("¿Desea eliminar el mensaje?", "Eliminar el Mensaje", function(a)
    {
        if (a == true)
        {
            $.ajax({
                url: base_url+"sys_eliminar_mensajes/" + id,
                type: 'GET',
                dataType: 'json',
                data: { token : xnToken },
                success: function(data) {
                    if(data != '' && data != null)
                    {
                        window.location.reload();
                    }
                    else
                    {
                        window.location.reload();
                    }
                },
                error: function(data) {
                    window.location.href = backend_url;
                }
            });
            return false;
        }
        else
        {
            return false;
        };
    });
  }
</script>