<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
	<div class="container">
    	<div class="row profile">
		<div class="col-md-3">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Curso</h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li><a href="<?php echo base_url(); ?>sys_cursos" class="">Curso</a></li>
						  <li>Semanas</li>
						</ol>
			   		</div>
					
			   		<div class="col-md-12">
			   			<?php if(isset($mensaje)): ?>
	                    <div class="alert alert-success">
	                        <?php echo $mensaje; ?>
	                    </div>
	                    <?php endif; ?>

						<ul class="nav nav-tabs">
						  <li class="active"><a data-toggle="tab" href="#datos_curso">Semana</a></li>
						  <?php if(isset($semana) && count($semana) > 0): ?>
							  <li class=""><a href="<?php echo base_url(); ?>sys_lecciones/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>">Lecciones</a></li>
							  <li class=""><a href="<?php echo base_url(); ?>sys_examenes/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>">Examenes</a></li>
						  <?php endif; ?>
						  <li><a href="<?php echo base_url(); ?>sys_curso_editar/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>">Regresar</a></li>
						</ul>
						<div class="tab-content">
							<div class="row">
						   		<div class="col-md-12">
									<div class="panel panel-default p-sombra">
										<div class="panel-heading">Datos de la semana</div>
									  	<div class="panel-body">
										    <div class="row">
										    	<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?php echo current_url(); ?>">
													<fieldset>
														<div class="form-group">
														  <label class="col-md-3 control-label" for="titulo">Semana</label>  
														  <div class="col-md-8">
														  <input id="titulo" name="titulo" type="text" class="form-control input-md" required="" value="<?php echo @$semana['titulo']; ?>">
														  </div>
														</div>
														
														<div class="form-group">
														  <label class="col-md-3 control-label" for="resumen">Resumen</label>  
														  <div class="col-md-8">
														  <textarea id="resumen" name="resumen" class="editor form-control"><?php echo @$semana['resumen']; ?></textarea>
														  </div>
														</div>
														<div class="form-group">
														  <label class="col-md-3 control-label" for="duracion">Duración</label>  
														  	<div class="col-md-8">
															  <input type="text" id="duracion" name="duracion" class="input-md form-control" value="<?php echo @$semana['duracion']; ?>" />
															</div>
											    		</div>
											    		<div class="form-group">
														    <div class="col-md-12" style="text-align: right;right: 5em;">
														    	<button type="submit" class="btn success cb"><strong>Guardar</strong></button>
														    </div>
														</div>
													</fieldset>
												</form>
										  	</div>						  
										</div>
									</div>
						   		</div>

							</div>
						</div>
						
			   		</div>
            	</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view('frontend/sistema/templates/footer_view.php') ?>
<script type=”text/javascript”>
CKEDITOR.replace("editor");
</script>
