<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>

<body>
<!-- Fixed navbar -->
<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>

<?php $_tipos = array('contenido' => 'align-justify', 'archivo' => 'file', 'video' => 'play', 'imagen' => 'picture'); ?>

<div id="wrapper" class="">
  	<?php $this->load->view("frontend/sistema/templates/sidebar_curso_view"); ?>
    <div id="main-wrapper" class="col-md-10 pull-right col-xs-12 col-sm-8" style="padding-top: 85px;">
        <div class="contenedor-video-tabla">
        	<div id="main" >
	        	<div class="app-controlls bgb row overhide">
	        		<div class="col-md-3 col-xs-6 col-sm-3">
	        			<?php if(count($anterior) > 0): ?>
	        				<a href="<?php echo base_url(); ?>sys_curso/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>/<?php echo $anterior['id']; ?>-<?php echo $anterior['alias']; ?>" class="left"><i class="icon-chevron-thin-left"></i> Anterior</a>
	        			<?php else: ?>
	        				<a href="javascript:;" class="left"><i class="icon-chevron-thin-left"></i> Anterior</a>
	        			<?php endif; ?>
	        		</div>
	        		<div class="col-md-3 col-xs-6 col-sm-3">
	        			<span class="left app-resp-open"><i class="glyphicon glyphicon-th-list"></i> Sesiones</span>
	        		</div>
	        		<div class="col-md-3 col-xs-6 col-sm-3">
	        			<a href="<?php echo base_url(); ?>sys_curso/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>" class="right"><i class="glyphicon glyphicon-align-justify"></i> Inicio</a> 
	        		</div>
	        		<div class="col-md-3 col-xs-6 col-sm-3 respo-next2">
	        			<?php if(count($siguiente) > 0): ?>
	        				<a href="<?php echo base_url(); ?>sys_curso/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>/<?php echo $siguiente['id']; ?>-<?php echo $siguiente['alias']; ?>" class="right">Siguiente <i class="icon-chevron-thin-right text-right"></i></a>
	        			<?php else: ?>
	        				<a href="javascript:;" class="right">Siguiente <i class="icon-chevron-thin-right text-right"></i></a>
	        			<?php endif; ?>
	        		</div>
	        	</div>

	        	<?php if($leccion['tipo'] == 'contenido'): ?>
        		<?php echo nl2br($leccion['contenido']); ?>
        		<?php endif; ?>
        		<?php if($leccion['tipo'] == 'video'): ?>
		        	<div class="contenido-video" style="background: #323232; padding: 2em;">
		        		<iframe width="100%" height="450" src="https://www.youtube.com/embed/<?php echo $leccion['video']; ?>" frameborder="0" allowfullscreen></iframe>
						<div class="pie_video">
							<span class="pull-left cb"><?php echo $leccion['titulo']; ?></span>
							<!-- div class="pull-right app-video-like">
								<i class="glyphicon glyphicon-thumbs-up cb" data-toggle="tooltip" data-placement="top" title="Me gustó"> 100</i>
								<i class="glyphicon glyphicon-thumbs-down cb" data-toggle="tooltip" data-placement="top" title="No me gustó"> 25</i>
								<i class="glyphicon glyphicon-flag cb" data-toggle="tooltip" data-placement="top" title="Tuve problemas"></i>

							</div -->
						</div>
		        	</div>
				<?php endif; ?>
				<?php if($leccion['tipo'] == 'imagen'): ?>
					<img src="<?php echo base_url(); ?>uploads/<?php echo $leccion['imagen']; ?>" class="img-responsive" alt="<?php echo $leccion['titulo']; ?>" />
				<?php endif; ?>
				<?php if($leccion['tipo'] == 'archivo'): ?>
					<iframe src="https://docs.google.com/viewer?url=<?php echo base_url(); ?>uploads/<?php echo $leccion['archivo']; ?>&embedded=true" style="height:550px;width:100%;border:0px;frameborder:0px;"></iframe>

					<div class="col-xs-12">
						<a href="<?php echo base_url(); ?>sys_descargar_archivo?archivo=<?php echo $leccion['archivo']; ?>" class="btn btn-primary btn-sm">Descargar Archivo</a>
					</div>
				<?php endif; ?>

	        	<div class="">
		        	<div class="contenido p2em contenedor-respo" style="">
		          	<div class="row" style="padding: 1em;">
		          		<div class="col-md-7">
		          			<div class="col-md-12">
			          			<h3>Lecciones</h3>
			          		</div>
			          		<div class="col-md-12 contenedor-respo">
			          			<div class="panel panel-default">
								  <!-- Default panel contents -->
								  	<div class="panel-heading" style="font-size: 20px;">
										<div class="row">
											<div class="col-md-6">Módulo: <?php echo $semana['titulo']; ?></div>
											<div class="col-md-6">
												<div class="progress" style="margin-bottom: 0">
												  <div id="modulo_<?php echo $semana['id']; ?>" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar">
												    <span class=""></span>
												  </div>
												</div>
											</div>
										</div>
								  	</div>
								  	<div class="panel-body list-group">
								    	<table class="table table-hover">
											<tbody>
												<?php $activado = TRUE; $ultimo = FALSE; $vistos = 0; $totales = count($lecciones); ?>
												<?php foreach($lecciones as $k => $v): ?>
												<tr class="active">
													<td>
														<a <?php if($activado === TRUE):  ?>href="<?php echo base_url(); ?>sys_curso/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>/<?php echo $v['id']; ?>-<?php echo $v['alias']; ?>"<?php else: ?>href="javascript:;"<?php endif; ?> class="cp app-session">
															<i class="glyphicon glyphicon-<?php echo $_tipos[$v['tipo']]; ?>"></i> <?php echo $v['titulo']; ?><?php if($v['tipo'] == 'archivo'): ?><?php $informacion = MY_Controller::obtener_informacion_archivo($v['archivo']); ?> (Tipo de Archivo: <strong><?php echo mb_strtoupper($informacion['tipo']); ?></strong> | Peso: <strong><?php echo $informacion['tamanio']; ?></strong>) <?php endif; ?>
															<span class="pull-right fs12"><?php echo $v['duracion']; ?></span>
															<!-- span class="badge success">new</span -->
														</a>
														<?php if($curso_asignado['id_leccion'] > $v['id'] AND $curso_asignado['id_leccion'] > 0): ?>
														<?php $activado = FALSE; ?>
														<?php endif; ?>

														<?php if($curso_asignado['id_leccion'] > $v['id'] AND $curso_asignado['id_leccion'] > 0): ?>
															<?php $vistos++; $ultimo = FALSE; $activado = TRUE; ?>
														<?php elseif($v['id'] == $curso_asignado['id_leccion']): ?>
															<?php $ultimo = TRUE; $vistos++; $activado = TRUE; ?>
														<?php else: ?>
															<?php $ultimo = FALSE; $activado = FALSE; ?>
														<?php endif; ?>
													</td>
												</tr>
												<?php endforeach; ?>
												<?php foreach($tests as $key => $value): ?>
												<tr>
													<td>
														<a <?php if($ultimo === TRUE): ?>href="<?php echo base_url(); ?>sys_test/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>/<?php echo $value['id']; ?>-<?php echo MY_Controller::limpiar_texto($value['titulo']); ?>"<?php else: ?>href="javascript:;"<?php endif; ?> class="cn app-session" data-toggle="tooltip" data-placement="top" title="Debes ver todos los videos antes">
															<i class="glyphicon glyphicon-star" ></i> <strong class="cn"><?php echo $value['titulo']; ?></strong>
															<span class="pull-right fs12">Duración: <?php echo $value['duracion']; ?></span>
														</a>
													</td>
												</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
									</div>

									<script type="text/javascript">
										var porcentaje = "<?php echo number_format(($vistos / $totales * 100), 2); ?>";
										$('#modulo_<?php echo $semana['id']; ?>').css('width', porcentaje + '%');
										$('#modulo_<?php echo $semana['id']; ?>').html(porcentaje + '%');
									</script>
								</div>
			          		</div>
		          		</div>
		          		<div class="col-md-5">
		          			<div class="col-md-12">
			          			<h3>Discusión</h3>
			          		</div>
			          		<div class="col-md-12 pl0 pr0">
			          			<div class="panel panel-default">
									<div class="panel-body" id="discusion" style="padding:7px; height:350px; overflow:scroll;">
										<?php foreach($discusion as $key => $value): ?>
										<?php $emisor = $this->module_model->buscar('administrador', $value['id_emisor']); ?>
										<div class="panel panel-default <?php echo ($emisor['id'] == MY_Controller::mostrar_session('id')) ? 'pull-right' : 'pull-left'; ?>">
										  <div class="panel-body" style="padding: 7px;">
										    <h4 class="mt0 mb0"><?php echo $emisor['nombres']; ?> <?php echo $emisor['apellidos']; ?></h4>
										    <p class="mt0 mb0"><?php echo $value['contenido']; ?></p>
										  </div>
										</div>
										<br class="clearfix" style="clear:both;width:100%;" />
										<?php endforeach; ?>
									</div>
									<iframe name="_oculto" style="display:none;"></iframe>
									<div class="app-cont-chat">
							    		<div class="col-md-12 pl0 pr0">
							    			<form method="POST" action="<?php echo current_url(); ?>" target="_oculto" autocomplete="off">
								    			<div class="input-group">
											      <span class="input-group-btn">
											        <button class="btn btn-default" type="submit">Enviar</button>
											      </span>
											      <input type="hidden" id="_time" name="time" value="<?php echo $time; ?>" />
											      <input type="text" id="contenido" class="form-control" placeholder="Escribe un mensaje.." name="contenido" required autocomplete="off" />
											    </div>
											</form>
							    		</div>
							    	</div>
								</div>
							</div>
						</div>
	          		</div>
          		</div>
          	</div>
          </div>
        </div>
	</div>
</div>

<!-- SCRIPTS --> 
<script src="<?php echo base_view(); ?>js/jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_view(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_view(); ?>js/modernizr.custom.js"></script>

<!-- time line -->
<script src="<?php echo base_view(); ?>plugins/timeline/js/jquery.mobile.custom.min.js"></script>
<script src="<?php echo base_view(); ?>plugins/timeline/js/main.js"></script> <!-- Resource jQuery -->

<!-- *********** -->

<script>
var time = '<?php echo $time; ?>'; var emisor = '<?php echo MY_Controller::mostrar_session('id'); ?>'; var usuario = '<?php echo MY_Controller::mostrar_session('nombres'); ?> <?php echo MY_Controller::mostrar_session('apellidos'); ?>';

$(function () {
  $('[data-toggle="tooltip"]').tooltip();

  $("#discusion").scrollTop($("#discusion")[0].scrollHeight);
});

function imprimir(data)
{
	time = data['time']; $('#_time').val(time);

	if(data != undefined && (data['discusion'] != undefined || data['contenido'] != undefined))
	{
		$('#contenido').val(''); $('#contenido').attr('value', '');

		if(data['discusion'] != undefined)
		{
			$.ajax({
				url: '<?php echo base_url(); ?>sys_update/' + <?php echo $leccion['id']; ?> + '/' + time,
				type: 'POST',
				dataType: 'json',
				success: function(data)
				{
					$.each(data['discusion'], function(i, item){

						var html = ''; var xnEmisor = data['emisor'][i];

						html += '<div class="panel panel-default ' + ((xnEmisor['id'] == emisor) ? 'pull-right' : 'pull-left') + '">';
						html += '<div class="panel-body" style="padding: 7px;">';
						html += '<h4 class="mt0 mb0">' + xnEmisor['nombres'] + ' ' + ((xnEmisor['apellidos'] != '' && xnEmisor['apellidos'] != null) ? xnEmisor['apellidos'] : '') + '</h4>';
						html += '<p class="mt0 mb0">' + item['contenido'] + '</p>';
						html += '</div>';
						html += '</div>';
						html += '<br class="clearfix" style="clear:both;width:100%;" />';

						$('#discusion').append(html); $("#discusion").scrollTop($("#discusion")[0].scrollHeight);

						time = data['time'];

						setTimeout(function(){
							imprimir({'time': time});
						}, 3000);
					});
				},
				error: function(data)
				{
					console.log(data);
				}
			});
		}
		else
		{
			/*
			var html = '';

			html += '<div class="panel panel-default pull-right">';
			html += '<div class="panel-body" style="padding: 7px;">';
			html += '<h4 class="mt0 mb0">' + usuario + '</h4>';
			html += '<p class="mt0 mb0">' + data['contenido'] + '</p>';
			html += '</div>';
			html += '</div>';
			html += '<br class="clearfix" style="clear:both;width:100%;" />';

			$('#discusion').append(html); $("#discusion").scrollTop($("#discusion")[0].scrollHeight);
			*/

			// time = data['time'];
		}
	}
	else
	{
		$.ajax({
			url: '<?php echo base_url(); ?>sys_update/' + <?php echo $leccion['id']; ?> + '/' + time,
			type: 'POST',
			dataType: 'json',
			success: function(data)
			{
				$.each(data['discusion'], function(i, item){

					var html = ''; var xnEmisor = data['emisor'][i];

					html += '<div class="panel panel-default ' + ((xnEmisor['id'] == emisor) ? 'pull-right' : 'pull-left') + '">';
					html += '<div class="panel-body" style="padding: 7px;">';
					html += '<h4 class="mt0 mb0">' + xnEmisor['nombres'] + ' ' + ((xnEmisor['apellidos'] != '' && xnEmisor['apellidos'] != null) ? xnEmisor['apellidos'] : '') + '</h4>';
					html += '<p class="mt0 mb0">' + item['contenido'] + '</p>';
					html += '</div>';
					html += '</div>';
					html += '<br class="clearfix" style="clear:both;width:100%;" />';

					$('#discusion').append(html); $("#discusion").scrollTop($("#discusion")[0].scrollHeight);
				});

				time = data['time'];

				setTimeout(function(){
					imprimir({'time': time});
				}, 3000);
			},
			error: function(data)
			{
				console.log(data);
			}
		});
	}
}

setTimeout(function(){
	imprimir({'time': time});
}, 1000);

//mostrar sidebar

$('.app-resp-open').click(function(){
	$('#main-wrapper').removeClass('col-md-12');
	$('#main-wrapper').addClass('col-md-10').css('transition','.6s');
	$('#sidebar-wrapper').css('display','block');
	$('#sidebar-wrapper').removeClass('slideOutLeft');
	$('#sidebar-wrapper').addClass('slideInLeft');
	
});
//ocultar sidebar
$('#app-resp-close').click(function(){
	$('#main-wrapper').removeClass('col-md-10');
	$('#main-wrapper').addClass('col-md-12').css('transition','.6s');
	$('#sidebar-wrapper').removeClass('slideInRight');
	$('#sidebar-wrapper').addClass('slideOutLeft');
	
});

/**********para q aparesca la respuesta************/
$('.response-btn').click(function(){
	$('.response-input').html('<div class="input-group"><span class="input-group-btn"><button class="btn btn-default" type="button">Enviar</button></span><input type="text" class="form-control" placeholder="Escribir..."></div>');
});

$('.response-btn2').click(function(){
	$('.response-input2').html('<div class="input-group"><span class="input-group-btn"><button class="btn btn-default" type="button">Enviar</button></span><input type="text" class="form-control" placeholder="Escribir..."></div>');
});

</script>
</body>

</body>
</html>
