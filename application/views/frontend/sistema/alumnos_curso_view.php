<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
		<div class="container contenedor-respo">
    <div class="row profile">
		<div class="col-md-3 col-sm-4 col-xs-12">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Curso <strong><?php echo $curso['titulo']; ?></strong></h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li><a href="<?php echo base_url(); ?>sys_alumnos"><strong>Alumnos por Cursos</strong></a></li>
						  <li><?php echo $curso['titulo']; ?></li>
						</ol>
			   		</div>

			   		<?php if(count($alumnos) > 0): ?>
				   		<div class="col-md-12 text-right mb1em">
				   			<a href="<?php echo base_url(); ?>sys_exportar_notas_curso/<?php echo $curso['id']; ?>" class="btn btn-primary btn-sm">Exportar Notas</a>
				   		</div>
				   	<?php endif; ?>
					
			   		<div class="col-md-12 app-respo-tabla-normal">
						<table id="tabla_alumnos_curso" class="table table-striped table-bordered" cellspacing="0" width="100%">
					        <thead>
					            <tr>
					                <th>Nombre</th>
					                <th>Apellido</th>
					                <!-- th>Dirección</th -->
					                <th>Nota</th>
					                <th>Teléfono</th>
					                <th>Correo</th>
					                <th>Acción</th>
					            </tr>
					        </thead>
					        <tbody>
					        	<?php foreach($alumnos as $key => $value): ?>
					        	<?php $alumno = $this->module_model->seleccionar('administrador', array('estado' => 1, 'activado' => 1, 'id' => $value['id_padre']), 1, 1); ?>
					        	<?php if(count($alumno) > 0): ?>
					        	<?php

					        	$nota = 0; $suma = 0; $examenes = 0;

					        	foreach($tests as $k => $v)
					        	{
					        		$busqueda = $this->module_model->seleccionar('calificaciones', array('id_padre' => $v['id'], 'id_usuario' => $alumno['id']), 1, 1);

					        		if(count($busqueda) > 0)
					        		{
					        			if($v['tipo'] == 0)
					        			{
					        				$suma += $busqueda['correctas'] / ($busqueda['correctas'] + $busqueda['incorrectas']) * 20;
					        			}
					        			else
					        			{
					        				$suma += $busqueda['nota'];
					        			}
					        			
					        			$examenes++;
					        		}
					        	}

					        	$nota = ($examenes > 0) ? ($suma / $examenes) : 'Sin Exámenes';

					        	?>
					        	<?php ?>
					            <tr>
					                <td><?php echo $alumno['nombres']; ?> </td>
					                <td><?php echo $alumno['apellidos']; ?></td>
					                <!-- td>Edinburgh</td -->
					                <td><?php echo $nota; ?></td>
					                <td><?php echo $alumno['telefono']; ?></td>
					                <td><?php echo $alumno['correo_electronico']; ?></td>
					                <th><a href="<?php echo base_url(); ?>sys_reporte_notas/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $alumno['id']; ?>" class="btn btn-primary"> Notas</a> <a href="javascript:;" class="btn btn-info" data-toggle="modal" data-target=".mensaje-alumno<?php echo $value['id_padre']; ?>">Contactar</a></th>
					            </tr>
					            
					            <!--***** mensaje**** -->
					            <!-- Modal -->
								<div class="modal fade mensaje-alumno<?php echo $value['id_padre']; ?>" id="mensaje-alumno-<?php echo $value['id_padre']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								  <div class="modal-dialog" role="document">
								    <div class="modal-content">
								      <div class="modal-header">
								        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								        <h4 class="modal-title" id="myModalLabel">Enviar Mensaje a <?php echo $alumno['nombres']; ?> <?php echo $alumno['apellidos']; ?></h4>
								      </div>
								      <form method="POST" action="<?php echo base_url(); ?>sys_mensaje/<?php echo $alumno['id']; ?>">
								      <div class="modal-body">
								        <div class="form-group">
								        	<label for="contenido" class="">Mensaje</label>
								        	<textarea class="form-control" required name="contenido" id="contenido"></textarea>
								        </div>
								      </div>
								      <div class="modal-footer">
								        <button type="submit" class="btn btn-primary">Enviar</button>
								      </div>
								  	  </form>
								    </div>
								  </div>
								</div>
								<?php endif; ?>
								<?php endforeach; ?>
					            <!--** fin del mensaje ****-->
					        </tbody>
					    </table>
						
			   		</div>
            	</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>
<script type=”text/javascript”>
CKEDITOR.replace("editor");
</script>