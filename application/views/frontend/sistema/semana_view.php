<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<link href="<?php echo base_view(); ?>css/curso.css" rel="stylesheet" />
<body>
<!-- Fixed navbar -->
<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>

<?php $_tipos = array('contenido' => 'align-justify', 'archivo' => 'file', 'video' => 'play', 'imagen' => 'picture'); ?>

<div id="wrapper">
	<?php $this->load->view("frontend/sistema/templates/sidebar_curso_view"); ?>
	<div id="main-wrapper" class="col-md-10 pull-right col-xs-12">
	    <div id="main">
			<div class="page-header text-center">
				<h3 style="font-size: 33px; margin-bottom: .5em;"><?php echo $curso['titulo']; ?></h3>
				<p class="linea-interna-semana"></p>
			</div>
			
			<div class="contenido" style="padding: 2em;">
              	<div class="app-obs-detalle sombra row" style="padding: 1em;">
	              	<div class="app-observaciones sombra">
	              		Observaciones
	              	</div>
	              	<p class="app-contenido-obs"><?php echo nl2br($semana['resumen']); ?></p>
	            </div>
          	</div>

			<div class="contenido" style="padding: 2em;">
              	<div class="app-profe-detalle sombra row" style="padding: 1em;">
              		<div class="col-md-12">
              			<h3>Lecciones</h3>
              		</div>
              		<div class="col-md-12">
              			<div class="panel panel-default">
						  	<div class="panel-heading" style="font-size: 20px;">
								<div class="row">
									<div class="col-md-6">Módulo: <?php echo $semana['titulo']; ?></div>
										<div class="col-md-6">
											<div class="progress" style="margin-bottom: 0">
											  <div id="modulo_<?php echo $semana['id']; ?>" class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar">
											    <span class=""></span>
											  </div>
											</div>
										</div>
								</div>
						  	</div>
						  	<div class="panel-body list-group">
						    	<table class="table table-hover">
									<tbody>
										<?php $activado = TRUE; $ultimo = FALSE; $vistos = 0; $totales = count($lecciones); ?>
										<?php foreach($lecciones as $k => $v): ?>
										<tr class="active">
											<td>
												<a <?php if($activado === TRUE):  ?>href="<?php echo base_url(); ?>sys_curso/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>/<?php echo $v['id']; ?>-<?php echo $v['alias']; ?>"<?php else: ?>href="javascript:;"<?php endif; ?> class="cp app-session">
													<i class="glyphicon glyphicon-<?php echo $_tipos[$v['tipo']]; ?>"></i> <?php echo $v['titulo']; ?> <?php if($v['tipo'] == 'archivo'): ?><?php $informacion = MY_Controller::obtener_informacion_archivo($v['archivo']); ?> (Tipo de Archivo: <strong><?php echo mb_strtoupper($informacion['tipo']); ?></strong> | Peso: <strong><?php echo $informacion['tamanio']; ?></strong>) <?php endif; ?>
													<span class="pull-right fs12"><?php echo $v['duracion']; ?></span>
													<!-- span class="badge success">new</span -->
												</a>
												<?php if($curso_asignado['id_leccion'] > $v['id'] AND $curso_asignado['id_leccion'] > 0): ?>
												<?php $activado = FALSE; ?>
												<?php endif; ?>

												<?php if($curso_asignado['id_leccion'] > $v['id']  AND $curso_asignado['id_leccion'] > 0): ?>
													<?php $vistos++; $ultimo = FALSE; $activado = TRUE; ?>
												<?php elseif($v['id'] == $curso_asignado['id_leccion']): ?>
													<?php $ultimo = TRUE; $vistos++; $activado = TRUE; ?>
												<?php else: ?>
													<?php $ultimo = FALSE; $activado = FALSE; ?>
												<?php endif; ?>
											</td>
										</tr>
										<?php endforeach; ?>
										<?php foreach($tests as $key => $value): ?>
										<tr>
											<td>
												<a <?php if($ultimo === TRUE): ?>href="<?php echo base_url(); ?>sys_test/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>/<?php echo $value['id']; ?>-<?php echo MY_Controller::limpiar_texto($value['titulo']); ?>"<?php else: ?>href="javascript:;"<?php endif; ?> class="cn app-session" data-toggle="tooltip" data-placement="top" title="Debes ver todos los videos antes">
													<i class="glyphicon glyphicon-star" ></i> <strong class="cn"><?php echo $value['titulo']; ?></strong>
													<span class="pull-right fs12">Duración: <?php echo $value['duracion']; ?></span>
												</a>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
              		</div>

              		<script type="text/javascript">
						var porcentaje = "<?php echo ($vistos / $totales * 100); ?>";
						$('#modulo_<?php echo $semana['id']; ?>').css('width', porcentaje + '%');
						$('#modulo_<?php echo $semana['id']; ?>').html(porcentaje + '%');
					</script>
              	</div>
			</div>
		</div>
	</div>
</div>

<!-- SCRIPTS --> 
<script src="<?php echo base_view(); ?>jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_view(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_view(); ?>js/modernizr.custom.js"></script>

<!-- time line -->
<script src="<?php echo base_view(); ?>plugins/timeline/js/jquery.mobile.custom.min.js"></script>
<script src="<?php echo base_view(); ?>plugins/timeline/js/main.js"></script> <!-- Resource jQuery -->
<!-- *********** -->

<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
})

//mostrar sidebar

$('.app-resp-open').click(function(){
	$('#sidebar-wrapper').css('display','block');
	$('#sidebar-wrapper').removeClass('slideOutLeft');
	$('#sidebar-wrapper').addClass('slideInLeft');
});
//ocultar sidebar
$('#app-resp-close').click(function(){
	$('#sidebar-wrapper').removeClass('slideInRight');
	$('#sidebar-wrapper').addClass('slideOutLeft');
});

</script>
</body>
</html>
