<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<link href="<?php echo base_view(); ?>/css/curso.css" rel="stylesheet" />
<body style="padding-top: 0!important;">
<!-- Fixed navbar -->
    <?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
<div id="wrapper" class="row">
  	<div id="sidebar-wrapper" class="col-md-2 col-xs-8 animated" >
  		<div style="position: absolute;top: 5em;right: 8px;">
        	<span id="app-resp-close" class="glyphicon glyphicon-remove-sign" style="display: block;"></span>
        </div>
        <div id="sidebar">
            <ul class="nav list-group text-center">
            	<h4 style="margin-top: .5em; margin-bottom: 2em;">Silabo</h4>
                <li  data-toggle="collapse" data-target="#modulo1" class="app-active" area-expanded="true">
                  <a href="#" class="cn active" data-toggle="tooltip" data-placement="top" title="Desplegar"><i class="fa fa-gift fa-lg"></i> <strong><?php echo $semana['titulo']; ?></strong> <i class="glyphicon glyphicon-chevron-down"></i></a>
                </li>
                <ul class="sub-menu collapse in" id="modulo1" area-expanded="true">
                    <?php foreach($lecciones as $key => $value): ?>
                    <li class="app-active">
                        <a class="list-group-item" href="<?php echo base_url(); ?>sys_curso/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><i class="icon-home icon-1x"></i><?php echo $value['titulo']; ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </ul>
        </div>
    </div>
    <div id="main-wrapper" class="col-md-10 pull-right col-xs-12" style="padding-top: 85px;">
        <div class="contenedor-video-tabla">
        	<div id="main" >
	        	<div class="row">
	        		<div class="col-md-12 text-center">
	        			<h3>Test - <?php echo $semana['titulo']; ?></h3>
	        		</div>
                    <?php $respuestas = explode("|", $calificacion['respuestas']); ?>
                    <form method="POST" action="<?php echo current_url(); ?>">
    	        		<?php foreach($preguntas as $key => $value): ?>
                        <input type="hidden" name="pregunta[<?php echo $key ?>]" value="<?php echo $value['id']; ?>">
    	        		<div class="col-md-12 mb1em">
    	        			<div class="col-md-2">
    	        				<div class="app-pregunta">
    	        					<p><strong>Pregunta <?php echo ($key + 1); ?></strong></p>
    	        					<p><?php echo $value['titulo']; ?></p>
    	        				</div>
    	        			</div>
    	        			<div class="col-md-10 pl0">
            					<div class="app-respuesta">
            					<p><strong>Respuesta:</strong></p>
                                    <?php echo ($respuestas[$key] != '') ? $respuestas[$key] : '----------------'; ?>
            					</div>
    	        			</div>
    	        		</div>
    	        		<?php endforeach; ?>
                        <div class="col-md-12 mb1em">
                            <input type="text" class="form-control" required name="nota" id="nota" />
                        </div>
    	        		<div class="col-md-12 mb1em" style="text-align: center;">
    	        			<button type="submit" class="btn btn-success">Colocar Nota</button>
    	        		</div>
                    </form>
	        	</div>
        	</div>

	        
        </div>
    </div>
</div>
<!-- SCRIPTS --> 
<script src="<?php echo base_view(); ?>js/jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_view(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_view(); ?>js/modernizr.custom.js"></script>
<!-- time line -->
<script src="<?php echo base_view(); ?>plugins/timeline/js/jquery.mobile.custom.min.js"></script>
<script src="<?php echo base_view(); ?>plugins/timeline/js/main.js"></script> <!-- Resource jQuery -->
</body>
</body>
</html>