<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>

<body>

<div class="content">
	
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>

	<div class="app-sidebar">
		<div class="container contenedor-respo">
			    <div class="row profile">
					<div class="col-md-3 col-sm-4 col-xs-12">
						<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
			            <div class="profile-content sombra">
						   	<div class="row">
						   		<div class="col-md-12 text-center">
						   			<h2>Inicio</h2>
						   			<ol class="breadcrumb">
									  <li>Inicio</li>
									</ol>
						   		</div>
								<!-- div class="col-md-12 app-rendimiento">
									<div class="profile-usertitle-job">
										Tu Progreso General
									</div>
									<div class="progress">
									  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
									    <span class="">40%</span>
									  </div>
									</div>
								</div -->

								<?php $message = $this->session->flashdata('message'); $comisiones_dolares = []; $comisiones_soles = []; $meses = array(1 => 'Enero', 2 => 'Febrero', 3 => 'Marzo', 4 => 'Abril', 5 => 'Mayo', 6 => 'Junio', 7 => 'Julio', 8 => 'Agosto', 9 => 'Setiembre', 10 => 'Octubre', 11 => 'Noviembre', 12 => 'Octubre'); ?>
								<?php if($message != ''): ?>
									<div class="col-md-12">
										<div class="alert alert-<?php echo $message['type']; ?>">
											<p><?php echo $message['content']; ?></p>
										</div>
									</div>
								<?php endif; ?>

								<?php if(MY_Controller::mostrar_session('nivel') == 2): ?>
								<div class="col-md-6">
									<div id="chart_div_dolares"></div>
									<div id="chart_div_soles"></div>
								</div>
								
						   		<div class="col-md-6">
									<div class="panel bg-info p-sombra">
									  <!-- Default panel contents -->
									  <div class="panel-heading bg-info cb">Cursos</div>
									  
									  <!-- List group -->
									  <ul class="list-group">
									  	<?php foreach($cursos as $key => $value): ?>
									    <li class="list-group-item" style="height: 60px;"><?php echo $value['titulo']; ?> <a href="<?php echo base_url(); ?>sys_curso_editar/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" class=""><span class="badge success pull-right">editar</span></a></li>
										<?php endforeach; ?>
									  </ul>
									</div>
						   		</div>

						   		<div class="col-md-12 text-center">
						   			<h2>Comisiones por Curso - Dólares</h2>
						   		</div>

						   		<div class="col-md-12" style="margin-bottom:20px;">
						   			<form method="POST" action="<?php echo base_url(); ?>sys_exportar_comisiones">
						   				<input type="hidden" name="moneda" value="_dolares" />
							   			Filtros: 
							   			<select name="mes" id="mes" onchange="javascript:filtrar_comision_dolares('mes', this);">
							   				<option value="">Seleccione un Mes</option>
							   				<?php foreach($meses as $key => $value): ?>
							   				<option value="<?php echo ($key < 10) ? ('0' . $key) : $key; ?>" <?php echo ($key == (int) date("m")) ? 'selected="selected"' : ''; ?>><?php echo $value; ?></option>
							   				<?php endforeach; ?>
							   			</select>

							   			<select name="anio" id="anio" onchange="javascript:filtrar_comision_dolares('anio', this);">
							   				<option value="">Seleccione un Año</option>
							   				<?php for($i=(date("Y") - 10);$i<=(date("Y") + 10);$i++): ?>
							   				<option value="<?php echo $i; ?>" <?php echo (date("Y") == $i) ? 'selected="selected"' : ''; ?>><?php echo $i; ?></option>
							   				<?php endfor; ?>
							   			</select>
							   			<button type="submit">Exportar Comisiones</button>
						   			</form>
						   		</div>

						   		<div class="col-md-12">
						   			<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								                <th>Curso</th>
								                <th>Cantidad de Alumnos</th>
								                <th>Comisión por Curso</th>
								                <th>Precio del Curso (con IGV)</th>
								                <th>Precio del Curso (sin IGV)</th>
								                <th>Comisión Total</th>
								            </tr>
								        </thead>
								        <tbody id="resultado_comision_dolares">
								        	<?php if(count($cursos) > 0): ?>
									        	<?php foreach($cursos as $key => $value): ?>
									        	<?php $alumnos = $this->module_model->seleccionar('cursos_asignados', array('moneda' => '_dolares', 'id_curso' => $value['id'], 'DATE_FORMAT(fecha_creacion, "%Y-%m") =' => date("Y-m"), 'estado' => 1, 'activado' => 1)); ?>
										        		<?php $cadena = ($value['id_padre'] == MY_Controller::mostrar_session('id')) ? 'comision' : 'comision_tutor'; $precio = $value['precio_dolares']; ?>
											        	<tr>
											                <td><?php echo $value['titulo']; ?></td>
											                <td><?php echo count($alumnos); ?></td>
											                <td><?php echo $value[$cadena]; ?>%</td>
											                <td>US$ <?php echo number_format($precio, 2); ?></td>
											                <td>US$ <?php echo number_format($precio / 1.18, 2); ?></td>
											                <?php $comision_total = (count($alumnos) * ($precio / 1.18) * ($value[$cadena] / 100)); $comisiones_dolares[] = array($value['titulo'], $comision_total); ?>
											                <td>US$ <?php echo number_format($comision_total, 2); ?></td>
											            </tr>
									       		<?php endforeach; ?>
									       	<?php else: ?>
										       	<tr>
													<td colspan="5" scope="row">No hay información para mostrar.</td>
												</tr>
											<?php endif; ?>
								        </tbody>
								    </table>
						   		</div>

						   		<div class="col-md-12 text-center">
						   			<h2>Comisiones por Curso - Soles</h2>
						   		</div>

						   		<div class="col-md-12" style="margin-bottom:20px;">
						   			<form method="POST" action="<?php echo base_url(); ?>sys_exportar_comisiones">
						   				<input type="hidden" name="moneda" value="_soles" />
							   			Filtros: 
							   			<select name="mes" id="mes" onchange="javascript:filtrar_comision_soles('mes', this);">
							   				<option value="">Seleccione un Mes</option>
							   				<?php foreach($meses as $key => $value): ?>
							   				<option value="<?php echo ($key < 10) ? ('0' . $key) : $key; ?>" <?php echo ($key == (int) date("m")) ? 'selected="selected"' : ''; ?>><?php echo $value; ?></option>
							   				<?php endforeach; ?>
							   			</select>

							   			<select name="anio" id="anio" onchange="javascript:filtrar_comision_soles('anio', this);">
							   				<option value="">Seleccione un Año</option>
							   				<?php for($i=(date("Y") - 10);$i<=(date("Y") + 10);$i++): ?>
							   				<option value="<?php echo $i; ?>" <?php echo (date("Y") == $i) ? 'selected="selected"' : ''; ?>><?php echo $i; ?></option>
							   				<?php endfor; ?>
							   			</select>
							   			<button type="submit">Exportar Comisiones</button>
						   			</form>
						   		</div>

						   		<div class="col-md-12">
						   			<table class="table table-striped table-bordered" cellspacing="0" width="100%">
								        <thead>
								            <tr>
								                <th>Curso</th>
								                <th>Cantidad de Alumnos</th>
								                <th>Comisión por Curso</th>
								                <th>Precio del Curso (con IGV)</th>
								                <th>Precio del Curso (sin IGV)</th>
								                <th>Comisión Total</th>
								            </tr>
								        </thead>
								        <tbody id="resultado_comision_soles">
								        	<?php if(count($cursos) > 0): ?>
									        	<?php foreach($cursos as $key => $value): ?>
									        	<?php $alumnos = $this->module_model->seleccionar('cursos_asignados', array('moneda' => '_soles', 'id_curso' => $value['id'], 'DATE_FORMAT(fecha_creacion, "%Y-%m") =' => date("Y-m"), 'estado' => 1, 'activado' => 1)); ?>
										        		<?php $cadena = ($value['id_padre'] == MY_Controller::mostrar_session('id')) ? 'comision' : 'comision_tutor'; $precio = $value['precio_soles']; ?>
											        	<tr>
											                <td><?php echo $value['titulo']; ?></td>
											                <td><?php echo count($alumnos); ?></td>
											                <td><?php echo $value[$cadena]; ?>%</td>
											                <td>S/ <?php echo number_format($precio, 2); ?></td>
											                <td>S/ <?php echo number_format($precio / 1.18, 2); ?></td>
											                <?php $comision_total = (count($alumnos) * (($precio / 1.18)) * ($value[$cadena] / 100)); $comisiones_soles[] = array($value['titulo'], $comision_total); ?>
											                <td>S/ <?php echo number_format($comision_total, 2); ?></td>
											            </tr>
									       		<?php endforeach; ?>
									       	<?php else: ?>
										       	<tr>
													<td colspan="5" scope="row">No hay información para mostrar.</td>
												</tr>
											<?php endif; ?>
								        </tbody>
								    </table>
						   		</div>
						   		<?php endif; ?>

								<?php if(MY_Controller::mostrar_session('nivel') == 3 OR MY_Controller::mostrar_session('nivel') < 2): ?>
						   		<div class="col-md-12">
									<div class="panel bg-info p-sombra">
									  <!-- Default panel contents -->
									  <div class="panel-heading bg-info cb">Cursos</div>
									  
									  <!-- List group -->
									  <ul class="list-group">
									  	<?php foreach($cursos as $key => $value): ?>
									  	<?php

							   				$semanas = $this->module_model->seleccionar('semanas', array('estado' => 1, 'id_padre' => $value['id'])); $cantidad_vistos = 0; $cantidad_lecciones = 0;

							   				foreach($semanas as $k => $v)
							   				{
							   					$lecciones = $this->module_model->seleccionar('lecciones', array('estado' => 1, 'id_padre' => $v['id']));

							   					foreach($lecciones as $a => $b)
							   					{
							   						if($cursos_asignados[$key]['id_leccion'] >= $b['id'])
								   					{
								   						$cantidad_vistos++;
								   					}

								   					$cantidad_lecciones++;
							   					}
							   				}

							   				$porcentaje = ($cantidad_lecciones > 0) ? ($cantidad_vistos / $cantidad_lecciones * 100) : 0;

							   			?>
									    <li class="list-group-item"><?php echo $value['titulo']; ?> - <a href="<?php echo base_url(); ?>sys_curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" class=""><strong>ir al curso</strong></a> <span class="badge danger"><?php echo number_format($porcentaje, 2); ?> %</span></li>
										<?php endforeach; ?>
									  </ul>
									</div>
						   		</div>
						   		<div class="col-md-6">
						   			<div class="panel bg-morado p-sombra">
									  <!-- Default panel contents -->
									  <div class="panel-heading cb">Exámenes</div>
									  <!-- List group -->
									  <ul class="list-group">
									  	<?php $logros = []; ?>
									  	<?php foreach($calificaciones as $key => $value): ?>
									  	<?php $test = $this->module_model->buscar('tests', $value['id_padre']); $semana = $this->module_model->buscar('semanas', $test['id_padre']); $curso = $this->module_model->buscar('cursos', $semana['id_padre']); ?>
									  	<?php $nota = ($test['tipo'] == 0) ? ($value['correctas'] / ($value['correctas'] + $value['incorrectas']) * 20) : $value['nota']; ?>
										  	<?php if((($nota > 0 AND $test['tipo'] == 1) OR $test['tipo'] == 0) AND count($curso) > 0): ?>
											  	<?php $logros[$curso['id']] = $nota; ?>
											    <li class="list-group-item"><?php echo $test['titulo']; ?> - <?php echo $curso['titulo']; ?> <span class="badge <?php echo ($nota > 13) ? 'success' : 'danger'; ?>"><?php echo $nota; ?></span></li>
											<?php endif; ?>
										<?php endforeach; ?>
									  </ul>
									</div>
						   		</div>
						   		<div class="col-md-6">
						   			<div class="panel success p-sombra">
									  <!-- Default panel contents -->
									  <div class="panel-heading cb">Logros</div>
									  <!-- List group -->
									  <ul class="list-group">
									  	<?php foreach($logros as $key => $value): ?>
									  	<?php if($nota > 13): ?>
										  	<?php $curso = $this->module_model->buscar('cursos', $key); ?>
										    <li class="list-group-item"><?php echo $curso['titulo']; ?> <span class="badge bg-info app-ver-medalla" data-toggle="modal" data-target=".medalla-<?php echo $key; ?>">ver</span></li>
											<!-- medallas -->
									   		<div class="modal fade medalla-<?php echo $key; ?>" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
											  <div class="modal-dialog modal-sm" role="document">
											    <div class="modal-content text-center">
											     	<img src="<?php echo base_view(); ?>img/medalla1.png" alt="">
											     	<p>Examen Aprobado</p>
											    </div>
											  </div>
											</div>
										<?php endif; ?>
										<?php endforeach; ?>
									  </ul>
									</div>
						   		</div>
								<?php endif; ?>
						   	</div>
			            </div>
					</div>
				</div>
			</div>
	</div>
</div>


<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>
<?php if(MY_Controller::mostrar_session('nivel') == 2): ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	var xnmes_dolares = '<?php echo date("m"); ?>'; var xnanio_dolares = '<?php echo date("Y"); ?>';
	var xnmes_soles = '<?php echo date("m"); ?>'; var xnanio_soles = '<?php echo date("Y"); ?>';

	var xnId = '<?php echo MY_Controller::mostrar_session('id'); ?>';

	function filtrar_comision_dolares(type, element)
	{
		eval('xn' + type + '_dolares = ' + $(element).val());

		$.ajax({
			url: "<?php echo base_url(); ?>sys_busqueda_reporte",
			type: 'POST',
			data: { mes: xnmes_dolares, anio: xnanio_dolares, moneda: '_dolares' },
			dataType: 'json',
			success: function(data){
				var html = '';

				$.each(data['cursos'], function(i, item){
					eval("var cadena = 'precio_dolares'"); eval("var cadena_comision = '" + ((item['id_padre'] == xnId) ? 'comision' : 'comision_tutor') + "'");

					var comision = parseFloat(data['alumnos'][item['id']].length * ((item[cadena] - (item[cadena] * 0.18)) * item[cadena_comision] / 100)).toFixed(2);

					html += '<tr>';
					html += '<td>' + item['titulo'] + '</td>';
					html += '<td>' + data['alumnos'][item['id']].length + '</td>';
					html += '<td>' + item[cadena_comision] + '%</td>';
					html += '<td>US$ ' + parseFloat(item[cadena]).toFixed(2) + '</td>';
					html += '<td>US$ ' + parseFloat((item[cadena] - (item[cadena] * 0.18))).toFixed(2) + '</td>';
					html += '<td>US$ ' + comision + '</td>';
					html += '</tr>';
				});

				$('#resultado_comision_dolares').html(html);
			},
			error: function(data){
				console.log(data);
			}
		});
	}

	function filtrar_comision_soles(type, element)
	{
		eval('xn' + type + '_soles = ' + $(element).val());

		$.ajax({
			url: "<?php echo base_url(); ?>sys_busqueda_reporte",
			type: 'POST',
			data: { mes: xnmes_soles, anio: xnanio_soles, moneda: '_soles' },
			dataType: 'json',
			success: function(data){
				var html = '';

				$.each(data['cursos'], function(i, item){
					eval("var cadena = 'precio_soles'"); eval("var cadena_comision = '" + ((item['id_padre'] == xnId) ? 'comision' : 'comision_tutor') + "'");

					var comision = parseFloat(data['alumnos'][item['id']].length * ((item[cadena] - (item[cadena] * 0.18)) * item[cadena_comision] / 100)).toFixed(2);

					html += '<tr>';
					html += '<td>' + item['titulo'] + '</td>';
					html += '<td>' + data['alumnos'][item['id']].length + '</td>';
					html += '<td>' + item[cadena_comision] + '%</td>';
					html += '<td>S/ ' + parseFloat(item[cadena]).toFixed(2) + '</td>';
					html += '<td>S/ ' + parseFloat((item[cadena] - (item[cadena] * 0.18))).toFixed(2) + '</td>';
					html += '<td>S/ ' + comision + '</td>';
					html += '</tr>';
				});

				$('#resultado_comision_soles').html(html);
			},
			error: function(data){
				console.log(data);
			}
		});
	}

	// Load the Visualization API and the corechart package.
	google.charts.load('current', {'packages':['corechart']});

	// Set a callback to run when the Google Visualization API is loaded.
	google.charts.setOnLoadCallback(drawChart_soles);

	google.charts.setOnLoadCallback(drawChart_dolares);

	// Callback that creates and populates a data table,
	// instantiates the pie chart, passes in the data and
	// draws it.
	function drawChart_soles() {

		var comisiones_soles = <?php echo json_encode($comisiones_soles); ?>;

		if(comisiones_soles.length > 0)
		{
			$('#chart_div_soles').removeClass('hidden');

			var data = new google.visualization.DataTable();
			data.addColumn('string', 'Topping');
			data.addColumn('number', 'Slices');
			data.addRows(<?php echo json_encode($comisiones_soles); ?>);

			// Set chart options
			var options = {'title':'Reporte de Comisiones - Soles','width':400,'height':300};

			// Instantiate and draw our chart, passing in some options.
			var chart = new google.visualization.PieChart(document.getElementById('chart_div_soles'));
			chart.draw(data, options);
		}
		else
		{
			$('#chart_div_soles').addClass('hidden');
		}
	}

	function drawChart_dolares() {

		var comisiones_dolares = <?php echo json_encode($comisiones_dolares); ?>;

		if(comisiones_dolares.length > 0)
		{
			$('#chart_div_dolares').removeClass('hidden');

			var data = new google.visualization.DataTable();
			data.addColumn('string', 'Topping');
			data.addColumn('number', 'Slices');
			data.addRows(<?php echo json_encode($comisiones_dolares); ?>);

			// Set chart options
			var options = {'title':'Reporte de Comisiones - Dólares','width':400,'height':300};

			// Instantiate and draw our chart, passing in some options.
			var chart = new google.visualization.PieChart(document.getElementById('chart_div_dolares'));
			chart.draw(data, options);
		}
		else
		{
			$('#chart_div_dolares').addClass('hidden');
		}
	}
</script>
<?php endif; ?>