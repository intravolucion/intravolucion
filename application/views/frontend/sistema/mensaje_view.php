<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
		<div class="container contenedor-respo">
			    <div class="row profile">
					<div class="col-md-3 col-sm-4 col-xs-12">
						<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
			            <div class="profile-content sombra">
						   	<div class="row">
						   		<div class="col-md-12 text-center">
						   			<h2>Mensajes</h2>
						   			<ol class="breadcrumb">
									  <li><a href="<?php echo base_url(); ?>sys_dashboard">Inicio</a></li>
									  <li>mensajes</li>
									</ol>
						   		</div>
								<div class="col-md-12 app-enviar-chat">
									<div class="profile-usertitle-job">
										<a class="btn btn-warning btn-xs" href="<?php echo base_url(); ?>sys_mensajes">Regresar</a> Conversación con <?php echo $emisor['nombres']; ?> <?php echo $emisor['apellidos']; ?>
									</div>
								</div>
						   		<div class="col-md-12" id="discusion" style="height:300px;overflow:auto;">
						   			<?php foreach($mensajes as $key => $value): ?>
						   				<?php $emisor = $this->module_model->buscar('administrador', $value['id_emisor']); ?>
										<div class="panel panel-default">
										  <div class="panel-body">
										  	<div class="row">
										  		<div class="col-md-3 text-center">
										  			<img src="<?php echo base_url(); ?>uploads/100x100/<?php echo $emisor['imagen']; ?>" alt="" style="width: 100px;border-radius: 50%">
										  		</div>
										  		<div class="col-md-9">
										  			<p style="color: #337ab7"><strong><?php echo $emisor['nombres']; ?> <?php echo $emisor['apellidos']; ?></strong></p>
										  			<!-- p style="color: #999"><i class="glyphicon glyphicon-time"></i> Hace 2 min</p -->
											    	<p><?php echo $value['contenido']; ?></p>
											    </div>
										  	</div>
										  </div>						  
										</div>
									<?php endforeach; ?>
						   		</div>
						   		<div class="col-md-12">
						   			<form method="POST" action="<?php echo current_url(); ?>" autocomplete="off">
						   			<div class="input-group input-group-lg">
									  <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-envelope"></i></span>
									  <input autocomplete="off" type="text" required class="form-control" placeholder="Escribir un mensaje.." aria-describedby="sizing-addon1" name="contenido" focus>
									</div>
									</form>
						   		</div>
						   	</div>
			            </div>
					</div>
				</div>
			</div>
	</div>
</div>

<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>
<script>
	$("#discusion").scrollTop($("#discusion")[0].scrollHeight);
</script>