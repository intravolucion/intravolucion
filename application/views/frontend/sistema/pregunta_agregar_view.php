<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
	<div class="container">
    	<div class="row profile">
		<div class="col-md-3">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Curso</h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li><a href="<?php echo base_url(); ?>sys_cursos" class="">Curso</a></li>
						  <li>Lecciones</li>
						  <li>Agregar</li>
						</ol>
			   		</div>
					
			   		<div class="col-md-12">
			   			<?php if(isset($mensaje)): ?>
	                    <div class="alert alert-success">
	                        <?php echo $mensaje; ?>
	                    </div>
	                    <?php endif; ?>

						<ul class="nav nav-tabs">
						  <li class="active"><a data-toggle="tab" href="#datos_curso">Preguntas</a></li>
						  <li><a href="<?php echo base_url(); ?>sys_examen_editar/<?php echo $examen['id']; ?>">Regresar</a></li>
						</ul>
						<div class="tab-content">
							<div class="panel panel-default p-sombra">
								<div class="panel-heading">Datos de la Pregunta</div>
							  	<div class="panel-body">
								    <div class="row">
								    	<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?php echo current_url(); ?>">
											<fieldset>
												<div class="form-group">
												  <label class="col-md-3 control-label" for="titulo">Titulo</label>  
												  <div class="col-md-8">
												  <input id="titulo" name="titulo" type="text" class="form-control input-md" required="" value="<?php echo @$pregunta['titulo']; ?>">
												  </div>
												</div>

												<?php if($examen['tipo'] == 0): ?>

												<?php if(@$pregunta['opciones'] != ''): ?>
													<?php $opciones = explode("|", $pregunta['opciones']); ?>
													<?php foreach($opciones as $key => $value): ?>
														<div class="form-group">
														  <label class="col-md-3 control-label" for="opcion">Opción</label>  
														  <div class="col-md-8">
														  	<input type="text" id="opcion" name="opciones[]" class="form-control" value="<?php echo $value; ?>" /><br />
														  	<button type="button" onclick="javascript:remover_opcion(this);" class="btn btn-danger btn-md">Quitar esta Opción</button>
														  </div>
														</div>
													<?php endforeach; ?>
												<?php else: ?>
													<div class="form-group">
													  <label class="col-md-3 control-label" for="opcion">Opción</label>  
													  <div class="col-md-8">
													  	<input type="text" id="opcion" name="opciones[]" class="form-control" value="" />
													  </div>
													</div>
												<?php endif; ?>

												<div id="opciones_adicionales"></div>

												<div class="form-group">
													<div class="col-md-3"></div>
													<div class="col-md-9">
														<button type="button" onclick="javascript:agregar_opcion(this);" class="btn btn-primary btn-md">Agregar una Opción</button>
													</div>
												</div>

												<div class="form-group">
												  <label class="col-md-3 control-label" for="contenido">Respuesta</label>  
												  	<div class="col-md-8">
													  <input type="text" id="respuesta" name="respuesta" class="form-control" value="<?php echo @$pregunta['respuesta']; ?>" />
													</div>
									    		</div>
									    		<?php else: ?>
									    			<div class="alert alert-warning">Este examen es subjetivo, por lo tanto no hay respuestas correctas o incorrectas. El profesor calificará este examen.</div>
									    		<?php endif; ?>

												<div class="form-group">
												    <div class="col-md-12" style="text-align: right;right: 5em;">
												    	<button type="submit" class="btn success cb"><strong>Guardar</strong></button>
												    </div>
												</div>
											</fieldset>
										</form>
								  	</div>						  
								</div>
							</div>
						</div>
			   		</div>
            	</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view('frontend/sistema/templates/footer_view.php') ?>

<script type="text/javascript">

function agregar_opcion(element)
{
	var html = '<div class="form-group">';
	html += '<label class="col-md-3 control-label" for="opcion">Opción</label>';
	html += '<div class="col-md-8">';
	html += '<input type="text" id="opcion" name="opciones[]" class="form-control" value="" /><br />';
	html += '<button type="button" onclick="javascript:remover_opcion(this);" class="btn btn-danger btn-md">Quitar esta Opción</button>';
	html += '</div>';
	html += '</div>';

	$('#opciones_adicionales').append(html);
}

function remover_opcion(element)
{
	$(element).parent().parent('.form-group').remove();
}

CKEDITOR.replace("editor");
</script>
