<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>

<body>

<div class="content">
	
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>

	<div class="app-sidebar">
		<div class="container contenedor-respo">
		    <div class="row profile">
				<div class="col-md-3 col-sm-4 col-xs-12">
					<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
				</div>
				<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
		            <div class="profile-content sombra">
					   	<div class="row">
					   		<div class="col-md-12 text-center">
					   			<h2>Tus Cursos</h2>
					   			<ol class="breadcrumb">
								  <li><a href="<?php echo base_url(); ?>sys_dashboard">Inicio</a></li>
								  <li>Cursos</li>
								</ol>
					   		</div>

					   		<?php if(MY_Controller::mostrar_session('nivel') == 2): ?>
					   		<div class="col-md-12 app-rendimiento">
								<div class="profile-usertitle-job">
									<a href="<?php echo base_url(); ?>sys_curso_agregar" class="btn success cb "><i class="glyphicon glyphicon-plus-sign"></i> Agregar</a>
								</div>
							</div>
							
					   		<div class="col-md-12">
					   			<?php foreach($cursos as $key => $value): ?>
					   			<?php $profesor = $this->module_model->buscar('administrador', $value['id_padre']); ?>
					   			<?php if(count($profesor) > 0): ?>
								<a href="<?php echo base_url(); ?>sys_curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" style="text-decoration: none; color: #4F4F4F;">
									<div class="panel panel-default p-sombra">
									  <div class="panel-heading">
									  	<div class="row">
									  		<div class="col-md-12"><?php echo $value['titulo']; ?></div>
									  	</div>

									  </div>

									  <div class="panel-body">
									  	<div class="row">
									  		<div class="col-md-3 text-center">
									  			<img src="<?php echo base_url(); ?>uploads/<?php echo $value['logo']; ?>" alt="" style="width: 100px;">
									  			<h4><?php echo $value['titulo']; ?></h4>
									  		</div>
									  		<div class="col-md-9">
										    	<p><?php echo nl2br($value['resumen']); ?></p>
										    	<p>
										    		<strong>Profesor:</strong> <?php if(count($profesor) > 0): ?><?php echo @$profesor['nombres'] ?> <?php echo @$profesor['apellidos']; ?><?php else: ?>--<?php endif; ?>
										    	</p>
										    	<p>
										    		<strong>Comisión por alumno registrado:</strong> <?php echo $value['comision']; ?>%
										    	</p>
										    	<p>
										    		<strong>Comisión Tutor por alumno registrado:</strong> <?php echo $value['comision_tutor']; ?>%
										    	</p>
										    	<?php $status = array('Pendiente de Publicación', 'Publicado'); ?>
										    	<p>
										    		<strong>Estado:</strong> <?php echo $status[$value['activado']]; ?>
										    	</p>
										    </div>

											<div class="col-md-12 text-right">
												<a href="javascript:;" onclick="javascript:eliminar('<?php echo $value['id']; ?>', 'cursos', this);" class="btn btn-danger">Eliminar</a>
												<a href="<?php echo base_url(); ?>sys_curso_editar/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" class="btn btn-info">Editar</a>
											</div>
									  	</div>
									  </div>						  
									</div>
								</a>
								<?php endif; ?>
								<?php endforeach; ?>
					   		</div>
					   		<?php endif; ?>

					   		<?php if(MY_Controller::mostrar_session('nivel') == 3 OR MY_Controller::mostrar_session('nivel') < 2): ?>
							<!-- div class="col-md-12 app-rendimiento">
								<div class="profile-usertitle-job">
									Tu Progreso General
								</div>
								<div class="progress">
								  <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
								    <span class="">40%</span>
								  </div>
								</div>
							</div -->
							
							<?php foreach($cursos as $key => $value): ?>
							<?php

				   				$semanas = $this->module_model->seleccionar('semanas', array('estado' => 1, 'id_padre' => $value['id'])); $cantidad_vistos = 0; $cantidad_lecciones = 0;

				   				foreach($semanas as $k => $v)
				   				{
				   					$lecciones = $this->module_model->seleccionar('lecciones', array('estado' => 1, 'id_padre' => $v['id']));

				   					foreach($lecciones as $a => $b)
				   					{
				   						if($b['id'] <= $cursos_asignados[$key]['id_leccion'])
					   					{
					   						$cantidad_vistos++;
					   					}

					   					$cantidad_lecciones++;
				   					}
				   				}

				   				$porcentaje = ($cantidad_lecciones > 0) ? ($cantidad_vistos / $cantidad_lecciones * 100) : 0;

				   			?>
							<?php $profesor = $this->module_model->buscar('administrador', $value['id_padre']); ?>
							<?php if(count($profesor) > 0): ?>
					   		<div class="col-md-12">
								<a href="<?php echo base_url(); ?>sys_curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" style="text-decoration: none; color: #4F4F4F;">
									<div class="panel panel-default p-sombra">
									  <div class="panel-heading">
									  	<div class="row">
									  		<div class="col-md-6"><?php echo $value['titulo']; ?></div>
											<div class="col-md-6">
												<div class="progress" style="margin-bottom: 0">
												  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" style="width: <?php echo number_format($porcentaje, 2); ?>%; ">
												    <span class=""><?php echo number_format($porcentaje, 2); ?>%</span>
												  </div>
												</div>
											</div>
									  	</div>

									  </div>

									  <div class="panel-body">
									  	<div class="row">
									  		<div class="col-md-3 text-center">
									  			<img src="<?php echo base_url(); ?>uploads/<?php echo $value['logo']; ?>" alt="" style="width: 100px;">
									  			<h4><?php echo $value['titulo']; ?></h4>
									  		</div>
									  		<div class="col-md-9">
										    	<p><?php echo nl2br($value['resumen']); ?></p>
										    	<p>
										    		<strong>Profesor:</strong> <?php echo @$profesor['nombres']; ?> <?php echo @$profesor['apellidos']; ?>
										    	</p>
										    	<?php if(date("Y-m-d") >= date("Y-m-d", strtotime($value['fecha_inicio']))): ?>
											    	<p>
											    		<strong>Tiempo de Disponibilidad:</strong> <span id="tiempo_<?php echo $value['id']; ?>"></span>
											    	</p>
											    	<?php $fecha = strtotime($value['fecha_fin']); ?>
											    <?php else: ?>
											    	<p>
											    		<strong>Disponible el <?php echo date("d-m-Y", strtotime($value['fecha_inicio'])); ?></strong>
											    	</p>
											    <?php endif; ?>
										    </div>
									  	</div>
									  </div>						  
									</div>
								</a>
					   		</div>
					   		<?php if(date("Y-m-d") >= date("Y-m-d", strtotime($value['fecha_inicio']))): ?>
					   		<script>

								function countdown_<?php echo $value['id']; ?>(id)
								{
								    var fecha=new Date(<?php echo date("Y", $fecha); ?>,'<?php echo ((int) date("m", $fecha)) - 1; ?>','<?php echo date("d", $fecha); ?>','00','00','00')
								    var hoy=new Date()
								    var dias=0
								    var horas=0
								    var minutos=0
								    var segundos=0

								    if (fecha>hoy)
								    {
								        var diferencia=(fecha.getTime()-hoy.getTime())/1000
								        dias=Math.floor(diferencia/86400)
								        diferencia=diferencia-(86400*dias)
								        horas=Math.floor(diferencia/3600)
								        diferencia=diferencia-(3600*horas)
								        minutos=Math.floor(diferencia/60)
								        diferencia=diferencia-(60*minutos)
								        segundos=Math.floor(diferencia)

								        document.getElementById(id).innerHTML='Quedan ' + dias + ' D&iacute;as, ' + horas + ' Horas, ' + minutos + ' Minutos, ' + segundos + ' Segundos'

								        if (dias>0 || horas>0 || minutos>0 || segundos>0){
								            setTimeout("countdown_<?php echo $value['id']; ?>(\"" + id + "\")",1000)
								        }
								    }
								    else
								    {
								        document.getElementById('restante').innerHTML='Quedan ' + dias + ' D&iacute;as, ' + horas + ' Horas, ' + minutos + ' Minutos, ' + segundos + ' Segundos'
								    }
								}

								countdown_<?php echo $value['id']; ?>('tiempo_<?php echo $value['id']; ?>');
							</script>
							<?php endif; ?>
							<?php endif; ?>
					   		<?php endforeach; ?>
					   		<?php endif; ?>
					   	</div>
		            </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>