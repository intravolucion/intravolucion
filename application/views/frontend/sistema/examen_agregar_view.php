<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
	<div class="container">
    	<div class="row profile">
		<div class="col-md-3">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Curso</h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li><a href="<?php echo base_url(); ?>sys_cursos" class="">Curso</a></li>
						  <li>Exámenes</li>
						  <li>Agregar</li>
						</ol>
			   		</div>
					
			   		<div class="col-md-12">
			   			<?php if(isset($mensaje)): ?>
	                    <div class="alert alert-success">
	                        <?php echo $mensaje; ?>
	                    </div>
	                    <?php endif; ?>

						<ul class="nav nav-tabs">
						  <li class="active"><a data-toggle="tab" href="#datos_curso">Exámenes</a></li>
						  <li><a href="<?php echo base_url(); ?>sys_examenes/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>">Regresar</a></li>
						</ul>
						<div class="tab-content">
							<div class="panel panel-default p-sombra">
								<div class="panel-heading">Datos del Exámen</div>
							  	<div class="panel-body">
								    <div class="row">
								    	<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?php echo current_url(); ?>">
											<fieldset>
												<div class="form-group">
												  <label class="col-md-3 control-label" for="titulo">Titulo</label>  
												  <div class="col-md-8">
												  <input id="titulo" name="titulo" type="text" class="form-control input-md" required="" value="<?php echo @$examen['titulo']; ?>">
												  </div>
												</div>

												<div class="form-group">
												  <label class="col-md-3 control-label" for="tipo">Tipo de Exámen</label>  
												  	<div class="col-md-8">
													  <select name="tipo" id="tipo" required="" class="form-control input-md">
													  	<option value="">Seleccione un tipo</option>
													  	<option value="0"<?php echo (@$examen['tipo'] == 0) ? ' selected="selected"' : ''; ?>>Objetivo</option>
													  	<option value="1"<?php echo (@$examen['tipo'] == 1) ? ' selected="selected"' : ''; ?>>Subjetivo</option>
													  </select>
													</div>
									    		</div>

									    		<div class="form-group">
												  <label class="col-md-3 control-label" for="duracion">Duración</label>  
												  <div class="col-md-8">
												  	<input id="duracion" name="duracion" type="text" class="form-control input-md" required="" value="<?php echo @$examen['duracion']; ?>">
												  </div>
												</div>

												<div class="form-group">
												  <label class="col-md-3 control-label" for="intentos">Intentos</label>  
												  <div class="col-md-8">
												  	<input id="intentos" name="intentos" type="text" class="form-control input-md" required="" value="<?php echo @$examen['intentos']; ?>">
												  </div>
												</div>

												<div class="form-group">
												    <div class="col-md-12" style="text-align: right;right: 5em;">
												    	<button type="submit" class="btn success cb"><strong>Guardar</strong></button>
												    </div>
												</div>
											</fieldset>
										</form>
								  	</div>
								  	<div class="row">
								  		<?php if(isset($examen)): ?>
								  			<div class="col-md-12 mb1em">
										    	<a href="<?php echo base_url(); ?>sys_pregunta_agregar/<?php echo $examen['id']; ?>" class="btn success cb " style="margin-top: 1em;"><i class="glyphicon glyphicon-plus-sign"></i> Agregar una Pregunta</a>
										    </div>
									  		<?php foreach($preguntas as $key => $value): ?>
										  		<div class="col-md-12">
													<a href="<?php echo base_url(); ?>sys_pregunta_editar/<?php echo $value['id']; ?>" style="text-decoration: none; color: #4F4F4F;">
														<div class="panel panel-default p-sombra">
														  <div class="panel-heading">
															<div class="row">
														  		<div class="col-md-6"><?php echo $value['titulo']; ?></div>
																<div class="col-md-6 text-right">
																	<a href="<?php echo base_url(); ?>sys_pregunta_editar/<?php echo $value['id']; ?>"><i class="glyphicon glyphicon-pencil"></i> Editar</a> <a href="javascript:;" onclick="javascript:eliminar('<?php echo $value['id']; ?>', 'preguntas', this);"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
																</div>
														  	</div>
														  </div>
														  <div class="panel-body">
														  	<div class="row">
														  		<div class="col-md-12">
															    	<p class="pull-left">
															    		<strong>Fecha Creación:</strong> <?php echo MY_Controller::fecha_muestra($value['fecha_creacion']); ?><br />
															    		<strong>Fecha Modificación:</strong> <?php echo MY_Controller::fecha_muestra($value['fecha_modificacion']); ?>
															    	</p>
															    </div>
														  	</div>
														  </div>						  
														</div>
													</a>
										   		</div>
										   	<?php endforeach; ?>
										<?php endif; ?>
								  	</div>
								</div>
							</div>
						</div>
			   		</div>
            	</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view('frontend/sistema/templates/footer_view.php') ?>

<script type="text/javascript">
CKEDITOR.replace("editor");
</script>
