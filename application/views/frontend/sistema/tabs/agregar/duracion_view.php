<div class="panel panel-default p-sombra">
	<div class="panel-heading">Duración del Curso</div>
  	<div class="panel-body">
	    <div class="row">
	    	<form class="form-horizontal respo-form-edit-perf" action="<?php echo current_url(); ?>" method="POST">
				<fieldset>
					<div class="form-group">
					  <label class="col-md-3 control-label" for="categoria">Categoría</label>  
					  <div class="col-md-8">
					  <select name="categoria" class="form-control" id="categoria" required>
					  	<option value="">Seleccione una categoría</option>
					  	
					  		<option> opcion </option>
					  	
					  </select>
					  </div>
					</div>
					<div class="form-group">
					  <label class="col-md-3 control-label" for="Empresa">Duración</label>  
					  <div class="col-md-8">
					  	<input type="text" name="duracion" class="form-control">
					  </div>
					</div>
					<div class="form-group">
					  <label class="col-md-3 control-label" for="fecha_inicio">Inicio</label>  
					  	<div class="col-md-8">
						  	<input type="text" class="app-fecha form-control" id="fecha_inicio" name="fecha_inicio" value="">
						</div>
		    		</div>
		    		<div class="form-group">
					  <label class="col-md-3 control-label" for="Puesto">Fin</label>  
					  	<div class="col-md-8">
						  	<input type="text" class="app-fecha form-control">
						</div>
		    		</div>
					<div class="form-group">
					    <div class="col-md-4 pull-right">
					    	<button type="submit" class="btn success cb"><strong>Guardar</strong></button>
					    </div>
					</div>
				</fieldset>
			</form>
	  	</div>						  
	</div>
	
</div>
