<div class="panel panel-default p-sombra">
	<div class="panel-heading">
		Semanas del Curso
	</div>
  	<div class="panel-body">
	    <div class="row">
		    <div class="col-md-12 mb1em">
		    	<a href="<?php echo base_url(); ?>sys_semana_agregar/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>" class="btn success cb "><i class="glyphicon glyphicon-plus-sign"></i> Agregar</a>
		    </div>
		    <?php if(isset($semanas) && count($semanas) > 0): ?>
		    <?php foreach($semanas as $key => $value): ?>
		    <?php $cantidad = $this->module_model->total('lecciones', array('estado' => 1, 'id_padre' => $value['id'])); ?>
	    	<div class="col-md-12">
				<a href="<?php echo base_url(); ?>sys_semana_editar/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>" style="text-decoration: none; color: #4F4F4F;">
					<div class="panel panel-default p-sombra">
					  <div class="panel-heading">
						<div class="row">
					  		<div class="col-md-6"><?php echo $value['titulo']; ?> <small>(<?php echo $cantidad; ?> lecci<?php echo ($cantidad > 1) ? 'ones' : 'ón'; ?>)</small></div>
							<div class="col-md-6 text-right">
								<a href="<?php echo base_url(); ?>sys_semana_editar/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><i class="glyphicon glyphicon-pencil"></i> Editar</a> <a href="javascript:;" onclick="javascript:eliminar('<?php echo $value['id']; ?>', 'semanas', this);"><i class="glyphicon glyphicon-trash"></i> Eliminar</a>
							</div>
					  	</div>
					  </div>
					  <div class="panel-body">
					  	<div class="row">
					  		<div class="col-md-12">
						    	<p><?php echo nl2br($value['resumen']); ?></p>
						    	<p class="pull-left">
						    		<strong>Fecha Creación:</strong> <?php echo MY_Controller::fecha_muestra($value['fecha_creacion']); ?><br />
						    		<strong>Fecha Modificación:</strong> <?php echo MY_Controller::fecha_muestra($value['fecha_modificacion']); ?>
						    	</p>
						    </div>
					  	</div>
					  </div>						  
					</div>
				</a>
	   		</div>
	   		<?php endforeach; ?>
	   		<?php else: ?>
	   		<br style="clear:both;" />
	   		<div class="col-md-12">
	   			<div class="alert alert-warning">No se han agregado semanas al curso.</div>
	   		</div>
	   		<?php endif; ?>
	  	</div>						  
	</div>
	
</div>
