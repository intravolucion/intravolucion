<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
		<div class="container contenedor-respo">
		    <div class="row profile">
				<div class="col-md-3 col-sm-4 col-xs-12">
					<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
				</div>
				<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
		            <div class="profile-content">
					   	<div class="row">
					   		<div class="col-md-12 text-center" style="background: url('<?php echo base_url(); ?>uploads/<?php echo $curso['banner']; ?>'); padding: 2em 1em;">
					   			<h2 class="cb"><img src="<?php echo base_url(); ?>uploads/<?php echo $curso['logo']; ?>" alt="" style="width: 160px;"></h2>
					   			<ol class="breadcrumb">
								  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Home</strong></a></li>
								  <li><a href="<?php echo base_url(); ?>sys_cursos">Cursos</a></li>
								  <li class="active"><?php echo $curso['titulo']; ?></li>
								</ol>
					   		</div>
							<div class="col-md-12 app-rendimiento">
								<!-- div class="panel panel-default" style="margin-top: 2em;padding: 0 1em;">
									<div class="profile-usertitle-job mt1em">
										Tu Progreso General
									</div>
									<div class="progress">
									  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
									    <span class="">20%</span>
									  </div>
									</div>
								</div -->
								<br /><br />
							</div>
					   		<div class="col-md-12">
								<div class="panel panel-info">
								  <!-- Default panel contents -->
								  <div class="panel-heading">Temas</div>
								  <!-- List group -->
								  <ul class="list-group">
								  	<?php foreach($semanas as $key => $value): ?>
										<?php foreach($lecciones[$key] as $k => $v): ?>
											<li class="list-group-item"><a href="<?php echo base_url(); ?>sys_curso/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>/<?php echo $v['id']; ?>-<?php echo $v['alias']; ?>" class="text-right"><?php echo $v['titulo']; ?></a> <span class="badge">Duración: <?php echo $v['duracion']; ?></span></li>
										<?php endforeach; ?>
									<?php endforeach; ?>
								  </ul>
								</div>
					   		</div>
					   	</div>
		            </div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- SCRIPTS --> 
<script src="<?php echo base_view(); ?>js/jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_view(); ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_view(); ?>js/modernizr.custom.js"></script>
</body>
</html>