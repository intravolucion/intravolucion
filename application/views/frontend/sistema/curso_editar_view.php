<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
	<div class="container">
    	<div class="row profile">
		<div class="col-md-3">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Curso</h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li><a href="<?php echo base_url(); ?>sys_cursos" class="">Curso</a></li>
						  <li>Editar</li>
						</ol>
			   		</div>
					
			   		<div class="col-md-12">
			   			<?php if(isset($mensaje)): ?>
	                    <div class="alert alert-success">
	                        <?php echo $mensaje; ?>
	                    </div>
	                    <?php endif; ?>

						<ul class="nav nav-tabs">
						  <li class="active"><a data-toggle="tab" href="#datos_curso">Datos del Curso</a></li>
						  <li><a data-toggle="tab" href="#duracion_curso">Duracion</a></li>
						  <?php if(isset($curso) && count($curso) > 0): ?>
						  <li><a data-toggle="tab" href="#lecciones_curso">Semanas</a></li>
						  <?php endif; ?>
						</ul>
						<div class="tab-content">
							<div id="datos_curso" class="tab-pane fade in active">
								<?php $this->load->view('frontend/sistema/tabs/datos_curso_view.php'); ?>
							</div>
							<div id="duracion_curso" class="tab-pane fade">
								<?php $this->load->view('frontend/sistema/tabs/duracion_view.php'); ?>
							</div>
							<?php if(isset($curso) && count($curso) > 0): ?>
							<div id="lecciones_curso" class="tab-pane fade">
								<?php $this->load->view('frontend/sistema/tabs/lecciones_view.php'); ?>
							</div>
							<?php endif; ?>
						</div>
						
			   		</div>
            	</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view('frontend/sistema/templates/footer_view.php') ?>
<script type=”text/javascript”>
CKEDITOR.replace("editor");
</script>
