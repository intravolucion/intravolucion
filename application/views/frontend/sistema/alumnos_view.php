<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
		<div class="container contenedor-respo">
    <div class="row profile">
		<div class="col-md-3 col-sm-4 col-xs-12">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Alumnos por Cursos</h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li>Alumnos por Cursos</li>
						</ol>
			   		</div>
					
			   		<div class="col-md-12">
			   			<?php foreach($cursos as $key => $value): ?>
			   			<?php $alumnos = $this->module_model->total('cursos_asignados', array('id_curso' => $value['id'], 'estado' => 1, 'activado' => 1)); ?>
						<div class="col-md-6">
							<a href="<?php echo base_url(); ?>sys_alumnos_curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>">
								<div class="panel panel-default p-sombra">
								  <div class="panel-body">
								  	<div class="row">
								  		<div class="col-md-4 text-center">
								  			<img src="<?php echo base_url(); ?>uploads/100x100/<?php echo $value['logo']; ?>" alt="" style="width: 100px;">
								  		</div>
								  		<div class="col-md-8">
								  			<p style="color: #337ab7"><strong><?php echo $value['titulo']; ?></strong></p>
								  			<p style="color: #999"><i class="glyphicon glyphicon-user"></i> <?php echo $alumnos; ?> alumno<?php echo (($alumnos > 0) ? 's' : ''); ?></p>
									    	<!-- p><i class="glyphicon glyphicon-comment"></i> 2 Necesitan ayuda</p -->
									    </div>
								  	</div>
								  </div>
								</div>
							</a>
						</div>
						<?php endforeach; ?>
			   		</div>
            	</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>
<script type=”text/javascript”>
CKEDITOR.replace("editor");
</script>