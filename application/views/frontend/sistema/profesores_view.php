<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
		<div class="container contenedor-respo">
    <div class="row profile">
		<div class="col-md-3 col-sm-4 col-xs-12">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Profesores por Cursos</h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li>Profesores por Curso</li>
						</ol>
			   		</div>
					
			   		<div class="col-md-12">
			   			<?php foreach($cursos as $key => $value): ?>
			   			<?php

			   			$profesor = $this->module_model->buscar('administrador', $value['id_padre']);
			   			$mensajes_totales = $this->module_model->seleccionar('mensajes', array('id_receptor' => MY_Controller::mostrar_session('id'), 'id_emisor' => $value['id'], 'activado' => 0, 'estado' => 1));

			   			?>
						<div class="col-md-6">
							<a href="<?php if($profesor['id'] != MY_Controller::mostrar_session('id')): ?><?php echo base_url(); ?>sys_mensaje/<?php echo $profesor['id']; ?><?php else: ?>javascript:;<?php endif; ?>">
								<div class="panel panel-default p-sombra">
								  <div class="panel-body">
								  	<div class="row">
								  		<div class="col-md-4 text-center">
								  			<img src="<?php echo base_url(); ?>uploads/100x100/<?php echo $value['logo']; ?>" alt="" style="width: 100px;">
								  		</div>
								  		<div class="col-md-8">
								  			<p style="color: #337ab7"><strong><?php echo $value['titulo']; ?></strong></p>
								  			<p style="color: #999"><i class="glyphicon glyphicon-user"></i> <?php echo $profesor['nombres']; ?> <?php echo $profesor['apellidos']; ?></p>
								  			<?php if(count($mensajes_totales) > 0): ?>
									    	<p><i class="glyphicon glyphicon-comment"></i> <?php count($mensajes_totales); ?> mensaje<?php echo ((count($mensajes_totales) > 1) ? 's' : ''); ?></p>
									    	<?php endif; ?>
									    </div>
								  	</div>
								  </div>
								</div>
							</a>
						</div>
						<?php endforeach; ?>
			   		</div>
            	</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>
<script type=”text/javascript”>
CKEDITOR.replace("editor");
</script>

