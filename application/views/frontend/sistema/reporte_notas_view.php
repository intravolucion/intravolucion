<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>

<body>

<div class="content">
	
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>

	<div class="app-sidebar">
		<div class="container contenedor-respo">
			    <div class="row profile">
					<div class="col-md-3 col-sm-4 col-xs-12">
						<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
					</div>
					<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
			            <div class="profile-content sombra">
						   	<div class="row">
						   		<div class="col-md-12 text-center">
						   			<h2>Reporte de Notas</h2>
						   			<ol class="breadcrumb">
									  <li>Reporte de Notas</li>
									</ol>
						   		</div>
								<!-- div class="col-md-12 app-rendimiento">
									<div class="profile-usertitle-job">
										Tu Progreso General
									</div>
									<div class="progress">
									  <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
									    <span class="">40%</span>
									  </div>
									</div>
								</div -->

								<div class="col-md-12">
						   			<h2>Curso: <?php echo $curso['titulo']; ?></h2>
						   		</div>

								<div class="col-md-12">
						   			Alumno: <?php echo $alumno['nombres']; ?> <?php echo $alumno['apellidos']; ?>
						   			<a href="javascript:;" class="btn btn-info pull-right" data-toggle="modal" data-target=".mensaje-alumno<?php echo $alumno['id']; ?>">Contactar</a> <a href="<?php echo base_url(); ?>sys_exportar_notas/<?php echo $curso['id']; ?>-<?php echo $curso['alias']; ?>/<?php echo $alumno['id']; ?>" class="btn btn-primary pull-right">Exportar Notas</a> <a href="javascript:;" onclick="javascript:history.back(1);" class="btn btn-warning pull-right">Regresar</a> 
						   		</div>

						   		<div class="modal fade mensaje-alumno<?php echo $alumno['id']; ?>" id="mensaje-alumno-<?php echo $alumno['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								  <div class="modal-dialog" role="document">
								    <div class="modal-content">
								      <div class="modal-header">
								        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								        <h4 class="modal-title" id="myModalLabel">Enviar Mensaje a <?php echo $alumno['nombres']; ?> <?php echo $alumno['apellidos']; ?></h4>
								      </div>
								      <form method="POST" action="<?php echo base_url(); ?>sys_mensaje/<?php echo $alumno['id']; ?>">
								      <div class="modal-body">
								        <div class="form-group">
								        	<label for="contenido" class="">Mensaje</label>
								        	<textarea class="form-control" required name="contenido" id="contenido"></textarea>
								        </div>
								      </div>
								      <div class="modal-footer">
								        <button type="submit" class="btn btn-primary">Enviar</button>
								      </div>
								  	  </form>
								    </div>
								  </div>
								</div>

						   		<?php $semanas = $this->module_model->seleccionar('semanas', array('estado' => 1, 'id_padre' => $curso['id'])); $curso_asignado = $this->module_model->seleccionar('cursos_asignados', array('id_padre' => $alumno['id'], 'id_curso' => $curso['id']), 1, 1); ?>

						   		<?php foreach($semanas as $key => $value): ?>
						   			<div class="col-md-12">
						   				<h4>Semana: <?php echo $value['titulo']; ?></h4>
						   			</div>

						   			<?php

						   			$lecciones = $this->module_model->seleccionar('lecciones', array('id_padre' => $value['id'], 'estado' => 1)); $porcentaje = 0;


						   			foreach($lecciones as $a => $b)
						   			{
						   				$porcentaje += ($b['id'] <= $curso_asignado['id_leccion'] AND $curso_asignado['id_leccion'] > 0) ? 1 : 0;
						   			}

						   			$porcentaje = ((count($lecciones) > 0) ? (($porcentaje / count($lecciones))) : '0') * 100; 

						   			?>

						   			<?php if($porcentaje > 0): ?>
						   				<div class="col-md-12 mb1em">
						   					Avance de semana: 
										  	<div class="progress pull-right" style="height: auto; overflow: auto; background-color: transparent; -webkit-box-shadow: none; box-shadow: none; margin: 1px;">
											  <div class="progress-bar progress-bar-info pull-right" role="progressbar" style="width: <?php echo $porcentaje; ?>%;margin:0px 0px 0px 300px;">
											    <?php echo $porcentaje; ?>%
											  </div>
											</div>
										</div>
									<?php endif; ?>

						   			<?php $tests = $this->module_model->seleccionar('tests', array('estado' => 1, 'id_padre' => $value['id'])); $tipo = array('Objetivo', 'Subjetivo'); ?>

							   		<div class="col-md-12">
							   			<table class="table table-striped table-bordered" cellspacing="0" width="100%">
									        <thead>
									            <tr>
									                <th>Test</th>
									                <th>Tipo</th>
									                <th>Nota</th>
									            </tr>
									        </thead>
									        <tbody>
									        	<?php foreach($tests as $k => $v): ?>
									        		<?php $calificacion = $this->module_model->seleccionar('calificaciones', array('id_padre' => $v['id'], 'id_usuario' => $alumno['id']), 1, 1); ?>
									        		<?php if(count($calificacion) > 0): ?>
										        		<tr>
											                <th><?php echo $v['titulo']; ?></th>
											                <th><?php echo $tipo[$v['tipo']]; ?></th>
											                <th><?php echo ($v['tipo'] == 0) ? ($calificacion['correctas'] / ($calificacion['correctas'] + $calificacion['incorrectas']) * 20) : ($calificacion['nota']);  ?></th>
											            </tr>
											        <?php endif; ?>
									        	<?php endforeach; ?>
									        </tbody>
									    </table>
							   		</div>
							   		<div><br /></div>
							   	<?php endforeach; ?>
						   	</div>
			            </div>
					</div>
				</div>
			</div>
	</div>
</div>


<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>