<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>

<body>

<div class="content">
	
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>

	<div class="app-sidebar">
		<div class="container contenedor-respo">
    <div class="row profile">
		<div class="col-md-3 col-sm-4 col-xs-12">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Inicio</h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li>Perfil</li>
						</ol>
			   		</div>
					
			   		<div class="col-md-12">
						<div class="panel panel-default p-sombra">
						  <div class="panel-heading">Datos personales</div>
						  <div class="panel-body">
						    <div class="row">
						    	<div class="col-md-6">
						    		<label for="">Nombre y Apellidos</label>
						    		<p><?php echo MY_Controller::mostrar_session('nombres'); ?> <?php echo MY_Controller::mostrar_session('apellidos'); ?></p>

						    		<label for="">DNI</label>
						    		<p><?php echo MY_Controller::mostrar_session('numero_documento'); ?></p>

						    		<label for="">Telefono</label>
						    		<p><?php echo MY_Controller::mostrar_session('telefono'); ?></p>
						    	</div>
						    	<div class="col-md-6 app-foto-perfil">
						    		<label for="">Foto</label><br>
						    		<img src="<?php echo base_url(); ?>uploads/100x100/<?php echo MY_Controller::mostrar_session('imagen'); ?>" alt="">
						    	</div>
						    </div>
						  </div>						  
						</div>
						<div class="app-botones pull-right">
						    	<div class="app-btn-editar">
						    		<a href="<?php echo base_url(); ?>sys_configuracion" class="btn success cb"><strong>Editar</strong></a>
						    	</div>
						    </div>
					</div>
			   	</div>
				


            </div>
		</div>
	</div>
</div>
</div>



</div>




<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>