<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>
<body>
<div class="content">
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>
	<div class="app-sidebar">
	<div class="container">
    	<div class="row profile">
		<div class="col-md-3">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Curso</h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li><a href="<?php echo base_url(); ?>sys_cursos" class="">Curso</a></li>
						  <li>Lecciones</li>
						  <li>Agregar</li>
						</ol>
			   		</div>
					
			   		<div class="col-md-12">
			   			<?php if(isset($mensaje)): ?>
	                    <div class="alert alert-success">
	                        <?php echo $mensaje; ?>
	                    </div>
	                    <?php endif; ?>

						<ul class="nav nav-tabs">
						  <li class="active"><a data-toggle="tab" href="#datos_curso">Lecciones</a></li>
						  <li><a href="<?php echo base_url(); ?>sys_lecciones/<?php echo $semana['id']; ?>-<?php echo $semana['alias']; ?>">Regresar</a></li>
						</ul>
						<div class="tab-content">
							<div class="panel panel-default p-sombra">
								<div class="panel-heading">Datos de la Lección</div>
							  	<div class="panel-body">
								    <div class="row">
								    	<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?php echo current_url(); ?>">
											<fieldset>
												<div class="form-group">
												  <label class="col-md-3 control-label" for="titulo">Titulo</label>  
												  <div class="col-md-8">
												  <input id="titulo" name="titulo" type="text" class="form-control input-md" required="" value="<?php echo @$leccion['titulo']; ?>">
												  </div>
												</div>
												
												<div class="form-group">
												  <label class="col-md-3 control-label" for="resumen">Resumen</label>  
												  <div class="col-md-8">
												  	<textarea id="resumen" name="resumen" class="editor form-control"><?php echo @$leccion['resumen']; ?></textarea>
												  </div>
												</div>

												<div class="form-group">
												  <label class="col-md-3 control-label" for="tipo">Tipo de Contenido</label>  
												  	<div class="col-md-8">
													  <select name="tipo" id="tipo" required="" class="form-control input-md">
													  	<option value="">Seleccione un tipo</option>
													  	<option value="contenido"<?php echo (@$leccion['tipo'] == 'contenido') ? ' selected="selected"' : ''; ?>>Texto</option>
													  	<option value="imagen"<?php echo (@$leccion['tipo'] == 'imagen') ? ' selected="selected"' : ''; ?>>Imagen</option>
													  	<option value="video"<?php echo (@$leccion['tipo'] == 'video') ? ' selected="selected"' : ''; ?>>Video</option>
													  	<option value="archivo"<?php echo (@$leccion['tipo'] == 'archivo') ? ' selected="selected"' : ''; ?>>Archivo</option>
													  </select>
													</div>
									    		</div>

												<div class="form-group">
												  <label class="col-md-3 control-label" for="contenido">Texto</label>  
												  	<div class="col-md-8">
													  <textarea id="contenido" name="contenido" type="text" class="editor form-control"><?php echo @$leccion['contenido']; ?></textarea>
													</div>
									    		</div>

									    		<div class="form-group">
												  <label class="col-md-3 control-label" for="video">Video</label>  
												  	<div class="col-md-8">
													  <input id="video" name="video" type="text" class="form-control input-md" value="<?php if(@$leccion['video'] != ''):  ?>https://www.youtube.com/watch?v=<?php echo @$leccion['video']; ?><?php endif; ?>">
													</div>

													<br />
													<?php if(@$leccion['video'] != ''): ?>
													<label class="col-md-3 control-label"></label>  
												  	<div class="col-md-8">
													  <iframe width="100%" height="350" src="https://www.youtube.com/embed/<?php echo $leccion['video']; ?>" frameborder="0" allowfullscreen></iframe>
													</div>
													<?php endif; ?>
									    		</div>

									    		<div class="form-group">
												  <label class="col-md-3 control-label" for="imagen">Imagen</label>  
												  	<div class="col-md-8">
													  <div class="input-group image-preview">
										                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
											                <span class="input-group-btn">
											                    <!-- image-preview-clear button -->
											                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
											                        <span class="glyphicon glyphicon-remove"></span> Quitar
											                    </button>
											                    <!-- image-preview-input -->
											                    <div class="btn btn-default image-preview-input">
											                        <span class="glyphicon glyphicon-folder-open"></span>
											                        <span class="image-preview-input-title">Seleccionar</span>
											                        <input type="file" name="imagen" id="imagen" /> <!-- rename it -->
											                    </div>
											                </span>
											            </div>
											            <br />
											            <?php if(@$leccion['imagen'] != ''): ?>
											            <img class="img-responsive" src="<?php echo base_url(); ?>uploads/<?php echo $leccion['imagen']; ?>" style="max-width:100px;max-height:100px;" />
											        	<?php endif; ?>
													</div>
									    		</div>

									    		<div class="form-group">
												  <label class="col-md-3 control-label" for="archivo">Archivo</label>  
												  	<div class="col-md-8">
													  <div class="input-group image-preview">
										                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
											                <span class="input-group-btn">
											                    <!-- image-preview-clear button -->
											                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
											                        <span class="glyphicon glyphicon-remove"></span> Quitar
											                    </button>
											                    <!-- image-preview-input -->
											                    <div class="btn btn-default image-preview-input">
											                        <span class="glyphicon glyphicon-folder-open"></span>
											                        <span class="image-preview-input-title">Seleccionar</span>
											                        <input type="file" name="archivo" id="archivo" /> <!-- rename it -->
											                    </div>
											                </span>
											            </div>
											            <br />
											            <?php if(@$leccion['archivo'] != ''): ?>
											            <a href="<?php echo base_url(); ?>uploads/<?php echo $leccion['archivo']; ?>" target="_blank">Ver Archivo</a>
											        	<?php endif; ?>
													</div>
									    		</div>

									    		<div class="form-group">
												  <label class="col-md-3 control-label" for="duracion">Duración</label>  
												  <div class="col-md-8">
												  	<input id="duracion" name="duracion" type="text" class="form-control input-md" required="" value="<?php echo @$leccion['duracion']; ?>">
												  </div>
												</div>

												<div class="form-group">
												    <div class="col-md-12" style="text-align: right;right: 5em;">
												    	<button type="submit" class="btn success cb"><strong>Guardar</strong></button>
												    </div>
												</div>
											</fieldset>
										</form>
								  	</div>						  
								</div>
							</div>
						</div>
			   		</div>
            	</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view('frontend/sistema/templates/footer_view.php') ?>

<script type="text/javascript">

var value_tipo = $('#tipo').val();

$('#contenido, #archivo, #imagen, #video').parents('.form-group').css('display', 'none');

$('#' + value_tipo).parents('.form-group').css('display', 'block');

$('#tipo').change(function(){
    var value = $(this).val(); $('#contenido, #archivo, #imagen, #video').parents('.form-group').css('display', 'none');

    $('#' + value).parents('.form-group').css('display', 'block');
});

CKEDITOR.replace("editor");
</script>
