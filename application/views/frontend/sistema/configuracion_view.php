<?php $this->load->view('frontend/sistema/templates/head_view.php'); ?>

<body>

<div class="content">
	
	<?php $this->load->view('frontend/sistema/templates/header_view.php'); ?>

	<div class="app-sidebar">
		<div class="container contenedor-respo">
    <div class="row profile">
		<div class="col-md-3 col-sm-4 col-xs-12">
			<?php $this->load->view('frontend/sistema/templates/sidebar_view.php'); ?>
		</div>
		<div class="col-md-9 col-sm-8 col-xs-12 respo-mt1em">
            <div class="profile-content sombra">
			   	<div class="row">
			   		<div class="col-md-12 text-center">
			   			<h2>Perfil</h2>
			   			<ol class="breadcrumb">
						  <li><a href="<?php echo base_url(); ?>sys_dashboard"><strong>Inicio</strong></a></li>
						  <li><a href="<?php echo base_url(); ?>sys_perfil" class="">Perfil</a></li>
						  <li>Editar</li>
						</ol>
			   		</div>
					
			   		<div class="col-md-12">
			   			<?php if(isset($mensaje)): ?>
	                    <div class="alert alert-success">
	                        <?php echo $mensaje; ?>
	                    </div>
	                    <?php endif; ?>

						<form class="form-horizontal respo-form-edit-perf" enctype="multipart/form-data" method="post" action="<?php echo current_url(); ?>">
						<div class="panel panel-default p-sombra">
						  <div class="panel-heading">Datos personales</div>
						  <div class="panel-body">
						    <div class="row">
								<fieldset>
									<!-- Text input-->
									<div class="form-group">
									  <label class="col-md-4 control-label" for="nombres">Nombre</label>  
									  <div class="col-md-5">
									  <input id="nombres" name="nombres" type="text" class="form-control input-md" required="" value="<?php echo MY_Controller::mostrar_session('nombres'); ?>">
									  </div>
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label class="col-md-4 control-label" for="apellidos">Apellidos</label>  
									  <div class="col-md-5">
									  <input id="apellidos" name="apellidos" type="text" class="form-control input-md" required="" value="<?php echo MY_Controller::mostrar_session('apellidos'); ?>">
									  </div>
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label class="col-md-4 control-label" for="numero_documento">Documento de Identidad</label>  
									  <div class="col-md-5">
									  <input id="numero_documento" name="numero_documento" type="text" class="form-control input-md" required="" value="<?php echo MY_Controller::mostrar_session('numero_documento'); ?>">
									  </div>
									</div>

									<!-- Text input-->
									<div class="form-group">
									  <label class="col-md-4 control-label" for="telefono">Teléfono</label>  
									  <div class="col-md-5">
									  <input id="telefono" name="telefono" type="text" class="form-control input-md" value="<?php echo MY_Controller::mostrar_session('telefono'); ?>">
									  </div>
									</div>
									<div class="form-group">
									  <label class="col-md-4 control-label" for="imagen">Imagen <small>(250x270px)</small></label>  
									  <div class="col-md-5">
									  <div class="input-group image-preview">
						                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
							                <span class="input-group-btn">
							                    <!-- image-preview-clear button -->
							                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
							                        <span class="glyphicon glyphicon-remove"></span> Quitar
							                    </button>
							                    <!-- image-preview-input -->
							                    <div class="btn btn-default image-preview-input">
							                        <span class="glyphicon glyphicon-folder-open"></span>
							                        <span class="image-preview-input-title">Seleccionar</span>
							                        <input type="file" name="imagen" id="imagen" /> <!-- rename it -->
							                    </div>
							                </span>
							            </div>
							            <?php if(MY_Controller::mostrar_session('imagen') != ''): ?>
							            	<div class="col-md-6 app-foto-perfil">
									    		<img src="<?php echo base_url(); ?>uploads/100x100/<?php echo MY_Controller::mostrar_session('imagen'); ?>" alt="">
									    	</div>
							        	<?php endif; ?>
									  </div>
									</div>

									<div class="form-group">
									  <label class="col-md-4 control-label" for="acerca_de">Resumen del Profesor</label>  
									  <div class="col-md-5">
									  <textarea id="acerca_de" name="acerca_de" class="form-control input-md"><?php echo MY_Controller::mostrar_session('resumen'); ?></textarea>
									  </div>
									</div>

									<div class="form-group">
									  <label class="col-md-4 control-label" for="archivo">Experiencia <small>(.PDF, .DOC, .PPT)</small></label>  
									  <div class="col-md-5">
									  <div class="input-group image-preview">
						                <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
							                <span class="input-group-btn">
							                    <!-- image-preview-clear button -->
							                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
							                        <span class="glyphicon glyphicon-remove"></span> Quitar
							                    </button>
							                    <!-- image-preview-input -->
							                    <div class="btn btn-default image-preview-input">
							                        <span class="glyphicon glyphicon-folder-open"></span>
							                        <span class="image-preview-input-title">Seleccionar</span>
							                        <input type="file" name="experiencia" id="experiencia" /> <!-- rename it -->
							                    </div>
							                </span>
							            </div>
							            <?php if(MY_Controller::mostrar_session('experiencia') != ''): ?>
									    	<a href="https://docs.google.com/viewer?url=<?php echo base_url(); ?>uploads/<?php echo MY_Controller::mostrar_session('experiencia'); ?>" target="_blank" alt="">Ver Archivo</a>
							        	<?php endif; ?>
									  </div>
									</div>
								</fieldset>
						    </div>
						  </div>						  
						</div>
						<div class="app-botones pull-right">
					    	<div class="app-btn-editar">
					    		<button type="submit" class="btn success cb"><strong>Guardar</strong></button>
					    	</div>
						</div>
						</form>
					</div>
			   	</div>
            </div>
		</div>
	</div>
</div>
	</div>



</div>


<?php $this->load->view('frontend/sistema/templates/footer_view.php'); ?>