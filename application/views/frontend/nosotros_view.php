<?php $this->load->view('frontend/templates/head_view.php'); ?>
<body>
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">


			<div id="wrapper">

				<!-- HEADER -->
	<?php $this->load->view('frontend/templates/header_view.php'); ?>


				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">Nosotros</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->


				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="<?php echo base_url(); ?>"><i class="icon stroke icon-House"></i></a></li>
										<li class="active">Nosotros</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->


				<main class="main-content">

					<section class="about rtd">
						<div class="container">
							<div class="row">
								<div class="col-md-5">
									<h2 class="about__title">Bienvenido a <strong>INTRAVOLUCION</strong></h2>
									<p class="text-justify">INTRAVOLUCION forma parte del grupo empresarial peruano Haro & Macciotta Asociados S.A.C que fue legalmente constituida a finales del mes de octubre de 2016 por dos socios que se conocieron mientras estudiaban una maestría en dirección de empresas en una de las más importantes casas de estudios de postgrados del país.</p>
									<h3 class="about__title-inner">Sobre <strong>los Fundadores</strong></h3>
									<ul class="list-mark">
										<li>Franco Macciotta: Soy magister en dirección de empresas con estudios universitarios en administración de empresas y finanzas. Tengo experiencia profesional en el sector financiero y soy docente universitario en el área de finanzas en una de las universidades más prestigiosas del Perú.</li>
										<li>Raul Haro: Soy psicólogo con estudios en Filosofía. Tengo experiencia profesional en actividades de consultoría organizacional, diseño y gestión de programas de capacitación y gestión del conocimiento en modalidad e-learning y presencial.</li>
									</ul>
								</div><!-- end col -->
								<div class="col-md-7">
									<img class="img-responsive" src="<?php echo base_view(); ?>img/nosotros/img1.jpg" height="350" width="670" alt="Foto">
									<div class="row">
										<div class="col-xs-6">
											<img class="img-responsive" src="<?php echo base_view(); ?>img/nosotros/img2.jpg" height="200" width="320" alt="Foto">
										</div>
										<div class="col-xs-6">
											<img class="img-responsive" src="<?php echo base_view(); ?>img/nosotros/img3.jpg" height="200" width="320" alt="Foto">
										</div><!-- end col -->
									</div><!-- end row -->

								</div><!-- end col -->
							</div><!-- end row -->
						</div><!-- end container -->
					</section><!-- end about -->


					<div class="section-advantages_mod-a">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<ul class="advantages advantages_mod-c list-unstyled">
										<li class="advantages__item">
											<span class="advantages__icon decor decor_mod-a"><i class="icon stroke icon-Users"></i></span>
											<span class="advantages__title ui-title-inner">GRAN COMUNIDAD</span>
										</li>
										<li class="advantages__item">
											<span class="advantages__icon decor decor_mod-a"><i class="icon stroke icon-Cup"></i></span>
											<span class="advantages__title ui-title-inner">CERTIFICACIÓN</span>
										</li>
										<li class="advantages__item">
											<span class="advantages__icon decor decor_mod-a"><i class="icon stroke icon-WorldGlobe"></i></span>
											<span class="advantages__title ui-title-inner">CONOCIMIENTO GLOBAL</span>
										</li>
										<li class="advantages__item">
											<span class="advantages__icon decor decor_mod-a"><i class="icon stroke icon-DesktopMonitor"></i></span>
											<span class="advantages__title ui-title-inner">ENTRENAMIENTO ONLINE</span>
										</li>
									</ul>
								</div><!-- end col -->
							</div><!-- end row -->
						</div><!-- end container -->
					</div><!-- end section-advantages -->


					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<section class="section-default">
									<h2 class="ui-title-block">¿Por qué <strong>INTRAVOLUCION?</strong></h2>
									<div class="panel-group accordion accordion" id="accordion-1">
										<div class="panel panel-default">
											<div class="panel-heading">
												<a class="btn-collapse" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-1"><i class="icon"></i></a>
												<h3 class="panel-title">Misión</h3>
											</div>
											<div id="collapse-1" class="panel-collapse collapse in">
												<div class="panel-body">
													<p class="ui-text">1.	Aumentar el acceso a la educación online de alta calidad para todo el mundo. <br>2.	Mejorar constantemente la metodología e-learning y las asesorías virtuales. <br>3.	Promover la gestión del conocimiento.</p>
												</div>
											</div>
										</div><!-- end panel -->

										<div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-2"><i class="icon"></i></a>
												<h3 class="panel-title">Visión</h3>
											</div>
											<div id="collapse-2" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">Ser la empresa con la mejor metodología de asesoría y enseñanza e-learning en el mundo.</p>
												</div>
											</div>
										</div><!-- end panel -->

										<div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-3"><i class="icon"></i></a>
												<h3 class="panel-title">Valores</h3>
											</div>
											<div id="collapse-3" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">
														1.	CREATIVIDAD <br> 2.	INNOVACIÓN <br> 3.	CORRESPONSABILIDAD <br> 4.	MEJORA CONTINUA <br> 5.	HONESTIDAD <br> 6.	SOLIDARIDAD

													</p>
												</div>
											</div>
										</div><!-- end panel -->

										<div class="panel">
											<div class="panel-heading">
												<a class="btn-collapse collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-4"><i class="icon"></i></a>
												<h3 class="panel-title">¿en qué consiste la plataforma?</h3>
											</div>
											<div id="collapse-4" class="panel-collapse collapse">
												<div class="panel-body">
													<p class="ui-text">INTRAVOLUCION es una plataforma virtual asociada a emprendedores y profesionales de todo el mundo que ofrece cursos y asesorías en línea mediante una revolucionaria idea de hacer negocio.</p>
												</div>
											</div>
										</div><!-- end panel -->
									</div><!-- end accordion -->
								</section>
							</div><!-- end col -->

							<div class="col-md-6">
								<section class="section-default">
									<h2 class="ui-title-block">¿Cómo <strong>Funciona?</strong></h2>
									<ul class="nav nav-tabs nav-tabs_mod-a">
										<li class="active"><a class="ui-title-inner decor decor_mod-d" href="#tab-1" data-toggle="tab">Inscripción</a></li>
										<li><a class="ui-title-inner decor decor_mod-d" href="#tab-2" data-toggle="tab">Clases</a></li>
									</ul>

									<div class="tab-content">
										<div class="tab-pane active" id="tab-1">
											<img class="pull-left img-responsive" src="<?php echo base_view(); ?>media/posts/113x113/1.jpg" height="113" width="113" alt="foto">
											<p style="text-align: justify;">Para poder entrar en cualquier curso, primero debes de registrarte en nuestra web, seguir los pasos de creación de perfil en INTRAVOLUCION para tener acceso a la plataforma virtual y por último puedes inscribirte en cualquiera de los cursos que ofrecemos.</p>
										</div>
										<div class="tab-pane" id="tab-2">
											<img class="pull-right img-responsive" src="<?php echo base_view(); ?>media/posts/113x113/2.jpg" height="113" width="113" alt="foto">
											<p style="text-align: justify;">Una vez registrado en INTRAVOLUCION e inscrito en el curso de tu preferencia, puedes entrar a la plataforma haciendo clic en el botón superior derecho que dice login.</p>
											<p style="text-align: justify;">Puedes tener acceso al curso desde su menú dinámico.</p></div>
									</div>
								</section>
							</div><!-- end col -->
						</div><!-- end row -->
					</div><!-- end container -->



				</main><!-- end main-content -->


 <?php $this->load->view('frontend/templates/footer_view.php'); ?>


</body>
</html>
