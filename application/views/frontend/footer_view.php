   <footer class="footer">
      <div class="container">
        <div class="footer-inner border-decor_top">
          <div class="row">
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">NOSOTROS</h3>
                <a href="javascript:void(0);"><img class="footer-logo img-responsive" src="<?php echo base_url(); ?>uploads/<?php echo $this->configuracion['logo_alternativo']; ?>" height="50" width="195" alt="Logo"></a>
                <div class="footer-info"><?php echo nl2br($this->configuracion['nosotros']); ?></div>
                <div class="footer-contacts footer-contacts_mod-a"> <i class="icon stroke icon-Pointer"></i>
                  <address class="footer-contacts__inner">
                  <?php echo $this->configuracion['direccion']; ?>
                  </address>
                </div> 
                <div class="footer-contacts"> <i class="icon stroke icon-Phone2"></i> <span class="footer-contacts__inner"><?php echo $this->configuracion['telefonos']; ?></span> </div>
                <div class="footer-contacts"> <i class="icon stroke icon-Mail"></i> <a class="footer-contacts__inner" href="mailto:<?php echo $this->configuracion['email_nosotros']; ?>"><?php echo $this->configuracion['email_nosotros']; ?></a> </div>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-2 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">CATEGORIAS</h3>
                <ul class="footer-list list-unstyled"><?php $items = 0; ?>
                  <?php foreach($this->categorias as $key => $value): ?>
                  <?php if($items < 4): ?>
                  <li class="footer-list__item"><a class="footer-list__link" href="<?php echo base_url(); ?>categoria/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><?php echo $value['titulo']; ?></a></li>
                  <?php else: ?>
                    <?php continue; ?>
                  <?php endif; ?>
                  <?php $items++; ?>
                  <?php endforeach; ?>
                </ul>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-3 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">CUENTAS</h3>
                <div>
                  <?php echo $this->configuracion['cuentas']; ?>
                </div>
                  
                <!-- <a class="tweets__link" href="https://twitter.com/intravolucion" target="_blank">Follow @INTRAVOLUCION</a> </section> -->
              <!-- end footer-section --> 
            </div>
            <!-- end col -->
            
            <div class="col-lg-4 col-sm-3">
              <section class="footer-section">
                <h3 class="footer-title">CONTACTO</h3>
                <form class="form" method="POST" action="<?php echo base_url(); ?>contacto">
                  <div class="form-group">
                    <input class="form-control" name="nombre" type="text" placeholder="Nombre *" required />
                    <input class="form-control" type="email" name="correo_electronico" placeholder="Email *" required />
                    <textarea class="form-control" name="mensaje" rows="7" placeholder="Mensaje *" required></textarea>
                    <button type="submit" class="btn btn-primary btn-effect">ENVIAR</button>
                  </div>
                </form>
              </section>
              <!-- end footer-section --> 
            </div>
            <!-- end col --> 
          </div>
          <!-- end row --> 
        </div>
        <!-- end footer-inner -->
        
        <div class="row">
          <div class="col-xs-12">
            <div class="footer-bottom">
              <div class="copyright">Copyright © 2017 <a href="javascript:void(0);">INTRAVOLUCION</a>, Online Learning  |  Creado por <a href="https://www.phsperu.com/" target="_blank"><img src="<?php echo base_view(); ?>img/phs-logo.png" alt=""></a></div>
              <ul class="social-links list-unstyled">
                <li><a class="icon fa fa-facebook" href="https://www.facebook.com/intravolucion" target="_blank"></a></li>
                <li><a class="icon fa fa-twitter" href="https://twitter.com/intravolucion" target="_blank"></a></li>
                <li><a class="icon fa fa-google-plus" href="https://plus.google.com/103289303330652925669" target="_blank"></a></li>
                <li><a class="icon fa fa-instagram" href="https://www.instagram.com/intravolucion" target="_blank"></a></li>
                <li><a class="icon fa fa-linkedin" href="javascript:void(0);" target="_blank"></a></li>
                <li><a class="icon fa fa-youtube-play" href="https://www.youtube.com/channel/UCkjYRD_jlBRoPO5FHnNcL7A" target="_blank"></a></li>
              </ul>
            </div>
            <!-- end footer-bottom --> 
          </div>
          <!-- end col --> 
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </footer>
  </div>
  <!-- end wrapper --> 
</div>
<!-- end layout-theme --> 
 <div class="alert alert-success alert-dismissable animated slideInRight app_mensaje" style="position: absolute; top: 20px; right: 20px; animation-delay: 1s;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> Indicates a successful or positive action.
  </div>



<!-- SCRIPTS --> 
<script src="<?php echo base_view(); ?>js/jquery-migrate-1.2.1.js"></script>
<script src="<?php echo base_view(); ?>plugins/bootstrap/js/bootstrap.min.js"></script> 
<script src="<?php echo base_view(); ?>js/modernizr.custom.js"></script> 
<script src="<?php echo base_view(); ?>js/waypoints.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 

<!-- slick-->
<script src="<?php echo base_view(); ?>plugins/slick/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_view(); ?>plugins/slick/slick.js"></script>
<script src="<?php echo base_view(); ?>plugins/slick/slick.min.js"></script>

<!--THEME--> 
<script  src="<?php echo base_view(); ?>plugins/sliderpro/js/jquery.sliderPro.min.js"></script> 
<script src="<?php echo base_view(); ?>plugins/owl-carousel/owl.carousel.min.js"></script> 
<script src="<?php echo base_view(); ?>plugins/isotope/jquery.isotope.min.js"></script> 
<script src="<?php echo base_view(); ?>plugins/prettyphoto/js/jquery.prettyPhoto.js"></script> 
<script src="<?php echo base_view(); ?>plugins/datetimepicker/jquery.datetimepicker.js"></script> 
<script src="<?php echo base_view(); ?>plugins/jelect/jquery.jelect.js"></script> 
<script src="<?php echo base_view(); ?>plugins/rendro-easy-pie-chart/dist/jquery.easypiechart.min.js"></script> 
<script src="<?php echo base_view(); ?>js/cssua.min.js"></script> 
<script src="<?php echo base_view(); ?>js/wow.min.js"></script> 
<script src="<?php echo base_view(); ?>js/custom.min.js"></script> 

<script>
  $('.slider-nav').slick({
          dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
          {
            breakpoint: 2000,
            settings: {
              autoplay: true,
              slidesToShow: 6,
              slidesToScroll: 1,
              infinite: true,
              dots: false
          }
            },
          {
            breakpoint: 1024,
            settings: {
              autoplay: true,
              slidesToShow: 6,
              slidesToScroll: 1,
              infinite: true,
              dots: false
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              dots: false
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: false
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
    });
</script>

  <script>
    
    $(document).ready(
        setTimeout(function(){ 
          $('.app_mensaje').removeClass('slideInRight');
          $('.app_mensaje').removeClass('slideOutRight');
      }, 5000);
    );

  </script>
