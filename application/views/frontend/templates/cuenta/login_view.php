<div id="login" class="dialog" style="z-index: 1000;">
	<div class="dialog__overlay"></div>
	<div class="dialog__content">
		<h3><img src="<?php echo base_url(); ?>uploads/<?php echo $this->configuracion['logo']; ?>" alt="" style="width: 250px;margin-bottom: 1em;"></h3>
		<form action="<?php echo backend_url(); ?>" method="post">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Correo Electrónico" name="correo_electronico" required />
			</div>
			<div class="form-group">
				<input type="password" class="form-control" placeholder="Contraseña" name="contrasenia" required />
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<div class="checkbox">
						  <label><input type="checkbox">Recordar contaseña</label>
						</div>
					</div>
					<div class="col-md-6" style="padding: 10px 0;">
						<a href="#" class="text-right">Olvidaste tu contraseña?</a>
					</div>
				</div>
				<button type="submit" class="btn btn-info btn-effect">Ingresar</button>
			</div>
		</form>
	</div>
</div>