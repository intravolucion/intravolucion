<header class="header">
      <div class="top-header">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <div class="top-header__contacts"><i class="icon stroke icon-Phone2"></i><?php echo $this->configuracion['telefonos'] ?></div>
              <div class="top-header__contacts"><i class="icon stroke icon-Mail"></i><a href="mailto:<?php echo $this->configuracion['email_nosotros']; ?>"><?php echo $this->configuracion['email_nosotros']; ?></a></div>
              <div class="top-header__contacts"><i class="icon stroke icon-Money"></i><a href="<?php echo current_url(); ?>?moneda=pen">Moneda PEN</a></div>
              <div class="top-header__contacts"><i class="icon stroke icon-Money"></i><a href="<?php echo current_url(); ?>?moneda=usd">Moneda US$</a></div>
              <div class="top-header__link">
                <!-- <button class="btn-header btn-effect">LATEST</button> -->
                <span></span> </div>
              <div class="header-login">
                <?php if($this->configuracion['facebook'] != ''): ?><a class="header-login__item" href="<?php echo $this->configuracion['facebook']; ?>" target="_blank"><i class="fa fa-facebook"></i></a><?php endif; ?>
                <?php if($this->configuracion['twitter'] != ''): ?><a class="header-login__item" href="<?php echo $this->configuracion['twitter']; ?>" target="_blank"><i class="fa fa-twitter"></i></a><?php endif; ?>
                <?php if($this->configuracion['google'] != ''): ?><a class="header-login__item" href="<?php echo $this->configuracion['google']; ?>" target="_blank"><i class="fa fa-google-plus"></i></a><?php endif; ?>
                <?php if($this->configuracion['instagram'] != ''): ?><a class="header-login__item" href="<?php echo $this->configuracion['instagram']; ?>" target="_blank"><i class="fa fa-instagram"></i></a><?php endif; ?>
                <?php if($this->configuracion['linkedin'] != ''): ?><a class="header-login__item" href="<?php echo $this->configuracion['linkedin']; ?>" target="_blank"><i class="fa fa-linkedin"></i></a><?php endif; ?>
                <?php if($this->configuracion['youtube'] != ''): ?><a class="header-login__item borde" href="<?php echo $this->configuracion['youtube']; ?>" target="_blank"><i class="fa fa-youtube"></i></a><?php endif; ?>
                <?php if(MY_Controller::mostrar_session('correo_electronico') != ''): ?>
                <a class="header-login__item" href="<?php echo backend_url(); ?>"><i class="icon stroke icon-User"></i>Bienvenido, <?php echo MY_Controller::mostrar_session('nombres'); ?> <?php echo MY_Controller::mostrar_session('apellidos'); ?></a>
                <?php else: ?>
                <a class="header-login__item borde trigger" href="<?php echo backend_url(); ?>" ><i class="icon stroke icon-User"></i>Ingresar</a>
                <a class="header-login__item" href="<?php echo base_url(); ?>registro" >Registro</a>
                <?php endif; ?>
              </div>
            </div>
            <!-- end col  --> 
          </div>
          <!-- end row  --> 
        </div>
        <!-- end container  --> 
      </div>
      <!-- end top-header  -->
      
      <div class="container">
        <div class="row">
          <div class="col-xs-12"> <a class="header-logo" href="<?php echo base_url(); ?>"><img class="header-logo__img" src="<?php echo base_url(); ?>uploads/<?php echo $this->configuracion['logo']; ?>" alt="Logo"></a>
            <div class="header-inner">
              <div class="header-search">
                <div class="navbar-search">
                  <form id="search-global-form" action="<?php echo base_url(); ?>">
                    <div class="input-group">
                      <input type="text" placeholder="Type your search..." class="form-control" autocomplete="off" name="s" id="search" value="">
                      <span class="input-group-btn">
                      <button type="submit" class="btn search-close" id="search-close"><i class="fa fa-times"></i></button>
                      </span> </div>
                  </form>
                </div>
              </div>
              <a class="header-cart" href="<?php echo base_url(); ?>carrito"> <i class="icon stroke icon-ShoppingCart"></i></a>
              <nav class="navbar yamm">
                <div class="navbar-header hidden-md  hidden-lg  hidden-sm ">
                  <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div id="navbar-collapse-1" class="navbar-collapse collapse" style="padding-top: 1em;">
                  <ul class="nav navbar-nav">
                    <li<?php echo ($urlvista == 'inicio') ? ' class="active"' : ''; ?>><a href="<?php echo base_url(); ?>">Inicio</a></li>
                    <li<?php echo ($urlvista == 'nosotros') ? ' class="active"' : ''; ?>><a href="<?php echo base_url(); ?>nosotros">Nosotros</a></li>
                    <li<?php echo ($urlvista == 'cursos') ? ' class="dropdown active"' : ' class="dropdown"'; ?>> <a href="<?php echo base_url(); ?>categorias">Cursos</a>
                      <?php if(count($this->categorias) > 0): ?>
                        <ul role="menu" class="dropdown-menu">
                          <?php foreach($this->categorias as $key => $value): ?>
                          <li><a href="<?php echo base_url(); ?>categoria/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><?php echo $value['titulo']; ?></a> </li>
                          <?php endforeach; ?>
                        </ul>
                      <?php endif; ?>
                    </li>
                    <li<?php echo ($urlvista == 'profesores') ? ' class="active"' : ''; ?>> <a href="<?php echo base_url(); ?>asociados">Asociados</a></li>
                    <li<?php echo ($urlvista == 'asesorias') ? ' class="active"' : ''; ?>> <a href="<?php echo base_url(); ?>asesorias">Asesorías</a></li>
                    <li<?php echo ($urlvista == 'contacto') ? ' class="active"' : ''; ?>><a href="<?php echo base_url(); ?>contacto">Contacto</a></li>
                  </ul>
                </div>
              </nav>
              <!--end navbar --> 
              
            </div>
            <!-- end header-inner --> 
          </div>
          <!-- end col  --> 
        </div>
        <!-- end row  --> 
      </div>
      <!-- end container--> 
    </header>
    