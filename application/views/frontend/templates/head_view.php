<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- META TAGS -->
<meta name="title" content="Intravolución - Cursos y asesorías virtuales para alcanzar tu potencial" />
<meta name="description" content="Somos una plataforma virtual asociada a emprendedores y profesionales de todo el mundo que ofrece cursos y asesorías en línea mediante una revolucionaria idea de hacer negocio." />

<meta name="keywords" content="cursos virtuales, intravolucion," />

<!-- OPENGRAPH -->
<meta property="og:url" content="https://intravolucion.com" />
<meta property="og:image" content="https://intravolucion.com/uploads/opengraph-intravolucion.jpg" />

<!-- <meta name="author" content="Intravolución" /> -->
<title><?php echo $this->configuracion['titulo']; ?></title>

<link href="<?php echo base_view(); ?>favicon.ico" type="image/x-icon" rel="shortcut icon">
<link href="<?php echo base_view(); ?>css/master.css" rel="stylesheet">
<script src="<?php echo base_view(); ?>plugins/jquery/jquery-1.11.3.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_view(); ?>plugins/DialogEffects/css/demo.css" />
		<!-- common styles -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_view(); ?>plugins/DialogEffects/css/dialog.css" />
		<!-- individual effect -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_view(); ?>plugins/DialogEffects/css/dialog-cathy.css" />
		<script src="<?php echo base_view(); ?>plugins/DialogEffects/js/modernizr.custom.js"></script>


		<!--  slick -->
	<!-- <script src="<?php echo base_view(); ?>plugins/jquery-3.1.1.min.js"></script> -->
	<link rel="stylesheet" href="<?php echo base_view(); ?>plugins/slick/slick.css">
	<link rel="stylesheet" href="<?php echo base_view(); ?>plugins/slick/slick-theme.css">

<script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
	
</head>