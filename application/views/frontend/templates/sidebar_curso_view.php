<div id="sidebar-wrapper" class="col-md-2 col-xs-8 animated">
	<div style="position: absolute;top: 5em;right: 8px;">
    	<span id="app-resp-close" class="glyphicon glyphicon-remove-sign" style="display: block"></span>
    </div>
    <div id="sidebar">
        <ul class="nav list-group text-center">
        	<img src="<?php echo base_url(); ?>uploads/100x100/<?php echo MY_Controller::mostrar_session('imagen'); ?>" alt="" width="100">
        	<h4 style="margin-top: .5em; margin-bottom: 2em;"><?php echo MY_Controller::mostrar_session('nombres'); ?> <?php echo MY_Controller::mostrar_session('apellidos'); ?></h4>
            <li  data-toggle="collapse" data-target="#semanas" class="app-active" area-expanded="true">
              <a href="#" class="cn active" data-toggle="tooltip" data-placement="top" title="Desplegar"><i class="fa fa-gift fa-lg"></i> <strong>Página de Inicio</strong> <i class="glyphicon glyphicon-chevron-down"></i></a>
            </li>
            <ul class="sub-menu collapse in" id="semanas" area-expanded="true">
            	<?php foreach($semanas as $key => $value): ?>
                <li>
                    <a class="list-group-item" href="<?php echo current_url(); ?>/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><i class="icon-home icon-1x"></i>Semana <?php echo ($key + 1); ?></a>
                </li>
            	<?php endforeach; ?>
            </ul>
            
            <li>
                <a class="list-group-item" href="<?php echo base_url(); ?>sys_calificaciones/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><i class="icon-home icon-1x"></i><strong class="cn">Calificaciones</strong></a>
            </li>
            <li>
                <a class="list-group-item" href="<?php echo base_url(); ?>curso/<?php echo $value['id']; ?>-<?php echo $value['alias']; ?>"><i class="icon-home icon-1x"></i><strong class="cn">Información</strong></a>
            </li>
            <li>
                <a class="list-group-item" href="<?php echo base_url(); ?>salir"><i class="glyphicon glyphicon-off cr"></i><strong class="cn"> Salir</strong></a>
            </li>
        </ul>
    </div>
</div>