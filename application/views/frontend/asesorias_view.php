<?php $this->load->view('frontend/templates/head_view.php'); ?>
<body>
<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->
		<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200">
			<div id="wrapper">
				<?php $this->load->view('frontend/templates/header_view.php'); ?>
				<div class="wrap-title-page">
					<div class="container">
						<div class="row">
							<div class="col-xs-12"><h1 class="ui-title-page">Asesorías y Consutorías</h1></div>
						</div>
					</div><!-- end container-->
				</div><!-- end wrap-title-page -->
				<div class="section-breadcrumb">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="wrap-breadcrumb clearfix">
									<ol class="breadcrumb">
										<li><a href="javascript:void(0);"><i class="icon stroke icon-House"></i></a></li>
										<li class="active">Asesorías y Consutorías</li>
									</ol>
								</div>
							</div>
						</div><!-- end row -->
					</div><!-- end container -->
				</div><!-- end section-breadcrumb -->
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<main class="main-content">
								<?php if(isset($asesorias) && count($asesorias) > 0): ?>
								<?php $meses = array('', 'ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SET', 'OCT', 'NOV', 'DIC'); ?>
								<?php foreach($asesorias as $key => $value): ?>
								<?php $creador = $this->module_model->buscar('administrador', $value['usuario_creacion']); ?>
								<article class="post post_mod-j clearfix">
									<div class="entry-media">
										<div class="entry-thumbnail">
											<a href="javascript:;" ><img class="img-responsive" src="<?php echo base_url(); ?>uploads/760x320/<?php echo $value['imagen']; ?>" width="760" height="320" alt="Foto"/></a>
										</div>
									</div>
									<?php $fecha = explode("-", $value['fecha']); ?>
									<div class="post-inner decor decor_mod-a clearfix">
										<div class="box-date"><span class="number"><?php echo $fecha[2]; ?></span><?php echo $meses[((int) $fecha[1])]; ?></div>
										<div class="entry-main">
											<h3 class="entry-title ui-title-inner"><a href="javascript:;"><?php echo $value['titulo']; ?></a></h3>
											<div class="entry-content">
												<p><?php echo nl2br($value['resumen']); ?></p>
											</div>
										</div>
										<div class="entry-meta">
											<span class="entry-autor">
												<span>Por </span>
												<a class="post-link" href="javascript:;"><?php echo $creador['nombres']; ?> <?php echo $creador['apellidos']; ?></a>
											</span>
											<span class="entry-date"><a class="post-link" href="javascript:;"><i class="icon stroke icon-Agenda"></i><?php echo MY_Controller::fecha_muestra($value['fecha']); ?></a></span>
											<!-- span class="entry-links">
												<i class="icon stroke icon-Tag"></i><a class="post-link" href="javascript:void(0);">universities</a>
											</span -->
										</div>
										<a href="javascript:;" class="post-btn btn btn-primary btn-effect">VER MÁS</a>
									</div>
								</article><!-- end post -->
								<?php endforeach; ?>
								<?php else: ?>
								<div class="clearfix">
									<div class="alert alert-warning">No se han encontrado asesorías disponibles.</div>
								</div>
								<?php endif; ?>
							</main><!-- end main-content -->


						</div><!-- end col -->


						<div class="col-md-4">
							<aside class="sidebar sidebar_mod-a">

								<!-- div class="widget widget_video">
									<div class="block_content">
										<a class="video-link" href="https://www.youtube.com/watch?v=DIGfO2Dgc9Y&rel=0" rel="prettyPhoto" title="YouTube">
											<img class="img-responsive" src="assets/media/video-bg/1.jpg" height="250" width="350" alt="video">
											<div class="video-link__inner">
												<i class="icon stroke icon-Next"></i>
												<span class="video-link__title">INTRO</span>
											</div>
										</a>
									</div>
								</div -->


								<section class="widget widget-default widget_categories">
									<h3 class="widget-title ui-title-inner decor decor_mod-a">Asesoría</h3>
									<div class="block_content">
										<ul class="list-categories list-unstyled">
											<?php foreach($categorias as $key => $value): ?>
											<?php $cantidad = $this->module_model->total('asesorias', array(), array(), array('categorias' => $value['id'])); ?>
											<li class="list-categories__item">
												<a class="list-categories__link" href="javascript:void(0);">
													<span class="list-categories__name"><?php echo $value['titulo']; ?></span>
													<span class="list-categories__number"><?php echo $cantidad; ?></span>
												</a>
											</li>
											<?php endforeach; ?>
										</ul>
									</div><!-- end block_content -->
								</section><!-- end widget -->



								<section class="widget widget-default widget_text">
									<h3 class="widget-title ui-title-inner decor decor_mod-a">Consultoría|</h3>
									<div class="block_content">
										<p>Esllentesque lacus.Vivamus lorem arcu semperd duiac. Cras ornare arcu avamus nda leo. Etiam ind arcu. Morbi justo mauris tempus pharetrad interd um at congue semper purus. Lorem ipsum dolor sit amet sed consectetura.</p>
									</div><!-- end block_content -->
								</section><!-- end widget_text -->

							</aside><!-- end sidebar -->
						</div><!-- end col -->
					</div><!-- end row -->
				</div><!-- end container -->
<?php $this->load->view('frontend/templates/footer_view.php'); ?>
</body>
</html>
