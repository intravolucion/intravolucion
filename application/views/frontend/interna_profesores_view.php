<?php $this->load->view('frontend/templates/head_view.php'); ?>

<body>

<!-- Loader -->
<div id="page-preloader"><span class="spinner"></span></div>
<!-- Loader end -->

<div class="layout-theme animated-css"  data-header="sticky" data-header-top="200"> 
  

  <div id="wrapper"> 
    
    <!-- HEADER -->
    <?php $this->load->view('frontend/templates/header_view.php'); ?>
    <!-- end header -->
    
    <div class="wrap-title-page">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <h1 class="ui-title-page">Profesores</h1>
          </div>
        </div>
      </div>
      <!-- end container--> 
    </div>
    <!-- end wrap-title-page -->
    
    <div class="section-breadcrumb">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="wrap-breadcrumb clearfix">
              <ol class="breadcrumb">
                <li><a href="javascript:void(0);"><i class="icon stroke icon-House"></i></a></li>
                <li><a href="<?php echo base_url(); ?>asociados">Asociados</a></li>
                <li class="active"><?php echo $profesor['nombres']; ?> <?php echo $profesor['apellidos']; ?></li>
              </ol>
            </div>
          </div>
        </div>
        <!-- end row --> 
      </div>
      <!-- end container --> 
    </div>
    <!-- end section-breadcrumb -->
    
    <div class="container">
      <div class="row">
        <div class="col-md-4">
        	<div class="app-foto-profe">
        		<img src="<?php echo base_url(); ?>uploads/<?php echo $profesor['imagen']; ?>" alt="" style="width: 100%">
        	</div>
          <div class="col-md-12" style="margin-top: 1em;">
          <section class="widget widget-default widget_social">
              <h3 class="widget-title ui-title-inner decor decor_mod-a">Redes Sociales</h3>
              <div class="block_content">
                <ul class="list-social list-inline">
                  <?php if($profesor['facebook'] != ''): ?><li><a target="_blank" href="https://<?php echo str_replace(array('https://', 'http://'), '', $profesor['facebook']); ?>"><i class="icon fa fa-facebook" ></i></a></li><?php endif; ?>
                  <?php if($profesor['twitter'] != ''): ?><li><a target="_blank" href="https://<?php echo str_replace(array('https://', 'http://'), '', $profesor['twitter']); ?>"><i class="icon fa fa-twitter"></i></a></li><?php endif; ?>
                  <!-- li><a class="icon fa fa-google-plus" href="javascript:void(0);"></a></li -->
                  <?php if($profesor['linkedin'] != ''): ?><li><a target="_blank" href="https://<?php echo str_replace(array('https://', 'http://'), '', $profesor['linkedin']); ?>"><i class="icon fa fa-linkedin"></i></a></li><?php endif; ?>
                  <?php if($profesor['youtube'] != ''): ?><li><a target="_blank" href="https://<?php echo str_replace(array('https://', 'http://'), '', $profesor['youtube']); ?>"><i class="icon fa fa-youtube"></i></a></li><?php endif; ?>
                </ul>
              </div>
              <!-- end block_content --> 
            </section>
        </div>
        </div>

        <div class="col-md-8">
        	<h2 style="font-size: 30px; color: #79A54D; font-weight: bold; text-transform: uppercase;" class="decor"><?php echo $profesor['nombres']; ?> <?php echo $profesor['apellidos']; ?></h2>
          <p><?php echo nl2br($profesor['acerca_de']); ?></p>
        <?php if($profesor['experiencia'] != ''): ?>
          <?php $url = base_url() . 'uploads/' . $profesor['experiencia']; ?>
      		<div class="col-md-12">
      			<section class="widget widget-default widget_social">
  		          <h3 class="widget-title ui-title-inner decor decor_mod-a">Experiencia</h3>
  		          <div class="block_content">
  		            <iframe src="https://docs.google.com/viewer?url=<?php echo urlencode($url); ?>&embedded=true" style="height:550px;width:100%;border:0px;frameborder:0px;"></iframe>
  		          </div>
  		          <!-- end block_content --> 
  	       		</section>
      		</div>
        <?php endif; ?>
		
        	</div>
        <!-- end col --> 
      </div>
      <!-- end row --> 

		


    </div>
    <!-- end container -->


    
 <?php $this->load->view('frontend/templates/footer_view.php'); ?>


<!-- PASARELLA DE PAGOS CULQI -->

</body>
</html>
