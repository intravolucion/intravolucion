<?php

$lang['usuarios'] = "Usuarios";
$lang['noticias'] = "Noticias";
$lang['iconos_grandes'] = "Iconos Grandes";
$lang['banner_programas'] = "Banner Programas";
$lang['banner_comunicados'] = "Banner Comunicados";
$lang['banner_campanias'] = "Banner Campañas";
$lang['institucional'] = "Quiénes Somos";
$lang['padres'] = "Qué Hacemos";
$lang['recomendados'] = "Recomendados";
$lang['banner_videos'] = "Banner Videos";
$lang['iconos_pequenios'] = "Iconos Pequeños";
$lang['efemerides'] = "Efemérides";
$lang['enlaces_interes'] = "Enlaces de Interés";
$lang['videoteca'] = "Videoteca";
$lang['fototeca'] = "Fototeca";
$lang['popup'] = "Pop Up";
$lang['convocatorias'] = "Convocatorias";
$lang['postula_aqui'] = "Postula Aquí";
$lang['configuracion_fotos'] = "Introducción de Álbumes";
$lang['configuracion_videos'] = "Introducción de Videos";

/* End of file application_lang.php */
/* Location: ./system/language/english/application_lang.php */