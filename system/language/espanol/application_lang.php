<?php

$lang['usuarios'] = "Usuarios";
$lang['banners'] = "Banners";
$lang['asesorias'] = "Asesorías";
$lang['suscriptores'] = "Suscriptores";
$lang['profesores'] = "Profesores";
$lang['alumnos'] = "Alumnos";
$lang['categorias'] = "Categorías";
$lang['cursos'] = "Cursos";
$lang['semanas'] = "Semanas";
$lang['lecciones'] = "Lecciones";
$lang['compras'] = "Compras";

/* End of file application_lang.php */
/* Location: ./system/language/espanol/application_lang.php */