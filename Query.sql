
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE IF NOT EXISTS `administrador` (
  `id` int(2) NOT NULL,
  `nivel` int(11) NOT NULL DEFAULT '0',
  `userlist` int(11) NOT NULL DEFAULT '0',
  `correo_electronico` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `contrasenia` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `nombres` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `apellidos` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `imagen` varchar(255) CHARACTER SET latin1 DEFAULT 'default.jpg',
  `acerca_de` text CHARACTER SET latin1,
  `usuario_creacion` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `usuario_modificacion` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `fecha_creacion` datetime DEFAULT NULL,
  `fecha_modificacion` datetime DEFAULT NULL,
  `estado` int(11) NOT NULL DEFAULT '0',
  `activado` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=1460665946 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id`, `nivel`, `userlist`, `correo_electronico`, `contrasenia`, `nombres`, `apellidos`, `imagen`, `acerca_de`, `usuario_creacion`, `usuario_modificacion`, `fecha_creacion`, `fecha_modificacion`, `estado`, `activado`) VALUES
(1, 0, 1, 'kluizsv@gmail.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Luis', 'Shepherd Vargas', 'default.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean porttitor vestibulum imperdiet. Ut auctor accumsan erat, a vulputate metus tristique non. Aliquam aliquam vel orci quis sagittis.', '1', '1', '2014-08-06 22:08:29', '2016-05-11 07:45:51', 1, 1),
(1415426074, 1, 0, 'admin@phsmedia.pe', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'Administrador', 'General', 'default.jpg', '', '0', '1415426074', '2014-11-07 23:54:34', '2016-05-11 08:03:41', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `backend_menu`
--

CREATE TABLE IF NOT EXISTS `backend_menu` (
  `id` int(11) NOT NULL,
  `icono` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `metodo` varchar(255) NOT NULL DEFAULT 'abrir',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `grupo` varchar(255) NOT NULL,
  `estado` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE IF NOT EXISTS `configuracion` (
  `id` int(1) NOT NULL,
  `logo` varchar(255) CHARACTER SET latin1 NOT NULL,
  `logo_ministerio` varchar(255) NOT NULL,
  `titulo` text CHARACTER SET latin1 NOT NULL,
  `keywords` text CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `email_contacto` varchar(100) CHARACTER SET latin1 NOT NULL,
  `email_reclamaciones` varchar(255) NOT NULL,
  `email_solicitud` varchar(255) NOT NULL,
  `oficina_noticias` int(11) NOT NULL,
  `oficina_fotos` int(11) NOT NULL,
  `oficina_videos` int(11) NOT NULL,
  `oficina_boletines` int(11) NOT NULL,
  `oficina_audios` int(11) NOT NULL,
  `oficina_publicaciones` int(11) NOT NULL,
  `postula_aqui` varchar(255) NOT NULL,
  `postula_aqui_alternativo` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `youtube` varchar(255) NOT NULL,
  `flickr` varchar(255) NOT NULL,
  `pinterest` varchar(255) NOT NULL,
  `instagram` varchar(255) NOT NULL,
  `color_fondo` varchar(255) DEFAULT NULL,
  `imagen_fondo` varchar(255) DEFAULT NULL,
  `tipo_fondo` int(11) NOT NULL,
  `color_texto_cabecera` varchar(255) DEFAULT NULL,
  `color_fondo_menu` varchar(255) DEFAULT NULL,
  `color_fondo_menu_hover` varchar(255) DEFAULT NULL,
  `correo_institucional` varchar(255) DEFAULT NULL,
  `tramite_documentario` varchar(255) DEFAULT NULL,
  `consulta_asistencia` varchar(255) DEFAULT NULL,
  `lista_anexos` varchar(255) DEFAULT NULL,
  `libro_reclamaciones` varchar(255) DEFAULT NULL,
  `transparencia` varchar(255) DEFAULT NULL,
  `presentacion_transparencia` text,
  `sede_lima` varchar(255) DEFAULT NULL,
  `sede_cuzco` varchar(255) NOT NULL,
  `sede_madre_dios` varchar(255) NOT NULL,
  `sede_puno` varchar(255) NOT NULL,
  `usuario_creacion` int(11) NOT NULL,
  `usuario_modificacion` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `fondo_transparencia` varchar(255) DEFAULT NULL,
  `titulo_transparencia` varchar(255) DEFAULT NULL,
  `ver_menu_transparencia` int(11) NOT NULL,
  `archivo_transparencia` varchar(255) NOT NULL,
  `documento_archivo_transparencia` varchar(255) NOT NULL,
  `introduccion_fotos` text NOT NULL,
  `introduccion_videos` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `logo`, `logo_ministerio`, `titulo`, `keywords`, `description`, `email_contacto`, `email_reclamaciones`, `email_solicitud`, `oficina_noticias`, `oficina_fotos`, `oficina_videos`, `oficina_boletines`, `oficina_audios`, `oficina_publicaciones`, `postula_aqui`, `postula_aqui_alternativo`, `facebook`, `twitter`, `youtube`, `flickr`, `pinterest`, `instagram`, `color_fondo`, `imagen_fondo`, `tipo_fondo`, `color_texto_cabecera`, `color_fondo_menu`, `color_fondo_menu_hover`, `correo_institucional`, `tramite_documentario`, `consulta_asistencia`, `lista_anexos`, `libro_reclamaciones`, `transparencia`, `presentacion_transparencia`, `sede_lima`, `sede_cuzco`, `sede_madre_dios`, `sede_puno`, `usuario_creacion`, `usuario_modificacion`, `fecha_creacion`, `fecha_modificacion`, `fondo_transparencia`, `titulo_transparencia`, `ver_menu_transparencia`, `archivo_transparencia`, `documento_archivo_transparencia`, `introduccion_fotos`, `introduccion_videos`) VALUES
(1, '1462944782.png', '1452289001.png', 'Website de Prueba - PHSmedia', '', '', 'lshepherd@phsmedia.pe', 'milumc3@hotmail.com', 'mmorales@minamcaf.gob.pe', -1, -1, -1, -1, -1, 1456866676, 'quienes-somos/concurso-2015-de-planes-de-econegocios/1456867075', '', 'https://www.facebook.com/ProgramaMinam.CAF/', 'https://twitter.com/minam_caf', 'https://www.youtube.com/channel/UCyTogw-yUMc67auV91Y2V6w', 'https://www.flickr.com/photos/minamcaf/', '', '', '#ffffff', '', 0, '#ffffff', '#9fd7da', '#47abbd', 'http://mail.iemp.gob.pe/exchange', 'http://tramite.inmp.gob.pe', 'http://asistencia.inmp.gob.pe/', 'http://www.inmp.gob.pe/institucional/ingreso-a-intranet-inmp/1432069046', 'http://libroreclamaciones.inmp.gob.pe', 'http://www.inmp.gob.pe/transparencia', '<div>\r\n<table width="600" cellspacing="1" cellpadding="1" border="0" align="center">\r\n    <tbody>\r\n        <tr>\r\n            <td width="400">\r\n            <p><span style="text-align: justify;">&iquest;Qu&eacute; es el Portal de Transparencia del Instituto Nacional Materno Perinatal? El Portal de Transparencia del Instituto Nacional Materno Perinatal es un medio inform&aacute;tico que promueve el libre acceso de la comunidad en tiempo real y oportuno a la informaci&oacute;n p&uacute;blica que manejamos. La implementaci&oacute;n del Portal de Transparencia del Instituto Nacional Materno Perinatal responde a la estrategia contenida en los Lineamientos de Pol&iacute;tica Sectorial para el Per&iacute;odo 2002-2012, de promover la participaci&oacute;n y el control ciudadano. Asimismo, materializa el inter&eacute;s de la Direcci&oacute;n Ejecutiva del Instituto Nacional Materno Perinatal de transparentar y publicitar la actuaci&oacute;n de la actual gesti&oacute;n, seg&uacute;n lo normado por el Texto &Uacute;nico Ordenado de la Ley N&ordm; 27806, Ley de Transparencia y Acceso a la Informaci&oacute;n P&uacute;blica.</span></p>\r\n            <p><span style="text-align: center;">Responsable de la actualizaci&oacute;n y elaboraci&oacute;n del Portal de Transparencia:</span><span style="text-align: center;">&nbsp;</span><span style="text-align: center;"><br />\r\n            </span></p>\r\n            <div style="text-align: center;">M.C. Juan Macedonio Torres Osorio</div>\r\n            <div style="text-align: center;"><br />\r\n            Responsable de entregar la informaci&oacute;n p&uacute;blica:<br />\r\n            M.C. Calagua Solis Victor Eduardo<br />\r\n            <br />\r\n            <a target="_blank" href="http://www.minsa.gob.pe/portada/transparencia/solicitud/frmFormulario.asp"><img width="62" height="60" border="0" alt="" src="/uploads/image/dw.png" /><br />\r\n            <br />\r\n            </a><a target="_blank" href="http://www.minsa.gob.pe/portada/transparencia/solicitud/frmFormulario.asp">Formulario Solicitud a la Informaci&oacute;n</a></div>\r\n            <div style="text-align: center;">&nbsp;</div>\r\n            <p style="text-align: center;"><a target="_blank" href="/uploads/file/Transparencia/R_398_2008_PCM.pdf"><font face="Arial">Descargar: Lineamientos para el portal transparencia</font></a></p>\r\n            <p style="text-align: center;"><a target="_blank" href="/uploads/file/Transparencia/Ley_27927.pdf"><font face="Arial"><br />\r\n            </font></a></p>\r\n            </td>\r\n            <td valign="top"><a target="_blank" href="http://www.peru.gob.pe/transparencia/pep_transparencia_lista_planes.asp?id_entidad=13181&amp;id_tema=1#.VVuXpvl_NBc"><img width="198" height="145" alt="" src="/uploads/image/intranet/transparencia.png" /></a></td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p style="text-align: justify;">&nbsp;</p>\r\n</div>', 'Calle Los Cipreses 221 - San Isidro\nTeléfono: 01 489-9854\n\nMesa de Partes: Av. Javier Prado Oeste 1440 San Isidro Lima-Perú (Ministerio del Ambiente)', 'Prolog. Arica 150 - Urcos, Quispicanchi', 'Jr. Crosby Mz. 6ll - Lote N°05 - Asentamiento Humano Huerto Familiar, Tambopata\nTeléfono: (082) 571182', 'Jr. Túpac Amaru N° 131 - Azángaro', 0, 1415426074, '0000-00-00 00:00:00', '2016-05-11 08:01:15', '', 'Transparencia Informativa', 1, '', '', 'Contenido Anterior a los Álbumes de Fotos', 'Contenido Anterior a los Videos.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log_administrador`
--

CREATE TABLE IF NOT EXISTS `log_administrador` (
  `id` int(11) NOT NULL,
  `accion` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ruta` varchar(255) NOT NULL,
  `ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(255) DEFAULT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `log_administrador`
--

INSERT INTO `log_administrador` (`id`, `accion`, `ruta`, `ip`, `usuario`, `fecha`) VALUES
(1, 'GET', 'http://localhost/website/backend/usuarios', '::1', '1415426074', '2016-05-11 07:41:33'),
(2, 'GET', 'http://localhost/website/backend/dashboard/configuracion', '::1', '1415426074', '2016-05-11 07:44:34'),
(3, 'POST', 'http://localhost/website/backend', '::1', '1415426074', '2016-05-11 07:44:57'),
(4, 'GET', 'http://localhost/website/backend/usuarios', '::1', '1415426074', '2016-05-11 07:56:23'),
(5, 'GET', 'http://localhost/website/backend/usuarios', '::1', '1415426074', '2016-05-11 07:57:01'),
(6, 'GET', 'http://localhost/website/backend/usuarios', '::1', '1415426074', '2016-05-11 07:57:21'),
(7, 'GET', 'http://localhost/website/backend/usuarios', '::1', '1415426074', '2016-05-11 07:57:57'),
(8, 'GET', 'http://localhost/website/backend/dashboard/configuracion', '::1', '1415426074', '2016-05-11 07:58:05'),
(9, 'GET', 'http://localhost/website/backend/usuarios', '::1', '1415426074', '2016-05-11 07:59:50'),
(10, 'GET', 'http://localhost/website/backend/usuarios', '::1', '1415426074', '2016-05-11 08:01:06'),
(11, 'GET', 'http://localhost/website/backend/dashboard/configuracion', '::1', '1415426074', '2016-05-11 08:01:10'),
(12, 'GET', 'http://localhost/website/backend/dashboard/perfil', '::1', '1415426074', '2016-05-11 08:01:39'),
(13, 'GET', 'http://localhost/website/backend/dashboard/contrasenia', '::1', '1415426074', '2016-05-11 08:01:41'),
(14, 'GET', 'http://localhost/website/backend/usuarios', '::1', '1415426074', '2016-05-11 08:02:51'),
(15, 'GET', 'http://localhost/website/backend/usuarios/actualizar/1415426074', '::1', '1415426074', '2016-05-11 08:03:25'),
(16, 'POST', 'http://localhost/website/backend', '::1', '1415426074', '2016-05-11 08:03:47');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `backend_menu`
--
ALTER TABLE `backend_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `log_administrador`
--
ALTER TABLE `log_administrador`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1460665946;
--
-- AUTO_INCREMENT de la tabla `log_administrador`
--
ALTER TABLE `log_administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;